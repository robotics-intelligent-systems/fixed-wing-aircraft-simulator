module.exports = {
    stylesheet: ['./readme-pdf-styles.css'],
    pdf_options: {
        format: 'A4',
        margin: '10mm',
        printBackground: true,
    }
};