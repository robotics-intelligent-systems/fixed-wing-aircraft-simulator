

TAKEOFF AND LANDING PERFORMANCE                                 /tol/readme.txt
   CAPABILITIES OF TRANSPORT CATEGORY AIRCRAFT

The files for this program are in the directory tol 
  lar13086.txt    the original program description from COSMIC
  readme.txt      this file of general description
  original.src    the original copy of the source code (from COSMIC)
  tol.f90         the complete source code
  case1.dat       input for sample case
  case1.out       output for sample case

To compile this program for your computer, use the command
   gfortran  tol.f90 -o tol.exe
Linux and Macintosh users may prefer to omit the .exe on the file name.

This computer program provides for the detailed analysis of the takeoff 
and landing performance capabilities of transport category aircraft. 
The program calculates these performance figures in accordance with the 
airworthiness standards of the Federal Aviation Regulations (FAR). 
The aircraft and flight constraints are represented in sufficient detail
to permit realistic sensitivity studies in terms of either configuration 
modifications or changes in operational procedures.

In contrast to many simplified procedures available today, this program
attempts to compute all numbers "by the book", meaning the FAR.
The airframe and performance data are read in as tables.
The program allows for the study of different speeds for the commencement
of engine rotation and the times required for the pilot to become aware
of various critical events and take corrective action.

The basic reference is NASA TM 80120 by Willard Foss. You can see from a
study of this document that this program is designed to be used in
conjunction with a noise prediction program to study effects of various
suggested techniques for noise reduction such as reduced power takeoff,
two-segment approach patterns, etc. The noise prediction part of the
program was not released to COSMIC.

The principal lack at this time is a guide to the input variables.
Many are obvious, but many are not. As I discover the meanings of various
variables, I have been annotating the source code and for now, this is
the only guide. 

I should caution you that this is supposed to be a program that is in 
sync with the Federal Air Regulations. However, the FAR can and do change 
over time.  This program and the FAR were aligned when the program was 
written in 1975, but there may be changes that should get reflected in the 
code. Another nice feature would be a little program that generated the
aerodynamic and propulsion tables in the correct format.

