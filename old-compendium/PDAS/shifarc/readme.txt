

SUPER/HYPERSONIC INVISCID FLOW                              /shifarc/readme.txt
   AROUND REAL CONFIGURATIONS

The files for this program are in the directory shifarc 
  readme.txt      this file of general description
  lar11891.txt    the original program description from COSMIC 
  blunt.src       the original source code for program BLUNT
  quick.src       the original source code for program QUICK
  stein.src       the original source code for program STEIN
  blunt.for       modern source code for program BLUNT
  quick.for       modern source code for program QUICK
  stein.for       modern source code for program STEIN


This program is a "work in progress" and is not ready for general release.
I have included it so those who have a special interest may see
the original code plus my modifications to date.
 
