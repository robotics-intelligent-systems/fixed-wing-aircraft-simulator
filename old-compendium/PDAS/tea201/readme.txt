

SUPERSONIC AIRPLANE DESIGN                            /tea201/readme.txt

The files for this program are in the directory tea201 

  readme.txt      this file
  id.f90          module of induced drag functions and subroutines
  induced.f90     sample program illustrating use
  input.txt       instructions for input to induced
  case1.ccl       sample cases
  case2.ccl
  case3.ccl
  case4.ccl
  case1.out       expected output from case1.ccl, etc.
  case2.out
  case3.out
  case4.out

To compile this program for your computer, use the command
   gfortran  induced.f90 -o induced.exe
Linux and Macintosh users may prefer to omit the .exe on the file name.
The module id.f90 will be included automatically.

