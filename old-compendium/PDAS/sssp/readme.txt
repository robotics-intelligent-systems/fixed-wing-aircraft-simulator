

SSSP- SPACE SHUTTLE SYNTHESIS PROGRAM                        /sssp/readme.txt

The files for this program are in the directory /sssp 
    readme.txt      this file
    original.src    the original source code (from COSMIC)
    msc13914.txt    original program description from COSMIC


This program is a "work in progress" and is not ready for general release.
I have included it on the disc so those who have a special interest may see
the original code plus my modifications to date.
 
The documentation for this program just came to light in June 2009 and I 
have not been able to get the test case from the report working yet.
