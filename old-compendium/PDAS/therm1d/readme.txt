

ONE-DIMENSIONAL NUMERICAL ANALYSIS OF THE                   /therm1d/readme.txt
TRANSIENT THERMAL RESPONSE OF MULTILAYER INSULATIVE SYSTEMS

The files for this program are in the directory therm1d 
  readme.txt      this file of general description
  original.src    the original copy of the source code (from COSMIC)
  sink.for        the source code converted to modern Fortran
  lar12057.txt    the original program description from COSMIC

This program is a "work in progress" and is not ready for general release.
I have included it so those who have a special interest may see
the original code plus my modifications to date.
 
