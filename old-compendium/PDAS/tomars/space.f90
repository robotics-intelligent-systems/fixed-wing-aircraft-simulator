INCLUDE 'mathcal.f90'
!+
MODULE InterplanetaryProcedures
! ------------------------------------------------------------------------------
! PURPOSE - A collection of procedures that are useful in computing 
!  interplanetary trajectories, launch, injection, capture and entry.
USE InterplanetaryMathProcedures
USE CalendarProcedures
IMPLICIT NONE

  INTEGER,PRIVATE:: DBG = 3
! gravitational constants of the sun and planets  km^3/s^2
  REAL(WP),PARAMETER:: USUN=1.3271411E+11_WP
  REAL(WP),PARAMETER:: MUMERCURY = 2.168E+4_WP
  REAL(WP),PARAMETER:: MUVENUS   = 3.2485E+5_WP
  REAL(WP),PARAMETER:: MUEARTH   = 3.986E+5_WP
  REAL(WP),PARAMETER:: MUMARS    = 4.29778E+4_WP
  REAL(WP),PARAMETER:: MUJUPITER = 1.267E+8_WP
  REAL(WP),PARAMETER:: MUSATURN  = 3.791E+7_WP
  REAL(WP),PARAMETER:: MUURANUS  = 5.788E+6_WP
  REAL(WP),PARAMETER:: MUNEPTUNE = 6.832E+6_WP

! Radii of the sun and planets (kilomeyters)
  REAL(WP),PARAMETER:: SUN_RADIUS     = 696000
  REAL(WP),PARAMETER:: MERCURY_RADIUS =   2439
  REAL(WP),PARAMETER:: VENUS_RADIUS   =   6052  ! maybe 6085
  REAL(WP),PARAMETER:: EARTH_RADIUS   =   6378
  REAL(WP),PARAMETER:: MARS_RADIUS    =   3393
  REAL(WP),PARAMETER:: JUPITER_RADIUS =  71400
  REAL(WP),PARAMETER:: SATURN_RADIUS  =  60000
  REAL(WP),PARAMETER:: URANUS_RADIUS  =  25400
  REAL(WP),PARAMETER:: NEPTUNE_RADIUS =  24300

! Semi-major axes of the planetary orbits in AU.
  REAL(WP),PARAMETER:: AU=149598845.0_WP   ! astronomical unit in kilometers
  REAL(WP),PARAMETER:: A_MERCURY =  0.3871_WP
  REAL(WP),PARAMETER:: A_VENUS   =  0.7233_WP
  REAL(WP),PARAMETER:: A_EARTH   =  1.000_WP
  REAL(WP),PARAMETER:: A_MARS    =  1.524_WP
  REAL(WP),PARAMETER:: A_JUPITER =  5.203_WP
  REAL(WP),PARAMETER:: A_SATURN  =  9.539_WP
  REAL(WP),PARAMETER:: A_URANUS  = 19.18_WP
  REAL(WP),PARAMETER:: A_NEPTUNE = 30.07_WP

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,4ES15.5)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(1X,A,4F12.5)'
!-------------------------------------------------------------------------------

CONTAINS

!+
SUBROUTINE CarSph(x,y,z, dx,dy,dz, r,ras,dec,v,gam,sig)
! ------------------------------------------------------------------------------
! PURPOSE - Convert cartesian coordinates to spherical coordinates.

  REAL(WP),INTENT(IN):: x,y,z ! components of positicn vector
  REAL(WP),INTENT(IN):: dx,dy,dz ! components of velocity vector
  REAL(WP),INTENT(OUT):: r ! magnitude cf position vector
  REAL(WP),INTENT(OUT):: ras,dec ! right ascension, declination (degrees)
  REAL(WP),INTENT(OUT):: v ! magnitude of velocity
  REAL(WP),INTENT(OUT):: gam,sig ! flight-path angle, azimuth (degrees)

  REAL(WP):: cp,sp   ! cosine, sine of declination
  REAL(WP):: ct,st   ! cosine, sine of right ascension
  REAL(WP):: dxp,dyp,dzp
!-------------------------------------------------------------------------------
  r=SQRT(x*x + y*y + z*z)
  ras=ATAN2(y,x)   ! ras in radians
  dec=ASIN(z/r)    ! dec in degrees
  v=SQRT(dx*dx + dy*dy + dz*dz)
  ct=COS(ras) 
  st=SIN(ras)
  cp=COS(dec)
  sp=SIN(dec)
  dxp= dx*ct*cp + dy*st*cp + dz*sp
  dyp=-dx*st + dy*ct
  dzp=-dx*sp*ct - dy*sp*st + dz*cp
  gam=RAD2DEG*ASIN(dxp/v)
  sig=RAD2DEG*ATAN2(dyp,dzp)
  ras=RAD2DEG*ras
  dec=RAD2DEG*dec 
  RETURN
END Subroutine CarSph   ! ------------------------------------------------------



!+
SUBROUTINE COMPEL(per,elp,gee,xi,capw,xitw,datem,ha,hp,ae,ee,days, &
  rs,key2,key3,elonp)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the elements of an ellipse at Mars that passes over the
!  landing point on the day of deorbit.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: per  ! periapsis to landing point angle, degrees
  REAL(WP),INTENT(IN):: elp  ! landing point latitude, degrees
  REAL(WP),INTENT(IN):: gee  ! sun lighting angle, degrees
  REAL(WP),INTENT(IN):: xi   ! orbit inclination at Mars
  REAL(WP),INTENT(OUT):: capw   ! ascending node, degrees
  REAL(WP),INTENT(OUT):: xitw   ! inclination, degrees
  REAL(WP),INTENT(IN):: datem  ! arrival date, Julian
  REAL(WP),INTENT(IN):: ha  ! height of apoapsis above surface of Mars
  REAL(WP),INTENT(IN):: hp  ! height of periapsis above surface of Mars
  REAL(WP),INTENT(OUT):: ae  ! semimajor axis
  REAL(WP),INTENT(OUT):: ee  ! eccentricity
  REAL(WP),INTENT(IN):: days  ! stay time in orbit
  REAL(WP),INTENT(IN):: rs   ! radius of Mars
  INTEGER,INTENT(IN OUT):: key2  ! only changed if it is set to 2 for error
  INTEGER,INTENT(IN):: key3  ! landing point control integer
                             ! =1 descending node, p.m. lighting
                             ! =2 ascending node, p.m. lighting
                             ! =3 descending node, a.m. lighting
                             ! =4 ascending node, a.m. lighting

  REAL(WP),INTENT(OUT):: elonp

  REAL(WP):: arc1,arc2
  REAL(WP):: arg
  REAL(WP):: cx,cy,cz   ! unit vector from Mars to Canopus
  REAL(WP):: decc,rac   ! declination and right ascension of Canopus
  REAL(WP):: dece, rae  ! declination and right ascension of Earth
  REAL(WP):: djul   ! date of deboost
  REAL(WP):: els,elons  ! declination and right ascension of Sun
  REAL(WP):: ex,ey,ez   ! unit vector from Mars to Earth
  REAL(WP):: ra,rp   ! apoapsis, periapsis
  REAL(WP):: sx,sy,sz  ! unit vector from Mars to Sun
  INTEGER:: icode = 4 ! planet code (4 for Mars)
!-------------------------------------------------------------------------------
  WRITE(DBG,FMT1) 'Entry to CompEl, xi,elp', xi, elp
  WRITE(DBG,*) 'Initial key2', key2
  IF (ABS(xi) < ABS(elp)) GO TO 50 
  djul=datem+days

! for Julian date djul, compute location of Sun, Earth, and Canopus
  icode=4  ! for Mars
  CALL Vector(djul,els,elons,dece,rae,decc,rac,sx,sy,sz,ex,ey,ez,cx,cy,cz,icode)
  WRITE(DBG,FMT1) 'After Vector, gee,els,elp', gee,els,elp
  IF (ABS(SIGN(gee,elp)+els) <= ABS(elp)) GO TO 50 
  arg=(Xcos(gee)-Xsin(els)*Xsin(elp))/(Xcos(els)*Xcos(elp)) 
  WRITE(DBG,FMT1) 'CompEl arg', arg
  IF( ABS(arg)>ONE) GO TO 50
  arc1=ASIN(Xsin(elp)/Xsin(xi))*RAD2DEG
  arc2=ASIN((Xsin(elp)/Xcos(elp))*(Xcos(xi)/Xsin(xi)))*RAD2DEG
  
  WRITE(DBG,*) 'In Compel, key3=', key3
  SELECT CASE(key3)
    CASE(1)
      elonp=RAD2DEG*ACOS(arg) + elons 
      xitw=per+180.-arc1
      capw=elonp-180.+arc2
    CASE(2)
      elonp=RAD2DEG*ACOS(arg) + elons 
      xitw=per+arc1
      capw=elonp-arc2
    CASE(3)
      elonp=elons - RAD2DEG*ACOS(arg)
      xitw=per+180.-arc1
      capw=elonp-180.+arc2
    CASE(4)
      elonp=elons - RAD2DEG*ACOS(arg)
      xitw=per+arc1
      capw=elonp-arc2
  END SELECT
  WRITE(DBG,FMT1) 'Compel', elonp,xitw,capw
  
  xitw=Angle(xitw)
  capw=Angle(capw)
  ra=rs   +ha
  rp=rs   +hp
  ae=(ra+rp)/TWO
  ee=(ra-rp)/(TWO*ae)
  RETURN

  50 key2=0 
  WRITE(DBG,*) 'key2 set to zero in CompEl. BAD.'
RETURN 
END Subroutine Compel   ! ------------------------------------------------------

!+
SUBROUTINE ConCar(a,e,xi,w,o,f, x,y,z,dx,dy,dz,u) 
! ------------------------------------------------------------------------------
! PURPOSE - Convert conic elements to cartesian coordinates.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: a,e,xi,w,o,f
  REAL(WP),INTENT(OUT):: x,y,z,dx,dy,dz
  REAL(WP),INTENT(IN):: u
  REAL(WP):: cwf,swf
  REAL(WP):: den   ! 1+eccentricity*Cos(true anomaly).  used often
  REAL(WP):: co,so
  REAL(WP):: ci,si
  REAL(WP):: cwfg,swfg
  REAL(WP):: or,xir
  REAL(WP):: gam
  REAL(WP):: r
  REAL(WP):: fr,wfr,wfgr
  REAL(WP):: v
!-------------------------------------------------------------------------------
  fr=DEG2RAD*f
  wfr=DEG2RAD*(w+f) 
  or=DEG2RAD*o
  xir=DEG2RAD*xi
  den=ONE+e*COS(fr)
  r=a*(ONE-e*e)/den
  v=SQRT(u*(TWO/r-ONE/a))
  gam=ATAN(e*SIN(fr)/den)
  wfgr=wfr-gam 
  cwf=COS(wfr) 
  swf=SIN(wfr) 
  so=SIN(or)
  co=COS(or)
  si=SIN(xir) 
  ci=COS(xir) 
  swfg=SIN(wfgr)
  cwfg=COS(wfgr)

  x=r*(cwf*co-swf*so*ci)
  y=r*(cwf*so+swf*co*ci)
  z=r*swf*si

  dx=v*(-swfg*co-cwfg*so*ci) 
  dy=v*(-swfg*so+cwfg*co*ci)
  dz=v*cwfg*si 
  RETURN
END Subroutine Concar   ! ------------------------------------------------------

!+
SUBROUTINE CONFPA(ao,eo,fo,per0,rs,re,fpae,u, al,el,fld,flo,delv,theta,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Convert ...
! Called by Events
 IMPLICIT NONE
  REAL(WP),INTENT(IN):: ao,eo,fo,per0,rs,re,fpae,u
  REAL(WP),INTENT(OUT):: al,el,fld,flo,delv,theta
  INTEGER,INTENT(OUT):: kk

  REAL(WP),DIMENSION(2):: p
  REAL(WP):: ang12
  REAL(WP):: c12,s12
  REAL(WP):: cfpa
  REAL(WP):: cf2,sf2
  REAL(WP):: cfo,sfo
  REAL(WP):: check
  REAL(WP):: cth, sth  ! cosine, sine of theta
  REAL(WP):: e2
  REAL(WP):: f1,f2
  REAL(WP):: fpao
  REAL(WP):: fpal1
  REAL(WP):: r1,r2
  REAL(WP):: a,b,c
  REAL(WP):: vo
  REAL(WP):: vl1
  INTEGER:: i
!-------------------------------------------------------------------------------
  delv=ZERO
  kk=0
  ang12=Angle(per0-fo)
  s12=SIN(ang12*DEG2RAD)
  c12=COS(ang12*DEG2RAD)
  cfpa=COS(fpae*DEG2RAD)
  sfo=SIN(fo*DEG2RAD)
  cfo=COS(fo*DEG2RAD)
  r1=ao*(ONE-eo*eo)/(ONE+eo*cfo)
  r2=rs
  vo=SQRT(u*(TWO/r1-ONE/ao)) 
  fpao=ASIN(eo*sfo/SQRT(ONE+TWO*eo*cfo+eo*eo))*RAD2DEG

  a=-r2*r2-r1*r1+2.*r1*r2*c12+(r1*r2*s12/re/cfpa)**2 
  b=TWO*(r1*r2*r2+r1*r1*r2-r1*r1*r2*c12-r1*r2*r2*c12-(r1*r2*s12)**2/re)
  c=r1*r1*r2*r2*(-TWO+TWO*c12+s12*s12)

  CALL Qadrat(a,b,c,p(1),p(2),kk)
  IF (kk==0) RETURN

! I have recoded the control variables for this loop.
! Modern Fortran does not allow you to reset a loop counter.
  i=1
  DO 
    IF (P(1)<=ZERO) GO TO 1
    e2=ONE-TWO*p(i)/re+(p(i)/re/cfpa)**2
    IF (e2<ZERO) GO TO 1
    el=SQRT(e2)
    al=p(i)/(ONE-e2)
    cf2=(p(i)-r2)/el/r2
    sf2=-SQRT(ONE-cf2*cf2) 
    check=r2*(ONE+el*cf2)-r1*(ONE+el*(c12*cf2+s12*sf2)) 
    IF (ABS(check)>ONE) GO TO 1
    i=2   ! might not be permitted
    f2=Angle(ATAN2(sf2,cf2)*RAD2DEG)
    f1=f2-ang12 
    fpal1=ASIN(el*SIN(f1*DEG2RAD)/SQRT(ONE+TWO*el*COS(f1*DEG2RAD)+el*el))*RAD2DEG 
    vl1=SQRT(u*(TWO/r1-ONE/AL))
    delv=SQRT(vo*vo+vl1*vl1-TWO*vo*vl1*COS((fpao-fpal1)*DEG2RAD)) 
    sth=vl1/delv*SIN((fpao-fpal1)*DEG2RAD) 
    cth=(vo*vo+delv*delv-vl1*vl1)/TWO/delv/vo
    theta=ATAN2(sth,cth)*RAD2DEG
    IF (i >= kk) EXIT
  1 i=i+1
  END DO

  IF (delv>=0.001_WP) THEN
    flo=f1
    fld=f2
    kk=1
  END IF

  RETURN
END Subroutine ConFpa   ! ------------------------------------------------------

!+
SUBROUTINE DeBoost(a1,e1,i1,w1,o1,f1,u,lats,lons,vinf,rpmin,inc, &
  az,ez,iz,wz,oz,fz,bt,br,deltv,tpere,m)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the minimum delta-V and elements of hyperbola associated
!   with it and the impact plane parameters.
!   Only called in minimum output mode.
IMPLICIT NONE
  REAL(WP):: a1,e1,i1,w1,o1,f1,u,lats,lons,vinf,rpmin
  INTEGER,INTENT(IN):: inc
  REAL(WP):: az,ez,iz,wz,oz,fz,bt,br,deltv
  REAL(WP),INTENT(OUT):: tpere
  INTEGER,INTENT(IN):: m


  REAL(WP):: NX,NY,NZ, N, IZP,IZM
  REAL(WP),DIMENSION(360):: dv,ta
  REAL(WP),DIMENSION(360,6):: hyp
  REAL(WP),DIMENSION(3):: px
  REAL(WP),DIMENSION(3,6):: py

  REAL(WP):: a,b,c
  REAL(WP):: ci,si, co,so
  REAL(WP):: clat,slat
  REAL(WP):: clon,slon
  REAL(WP):: cwf,swf
  REAL(WP):: det
  REAL(WP):: disc
  REAL(WP):: dx,dy,dz
  REAL(WP):: ezp,ezm
  REAL(WP):: f
  REAL(WP):: fzp,fzm
  INTEGER:: i
  INTEGER:: imin, iminp,iminm 
  REAL(WP):: phi, phim,phip
  REAL(WP):: r0, rs
  REAL(WP):: rx,ry,rz
  REAL(WP):: rpz
  REAL(WP):: sx,sy,sz
  REAL(WP):: test
  REAL(WP):: vx,vy,vz
  REAL(WP):: ws
  REAL(WP):: x,y,z

!-------------------------------------------------------------------------------
  clat=COS(lats*DEG2RAD) 
  slat=SIN(lats*DEG2RAD) 
  clon=COS(lons*DEG2RAD)
  slon=SIN(lons*DEG2RAD)
  sx=clat*clon
  sy=clat*slon
  sz=slat

  DO 1 I=1,360,M
    ta(i)=REAL(I)-A180
    f=DEG2RAD*ta(i)
    cwf=COS(DEG2RAD*w1+f) 
    swf=SIN(DEG2RAD*w1+f)
    ci=COS(DEG2RAD*I1)
    si=SIN(DEG2RAD*I1)
    co=COS(DEG2RAD*o1)
    so=SIN(DEG2RAD*o1)
    rx=cwf*co-swf*so*ci
    ry=cwf*so+swf*co*ci
    rz=swf*si 
    az=-u/vinf**2
    r0=a1*(ONE-e1*e1)/(ONE+E1*COS(F))
    RS=RX*SX+RY*SY+RZ*SZ
    a=az**2
    b=r0**2*rs**2 + 2.*r0*az*rs - r0**2 - 2.*az**2      +2.*az*r0
    C=TWO*r0**2*rs - 2.*r0*az*rs + 2.*r0**2 + az**2 - 2.*az*r0
    dv(i)=1.E20 
    test=b*b-4.*a*c
    IF (test.LT.ZERO)GO TO 1

    disc=SQRT(test)
    ezp=SQRT((-b+disc)/(TWO*a))
    ezm=SQRT((-b-disc)/(TWO*a))
    IF (ezm .LE. ONE) ezm=ezp
    phip=ACOS(ONE/ezp)
    phim=ACOS(ONE/ezm)
    fzp=ACOS((az*(1.-ezp**2)-r0)/(ezp*r0)) 
    fzm=ACOS((az*(1.-ezm**2)-r0)/(ezm*r0)) 
    IF (ABS(COS(Angle(phip-fzp))-rs).GT. 1.E-7) fzp=-fzp 
    IF (ABS(COS(Angle(phim-fzm))-rs).GT. 1.E-7) fzm=-fzm
    nx=ry*sz-rz*sy
    ny=rz*sx-rx*sz
    nz=rx*sy-ry*sx
    N=SQRT(nx**2+ny**2+nz**2)
    izp=ACOS(NZ/N)
    IF (ANGLE(phip-fzp).GT. PI) izp=ACOS(-nz/n)
    izm=ACOS(nz/n)
    IF (ANGLE(phim-fzm).GT. PI)izm=ACOS(-nz/n)
    IF ((izp <= HALFPI .AND. inc==1) .OR. (izp>HALFPI .AND. inc==2)) THEN
      ez=ezp
      iz=RAD2DEG*izp 
      fz=RAD2DEG*fzp 
      phi=RAD2DEG*phip 
    ELSE
      ez=ezm
      iz=RAD2DEG*izm 
      fz=RAD2DEG*fzm 
      phi=RAD2DEG*phim
    END IF

    rpz=az*(ONE-ez)
    dv(i)=1.E20 
    IF (rpz .LT. rpmin) GO TO 1
    WS=ASIN(SZ/SIN(DEG2RAD*IZ))
    WZ=RAD2DEG*WS-PHI 
    IF (ABS(RZ-SIN(DEG2RAD*(WZ+FZ))*SIN(DEG2RAD*IZ)).GT.1.E-7) WZ=180.-RAD2DEG*WS-PHI 
    DET=COS(DEG2RAD*(WZ+PHI))**2+COS(DEG2RAD*IZ)**2*SIN(DEG2RAD*(WZ+PHI))**2 
    CO=(COS(DEG2RAD*(WZ+PHI))*SX+COS(DEG2RAD*IZ)*SIN(DEG2RAD*(WZ+PHI))*SY)/DET 
    SO=(-COS(DEG2RAD*IZ)*SIN(DEG2RAD*(WZ+PHI))*SX+COS(DEG2RAD*(WZ+PHI))*SY)/DET 
    OZ=RAD2DEG*ATAN2(SO,CO)
    hyp(i,1)=az
    hyp(i,2)=ez
    hyp(i,3)=iz
    hyp(i,4)=wz
    hyp(i,5)=oz
    f1=RAD2DEG*f
    hyp(i,6)=fz
    CALL CONCAR(az,ez,iz,wz,oz,fz,x,y,z,dx,dy,dz,u)
    CALL CONCAR(a1,e1,i1,w1,o1,f1,x,y,z,vx,vy,vz,u) 
    dv(i)=SQRT((dx-vx)**2+(dy-vy)**2+(dz-vz)**2)
  1 CONTINUE
  IMIN=0
  DELTV=1.E20
  DO 8 I=1,360,M 
    IF(DV(I).GT.DELTV)GO TO 8
    IMIN=I
    DELTV=DV(I)
 8 CONTINUE

  iminm=imin-m
  iminp=imin+m
  IF (iminm .LE. ZERO .OR. iminp.GE.361) GO TO 6 
  IF (dv(imin-m) .EQ. 1.E20_WP .OR. dv(imin+m).EQ. 1.E20_WP) GO TO 6 
  px(1)=ta(imin-m)
  px(2)=ta(imin) 
  px(3)=ta(imin+m) 
  py(1,1)=dv(imin-m)
  py(2,1)=dv(imin) 
  py(3,1)=dv(imin+m)

  do 5 i=2,6
    py(1,i)=hyp(imin-m,i)
    py(2,i)=hyp(imin,i)
  5 py(3,i)=hyp(imin+m,i)

  CALL Parin(f1,deltv,px,py(1:3,1),0)
  CALL Parin(f1,ez,   px,py(1:3,2),1)
  CALL Parin(f1,iz,   px,py(1:3,3),1)
  CALL Parin(f1,wz,   px,py(1:3,4),1)
  CALL Parin(f1,oz,   px,py(1:3,5),1)
  CALL Parin(f1,fz,   px,py(1:3,6),1)
  GO TO 9

6 f1=ta(imin)
  az=hyp(imin,1) 
  ez=hyp(imin,2) 
  iz=hyp(imin,3) 
  wz=hyp(imin,4) 
  oz=hyp(imin,5) 
  fz=hyp(imin,6)

9 b=-az*SQRT(ez*ez-ONE)
  bt=b*COS(DEG2RAD*iz)/clat
  br=b*SIN(DEG2RAD*iz)*COS(DEG2RAD*(lons-oz))
  CALL Tconic(u,e1,a1,f1,tpere)   ! computes tpere in seconds
  RETURN
END Subroutine Deboost   ! -----------------------------------------------------

!+
SUBROUTINE DBST1(a1,e1,i1,w1,o1,f1,u,lats,lons,vinf,rpmin,inc, &
                 az,ez,iz,wz,oz,fz,bt,br,deltv,tpere,tperh,b, &
   hprad,dbrad,vxh,vyh,vzh,vxe,vye,vze, & 
  vxd,vyd,vzd,dechp,rahp,decep,raep,vdbh,vdbe,plane,m)
! ------------------------------------------------------------------------------
! PURPOSE - 
IMPLICIT NONE
  REAL(WP),INTENT(IN):: a1,e1,i1,w1,o1
  REAL(WP),INTENT(OUT):: f1
  REAL(WP),INTENT(IN):: u,lats,lons
  REAL(WP):: VINF,RPMIN   ! ???
  INTEGER,INTENT(IN):: INC
  REAL(WP),INTENT(OUT):: AZ,EZ
  REAL(WP),INTENT(OUT):: IZ,WZ,OZ,FZ,BT,BR,DELTV,TPERE,TPERH,B,HPRAD,DBRAD,VXH,VYH,VZH
  REAL(WP),INTENT(OUT):: VXE,VYE,VZE
  REAL(WP),INTENT(OUT):: VXD,VYD,VZD,DECHP,RAHP,DECEP,RAEP,VDBH,VDBE,PLANE
  INTEGER,INTENT(IN):: M

  REAL(WP),DIMENSION(360):: dv,ta
  REAL(WP),DIMENSION(360,6):: hyp
  REAL(WP):: px(3),py(3,6)

  REAL(WP):: a,c
  REAL(WP):: ci,si, co,so
  REAL(WP):: clat,slat
  REAL(WP):: clon,slon
  REAL(WP):: cwf,swf
  REAL(WP):: det
  REAL(WP):: disc
  REAL(WP):: dx,dy,dz
  REAL(WP):: ezp,ezm
  REAL(WP):: f
  REAL(WP):: fzp,fzm
  INTEGER:: i
  INTEGER:: imin, iminp,iminm 
  REAL(WP):: izp,izm
  REAL(WP):: nx,ny,nz
  REAL(WP):: n
  REAL(WP):: phi, phim,phip
  REAL(WP):: r0, rs
  REAL(WP):: rx,ry,rz
  REAL(WP):: rpz
  REAL(WP):: sx,sy,sz
  REAL(WP):: test
  REAL(WP):: vx,vy,vz
  REAL(WP):: ws
  REAL(WP):: wxe,wye,wze
  REAL(WP):: wxh,wyh,wzh
  REAL(WP):: x,y,z
  REAL(WP):: xpe,ype,zpe
  REAL(WP):: xph,yph,zph


!-------------------------------------------------------------------------------
  WRITE(DBG,*) 'Entering Deboost1, m=', m
  WRITE(DBG,FMT1) 'In Deboost1, lats,lons', lats,lons
  WRITE(DBG,FMT1) 'In Deboost1, a1,e1,i1', a1,e1,i1
  WRITE(DBG,FMT1) 'In Deboost1, w1,o1', w1,o1
  CLAT=COS(DEG2RAD*LATS)
  SLAT=SIN(DEG2RAD*LATS) 
  CLON=COS(DEG2RAD*LONS) 
  SLON=SIN(DEG2RAD*LONS) 
  SX=CLAT*CLON
  SY=CLAT*SLON
  SZ=SLAT

  DO 1 i=1,360,M
    TA(I)=FLOAT(I)-A180   ! true anomaly, degrees
    F=DEG2RAD*TA(I)       ! true anomaly, radians
    CWF=COS(DEG2RAD*W1+F) 
    SWF=SIN(DEG2RAD*W1+F) 
    CI=COS(DEG2RAD*I1)
    SI=SIN(DEG2RAD*I1)
    CO=COS(DEG2RAD*O1)
    SO=SIN(DEG2RAD*O1)
    RX=CWF*CO-SWF*SO*CI
    RY=CWF*SO+SWF*CO*CI
    RZ=SWF*SI
    AZ=-U/VINF**2
    R0=A1*(ONE-E1*E1)/(ONE+E1*COS(F)) 
    rs=rx*sx+ry*sy+rz*sz
    a=az**2
    b= r0**2*rs**2 + TWO*r0*az*rs - r0**2     - TWO*az**2 + TWO*az*r0
    c=TWO*r0**2*rs - TWO*r0*az*rs + TWO*r0**2 + az**2     - TWO*az*r0
    dv(i)=1.0E20
    test=b*b-FOUR*a*c
    IF (test <= ZERO) GO TO 1

    disc=SQRT(test)
    ezp=SQRT((-b+disc)/(2.*a)) 
    ezm=SQRT((-b-disc)/(2.*a)) 
    IF (ezm<=ONE) ezm=ezp
    phip=ACOS(ONE/ezp)
    phim=ACOS(ONE/ezm)
    fzp=ACOS((az*(ONE-ezp**2)-r0)/(ezp*r0)) 
    fzm=ACOS((az*(ONE-ezm**2)-r0)/(ezm*r0))
    IF (ABS(COS(Angle2(phip-fzp))-rs).GT. 1.E-7) fzp=-fzp 
    IF (ABS(COS(Angle2(phim-fzm))-rs).GT. 1.E-7) fzm=-fzm
    nx=ry*sz-rz*sy 
    ny=rz*sx-rx*sz 
    nz=rx*sy-ry*sx 
    n=SQRT(nx**2+ny**2+nz**2)
    izp=ACOS(nz/n) 
    IF (Angle2(phip-fzp)>PI) izp=ACOS(-nz/n)
    izm=ACOS(nz/n) 
    IF (Angle2(phim-fzm)>PI) izm=ACOS(-nz/n)
    IF ((izp<=HALFPI .AND. inc==1) .OR. (izp>HALFPI .AND. inc==2)) THEN
      ez=ezp
      iz=RAD2DEG*izp
      fz=RAD2DEG*fzp
      phi=RAD2DEG*phip
    ELSE
      ez=ezm
      iz=RAD2DEG*izm 
      fz=RAD2DEG*fzm
      phi=RAD2DEG*phim
    END IF

    rpz=az*(ONE-ez)
    dv(i)=1.E20 
    IF (rpz < rpmin) GO TO 1

    ws=ASIN(sz/SIN(DEG2RAD*iz))
    wz=RAD2DEG*ws-phi 
    IF(ABS(RZ-SIN(DEG2RAD*(WZ+FZ))*SIN(DEG2RAD*IZ)).GT.1.E-7)WZ=180.-RAD2DEG*WS-PHI 
    DET=COS(DEG2RAD*(WZ+PHI))**2+COS(DEG2RAD*IZ)**2*SIN(DEG2RAD*(WZ+PHI))**2 
    CO=(COS(DEG2RAD*(WZ+PHI))*SX+COS(DEG2RAD*IZ)*SIN(DEG2RAD*(WZ+PHI))*SY)/DET
    SO=(-COS(DEG2RAD*IZ)*SIN(DEG2RAD*(WZ+PHI))*SX+COS(DEG2RAD*(WZ+PHI))*SY)/DET 
    OZ=RAD2DEG*ATAN2(SO,CO)
    hyp(i,1)=az
    hyp(i,2)=ez
    hyp(i,3)=iz 
    hyp(i,4)=wz 
    hyp(i,5)=oz 
    f1=RAD2DEG*f
    hyp(i,6)=fz
    CALL CONCAR(az,ez,iz,wz,oz,fz,x,y,z,dx,dy,dz,u)
    CALL CONCAR(a1,e1,i1,w1,o1,f1,x,y,z,vx,vy,vz,u) 
    dv(i)=SQRT((dx-vx)**2 + (dy-vy)**2 + (dz-vz)**2)
!    WRITE(DBG,FMT1) 'loop', REAL(i), az,ez,iz
!    WRITE(DBG,FMT1) 'loop', REAL(i), wz,oz,fz
 1 CONTINUE
!  WRITE(DBG,*) 'Deboost1, hyp'
!  WRITE(DBG,'(I4,6ES13.3)') (i,hyp(i,1:6),i=1,360,m) 
!  WRITE(DBG,*) 'Deboost1, dv,ta'
!  WRITE(DBG,'(I4,2ES13.3)' ) (i,dv(i),ta(i), i=1,360,m)

  imin=0
  deltv=1.E20
  DO 8 i=1,360,M
    IF (DV(I) .GT. DELTV) GO TO 8 
    IMIN=I
    DELTV=DV(I)
8 CONTINUE
  WRITE(DBG,*) 'After search of dv, deltv=', deltv, '  imin=', imin

  IMINM=IMIN-M
  IMINP=IMIN+M
  WRITE(DBG,*) 'iminm,imin,iminp', iminm,imin,iminp
  IF(IMINM.LE.ZERO .OR. IMINP.GE.361) GO TO 6 
  IF (DV(IMIN-M)==1E20 .OR. DV(IMIN+M)==1.E20) GO TO 6 
  px(1)=ta(imin-m)
  px(2)=ta(imin)
  px(3)=ta(imin+m)
  py(1,1)=dv(imin-m) 
  py(2,1)=dv(imin)
  py(3,1)=dv(imin+m) 
  
  DO 5 i=2,6
    py(1,i)=hyp(imin-m,i) 
    py(2,i)=hyp(imin,i)
  5 py(3,i)=hyp(imin+m,i)

  CALL Parin(f1,deltv,px,py(1:3,1),0)
  CALL Parin(f1,ez,   px,py(1:3,2),1)
  CALL Parin(f1,iz,   px,py(1:3,3),1)
  CALL Parin(f1,wz,   px,py(1:3,4),1)
  CALL Parin(f1,oz,   px,py(1:3,5),1)
  CALL Parin(f1,fz,   px,py(1:3,6),1)
  GO TO 9

6 f1=ta(imin)
  az=hyp(imin,1) 
  ez=hyp(imin,2) 
  iz=hyp(imin,3) 
  wz=hyp(imin,4) 
  oz=hyp(imin,5) 
  fz=hyp(imin,6)

9 continue
  WRITE(DBG,FMT1) 'Deboost1, after Parin, f1,az,ez', f1,az,ez
  WRITE(DBG,FMT1) 'Deboost1, after Parin, wz,oz,fz', wz,oz,fz
  B=-AZ*SQRT(EZ*EZ-ONE)
  BT=B*COS(DEG2RAD*IZ)/CLAT
  BR=B*SIN(DEG2RAD*IZ)*COS(DEG2RAD*(LONS-OZ))
  CALL Tconic(u,e1,a1,f1,tpere)
  CALL Tconic(u,ez,az,fz,tperh)
  hprad=az-az*ez 
  dbrad=(a1-a1*e1*e1)/(ONE+e1*COS(DEG2RAD*f1))
  CALL CONCAR(az,ez,iz,wz,oz,fz,x,y,z,vxh,vyh,vzh,u) 
  CALL CONCAR(a1,e1,i1,w1,o1,f1,x,y,z,vxe,vye,vze,u) 
  VDBH=SQRT(VXH*VXH+VYH*VYH+VZH*VZH) 
  VDBE=SQRT(VXE*VXE+VYE*VYE+VZE*VZE)
  VXD=VXE-VXH
  VYD=VYE-VYH
  VZD=VZE-VZH

  CALL CONCAR(az,ez,iz,wz,oz,zero,xph,yph,zph,dx,dy,dz,u) 
  CALL CONCAR(a1,e1,i1,w1,o1,zero,xpe,ype,zpe,dx,dy,dz,u) 
  CALL LATLNG(xph,yph,zph,dechp,rahp)
  CALL LATLNG(xpe,ype,zpe,decep,raep) 
  wxe= SIN(DEG2RAD*i1)*SIN(DEG2RAD*o1)
  wye=-COS(DEG2RAD*o1)*SIN(DEG2RAD*i1)
  wze= COS(DEG2RAD*i1) 
     
  wxh= SIN(DEG2RAD*iz)*SIN(DEG2RAD*oz)
  wyh=-COS(DEG2RAD*oz)*SIN(DEG2RAD*iz)
  wzh= COS(DEG2RAD*iz)
  CALL Dot(wxe,wye,wze, wxh,wyh,wzh, plane)
  RETURN
END Subroutine DBST1   ! -------------------------------------------------------

!+
SUBROUTINE Eearth(jd,xhe,yhe,zhe, dxhe,dyhe,dzhe)
! ------------------------------------------------------------------------------
!PURPOSE - Compute the heliocentric position and velocity of the earth in mean
! equinox and ecliptic of date coordinate system.
! NOTE - Called by Plug, Plug1, Vector. Calls Tinvs and Concar.

IMPLICIT NONE
  REAL(WP),INTENT(IN):: jd   ! Julian date
  REAL(WP),INTENT(OUT):: xhe,yhe,zhe  ! position of earth
  REAL(WP),INTENT(OUT):: dxhe,dyhe,dzhe  ! velocity of earth

  REAL(WP):: ae,ee,oe,xie,we,xme
  REAL(WP):: cd,d,te
  REAL(WP):: ece,fe
!-------------------------------------------------------------------------------

  d=jd-2415020.0
  cd=d/10000.
  te=d/36525.

  ae=1.00000023*AU
  ee=0.01675104 - 0.00004180*te - 0.000000126*te**2
  xie=ZERO
  we=101.220833 + 0.000047068*d + 0.0000339*cd**2 + 0.00000007*cd**3
  oe=ZERO
  xme=Angle(358.475845 + 0.985600267*d - 0.0000112*cd**2 - 0.00000007*Cd**3)

  CALL TINVS(xme*DEG2RAD, ee, ece, fe)
  CALL CONCAR(ae,ee,xie,we,oe,fe*RAD2DEG,xhe,yhe,zhe,dxhe,dyhe,dzhe,USUN)

  RETURN
END Subroutine Eearth   ! ------------------------------------------------------

SUBROUTINE EMARS(jd, xhm,yhm,zhm, dxhm,dyhm,dzhm)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the mean heliocentric position and velocity of Mars in the 
!   mean earth equinox and ecliptic of date coordinate system. 
! NOTE - Called by Plug, Plug1, Vector. Calls Tinvs and Concar.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: jd   ! julian date
  REAL(WP),INTENT(OUT):: xhm,yhm,zhm   ! pcsition of Mars
  REAL(WP),INTENT(OUT):: dxhm,dyhm,dzhm ! velocity of Mars

  REAL(WP):: am
  REAL(WP):: cd,d,te
  REAL(WP):: em,xim,om,wm
  REAL(WP):: xmm   ! mean anomaly, radians
  REAL(WP):: fm    ! true anomaly, radians
  REAL(WP):: ecm   ! eccentric anomaly, radians
!-------------------------------------------------------------------------------
  d=jd-2415020.
  cd=d/10000
  te=d/36525.

  am=1.5236915*AU
  em=0.09331290 + 0.000092064*te - 0.000000077*te**2 
  xim=1.850334-0.000675*te + 0.000012*te**2 
  om=48.786442 + 0.770991*te - 0.0000015*te**2 - 0.00000576*te**3 
  wm=334.218203 + 1.840759*te + 0.000130*te**2 - 0.00000129*te**3-om 
  xmm=Angle(319.529425+0.5240207666*d+0.000013553*cd**2 + 0.000000025*cd**3)

  CALL Tinvs(xmm*DEG2RAD,em, ecm,fm)
  CALL Concar(am,em,xim,wm,om,fm*RAD2DEG,xhm,yhm,zhm,dxhm,dyhm,dzhm,USUN)

  RETURN
END Subroutine Emars   ! -------------------------------------------------------


!+
SUBROUTINE Evenus(jd, xhv,yhv,zhv, dxhv,dyhv,dzhv)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the mean heliocentric position and velocity of Venus in the
!   mean Earth equinox and ecliptic of date ccordinate system. 
! NOTE - Calls subroltines tinvs and concar
  REAL(WP),INTENT(IN):: jd ! julian date
  REAL(WP),INTENT(OUT):: xhv,yhv,zhv  ! positicn of Venus
  REAL(WP),INTENT(OUT):: dxhv,dyhv,dzhv ! velocity cf Venus

  REAL(WP):: av
  REAL(WP):: cd
  REAL(WP):: d
  REAL(WP):: ecv
  REAL(WP):: ev
  REAL(WP):: fv
  REAL(WP):: ov
  REAL(WP):: te
  REAL(WP):: wv
  REAL(WP):: xiv
  REAL(WP):: xmv
!-------------------------------------------------------------------------------

  d=jd-2415020.0
  cd=d/10000
  te=d/36525

  av=0.7233316*au
  ev=0.00682069-0.00004774*te 
  xiv=3.393630 + 0.001005*te - 0.000000197*te**2 
  ov=75.779648 + 0.899850*te + 0.000411*te**2 
  wv=130.180500 + 1.408036*te - 0.000976*te**2 - ov 
  xmv=Angle(212.603219 + 1.6021301540*d + 0.000096400*cd**2)

  CALL Tinvs(xmv*DEG2RAD,ev, ecv,fv)
  CALL ConCar(av,ev,xiv,wv,ov,fv*RAD2DEG,xhv,yhv,zhv, dxhv,dyhv,dzhv,usun)

  RETURN
END Subroutine Evenus   ! ------------------------------------------------------


!+
SUBROUTINE Euler(x,y,z,xp,yp,zp,phi,psi,theta,dphi,dpsi,dtheta,wxp,wyp,wzp,j,k)
! ------------------------------------------------------------------------------
! PURPOSE -Perform an Euler rotation and/or compute instantaneous angular velocity vector
IMPLICIT NONE
  REAL(WP),INTENT(IN OUT):: x,y,z   ! components in unprimed system
  REAL(WP),INTENT(IN OUT):: xp,yp,zp  ! components in primed system
  REAL(WP),INTENT(IN):: phi,psi,theta  ! the three Euler angles, degrees
  REAL(WP),INTENT(IN OUT):: dphi,dpsi,dtheta  ! time rate of change of Euler angles
  REAL(WP),INTENT(IN OUT):: wxp,wyp,wzp  ! primed components of angular velocity
  INTEGER,INTENT(IN):: j  ! =-1 xp,yp,zp INPUT and x,y,z OUTPUT
                          ! =0 for no rotation
                          ! =1 for x,y,z INPUT and xp,yp,zp OUTPUT
  INTEGER,INTENT(IN):: k  ! =-1 wxp,wyp,wzp INPUT and dphi,dpsi,dtheta OUTPUT
                          ! =0 for no angular velocity computations
                          ! =1 for dphi,dpsi,dtheta INPUT and wxp,wyp,wzp OUTPUT

  REAL(WP):: xphi,xpsi,xth
!-------------------------------------------------------------------------------
  WRITE(DBG,*) 'Entering Euler with j,k', j,k
  WRITE(DBG,FMT1) 'Euler, initial x,y,z', x,y,z
  WRITE(DBG,FMT1) 'Euler, initial xp,yp,zp', xp,yp,zp
  WRITE(DBG,FMT1) 'Euler, initial phi,psi,theta', phi,psi,theta
  WRITE(DBG,FMT1) 'Euler, initial wxp,wyp,wzp', wxp,wyp,wzp


  xphi=phi*DEG2RAD
  xpsi=psi*DEG2RAD
  xth=theta*DEG2RAD

  IF (j) 10,12,11
10 x=(COS(xpsi)*COS(xphi)-COS(xth)*SIN(xphi)*SIN(xpsi))*XP + &
     (SIN(xpsi)*COS(xphi)-COS(xth)*SIN(xphi)*COS(xpsi))*YP - &
     (SIN(xth)*SIN(xphi))*ZP
   y=( COS(xpsi)*SIN(xphi)+COS(xth)*COS(xphi)*SIN(xpsi))*XP + &
     (-SIN(xpsi)*SIN(xphi)+COS(xth)*COS(xphi)*COS(xpsi))*YP + &
     (-SIN(xth)*COS(xphi))*ZP
   z=(SIN(xth)*SIN(xpsi))*XP + (SIN(xth)*COS(xpsi))*YP + (COS(xth))*ZP 
   GO TO 12

11 xp=(COS(xpsi)*COS(xphi)-COS(xth)*SIN(xphi)*SIN(xpsi))*X + &
      (COS(xpsi)*SIN(xphi)+COS(xth)*COS(xphi)*SIN(xpsi))*Y + &
      (SIN(xth)*SIN(xpsi))*Z 
   YP=(-SIN(xpsi)*COS(xphi)-COS(xth)*SIN(xphi)*COS(xpsi))*X + &
      ( SIN(xpsi)*SIN(xphi)+COS(xth)*COS(xphi)*COS(xpsi))*Y + &
      ( COS(xpsi)*SIN(xth))*Z
   ZP=(SIN(xth)*SIN(xphi))*X + (SIN(xth)*COS(xphi))*Y + COS(xth)*Z


12 IF(k)13,15,14
13 dphi=(wxp*SIN(xpsi)+wyp*COS(xpsi))/SIN(xth) 
   dpsi=wzp-(COS(xth)*(wxp*SIN(xpsi)+wyp*COS(xpsi)))/SIN(xth) 
   dtheta=wxp*COS(xpsi)-wyp*SIN(xpsi)
   GO TO 15
14 wxp=dphi*SIN(xth)*SIN(xpsi)+dtheta*COS(xpsi) 
   wyp=dphi*SIN(xth)*COS(xpsi)-dtheta*SIN(xpsi) 
   wzp=dpsi+dphi*COS(xth)
15 RETURN
END Subroutine Euler   ! -------------------------------------------------------


!+
SUBROUTINE Euler2(phi,psi,theta,x,y,z,xp,yp,zp)
! ------------------------------------------------------------------------------
! PURPOSE -Perform an Euler rotation.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: phi,psi,theta  ! the three Euler angles, degrees
  REAL(WP),INTENT(IN):: x,y,z   ! components in unprimed system
  REAL(WP),INTENT(OUT):: xp,yp,zp  ! components in primed system

  REAL(WP):: xphi,xpsi,xth
!-------------------------------------------------------------------------------

  WRITE(DBG,FMT1) 'Euler2, initial x,y,z', x,y,z

  xphi=phi*DEG2RAD
  xpsi=psi*DEG2RAD
  xth=theta*DEG2RAD

  xp=(COS(xpsi)*COS(xphi)-COS(xth)*SIN(xphi)*SIN(xpsi))*x + &
      (COS(xpsi)*SIN(xphi)+COS(xth)*COS(xphi)*SIN(xpsi))*y + &
      (SIN(xth)*SIN(xpsi))*z
  yp=(-SIN(xpsi)*COS(xphi)-COS(xth)*SIN(xphi)*COS(xpsi))*x+ &
      ( SIN(xpsi)*SIN(xphi)+COS(xth)*COS(xphi)*COS(xpsi))*y + &
      ( COS(xpsi)*SIN(xth))*z
  zp=(SIN(xth)*SIN(xphi))*X + (SIN(xth)*COS(xphi))*Y + COS(xth)*z

  RETURN
END Subroutine Euler2   ! -------------------------------------------------------

!+
SUBROUTINE Events(elonp,speclon,ae,ee,fe,days,pd,datem,per,fpa,rentry, &
  rs,tdest,u,tadeorb,  timland,timdeor,timdbst,vdeorb,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Computes event times for landing,deorbit and deboost, given landing
!       point and elements of ellipse and true anomaly of deorbit.
IMPLICIT NONE
! Correct for time of day.
  REAL(WP),INTENT(IN):: elonp,speclon
  REAL(WP),INTENT(IN):: ae,ee,fe,days
  REAL(WP),INTENT(IN):: pd
  REAL(WP),INTENT(IN):: datem,per,fpa,rentry
  REAL(WP),INTENT(IN):: rs,tdest,u
  REAL(WP),INTENT(IN):: tadeorb
  REAL(WP),INTENT(OUT):: timland
  REAL(WP),INTENT(OUT):: timdeor
  REAL(WP),INTENT(OUT):: timdbst
  REAL(WP),INTENT(OUT):: vdeorb
  INTEGER,INTENT(OUT):: kk

  REAL(WP):: deltlon
  REAL(WP):: al,el
  REAL(WP):: deltjd
  REAL(WP):: deldeor
  REAL(WP):: delodb
  REAL(WP):: deltim
  REAL(WP):: fld,fl0

  REAL(WP):: ha

  REAL(WP):: backup
  REAL(WP):: pdt
  REAL(WP):: per0
  REAL(WP):: tpere
  REAL(WP):: tdeorb
  REAL(WP):: tdeor,tland
  REAL(WP):: theta
  REAL(WP),PARAMETER:: PMDOT = 350.891962_WP
!-------------------------------------------------------------------------------
  ha=145.845+PMDOT*(datem+days-2418322.) 
  deltlon=elonp-speclon-ha
  deltlon=Angle(deltlon)
  deltjd=deltlon/pmdot
  timland=datem+days+deltjd
  WRITE(DBG,FMT1) 'In Events, deltlon,deltjd,timland', deltlon,deltjd,timland
  WRITE(DBG,FMT1) 'In Events, datem,days', datem,days
  
!... If tdest is input as zero, an impulsive deorbit maneuver is performed and
!--- the deorbit quantities of Appendix C are computed.
  IF (tdest==ZERO) THEN
    per0=-per
    CALL Confpa(ae,ee,tadeorb,per0,rs,rentry,fpa,u, &   ! inputs
                al,el,fl0,fld,vdeorb,theta,kk)          ! outputs
    IF (kk==0) WRITE(DBG,*) 'In Events, kk=0 return from Confpa. BAD.'
    IF (kk==ZERO) RETURN
    CALL Tconic(u,el,al,fld,tdeor) 
    CALL Tconic(u,el,al,fl0,tland) 
    tdeor=tdeor/86400.
    tland=tland/86400.
    pdt=TWOPI*SQRT(al*al*al)/SQRT(u) 
    pdt=pdt/86400.
    WRITE(DBG,FMT1) 'In Events, tdeor,tland,pdt', tdeor,tland,pdt
    IF (tdeor>ZERO) deldeor=tland+pdt-tdeor 
    IF (tdeor<ZERO) deldeor=tland-tdeor
  ELSE
    deldeor=tdest
  END IF
  timdeor=timland-deldeor
  WRITE(DBG,FMT1) 'In Events, timdeor,timland,deldeor', timdeor,timland,deldeor
  !       calculate deboost
  
  backup=days/pd
  deltim=AINT(backup) ! truncate the fractional part ! was deltim=ifix(backup)   
  WRITE(DBG,FMT1) 'In Events, timdeor,backup,deltim', timdeor,backup,deltim

  CALL Tconic(u,ee,ae,fe,tpere)   ! computes tpere
  CALL Tconic(u,ee,ae,tadeorb,tdeorb)   ! computes tdeorb
  tpere=tpere/86400.
  tdeorb=tdeorb/86400.
  WRITE(DBG,FMT1) 'In Events, tpere,tdeorb', tpere,tdeorb
  if(tdeorb>ZERO) delodb=tdeorb-tpere 
  if(tdeorb<ZERO) delodb=tdeorb-tpere+pd 
  timdbst = timdeor - deltim - delodb
  WRITE(DBG,FMT1) 'In Events, timdeor,deltim,delodb',timdeor,deltim,delodb
  RETURN
END Subroutine Events   ! ------------------------------------------------------

!+
SUBROUTINE Fudge(xj2,xi,ae,ee,delcom,delsom,pd,xitw,capw,oe,we,u,days,rs)
! ------------------------------------------------------------------------------
! PURPOSE - Modify the right ascension and argument of periapsis as computed
!  by CompEl to accout for oblateness effects accumulated during the specified
!  stay time in orbit.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: xj2,xi,ae,ee
  REAL(WP),INTENT(OUT):: delcom,delsom
  REAL(WP),INTENT(OUT):: pd
  REAL(WP),INTENT(IN):: xitw,capw
  REAL(WP),INTENT(OUT):: oe,we
  REAL(WP),INTENT(IN):: u,days
  REAL(WP),INTENT(IN):: rs   ! 

  REAL(WP):: xn
  REAL(WP):: b
  REAL(WP):: c1
  REAL(WP):: enbar
!-------------------------------------------------------------------------------
  xn=SQRT(u/ae**3)
  c1=(rs /(ae*(1.-ee*ee)))**2 
  delsom=SIX*PI*xj2*c1*(1.-1.25*SIN(xi*DEG2RAD)**2) 
  delcom=-THREE*PI*xj2*c1*COS(xi*DEG2RAD) 
  enbar=xn*(ONE+1.5*c1*xj2*SQRT(ONE-ee*ee)*(ONE-1.5*SIN(xi*DEG2RAD)**2)) 
  pd=TWOPI/(ENBAR*86400)
  b=days/pd
  oe=capw-(b*delcom)*RAD2DEG
  we=xitw-(b*delsom)*RAD2DEG 
  RETURN
END Subroutine Fudge   ! -------------------------------------------------------


!+
SUBROUTINE LAMBRT(x1,y1,z1,x2,y2,z2,time12,a,e,xi,w,o,ta1,ta2,u,kk)
! ------------------------------------------------------------------------------
! PURPOSE - If x2,y2, and z2 are zero, then x1 is considered the radial distance
!   to point 1, y1 the distance to point 2, and z1 the angle from point 1 to 
!   point 2. Motion is always considered frcm point 1 to point 2.
! NOTE - Lambrt is called from Plug and Plug1. Calls Angle and ATANH.

  REAL(WP),INTENT(IN):: x1,y1,z1,x2,y2,z2
  REAL(WP),INTENT(IN):: time12
  REAL(WP),INTENT(OUT):: a,e,xi,w,o,ta1,ta2
  REAL(WP),INTENT(IN):: u
  INTEGER,INTENT(OUT):: kk

  REAL(WP):: am
  REAL(WP):: c
  REAL(WP):: co,so
  REAL(WP):: cosxi
  REAL(WP):: cpsi,spsi
  REAL(WP):: cta1,cta2
  REAL(WP):: d1,d2,d3
  REAL(WP):: delec
  REAL(WP):: dfdta2
  REAL(WP):: ec1,ec2
  REAL(WP):: error
  REAL(WP):: f
  REAL(WP):: flast
  REAL(WP):: hh
  INTEGER:: iorbit
  INTEGER:: key
  INTEGER:: m
  REAL(WP):: m12,n
  REAL(WP):: psi
  REAL(WP):: q
  REAL(WP):: r1,r2
  REAL(WP):: s
  REAL(WP):: talast
  REAL(WP):: temp
  REAL(WP):: tp,ttp

!  CHARACTER(LEN=*),PARAMETER:: FMT1= '(1X,A,4ES15.5)'
!-------------------------------------------------------------------------------
  WRITE(DBG,FMT1) 'Lambert input', x1,y1,z1
  WRITE(DBG,FMT1) 'Lambert input', x2,y2,z2
  WRITE(DBG,FMT1) 'Lambert input', time12,u

  m=0
  kk=1
  ta2=ZERO
  key=1
  iorbit=1

  IF (time12<=ZERO) GO TO 800 

!... See if vector (x2,y2,z2) is essentially zero...
  IF ((ABS(x2)<0.1).AND.(ABS(y2)<0.1).AND.(ABS(z2)<0.1)) GO TO 1 

  r1=SQRT(x1*x1+y1*y1+z1*z1)
  r2=SQRT(x2*x2+y2*y2+z2*z2) 
  cpsi=(x1*x2+y1*y2+z2*z2)/r1/r2 
  spsi=(x1*y2-x2*y1)/ABS(x1*y2-x2*y1)*SQRT(ONE-cpsi*cpsi) 
  psi=Angle(ATAN2(spsi,cpsi)*RAD2DEG)
  GO TO 2

1 r1=x1
  r2=y1
  psi=Angle(z1)
  xi=ZERO
  o=ZERO
  w=ZERO

2 c=SQRT(r1*r1 + r2*r2 - TWO*r1*r2*COS(psi*DEG2RAD))
  IF (psi < 0.01) GO TO 800

  am=(r1+r2+c)/FOUR
  s=TWO*am
  tp=SQRT(TWO/u)*(s**1.5-(s-c)**1.5)/THREE
  ttp=SQRT(TWO/u)*(s**1.5+(s-c)**1.5)/THREE
  IF((psi<A180).AND.(time12<tp)) iorbit=2 
  IF((psi>=A180).AND.(time12<ttp)) iorbit=2
  WRITE(DBG,FMT1) 'Lambert setup', r1,r2,psi
  WRITE(DBG,FMT1) 'Lambert setup', am,s,tp,ttp
  WRITE(DBG,*) 'Lambert iorbit', iorbit

3 CONTINUE
  WRITE(DBG,*) "Iterating in Lambert, ta2", ta2
  cta2=COS(ta2*DEG2RAD)
  cta1=COS((ta2-PSI)*DEG2RAD)
  q=r2*cta2-r1*cta1
  IF (ABS(q)>ONE) GO TO 5

4 IF (key>1)GO TO 25
  ta2=ta2+FIVE   ! take big steps
  GO TO 3

5 e=(r1-r2)/q
  IF(e<ZERO) GO TO 4

  a=r2*(ONE+e*cta2)/(ONE-e*e)
  GO TO (6,7),iorbit

6 IF(e>ONE .OR. a<ZERO) GO TO 4
  temp=SQRT((ONE-e)/(ONE+e)) 
  ec1=Angle(TWO*ATAN(temp*TAN((ta2-psi)*DEG2RAD/TWO))*RAD2DEG)*DEG2RAD 
  ec2=Angle(TWO*ATAN(temp*TAN(ta2*DEG2RAD/TWO))*RAD2DEG)*DEG2RAD
  delec=ec2-ec1
  IF (delec<ZERO) delec=TWOPI+delec
  m12=delec-e*(SIN(ec2)-SIN(ec1))
  GO TO 8

7 IF (e<ONE .OR. a>ZERO) GO TO 4 
  temp=SQRT((e-ONE)/(e+ONE))
  ec1=TWO*ATANH(temp*TAN((ta2-psi)*DEG2RAD/TWO))
  ec2=TWO*ATANH(temp*TAN((ta2*DEG2RAD/TWO)))
  m12=e*(SINH(ec2)-SINH(ec1))-ec2+ec1

8 n=SQRT(u/ABS(a)**3)
  f=time12-m12/n
  GO TO (9,10,11),key

9 key=2 
  talast=ta2
  ta2=ta2+ONE
  WRITE(DBG,FMT1) 'Lambert9', talast,ta2
  GO TO 13

10 key=3
  WRITE(DBG,*) 'Setting key to 3 in Lambert'
  GO TO 12

11 m=m+1
  IF (m>60) GO TO 800
  WRITE(DBG,FMT1) 'Lambert f,flast', f,flast
  WRITE(DBG,*) 'Lambert m', m
  IF (ABS(f)<=ABS(flast)) GO TO 12

25 dfdta2=dfdta2*TWO
  m=m+1
  IF (m>60)GO TO 800
  ta2=talast-flast/dfdta2
  go to 3

12 error=f/time12
  IF(ABS(error)< .00001) GO TO 14 
  dfdta2=(f-flast)/(ta2-talast)
  talast=ta2
  ta2=ta2-f/dfdta2
13 flast=f
  GO TO  3

!... Escaping to statement 14 means that the iteration has succeded.
14 ta1=ta2-psi 
  IF ((ABS(x2)<0.1).AND.(ABS(y2)<0.1).AND.(ABS(z2)<0.1))GO TO 900 

  d1=y1*z2-z1*y2
  d2=z1*x2-x1*z2 
  d3=x1*y2-y1*x2 
  hh=SQRT(d1*d1+d2*d2+d3*d3)

  IF (d3<=ZERO) THEN
    d1=-d1
    d2=-d2
    d3=-d3
  END IF

  cosxi=d3/hh
  xi=ATAN2(SQRT(ONE-cosxi*cosxi),cosxi)*RAD2DEG
  so=d1/(hh*SIN(xi*DEG2RAD))
  co=-d2/(hh*SIN(xi*DEG2RAD))
  IF(so==ZERO .AND. co==ZERO) co=ONE
  o=Angle(ATAN2(so,co)*RAD2DEG) 
  w=Angle(ATAN2((-x1*so+y1*co)*cosxi+z1*SIN(xi*DEG2RAD),x1*co+y1*so)*RAD2DEG-ta1) 
  GO TO 900

800 kk=0
900 RETURN
END Subroutine Lambrt   ! ------------------------------------------------------

!+
SUBROUTINE Latlng(x,y,z, xlat,xlong)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the latitude and longitude of a given position vector
IMPLICIT NONE
  REAL(WP),INTENT(IN):: x,y,z   ! components of the position vector
  REAL(WP),INTENT(OUT):: xlat,xlong ! latitude and longitude

  REAL(WP):: r
!-------------------------------------------------------------------------------
  r=SQRT(x**2 + y**2 + z**2)
  xlong=ATAN2(y,x)*RAD2DEG
  xlat=ASIN(z/r)*RAD2DEG
  RETURN
END Subroutine LatLng   ! ------------------------------------------------------

!+
SUBROUTINE Occult(a,e,xi,w,o,u,rs,ex,ey,ez,tocc,t1,alt1,f1,dec1,ra1,&
                                                t2,alt2,f2,dec2,ra2,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the entrance and exit true anomalies of occultation. 
! NOTE - Occult is called by Seese. Calls procedures Angle, Rxyzpqw, Qartic, 
!   Cross, Dot, Tconic, Rpqwxyz, and Latlng.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: a,e,xi ! semimajor axis, eccentricity, inclination
  REAL(WP),INTENT(IN):: w,o  ! argument of periapsis, longitude of ascending node
  REAL(WP),INTENT(IN):: u,rs  !   gravitational constant and radius of the planet
  REAL(WP),INTENT(IN):: ex,ey,ez ! components of unit vector toward the body occulted
  REAL(WP),INTENT(OUT):: tocc  ! length of time in shadow
  REAL(WP),INTENT(OUT):: t1,alt1,f1,dec1,ra1! conditions at entry into the shadow
                                ! time from periapsis, altitude, true anomaly, 
                                ! declination, and right ascension
  REAL(WP),INTENT(OUT):: t2,alt2,f2,dec2,ra2 ! conditions at exit from the shadow
  INTEGER,INTENT(OUT):: kk! 0 implies no occultation, 1 implies occult.

  REAL(WP),DIMENSION(4):: cf
  REAL(WP),DIMENSION(3,3):: rpqw, rxyz

  REAL(WP):: ang
  REAL(WP):: beta
  REAL(WP):: c1,c2,c3,c4,c5
  REAL(WP):: cf1,sf1
  REAL(WP):: dsdf
  INTEGER:: i,jj
  REAL(WP):: p
  REAL(WP):: px,py,pz
  REAL(WP):: product
  REAL(WP):: r
  REAL(WP):: rx,ry,rz
  REAL(WP):: sf
  REAL(WP):: temp
  REAL(WP):: xxi
  REAL(WP):: zbody
!-------------------------------------------------------------------------------
  f1=2000
  f2=2000
  kk=0
  p=a*(1-1*e)
  CALL RxyzPqw(ex,ey,ez,xi,w,o,rpqw,beta,xxi,zbody) 
  c1=(rs/p)**4*e**4-TWO*(rs/p)**2*(xxi**2-beta**2)*e**2+(beta**2+xxi**2)**2
  c2=FOUR*(rs/p)**4*e**3-FOUR*(rs/p)**2*(xxi**2-beta**2)*e 
  c3=6.*(rs/p)**4*e**2- &
       TWO*(rs/p)**2*(xxi**2-beta**2)- &
       TWO*(rs/p)**2*(ONE-xxi**2)*e**2+ &
       TWO*(xxi**2-beta**2)*(ONE-xxi**2)-FOUR*beta**2*xxi**2 
  c4=FOUR*(rs/p)**4*e-FOUR*(rs/p)**2*(ONE-xxi**2)*e 
  c5=(rs/p)**4-TWO*(rs/p)**2*(ONE-xxi**2)+(ONE-xxi**2)**2
  CALL QARTIC(c1,c2,c3,c4,c5,cf(1),cf(2),cf(3),cf(4),jj) 
  IF (jj==0) GO TO 800
  DO 3 i=1,jj
  IF (ABS(cf(i))<1.0001 .AND. ABS(CF(i))>ONE) cf(i)=0.999999_WP
  IF (ABS(cf(i))>ONE) GO TO 3

  sf=SQRT(ONE-cf(i)**2)
  r=p/(ONE+e*cf(i))
  CALL Cross(r*cf(i),r*sf,ZERO, beta,xxi,zbody, px,py,pz, product) 
  CALL Dot(cf(i),sf,ZERO, beta,xxi,zbody, ang) 
  IF(ABS(product-rs)<0.01.and. ang>90.) go to 1
  sf=-sf
  CALL Cross(r*cf(i),r*sf,ZERO, beta,xxi,zbody, px,py,pz, product) 
  CALL Dot(cf(i),sf,ZERO,beta,xxi,zbody,ang) 
  IF (ABS(product-rs)<0.01 .AND. ang>90.) GO TO 1
  GO TO 3

1 IF (f1<1000.0_WP) GO TO 2
  f1=Angle(ATAN2(sf,cf(i))*RAD2DEG)
  GO TO 3
2 f2=Angle(ATAN2(sf,cf(i))*RAD2DEG)
  GO TO 4
3 CONTINUE

4 IF (f2>1000.0_WP) GO TO 800
  cf1=COS(f1*DEG2RAD)
  sf1=SIN(f1*DEG2RAD) 
  dsdf=TWO*rs*rs*(ONE+e*cf1)*(-e*sf1)+ &
    TWO*p*p*(beta*cf1+xxi*sf1)*(-beta*sf1+xxi*cf1)
  IF (dsdf>ZERO) GO TO 5

  temp=f1 
  f1=f2
  f2=temp

5 CALL Tconic(u,e,a,f1,t1)
  call Tconic(u,e,a,f2,t2)
  tocc=t2-t1
  IF (tocc<ZERO) tocc=TWOPI*SQRT(a**3/u)+tocc  ! add a period
  t1=t1/60
  t2=t2/60
  tocc=tocc/60
  alt1=p/(ONE+e*COS(f1*DEG2RAD))-rs
  alt2=p/(ONE+e*COS(f2*DEG2RAD))-rs
  CALL Rpqwxyz(COS(f1*DEG2RAD),SIN(f1*DEG2RAD),ZERO, xi,w,o,rxyz,rx,ry,rz)
  CALL Latlng(rx,ry,rz,dec1,ra1)
  CALL Rpqwxyz(COS(f2*DEG2RAD),SIN(f2*DEG2RAD),ZERO,xi,w,o,rxyz,rx,ry,rz)
  CALL Latlng(rx,ry,rz,dec2,ra2)
  kk=1
  GO TO 900

800 CONTINUE 
  tocc=ZERO 
  t1=ZERO
  alt1=ZERO 
  f1=ZERO
  dec1=ZERO 
  ra1=ZERO 
  t2=ZERO
  alt2=ZERO 
  f2=ZERO
  dec2=ZERO 
  ra2=ZERO

900 CONTINUE 
  RETURN 
END Subroutine Occult   ! ------------------------------------------------------

!+
SUBROUTINE Orbit(slat,slong,vinf,ha,hp,beta,a,e,xi,w,o,plat,plong,u,rs)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the Keplerian orbital elements for a hyperbol1c periapsis 
!   to an elliptical periapsis transfer

  REAL(WP),INTENT(IN):: slat,slong ! latitlde and longitude of the s-vector
  REAL(WP),INTENT(IN):: vinf ! hyperbolic excess velocity
  REAL(WP),INTENT(IN):: ha,hp ! apoapsis and periapsis altitudes of the ellipse
  REAL(WP),INTENT(IN):: beta ! angular measurement of the orientation of the orbital plane
  REAL(WP),INTENT(OUT):: a,e,xi ! semimajor axis,eccentricity, and inclination
  REAL(WP),INTENT(OUT):: w,o ! argument of periapsis and longitude of the ascending node
  REAL(WP),INTENT(OUT):: plat,plong ! latitude and longitude cf periapsis
  REAL(WP),INTENT(IN):: u,rs ! gravitaticnal constant and radius of the planet

  REAL(WP):: apsht
  REAL(WP):: b
  REAL(WP):: csdel,ssdel
  REAL(WP):: del
  REAL(WP):: sdel

!-------------------------------------------------------------------------------
  xi=beta
  IF (xi .gt. 89.9999 .and. xi.lt.90.0001) xi=89.9999 
  IF (xi .gt. 269.9999 .and. xi.lt.270.0001) xi=269.9999

  a=HALF*(TWO*rs+ha+hp)
  e=(rs+ha)/a - ONE
  apsht=RAD2DEG*ACOS(u/(u+(rs+hp)*vinf**2))
  b=RAD2DEG*ASIN(Xsin(slat)/ABS(Xsin(xi)))
  del=RAD2DEG*ASIN(Xtan(slat)/ABS(Xtan(xi)))

  IF (xi .lt. A180) GO TO 2
  w=Angle(A180-b-apsht)
  
  IF (xi .lt. 270.0_WP) GO TO 1
  o=Angle(A180+slong+del)
  GO TO 4

1 o=Angle(A180+slong-del)
  GO TO 4

2 w=Angle(b-apsht) 
  IF (xi .GT. A90) go to 3
  o=Angle(slong+del)
  go to 4

3 o=Angle(slong-del)
4 continue

  IF (xi .ge. A180) xi=360-xi

  plat=RAD2DEG*ASIN(Xsin(w)*Xsin(xi)) 
  ssdel=Xtan(plat)/Xtan(xi)
  csdel=ssdel/Xtan(w)/Xcos(xi) 
  sdel=RAD2DEG*ATAN2(ssdel, csdel)
  plong=Angle(o+sdel)
  RETURN
END Subroutine Orbit   ! -------------------------------------------------------

!+
SUBROUTINE PARIN(xp,yp,x,y,ii)
! ------------------------------------------------------------------------------
! PURPOSE - 
! NOTE - Parin is called by Deboost and Dbst1. Calls Deter.
IMPLICIT NONE
  REAL(WP),INTENT(OUT):: xp,yp
  REAL(WP),INTENT(IN),DIMENSION(3):: x,y
  INTEGER,INTENT(IN):: ii

  REAL(WP):: a,b,c,d
  REAL(WP),DIMENSION(3,3):: det
  INTEGER:: i
!-------------------------------------------------------------------------------
  DO  i=1,3
    det(i,1)=x(i)**2 
    det(i,2)=x(i)
    det(i,3)=ONE
  END DO
  CALL DETER(d,det) ! computes d
  IF (ABS(d)-1.0E16) 100,100,20

20 DO i=1,3
     det(i,1)=y(i)
   END DO
  CALL DETER(a,det) ! computes a
  a=a/d

  DO i=1,3
    det(i,1)=x(i)**2 
    det(i,2)=y(i)
  END DO
  CALL Deter(b,det) ! computes b
  b=b/d

  DO i=1,3
    det(i,2)=x(i)
    det(i,3)=y(i)
  END DO
  CALL Deter(c,det)  ! computes c
  c=c/d

  IF (ii) 200,150,200
150 xp=-b/(TWO*a)
    yp=c-b**2/(FOUR*a) 
    GO TO 300

200 yp=(a*xp+b)*xp+c 
    GO TO 300

100 xp=ZERO
    yp=ZERO
300 RETURN
END Subroutine Parin   ! -------------------------------------------------------


!+
SUBROUTINE PLUG(datee,datem,c3,dla,vhp,dpa,rap,kk) 
! ------------------------------------------------------------------------------
! PURPOSE - 
! NOTE - Plug is called by the main program.
 IMPLICIT NONE
  REAL(WP),INTENT(IN):: datee,datem
  REAL(WP),INTENT(OUT):: c3
  REAL(WP),INTENT(OUT):: dla,vhp
  REAL(WP),INTENT(OUT):: dpa,rap
  INTEGER,INTENT(OUT):: kk

  REAL(WP):: a,e,xi,w,o
  REAL(WP):: dx1,dy1,dz1
  REAL(WP):: dx2,dy2,dz2
  REAL(WP):: dx1e,dy1e,dz1e
  REAL(WP):: dx2m,dy2m,dz2m
  REAL(WP):: ral
  REAL(WP):: ta1,ta2
  REAL(WP):: tt
  REAL(WP):: x1,y1,z1
  REAL(WP):: x2,y2,z2
  REAL(WP):: xe,ye,ze, dxe,dye,dze
  REAL(WP):: xeq,yeq,zeq
  REAL(WP):: xmeq,ymeq,zmeq
  REAL(WP):: xm,ym,zm, dxm,dym,dzm
!-------------------------------------------------------------------------------
!!! USUN=1.3271411E+11
  CALL EEARTH(datee, xe,ye,ze, dxe,dye,dze)
  CALL EMARS(datem, xm,ym,zm, dxm,dym,dzm)
  WRITE(DBG,FMT1) 'Earth launch position', xe,ye,ze
  WRITE(DBG,FMT1) 'Earth launch velocity', dxe,dye,dze
  WRITE(DBG,FMT1) 'Mars arrival position', xm,ym,zm
  WRITE(DBG,FMT1) 'Mars arrival velocity', dxm,dym,dzm

  tt=datem-datee
  WRITE(DBG,*) "In Plug1, dates:", datee,datem,tt
  WRITE(DBG,'(3ES16.5)') xe,ye,ze, dxe,dye,dze
  WRITE(DBG,'(3ES16.5)') xm,ym,zm, dxm,dym,dzm
  WRITE(DBG,*) 'tt(sec):', tt*24*3600
  CALL Lambrt(xe,ye,ze,xm,ym,zm,tt*24*3600,a,e,xi,w,o,ta1,ta2,USUN,kk)
  WRITE(DBG,'(A,5ES14.4)') ' After Lambert', a,e,xi,w,o
  WRITE(DBG,'(A,2ES14.4,I3)') ' Lambert ta1,ta2', ta1,ta2,kk
  IF (kk==0) RETURN
  CALL Concar(a,e,xi,w,o,ta1,x1,y1,z1,dx1,dy1,dz1,USUN)
  dx1e=dx1-dxe 
  dy1e=dy1-dye 
  dz1e=dz1-dze 
  c3=dx1e**2+dy1e**2+dz1e**2
  WRITE(DBG,*) 'c3', dx1e,dy1e,dz1e,c3
  CALL RECEQ(datee, dx1e,dy1e,dz1e, xeq,yeq,zeq)
  CALL LATLNG(xeq,yeq,zeq,dla,ral)
  CALL CONCAR(a,e,xi,w,o,ta2,x2,y2,z2,dx2,dy2,dz2,USUN)
  dx2m=dx2-dxm 
  dy2m=dy2-dym
  dz2m=dz2-dzm 
  vhp=SQRT(dx2m**2+dy2m**2+dz2m**2)
  CALL RECEQ(datem,dx2m,dy2m,dz2m,xeq,yeq,zeq)
  CALL REQMEQ(datem,xeq,yeq,zeq,xmeq,ymeq,zmeq,dpa,rap)
  RETURN
END Subroutine Plug   ! --------------------------------------------------------

!+
SUBROUTINE Plug1(datee,datem,c3,dla,vhp,dpa,rap,tt,ral,dcom,probper, &
  probap,probinc,tal,taa,helang,gda,gra,zap,ets,zae,ete,zac,etc,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Compute C3, dla, vinf, and the declination and right asension of
!   the S-vector. And other things.
! NOTE - Plug1 is called by the main program
IMPLICIT NONE
  REAL(WP),INTENT(IN):: datee,datem
  REAL(WP),INTENT(OUT):: c3,dla,vhp,dpa,rap
  REAL(WP),INTENT(OUT):: tt
  REAL(WP),INTENT(OUT):: ral
  REAL(WP),INTENT(OUT):: dcom     ! communication distance, km.
  REAL(WP),INTENT(OUT):: probper  ! perihelion of interplanetary orbit,km
  REAL(WP),INTENT(OUT):: probap   ! aphelion of interplanetary orbit,km
  REAL(WP),INTENT(OUT):: probinc  ! inclination of interplanetary orbit,km
  REAL(WP),INTENT(OUT):: tal,taa  ! true anomaly at launch and arrival, deg.
  REAL(WP),INTENT(OUT):: helang   ! heliocentric angle of travel, deg.
  REAL(WP),INTENT(OUT):: gda,gra  ! S-vector decl, RA in Mars coor
  REAL(WP),INTENT(OUT):: zap
  REAL(WP),INTENT(OUT):: ets,zae,ete,zac,etc
  INTEGER,INTENT(OUT):: kk

  REAL(WP):: a,e,xi,w,o,ta1,ta2
  REAL(WP):: cat,car,cas
  REAL(WP):: cdec,edec,sdec
  REAL(WP):: cx,cy,cz
  REAL(WP):: dec
  REAL(WP):: dx1,dy1,dz1
  REAL(WP):: dx2,dy2,dz2
  REAL(WP):: dx2m,dy2m,dz2m
  REAL(WP):: dx1e,dy1e,dz1e
  REAL(WP):: dxe,dye,dze  ! Earth velocity at datee  (launch)
  REAL(WP):: dxea,dyea,dzea  ! Earth velocity at datem (arrival)
  REAL(WP):: dxm,dym,dzm   ! Mars velocity at datem (arrival)
  REAL(WP):: dxmZero,dymZero,dzmZero   ! Mars velocity at datee (launch)
  REAL(WP):: eat,ear,eas
  REAL(WP):: error
  REAL(WP):: ex,ey,ez
  REAL(WP):: ra
  REAL(WP):: rmag
  REAL(WP):: rx,ry,rz
  REAL(WP):: srt,srt1
  REAL(WP):: sunt,sunr,suns
  REAL(WP):: sunx,suny,sunz
  REAL(WP):: sra,era,cra
  REAL(WP):: sx,sy,sz
  REAL(WP):: sx1,sy1,sz1
  REAL(WP):: tx,ty,tz
  REAL(WP):: txeq,tyeq,tzeq
  REAL(WP):: txmeq,tymeq,tzmeq
  REAL(WP):: tx1,ty1,tz1
  REAL(WP):: x
  REAL(WP):: x1,y1,z1
  REAL(WP):: x2,y2,z2
  REAL(WP):: x3
  REAL(WP):: xe,ye,ze  ! Earth position at datee (launch)
  REAL(WP):: xea,yea,zea  ! Earth position at datem (arrival)
  REAL(WP):: xeq,yeq,zeq
  REAL(WP):: xmZero,ymZero,zmZero  ! Mars position at datee (launch)
  REAL(WP):: xm,ym,zm  ! Mars position at datem (arrival)
  REAL(WP):: xme,yme,zme  ! difference, Mars to Earth
  REAL(WP):: xmeq,ymeq,zmeq
  REAL(WP):: zae1,zap1


!-------------------------------------------------------------------------------
  CALL Eearth(datee,xe,ye,ze,dxe,dye,dze) ! Earth at launch
  CALL Emars(datee,xmZero,ymZero,zmZero,dxmZero,dymZero,dzmZero) ! Mars at launch
  CALL Eearth(datem,xea,yea,zea,dxea,dyea,dzea)  ! Earth on arrival date
  CALL Emars(datem,xm,ym,zm,dxm,dym,dzm)  ! Mars on arrival date

  WRITE(DBG,FMT1) 'Earth position on launch date', xe,ye,ze
  WRITE(DBG,FMT1) 'Earth position on launch date, AUs', xe/AU,ye/AU,ze/AU
  WRITE(DBG,FMT1) 'Earth velocity on launch date', dxe,dye,dze
  WRITE(DBG,FMT1) 'Mars position on launch date', xe,ye,ze
  WRITE(DBG,FMT1) 'Mars position on launch date, AUs', xe/AU,ye/AU,ze/AU
  WRITE(DBG,FMT1) 'Mars velocity on launch date', dxe,dye,dze
  WRITE(DBG,FMT1) 'Earth position on arrival date', xea,yea,zea
  WRITE(DBG,FMT1) 'Earth position on arrival date, AUs', xea/AU,yea/AU,zea/AU
  WRITE(DBG,FMT1) 'Earth velocity on arrival date', dxea,dyea,dzea
  WRITE(DBG,FMT1) 'Mars position on arrival date', xm,ym,zm
  WRITE(DBG,FMT1) 'Mars position on arrival date, AUs', xm/AU,ym/AU,zm/AU
  WRITE(DBG,FMT1) 'Mars velocity on arrival date', dxm,dym,dzm

  xme=xea-xm
  yme=yea-ym 
  zme=zea-zm 
  dcom=SQRT(xme*xme + yme*yme + zme*zme)
  tt=datem-datee
  CALL Lambrt(xe,ye,ze,xm,ym,zm,tt*24.*3600.,a,e,xi,w,o,ta1,ta2,USUN,kk)
  WRITE(DBG,FMT1) 'In Plug1, return from Lambert, a,e,xi,w=',a,e,xi,w
  WRITE(DBG,FMT1) 'In Plug1, return from Lambert, o,ta1,ta2=', o,ta1,ta2
  WRITE(DBG,*) 'In Plug1, return from Lambert, kk=',kk
  IF (kk==0) RETURN
  
  probper=a-a*e
  probap=a+a*e 
  probinc=xi 
  tal=ta1
  taa=ta2
  helang=taa-tal

!... Compute position, velocity at true anomaly ta1 ...
  CALL ConCar(a,e,xi,w,o,ta1, x1,y1,z1,dx1,dy1,dz1,USUN) 

!... In heliocentric coordinates, the velocity of Earth is (dxe,dye,dze) and
!... the velocity of the spacecraft is (dx1,dy1,dz1). Compute the difference.
!... The square of this velocity is C3, the injection energy
  dx1e=dx1-dxe
  dy1e=dy1-dye 
  dz1e=dz1-dze 
  c3=dx1e**2+dy1e**2+dz1e**2
  WRITE(DBG,FMT1) 'Plug1, injection velocity, C3', dx1e,dy1e,dz1e,c3

!... The velocity (dx1e,dy1e,dz1e) is in the ecliptic coordinate system.
!... Convert to Earth equatorial coordinates and express as latitude and
!... longitude (declination and right ascension)
  CALL Receq(datee, dx1e,dy1e,dz1e, xeq,yeq,zeq)
  CALL Latlng(xeq,yeq,zeq, dla,ral)
  WRITE(DBG,FMT1) 'Plug1, xeq,yeq,zeq', xeq,yeq,zeq
  WRITE(DBG,FMT1) 'Plug1, dla,ral', dla,ral

!... Compute position, velocity at true anomaly ta2 ...
  CALL ConCar(a,e,xi,w,o,ta2, x2,y2,z2,dx2,dy2,dz2,USUN)

!... In heliocentric coordinates, the velocity of Mars is (dxm,dym,dzm) and
!... the velocity of the spacecraft is (dx2,dy2,dz2). 
!... Compute the difference, which is the approach velocity of the spacecraft
!... as seen from Mars. The S-vector is a unit vector in this direction.
  dx2m=dx2-dxm
  dy2m=dy2-dym 
  dz2m=dz2-dzm 
  WRITE(DBG,FMT1) 'Plug1, arrival excess velocity', dx2m,dy2m,dz2m

!... Compute angle between S-vector and Mars-to-Sun vector
  CALL DOT(-xm,-ym,-zm, dx2m,dy2m,dz2m, zap1)
!... Compute angle between S-vector and Mars-to-Earth vector
  CALL DOT(xme,yme,zme, dx2m,dy2m,dz2m, zae1)
!... Compute the magnitude of the hyperbolic excess velocity
  vhp=SQRT(dx2m**2 + dy2m**2 + dz2m**2)
!... (dx2m,dy2m,dz2m) is in the ecliptic coordinate system. 
!... Convert to Earth equatorial coordinates.
  CALL Receq(datem,dx2m,dy2m,dz2m, xeq,yeq,zeq)
  CALL Latlng(xeq,yeq,zeq, gda,gra)  ! S-vector declination and right ascension
  WRITE(DBG,FMT1) 'Plug1, S (to Earth)', xeq,yeq,zeq
  WRITE(DBG,FMT1) 'Plug1, S (to Earth), decl,RA', gda,gra
!... Convert to Mars equatorial coordinates
  CALL REQMEQ(datem,xeq,yeq,zeq, xmeq,ymeq,zmeq, dpa,rap)
  WRITE(DBG,FMT1) 'Plug1, S (to Mars)', xmeq,ymeq,zmeq
  WRITE(DBG,FMT1) 'Plug1, S (to Mars), decl,RA', dpa,rap

  sx=xmeq/vhp 
  sy=ymeq/vhp 
  sz=zmeq/vhp 
  srt=SQRT(sx*sx+sy*sy)

  tx= sy/srt
  ty=-sx/srt
  tz=ZERO
  
  sx1=dx2m/vhp 
  sy1=dy2m/vhp 
  sz1=dz2m/vhp 
  srt1=SQRT(sx1*sx1+sy1*sy1)

  tx1= sy1/srt1 
  ty1=-sx1/srt1
  tz1=ZERO

  CALL RECEQ(datem,tx1,ty1,tz1, txeq,tyeq,tzeq)
  CALL REQMEQ(datem,txeq,tyeq,tzeq, txmeq,tymeq,tzmeq,dec,ra)
  CALL DOT(tx,ty,tz,txmeq,tymeq,tzmeq, error)
  CALL CROSS(sx,sy,sz, tx,ty,tz, rx,ry,rz, rmag)
  CALL VECTOR(datem,x1,x2,x3,x,x,x,sunx,suny,sunz, ex,ey,ez, cx,cy,cz, 4)
  suns=sx*sunx + sy*suny + sz*sunz 
  sunt=tx*sunx + ty*suny + tz*sunz 
  sunr=rx*sunx + ry*suny + rz*sunz 
  
  eas = sx*ex +sy*ey +sz*ez
  eat = tx*ex +ty*ey +tz*ez 
  ear = rx*ex +ry*ey +rz*ez 
  
  cas =sx*cx +sy*cy +sz*cz 
  cat =tx*cx +ty*cy +tz*cz 
  car =rx*cx +ry*cy +rz*cz
  
  CALL LatLng(sunt,sunr,suns, sdec,sra)
  CALL LatLng(eat,ear,eas, edec,era)
  CALL LatLng(cat,car,cas, cdec,cra)
  ets=sra+A180
  ete=era+A180 
  etc=cra+A180
  zap=A90-sdec 
  zae=A90-edec 
  zac=A90-cdec
  IF(ABS(zap1-zap)>ONE) WRITE(DBG,100)zap1,zap, zap1-zap
  IF(ABS(zae1-zae)>ONE) WRITE(DBG,200)zae1,zae, zae1-zae
  IF(ABS(zap1-zap)>ONE) WRITE(*,100)zap1,zap, zap1-zap
  IF(ABS(zae1-zae)>ONE) WRITE(*,200)zae1,zae, zae1-zae
  100 FORMAT(3E12.4," ERROR IN ZAP")
  200 FORMAT(3E12.4," ERROR IN ZAE")
RETURN
END Subroutine Plug1   ! -------------------------------------------------------

!+
SUBROUTINE PRECES(jd1,xe1,ye1,ze1, jd2,xe2,ye2,ze2)
! ------------------------------------------------------------------------------
! PURPOSE - Transform geocentric earth equatorial coordinates from epoch jd1 to
!   epoch jd2. 
! NOTE - Preces is called by Vector. Calls subroutine Euler2.
  REAL(WP),INTENT(IN):: jd1   ! Julian date of initial epoch
  REAL(WP),INTENT(IN):: xe1,ye1,ze1   ! vector in jd1 coordinate system
  REAL(WP),INTENT(IN):: jd2   ! Julian date of final epoch
  REAL(WP),INTENT(OUT):: xe2,ye2,ze2   ! vector in jd2 coordinate system

  REAL(WP):: czeta0
!!!  REAL(WP):: dphi,dpsi ! not really used
  REAL(WP):: t,t0
  REAL(WP):: temp
  REAL(WP):: theta0
!!!  REAL(WP):: wxp,wyp,wzp  ! not used
  REAL(WP):: zeta0
!-------------------------------------------------------------------------------
  t=ABS((jd2-jd1)/36524.219879)
  t0=(jd2-2415020.)/36524.219879 
  zeta0=(0.64006944+0.38777778E-3*t0)*t + 0.83888889e-4*t**2 + 0.5e-5*t**3
  czeta0=zeta0 + 0.21972222e-3*t**2 
  theta0=(0.55685611-0.2369444e-3*t0)*t - 0.11833333e-3*t**2-0.11666667e-4*t**3

  IF (jd2-jd1 < ZERO) THEN
    temp=zeta0
    zeta0=-czeta0
    czeta0=-temp
    theta0=-theta0
  END IF

!!! CALL Euler(xe1,ye1,ze1, xe2,ye2,ze2, 90.-zeta0, -(90.+czeta0),theta0,  &
!!!    dphi,dpsi,dpsi,wxp,wyp,wzp,1,0)
  WRITE(DBG,FMT2) 'Preces Euler angles', A90-zeta0, -(A90+czeta0), theta0
  CALL Euler2(A90-zeta0, -(A90+czeta0),theta0, xe1,ye1,ze1, xe2,ye2,ze2)
  WRITE(DBG,FMT1) 'Preces, Euler2 in:', xe1,ye1,ze1
  WRITE(DBG,FMT1) 'Preces, Euler2 out:', xe2,ye2,ze2
  RETURN
END Subroutine Preces   ! ------------------------------------------------------

!+
SUBROUTINE Receq(jd,xec,yec,zec,xeq,yeq,zeq)
! ------------------------------------------------------------------------------
! PURPOSE - Rotates a vector from geocentric, ecliptic, to
!       the geocentric, earth equatorial coordinate system
! NOTE - Receq is called by Plug, Plug1, and Vector.

  REAL(WP),INTENT(IN):: jd   !  Julian date
  REAL(WP),INTENT(IN):: xec,yec,zec ! components of the vector in the 
                                    ! geocentric, ecliptic coordinate system
  REAL(WP),INTENT(OUT):: xeq,yeq,zeq ! components of the vector in the geocentric, 
                                     ! earth equatorial, coordinate system

  REAL(WP):: c,s
  REAL(WP):: te  ! Julian centuries since 1900
  REAL(WP):: xie  ! mean obliquity of equator, degrees
!-------------------------------------------------------------------------------
  te=(jd-2415020.)/36525. 
  xie=23.452294-0.0130125*te-0.00000164*te**2+0.000000503*te**3 
  c=COS(xie*DEG2RAD)
  s=SIN(xie*DEG2RAD)
  xeq=xec
  yeq=yec*c-zec*s 
  zeq=yec*s+zec*c 
  RETURN
END Subroutine Receq   ! -------------------------------------------------------

!+
SUBROUTINE ReqMeq(jd, xeq,yeq,zeq, xmeq,ymeq,zmeq, decmeq,rameq)
! ------------------------------------------------------------------------------
! PURPOSE - Rotate a vector from the mean earth equator-equinox to the mean Mars
!   equator-equinox coordinate system. 
! NOTE this routine calls subrcutines ReqPeq and latlng.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: jd ! Julian date at time of interest
  REAL(WP),INTENT(IN):: xeq,yeq,zeq ! vector in the earth equatorial system
  REAL(WP),INTENT(OUT):: xmeq,ymeq,zmeq ! vector in the mars equatorial system
  REAL(WP),INTENT(OUT):: decmeq,rameq ! declination and right ascension of the 
                                      ! vector in the mars equatorial system

  REAL(WP):: alphao  ! right ascension of Mars axis of rotation
  REAL(WP):: gammao   ! declination
  REAL(WP):: omega  ! longitude of ascending node
  REAL(WP):: tau   ! fractional part of te*100
  REAL(WP):: te   ! Julian centuries since 1900
  REAL(WP):: tp   ! 100*te -tau -50
  REAL(WP):: xi   ! inclination
!-------------------------------------------------------------------------------
  te=(jd-2415020.)/36525. 
  tau=MOD(te*100.,ONE) 
  tp=te*100.0-tau-50.

  alphao=316.55 + 45.*0.006751 + .006751*tp -0.001013*tau 
  gammao=52.85 + 45.*.00348 + .00348*tp - 0.000631*tau 
  omega=48.78644167 + 0.77059167*te-0.13888889E-5*te**2 
  xi=1.850333333-0.675e-3*te + 0.12611111E-4*te**2

  CALL ReqPeq(jd,alphao,gammao,omega,xi,xeq,yeq,zeq,xmeq,ymeq,zmeq) ! last 3 are OUT
  CALL Latlng(xmeq,ymeq,zmeq,decmeq,rameq)

  RETURN 
END Subroutine ReqMeq   ! ------------------------------------------------------

!+
SUBROUTINE ReqPeq(jd, alphao,gammao,omega,xi,xeq,yeq,zeq,xpeq,ypeq,zpeq)
! ------------------------------------------------------------------------------
! PURPOSE - Rotate a vector from mean earth equator-equinox to planet 
!   equator-equinox coordinate system. 
! NOTE - ReqPeq is called by ReqMeq. Calls subroutine Euler.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: jd   ! Julian date at time of interest
  REAL(WP),INTENT(IN):: alphao,gammao! right ascension and declination of the planets
!       axis cf rotaticn expressed in the earth equatorial coordinate system

  REAL(WP),INTENT(IN):: omega,xi ! longitude of the ascending node and inclination of the
!       planets orbital plane referenced to the ecliptic and
!       vernal ecuinox

  REAL(WP),INTENT(IN):: xeq,yeq,zeq ! vector in the earth equatorial
                                     ! coordinate system

  REAL(WP),INTENT(OUT):: xpeq,ypeq,zpeq ! vector in the planet equatorial
                                        ! coordinate system

  REAL(WP):: dummy   ! used in call to Euler to return unneeded data
  REAL(WP),DIMENSION(3,3):: rpqw

  REAL(WP):: cal,sal
  REAL(WP):: cgm,sgm
  REAL(WP):: ce,se
  REAL(WP):: ci,si
  REAL(WP):: com,som
  REAL(WP):: cwp,swp
  REAL(WP):: cxp,sxp
  REAL(WP):: cyp,syp
  REAL(WP):: czp,szp
  REAL(WP):: e
  REAL(WP):: te
  REAL(WP):: wpp
  REAL(WP):: xp,yp,zp
!-------------------------------------------------------------------------------
  WRITE(DBG,*) 'Entering ReqPeq at date', jd
  WRITE(DBG,FMT1) 'ReqPeq, alphao,gammao,omega,xi',alphao,gammao,omega,xi
  WRITE(DBG,FMT1) 'ReqPeq, xeq,yeq,zeq', xeq,yeq,zeq
  te=(jd-2415020.)/36525.
  e=23.45225444 - 0.130125E-1*te - 0.16388889E-5*te**2 + 0.50277778E-6*te**3

  ce=COS(e*DEG2RAD)  
  se=SIN(e*DEG2RAD)
  cal=COS(alphao*DEG2RAD) 
  sal=SIN(alphao*DEG2RAD) 
  cgm=COS(gammao*DEG2RAD) 
  sgm=SIN(gammao*DEG2RAD) 
  com=COS(omega*DEG2RAD) 
  som=SIN(omega*DEG2RAD)

  czp=ce*som*cal - com*sal 
  szp=SQRT(ONE-czp*czp) 
  zp=RAD2DEG*ATAN2(szp,czp)
  sxp=se*cal/szp
  cxp=(-ce*com*cal-som*sal)/szp
  xp=RAD2DEG*ATAN2(sxp,cxp)
  syp=se*som/szp
  cyp=(ce*som*sal+com*cal)/szp
  yp=RAD2DEG*ATAN2(syp,cyp)
  ci=COS((xp-xi)*DEG2RAD)*SIN((yp-gammao)*DEG2RAD)+ &
     SIN((xp-xi)*DEG2RAD)*COS((yp-gammao)*DEG2RAD)*czp
  si=SQRT(one-ci*ci) 
  swp=szp*SIN((xp-xi)*DEG2RAD)/si
  cwp=(-COS((xp-xi)*DEG2RAD)*COS((yp-gammao)*DEG2RAD)+ &
        SIN((xp-xi)*DEG2RAD)*SIN((yp-gammao)*DEG2RAD)*czp)/si
  wpp=RAD2DEG*ATAN2(swp,cwp)  ! was wp
  WRITE(DBG,FMT1) 'ReqPeq, swp,cwp,wpp', swp,cwp,wpp
! 1,0 means xeq,yeq,zeq IN xpeq,ypeq,zpeq OUT
!!!  CALL EULER(xeq,yeq,zeq, xpeq,ypeq,zpeq, A90+alphao, wpp+A180, A90-gammao, &
!!!    dummy,dummy,dummy,dummy,dummy,dummy, 1,0)  

  WRITE(DBG,FMT2) 'Euler2 rotation angles',A90+alphao, wpp+A180, A90-gammao
  CALL Euler2(A90+alphao, wpp+A180, A90-gammao, xeq,yeq,zeq, xpeq,ypeq,zpeq)
  RETURN 
END Subroutine ReqPeq   ! ------------------------------------------------------

!+
SUBROUTINE REQVEQ(jd,xeq,yeq,zeq, xveq,yveq,zveq, decveq,raveq)
! ------------------------------------------------------------------------------
! PURPOSE - Rotate a vector from the mean earth equator-equinox to the mean
!   Venus equator-equinox ccordinate system. 
! NOTE - This routine calls subroutines reqpeq and latlng.

  REAL(WP),INTENT(IN):: jd  ! julian date at time of interest.
  REAL(WP),INTENT(IN):: xeq,yeq,zeq   ! vector in the earth equatorial system
  REAL(WP),INTENT(OUT):: xveq,yveq,zveq  ! vector in the venus equatorial system
  REAL(WP),INTENT(OUT):: decveq,raveq ! declination and right ascension of the
                                      ! vector in the venus equatorial system

  REAL(WP):: alphao  ! right ascension of Venus axis of rotation expressed in
                     ! Earth equatorial coordinate system
  REAL(WP):: gammao  ! right ascension of Venus axis of rotation expressed in
                     ! Earth equatorial coordinate system
  REAL(WP):: omega   ! longitude of the ascending node of the Venus orbital 
                     !   plane referenced to the ecliptic and vernal equinox.
  REAL(WP):: te  ! Julian centuries since 1900
  REAL(WP):: xi      ! inclination of planets orbital plane 
                     !    relative to the ecliptic
!-------------------------------------------------------------------------------

  te=(jd-2415020.0)/36525.0

  alphao=272.75
  gammao=71.50
  omega=75.779648 + 0.899850*te + 0.000411*te**2
  xi=3.393630 + 0.001005*te - 0.97E-6*te**2

  CALL ReqPeq(jd, alphao,gammao,omega,xi, xeq,yeq,zeq, xveq,yveq,zveq)
  CALL LatLng(xveq,yveq,zveq, decveq,raveq)

  RETURN
END Subroutine ReqVeq   ! ------------------------------------------------------


!+
SUBROUTINE RPQWXYZ(vp,vq,vw,xi,w,o,rxyz,vx,vy,vz)
! ------------------------------------------------------------------------------
! PURPOSE - Rotates a vector from the PQW to the XYZ coordinate system
! NOTE - Called by Occult. No calls.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: vp,vq,vw   ! vector in the PQW system
  REAL(WP),INTENT(IN):: xi   ! inclination, deg.
  REAL(WP),INTENT(IN):: w   ! argument of periapis, deg.
  REAL(WP),INTENT(IN):: o   ! longitude of ascending node, deg.
  REAL(WP),INTENT(OUT),DIMENSION(3,3):: rxyz ! rotational matrix from the PQW to
                                             ! the XYZ coordinate system
  REAL(WP),INTENT(OUT):: vx,vy,vz ! vector in XYZ system

  REAL(WP):: cw,sw,co,so,cxi,sxi
!-------------------------------------------------------------------------------
  cw=COS(w*DEG2RAD)
  sw=SIN(w*DEG2RAD)
  co=COS(o*DEG2RAD)
  so=SIN(o*DEG2RAD)
  cxi=COS(xi*DEG2RAD)
  sxi=SIN(xi*DEG2RAD)

  rxyz(1,1)=  cw*co-sw*so*cxi
  rxyz(1,2)= -sw*co-cw*so*cxi
  rxyz(1,3)=  so*sxi
  rxyz(2,1)=  cw*so+sw*co*cxi
  rxyz(2,2)= -sw*so+cw*co*cxi
  rxyz(2,3)= -co*sxi
  rxyz(3,1)=  sw*sxi
  rxyz(3,2)=  cw*sxi
  rxyz(3,3)=  cxi

  vx=rxyz(1,1)*vp + rxyz(1,2)*vq + rxyz(1,3)*vw 
  vy=rxyz(2,1)*vp + rxyz(2,2)*vq + rxyz(2,3)*vw 
  vz=rxyz(3,1)*vp + rxyz(3,2)*vq + rxyz(3,3)*vw

  RETURN
END Subroutine RPQWXYZ   ! -----------------------------------------------------

!+
SUBROUTINE RXYZPQW(vx,vy,vz, xi,w,o,rpqw, vp,vq,vw)
! ------------------------------------------------------------------------------
! PURPOSE - Rotate a vector frcm the XYZ to the PQW coordinate system.
! NOTE - Called by Occult. No calls.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: vx,vy,vz ! vector in the XYZ system
  REAL(WP),INTENT(IN):: xi ! inclination, deg.
  REAL(WP),INTENT(IN):: w  ! argument of periapis, deg.
  REAL(WP),INTENT(IN):: o  ! longitude of ascending node, deg.
  REAL(WP),INTENT(OUT),DIMENSION(3,3):: rpqw! rotational matrix from the XYZ to
                                             ! the PQW coordinate system
  REAL(WP),INTENT(OUT):: vp,vq,vw   ! vector in the PQW
  REAL(WP):: cw,sw, co,so, cxi,sxi
!-------------------------------------------------------------------------------
  cw=COS(w*DEG2RAD)
  sw=SIN(w*DEG2RAD)
  co=COS(o*DEG2RAD)
  so=SIN(o*DEG2RAD)
  cxi=COS(xi*DEG2RAD)
  sxi=SIN(xi*DEG2RAD)

  rpqw(1,1)=  cw*co-sw*so*cxi
  rpqw(1,2)=  cw*so+sw*co*cxi
  rpqw(1,3)=  sw*sxi
  rpqw(2,1)= -sw*co-cw*so*cxi
  rpqw(2,2)= -sw*so+cw*co*cxi
  rpqw(2,3)=  cw*sxi
  rpqw(3,1)=  so*sxi
  rpqw(3,2)= -co*sxi
  rpqw(3,3)=  cxi
  
  vp=rpqw(1,1)*vx + rpqw(1,2)*vy + rpqw(1,3)*vz 
  vq=rpqw(2,1)*vx + rpqw(2,2)*vy + rpqw(2,3)*vz 
  vw=rpqw(3,1)*vx + rpqw(3,2)*vy + rpqw(3,3)*vz
  
  RETURN
END Subroutine RXYZPQW   ! -----------------------------------------------------


!+
SUBROUTINE SEESE(datem,pd,ae,ee,xi,we,oe,u,rs,ex,ey,ez,is,ie,tsunin, &
  dursun,       tasin,tasout,tearin,durear,taein,taeout,delsom,delcom, days)
! ------------------------------------------------------------------------------
! PURPOSE - Determine whether occultations of Sun or Earth as seen by the
!  spacecraft ocur during the specified stay time in orbit. If so, compute the
!  occultation parameters.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: datem,pd,ae,ee,xi,we,oe,u,rs
  REAL(WP),INTENT(OUT):: ex,ey,ez
  REAL(WP),INTENT(OUT):: tsunin
  REAL(WP),INTENT(OUT)::   dursun,       tasin,tasout,tearin,durear,taein,taeout
  REAL(WP),INTENT(IN):: delsom,delcom
  REAL(WP),INTENT(IN):: days


  INTEGER,INTENT(OUT):: is,ie

  INTEGER:: istop,j
  INTEGER:: ke,ks
  REAL(WP):: alti,alt1,alt2   ! looks like trouble
  REAL(WP):: cx,cy,cz
  REAL(WP):: dec1,dec2, decc,dece,decs
  REAL(WP):: dure
  REAL(WP):: dursu
  REAL(WP):: ra1,ra2
  REAL(WP):: rac,rae,ras
  REAL(WP):: sx,sy,sz
  REAL(WP):: teari
  REAL(WP):: taei,taeou
  REAL(WP):: tasi,tasou
  REAL(WP):: time
  REAL(WP):: tsuni
  REAL(WP):: t2
!-------------------------------------------------------------------------------
  is=0
  ie=0
  tsunin=0.
  dursun=0. 
  tasin=0. 
  tasout=0. 
  tearin=0. 
  durear=0. 
  taein=0. 
  taeout=0. 
  istop=days/pd+1
  DO 20 J=1,ISTOP
    time=datem+pd*INT(j-1)
    CALL VECTOR(time,decs,ras,dece,rae,decc,rac,sx,sy,sz,ex,ey,ez,cx,cy,cz,4)
    CALL OCCULT(ae,ee,xi,we+pd*float(j-1)*delsom,oe+pd*float(j-1)*delcom, &
      u,rs,sx,sy,sz,dursu,tsuni,alti,tasi,dec1,ra1,t2,alt2,tasou,dec2,ra2,ks)
    CALL OCCULT(ae,ee,xi,we+pd*float(j-1)*delsom,oe+pd*float(j-1)*delcom, &
      u,rs,ex,ey,ez,dure,teari,alt1,taei,dec1,ra1,t2,alt2,taeou,dec2,ra2,ke)
    IF (IS.NE.0) GO TO 10
    IF (KS.EQ.1) GO TO 8
    GO TO 10

  8 IS=J
    tsunin=tsuni
    dursun=dursu
    tasin=tasi
    tasout=tasou
 10 IF(IE.NE.0)GO TO 15
    IF (KE==1)GO TO 11
    CYCLE   !GO TO 20

 11 IE=J
    tearin=teari
    durear=dure
    taein=taei
    taeout=taeou
 15 IF(ie.NE.0 .AND. IS.NE.0)RETURN
 20 END DO

  RETURN
END Subroutine SEESE   ! -------------------------------------------------------

!+
SUBROUTINE Tconic(u,ec,a,ta,t)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the time from periapsis passage for a given true amomaly.
! NOTE - Called by Deboost, Dbst1, Events, Occult. Calls nothing.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: u ! gravitational constant of planet
  REAL(WP),INTENT(IN):: ec ! eccentricity
  REAL(WP),INTENT(IN):: a  ! semi-major axis
  REAL(WP),INTENT(IN):: ta  ! true anomaly, degrees
  REAL(WP),INTENT(OUT):: t  ! time, seconds

  REAL(WP):: ab, abe
  REAL(WP):: ang
  REAL(WP):: fac
  REAL(WP):: ec1
  REAL(WP):: eca
  REAL(WP):: slr
  REAL(WP):: ta2  ! ta in radians
  REAL(WP):: the
!-------------------------------------------------------------------------------
  ta2=ta*DEG2RAD
  slr=a*(ONE-ec*ec)
  ab=ABS(a) 
  fac=ab*SQRT(ab/u)
  eca=(ONE-ec)/(ONE+ec)
  abe=SQRT(ABS(eca))
  the=TAN(HALF*ta2)
  IF (abe-0.5E-10) 11,11,12
  12 CONTINUE
  eca=TWO*ATAN(abe*the)
  IF (a) 14,11,13
  13 t=fac*(eca-ec*SIN(eca))
  GO TO 16

  14 ang=abe*the 
  ang=ONE+TWO*ang/(ONE-ang)
  t=fac*(ec*TAN(eca)-LOG(ang))
  GO TO 16

  11 fac=SQRT(slr**3/u)*TWO/((ONE+ec)**2)
  ec1=eca*the**2 

  t=fac*(the+the**3*((ONE-TWO*eca)/THREE - &
      (TWO-THREE*eca)*ec1/FIVE + &
      (THREE-FOUR*eca)*ec1**2/7 - &
      (FOUR-FIVE*eca)*ec1**3/9 ))
  16 CONTINUE
    RETURN
END Subroutine Tconic   ! ------------------------------------------------------

!+
SUBROUTINE Tinvs(m,e,ec,f)
! ------------------------------------------------------------------------------
! PURPOSE - Convert mean anomaly to eccentric and true anomaly
! NOTE - Given mean anomaly, solve for eccentric anomaly using Newton's method.
!   Then use eccentric anomaly to compute true anomaly.
!   M=E-e*SIN(E).  (elliptic)
!   If M is known, search for a root to g(x)=x-e*SIN(x)-m
!                                      g'(x)=1-e*COS(x)
!   Initial guess for x is m
!   Then iterate xnew-xold-g(xold)/g'(xold) until it converges
! For parabolic or hyperbolic, M=e*SINH(E)-E, g(x)=e*SINH(x)-x-m
!                                            g'(x)=e*COSH(x)-1
! NOTE - The angles are in radians, unlike all of the other procedures.
! NOTE - Called by Eearth, Emars
IMPLICIT NONE
  REAL(WP),INTENT(IN):: m  ! mean anomaly, radians
  REAL(WP),INTENT(IN):: e  ! eccentricity
  REAL(WP),INTENT(OUT):: ec  ! eccentric anomaly, radians
  REAL(WP),INTENT(OUT):: f  ! true anomaly, radians

  REAL(WP):: de
  REAL(WP):: dm
  REAL(WP):: hec
  REAL(WP):: hf
  REAL(WP):: mo


!-------------------------------------------------------------------------------
  IF (e <= ONE) THEN  ! elliptic orbit
    ec=m
10 mo=ec-e*SIN(ec)
  dm=m-mo
  de=dm/(ONE-e*COS(ec))
  ec=ec+de
  IF (ABS(de)>1.E-12) GO TO 10
  
  hec=HALF*ec
  hf=ATAN(SQRT((ONE+e)/(ONE-e))*SIN(hec)/cos(hec))
  IF (hf<ZERO) hf=hf+PI
  f=TWO*hf

  ELSE   ! hyperbolic orbit


    ec=ASINH(m/e)
  101 mo=e*SINH(ec)-ec
  dm=m-mo 
  de=dm/(e*COSH(ec)-ONE)
  ec=ec+de
  IF (ABS(de)>1.0E-12)GO TO 101

  f=TWO*ATAN(SQRT((e+ONE)/(e-ONE))*TANH(HALF*ec))
  END IF
  RETURN
END Subroutine Tinvs   ! ------------------------------------------------------

!+
SUBROUTINE Vector(jd, decs,ras, dece,rae, decc,rac, sx,sy,sz, ex,ey,ez, cx,cy,cz,ibody)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the position of the sun, earth, and Canopus in planet 
!  equator, mean planet equinox of date and writes data. 
! NOTE - Calls Eearth, Emars, [Evenus], Preces, Latlng, Dot, 
!   Receq, Regveop and reqmeq.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: jd ! Julian date at time of interest
  REAL(WP),INTENT(OUT):: decs,ras  ! declination and right ascension of the sun.
  REAL(WP),INTENT(OUT):: dece,rae  ! declination and right asctnsion of the earth.
  REAL(WP),INTENT(OUT):: decc,rac  ! declination and right ascension of Canopus.
  REAL(WP),INTENT(OUT):: sx,sy,sz  ! unit vector from the planet to the sun.
  REAL(WP),INTENT(OUT):: ex,ey,ez  ! unit vector from the planet to the earth.
  REAL(WP),INTENT(OUT):: cx,cy,cz  ! unit vector from the planet to Canopus.
  INTEGER,INTENT(IN):: ibody   ! =2 for Venus, =4 for Mars

  REAL(WP),PARAMETER:: A1=-0.060340592_WP, A2=0.60342839_WP, A3=-0.79513092_WP

  REAL(WP):: cxe,cye,cze
  REAL(WP):: dxhe,dyhe,dzhe
  REAL(WP):: dxhp,dyhp,dzhp
  REAL(WP):: ehlong,ehlat
  REAL(WP):: esp
  REAL(WP):: exe,eye,eze
  REAL(WP):: pex,pey,pez
  REAL(WP):: phlong,phlat
  REAL(WP):: rse,rsp,rpe
  REAL(WP):: sep, spe
  REAL(WP):: sex,sey,sez
  REAL(WP):: spx,spy,spz
  REAL(WP):: sxe,sye,sze
  REAL(WP):: xhe,yhe,zhe
  REAL(WP):: xhp,yhp,zhp
  REAL(WP):: xhpe,yhpe,zhpe
  REAL(WP):: xpe,ype,zpe
  REAL(WP):: xps,yps,zps
!-------------------------------------------------------------------------------
  CALL EEARTH(jd, xhe,yhe,zhe, dxhe,dyhe,dzhe)
  CALL PRECES(2433282._WP, A1,A2,A3 ,jd,cxe,cye,cze)
  
  IF (ibody==4) GO TO 2
  2 CALL EMARS(jd,xhp,yhp,zhp, dxhp,dyhp,dzhp)

  xhpe=xhe-xhp 
  yhpe=yhe-yhp 
  zhpe=zhe-zhp

  rse=SQRT(xhe**2  +yhe**2  + zhe**2) 
  rsp=SQRT(xhp**2  +yhp**2  + zhp**2) 
  rpe=SQRT(xhpe**2 +yhpe**2 + zhpe**2)

  sex=xhe/rse 
  sey=yhe/rse 
  sez=zhe/rse 

  spx=xhp/rsp 
  spy=yhp/rsp 
  spz=zhp/rsp 

  pex=xhpe/rpe 
  pey=yhpe/rpe 
  pez=zhpe/rpe 

  CALL LatLng(sex,sey,sez, ehlat,ehlong)
  CALL LatLng(spx,spy,spz, phlat,phlong)
  CALL Dot(sex,sey,sez, spx,spy,spz, esp)
  CALL Dot(sex,sey,sez, pex,pey,pez, sep)
  CALL Dot(spx,spy,spz, -pex,-pey,-pez,spe)
  CALL Receq(jd, -spx,-spy,-spz, sxe,sye,sze)
  CALL Receq(jd, pex,pey,pez, exe,eye,eze)
  
  IF (ibody==4) go to 5
5 CALL Reqmeq(jd, sxe,sye,sze, sx,sy,sz, decs,ras) 
  CALL Reqmeq(jd, exe,eye,eze, ex,ey,ez, dece,rae) 
  CALL Reqmeq(jd, cxe,cye,cze, cx,cy,cz, decc,rac)
  
  xps=sx*rsp 
  yps=sy*rsp 
  zps=sz*rsp 
  xpe=ex*rpe 
  ype=ey*rpe 
  zpe=ez*rpe
  
  RETURN
END Subroutine Vector   ! ------------------------------------------------------


! REFERENCES
! 1. Escobal, Pedro Ramon: Methods of Orbit Determination. 
!    John Wiley & Sons, Inc., 1965.     TL 1050.E8
! 2. Green, Richard N.: Investigation of Occultation and Imagery Problems 
!    for Orbital Missions to Venus and Mars. NASA TN D-5104, 1969.

END Module InterplanetaryProcedures   ! ===========================================
