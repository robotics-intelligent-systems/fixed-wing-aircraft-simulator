!+
MODULE InterplanetaryMathProcedures
! ------------------------------------------------------------------------------
! PURPOSE - Collect all of the mathematical procedures used in the
!  interplanetary mission planing program.

IMPLICIT NONE
!... Set the working precision...
  INTEGER,PARAMETER:: WP = SELECTED_REAL_KIND(12)  ! 12 decimals
!... Mathematical constants...
  REAL(WP),PARAMETER:: ZERO=0.0_WP, ONE=1.0_WP, TWO=2.0_WP
  REAL(WP),PARAMETER:: THREE=3.0_WP, FOUR=4.0_WP, FIVE=5.0_WP, SIX=6.0_WP
  REAL(WP),PARAMETER:: A360=360.0_WP, A180=180.0_WP, A90=90.0_WP
  REAL(WP),PARAMETER:: HALF=ONE/TWO, THIRD=ONE/THREE
  REAL(WP),PARAMETER:: PI = 3.141592653589793238462643383279502884197_WP
  REAL(WP),PARAMETER:: HALFPI=PI/TWO, TWOPI=TWO*PI

  REAL(WP),PARAMETER:: DEG2RAD=PI/180
  REAL(WP),PARAMETER:: RAD2DEG=180/PI
!-------------------------------------------------------------------------------

CONTAINS

!+
PURE FUNCTION Angle(x) RESULT(a)
! ------------------------------------------------------------------------------
! PURPOSE - Return an angle that is equivalent to x, but in [0,360).
  REAL(WP),INTENT(IN):: x   ! input angle in degrees
  REAL(WP):: a              ! output angle in degrees
!-------------------------------------------------------------------------------
  a=MOD(X,A360) + A180 - SIGN(A180,x)
! modern way to do this is a=MODULO(x,A360)
  RETURN
END Function Angle   ! ---------------------------------------------------------

!+
PURE FUNCTION Angle2(x) RESULT(a)
! ------------------------------------------------------------------------------
! PURPOSE - Return an angle that is equivalent to x, but in [0,TWOPI).
  REAL(WP),INTENT(IN):: x   ! input angle in radians
  REAL(WP):: a              ! output angle in radians
!-------------------------------------------------------------------------------
  a=MOD(X,TWOPI)+PI-SIGN(PI,x)
! modern way to do this is a=MODULO(x,TWOPI)
  RETURN
END Function Angle2   ! --------------------------------------------------------

!+
PURE ELEMENTAL FUNCTION ASINH(x) RESULT(a)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the inverse hyperbolic sine
  REAL(WP),INTENT(IN):: x
  REAL(WP):: a
!-------------------------------------------------------------------------------
  a=SIGN(LOG(ABS(x)+SQRT(x*x+ONE)), x)
  RETURN
END Function ASINH   ! ---------------------------------------------------------

!+
PURE ELEMENTAL FUNCTION ATANH(x) RESULT(a)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the inverse hyperbolic tangent
IMPLICIT NONE
  REAL(WP),INTENT(IN):: x
  REAL(WP):: a
  INTRINSIC:: LOG
!-------------------------------------------------------------------------------
  IF (ABS(x) < ONE) THEN
    a = HALF*LOG((ONE+x)/(ONE-x))
  ELSE
    a=SIGN(HUGE(x), x)
  END IF
  RETURN
END Function ATANH   ! ---------------------------------------------------------

!+
SUBROUTINE CROSS(x1,y1,z1, x2,y2,z2, px,py,pz, crossProduct)
! ------------------------------------------------------------------------------
! PURPOSE - Calculate the vector cross product
IMPLICIT NONE
  REAL(WP), INTENT(IN):: x1,y1,z1, x2,y2,z2
  REAL(WP),INTENT(OUT):: px,py,pz, crossProduct
!-------------------------------------------------------------------------------
  px=y1*z2-z1*y2
  py=z1*x2-x1*z2
  pz=x1*y2-y1*x2 
  crossProduct=SQRT(px*px + py*py + pz*pz)
  RETURN
END Subroutine Cross   ! -------------------------------------------------------

!+
PURE ELEMENTAL FUNCTION CubeRoot(x) RESULT(root)
! ------------------------------------------------------------------------------
! PURPOSE - Get the cube root of a real. Negative arg => negative result
  REAL(WP),INTENT(IN):: x
  REAL(WP):: root
!-------------------------------------------------------------------------------
  root=SIGN(ABS(x)**THIRD, x) 
  RETURN
END Function CubeRoot   ! ------------------------------------------------------

!+
SUBROUTINE Cubic(a,b,c,d,x1,x2,x3,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Solves the equation ax**3 + bx**2 + cx + d = 0 for the real roots.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: a,b,c,d   ! coefficients
  REAL(WP),INTENT(OUT):: x1,x2,x3  ! the real roots
  INTEGER,INTENT(OUT):: kk   ! number of real roots

  REAL(WP),PARAMETER:: SMALL = 1.0E-29_WP
  REAL(WP):: beta
  REAL(WP):: cphi,sphi
  REAL(WP):: del
  REAL(WP):: dis
  REAL(WP):: eo
  REAL(WP):: phi
  REAL(WP):: p,q,r
  REAL(WP):: sa,sb
!-------------------------------------------------------------------------------
  kk=0
  IF (ABS(a)<SMALL) GO TO 4   ! call a zero and solve as quadratic

  p=b/a
  q=c/a
  r=d/a
  sa=(3*q-p**2)/3
  sb=(2*P**3 - 9*P*Q + 27*R)/27
  del=(4*Q**3 - q**2*p**2 -18*q*p*r + 27*r**2 + 4*p**3*r)/108
  IF (ABS(del)<SMALL) GO TO 3
  IF (del) 1,3,2
1 kk=3
  cphi=-sb/2/SQRT(sa**3/(-27)) 
  IF (ABS(cphi)>ONE) GO TO 10 
  sphi=SQRT(ONE-cphi**2)
  phi=ATAN2(sphi,cphi)
  GO TO 11

10 sphi=SQRT((27*del)/sa**3)
!       since,  for small angles spmi=phi
  beta=sphi
  IF (-sb>ZERO) phi=beta
  IF (-sb<ZERO) phi=PI-beta
11 eo=TWO*SQRT(-sa/3)
  x1=eo*COS(phi/THREE)-p/THREE
  x2=eo*COS(phi/THREE+TWOPI/THREE)-p/THREE 
  x3=eo*COS(phi/THREE+TWO*TWOPI/THREE)-p/THREE
  GO TO 7

2 kk=1
  x1=CubeRoot(-HALF*sb + SQRT(del)) + CubeRoot(-HALF*sb-SQRT(del))-p/THREE
  GO TO 7

3 kk=3
  x1=TWO*CubeRoot(-HALF*sb)-p/THREE
  x2=CubeRoot(HALF*sb)-p/THREE
  x3=x2
  GO TO 7

4 CONTINUE
  dis=c**2-FOUR*b*d
  IF (dis >= ZERO) THEN
    x1=(-c + SQRT(dis))/(TWO*b) 
    x2=(-c - SQRT(dis))/(TWO*b)
    kk=2
  END IF

7 CONTINUE
  RETURN
END Subroutine Cubic   ! -------------------------------------------------------

!+
SUBROUTINE Deter(A,B)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the determinant of a 3 by 3 matrix.
IMPLICIT NONE
  REAL(WP),INTENT(OUT):: a
  REAL(WP),INTENT(IN),DIMENSION(3,3):: b
!-------------------------------------------------------------------------------
  a = b(1,1)*b(2,2)*b(3,3) - b(3,1)*b(2,2)*b(1,3) + b(1,2)*b(2,3)*b(3,1) - &
      b(1,2)*b(2,1)*b(3,3) + b(1,3)*b(2,1)*b(3,2) - b(1,1)*b(2,3)*b(3,2)
  RETURN
END Subroutine Deter   ! -------------------------------------------------------

!+
SUBROUTINE Dot(x1,y1,z1,x2,y2,z2,vectorAngle)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the angle between two vectors
IMPLICIT NONE
  REAL(WP),INTENT(IN):: x1,y1,z1   ! components of the vector r1
  REAL(WP),INTENT(IN):: x2,y2,z2   ! components of the vector r2
  REAL(WP),INTENT(OUT):: vectorAngle   ! angle between vectors r1 and r2 (deg)

  REAL(WP):: r1,r2
!-------------------------------------------------------------------------------
  r1=SQRT(x1*x1+y1*y1+z1*z1) 
  r2=SQRT(x2*x2+y2*y2+z2*z2)
  vectorAngle=ACOS((x1*x2+y1*y2+z1*z2)/(r1*r2))*RAD2DEG
  RETURN
END Subroutine Dot   ! ---------------------------------------------------------

!+
PURE FUNCTION EvaluatePolynomial(x,a) RESULT(f)
! ------------------------------------------------------------------------------
! PURPOSE -Evaluate a polynomial with coeff a(1),a(2),... at x.
! NOTES - f = a(1) + a(2)*x + a(3)*x**2 + .... + a(n)*x**(n-1)
!   Straightforward implementation of Horner's rule
  REAL(WP),INTENT(IN):: x   ! evaluation point
  REAL(WP),INTENT(IN),DIMENSION(:):: a   ! coefficients
  REAL(WP):: f   ! value of polynomial at x
  INTEGER:: k,n
!-------------------------------------------------------------------------------
  n=SIZE(a)   ! order of polynomial is n-1, not n
  IF (n <= 0) THEN
    f=0.0
    RETURN
  END IF

  f=a(n)
  DO k=n-1,1,-1
    f=a(k)+x*f
  END DO

  RETURN
END FUNCTION EvaluatePolynomial   ! --------------------------------------------


!+
SUBROUTINE QADRAT(a,b,c, x1,x2,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Solve equation a*x**2 + b*x + c = 0
! NOTES - See Numerical Recipes for a better solution.
IMPLICIT NONE
  REAL(WP),INTENT(IN):: a,b,c
  REAL(WP),INTENT(OUT):: x1,x2
  INTEGER,INTENT(OUT):: kk ! number of real roots

  REAL:: dis
!-------------------------------------------------------------------------------
  kk=0
  dis=b*b-FOUR*a*c
  IF (dis >= 0.) THEN
    x1=(-b+SQRT(dis))/TWO/a 
    x2=(-b-SQRT(dis))/TWO/a 
    kk=2
  END IF
  RETURN
END Subroutine Qadrat   ! ------------------------------------------------------

SUBROUTINE QARTIC(a,b,c,d,e, x1,x2,x3,x4,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Solve the equation ax**4 +bx**3 +cx**2 +dx +e = 0 
! for the real roots. this routine calls subroutines qadrat and conic 
  REAL(WP),INTENT(IN):: a,b,c,d,e  ! coefficients
  REAL(WP),INTENT(OUT):: x1,x2,x3,x4   ! real roots
  INTEGER,INTENT(OUT):: kk   ! number of real roots

  REAL(WP):: bp,cp,dp,ep
  REAL(WP):: h,h2,h3,h4
  INTEGER:: iroot, jroot, nroot
  REAL(WP):: p,q,r
  REAL(WP):: rp, sqrp
  REAL(WP):: t1,t2,t3
  REAL(WP):: xi,beta
  REAL(WP):: y1,y2,y3,y4
!-------------------------------------------------------------------------------
  kk=0
  bp=b/a 
  cp=c/a 
  dp=d/a 
  ep=e/a

  h=-bp/FOUR
  h2=h**2 
  h3=h2*h 
  h4=h3*h 
  p=SIX*h2 + THREE*bp*h + cp
  q=FOUR*h3 +THREE*bp*h2 + TWO*cp*h + dp
  r=h4 + bp*h3 + cp*h2 + dp*h + ep

  CALL CUBIC(ONE, TWO*p, p*p-FOUR*r, -q*q, t1,t2,t3, nroot)

  GO TO (1,2,3),nroot
1 rp=t1
  go to 4
2 rp=MAX(t1,t2)
  go to 4
3 rp=MAX(t1,t2,t3)

4 CONTINUE
  sqrp=SQRT(rp)
  xi=(p+rp-q/sqrp)/TWO
  beta=(p+rp+q/sqrp)/TWO

  CALL Qadrat(ONE, sqrp,xi,y1,y2,iroot)
  CALL Qadrat(ONE,-sqrp,beta,y3,y4,jroot)
  IF (iroot+jroot==0) go to 800
  IF (iroot+jroot==4) go to 6
  IF (iroot==0) GO TO 5
  x1=y1+h 
  x2=y2+h
  kk=2
  go to 800

5 x1=y3+h 
  x2=y4+h
  kk=2
  go to 800 

6 x1=y1+h 
  x2=y2+h 
  x3=y3+h 
  x4=y4+h 
  kk=4

800 CONTINUE
  RETURN
END Subroutine Qartic   ! ------------------------------------------------------

!+
PURE ELEMENTAL FUNCTION Xcos(x) RESULT(a)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the cosine of x with x in degrees
  REAL(WP),INTENT(IN):: x
  REAL(WP):: a
!-------------------------------------------------------------------------------
  a=COS(DEG2RAD*x)
  RETURN
END Function Xcos   ! ----------------------------------------------------------

!+
PURE ELEMENTAL FUNCTION Xsin(x) RESULT(a)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the sine of x with x in degrees
  REAL(WP),INTENT(IN):: x
  REAL(WP):: a
!-------------------------------------------------------------------------------
  a=SIN(DEG2RAD*x)
  RETURN
END Function Xsin   ! ----------------------------------------------------------

!+
PURE ELEMENTAL FUNCTION Xtan(x) RESULT(a)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the tangent of x with x in degrees
  REAL(WP),INTENT(IN):: x
  REAL(WP):: a
!-------------------------------------------------------------------------------
  a=TAN(DEG2RAD*x)
  RETURN
END Function Xtan   ! ----------------------------------------------------------
  
END Module InterplanetaryMathProcedures   ! =======================================

!+
MODULE CalendarProcedures
! ------------------------------------------------------------------------------
! PURPOSE - Collect the procedures for date and time calculation, conversion
!  from Julian to calendar date formats, etc.
USE InterplanetaryMathProcedures
IMPLICIT NONE

  REAL(WP),PARAMETER:: D50=2433282.0_WP   ! January 1,1950

  PUBLIC:: CalJul
  PUBLIC:: GetDateTimeStr
  PUBLIC:: JulCal
  PUBLIC:: JulianToCalendar
!-------------------------------------------------------------------------------

CONTAINS

!+
SUBROUTINE CALJUL(WJD,FJD,WND,FD,X)
! ------------------------------------------------------------------------------
! PURPOSE - Convert a date in calendar format (year,month,day,hour,minute,second)
!  to a Julian date. Year is from 1900
  REAL(WP),INTENT(OUT):: wjd,fjd,wnd,fd
  REAL(WP),INTENT(IN),DIMENSION(6):: x

  REAL(WP),DIMENSION(12):: a
  REAL(WP):: ck
  REAL(WP):: ds

  INTEGER:: k,kyl
  REAL(WP):: yd,yl
!-------------------------------------------------------------------------------

  yd=x(1)-48._WP
  yl=yd/FOUR
  kyl=INT(yl)
  ck=kyl
  IF(yl-ck)1,1,3   ! year a multiple of 4
1 IF(x(2)-TWO) 4,4,3
3 ds=ck
  GO TO 5
4 ds=ck-ONE
5 ds=ds+365.*(yd-TWO)
  a(1:12)=ONE
  k=INT(x(2))   ! month
  a(k:12)=ZERO
  ds=ds+31._WP*(a(1)+a(3)+a(5)+a(7)+a(8)+a(10)+a(12)) + &
        30._WP*(a(4)+a(6)+a(9)+a(11)) + 28._WP*a(2) 
  ds=ds+x(3)-ONE
  wnd=ds
  fd=x(4)/24._WP + x(5)/1440._WP + x(6)/86400._WP
  IF(FD-.49999999_WP) 9,8,8
8 fjd=fd-HALF
  wjd=ONE
  GO TO 10

9 fjd=fd+HALF
  wjd=ZERO
 10 wjd=D50+wjd+wnd 
  RETURN
END Subroutine CALJUL   ! ------------------------------------------------------

!+
FUNCTION GetDateTimeStr() RESULT(s)
! ------------------------------------------------------------------------------
! PURPOSE - Return a string with the current date and time
!   You can change the first I2.2 below to I2 if you don't like a leading
!   zero on early morning times, i.e., 6:45 instead of 06:45
  IMPLICIT NONE
  CHARACTER(LEN=*),PARAMETER:: MONTH='JanFebMarAprMayJunJulAugSepOctNovDec'
  CHARACTER(LEN=*),PARAMETER:: FMT = '(I2.2,A1,I2.2,I3,A3,I4)'
  CHARACTER(LEN=15):: s
  INTEGER,DIMENSION(8):: v
!-------------------------------------------------------------------------------
  CALL DATE_AND_TIME(VALUES=v)

  WRITE(s,FMT) v(5), ':', v(6), v(3), MONTH(3*v(2)-2:3*v(2)), v(1)
  RETURN
END FUNCTION GetDateTimeStr   ! ------------------------------------------------

!+
SUBROUTINE JULCAL(X,WDI,FDI,IND)
! ------------------------------------------------------------------------------
! PURPOSE - Converts a given julian date or the number of whole and fractional 
!   days since january 1, 1950, 0 hrs., to the corresponding calendar date.
IMPLICIT NONE
  REAL(WP),INTENT(OUT),DIMENSION(6):: x   ! calendar date 
                                          ! (year,month,day,hour,minute,second )
  REAL(WP),INTENT(IN):: wdi   ! integral part of julian date or whole number of 
                              ! days since January 1, 1950, 0 hrs.
  REAL(WP),INTENT(IN):: fdi   ! fractional part of julian date or fractional 
                              ! number of days since January 1, 1950, 0 hrs.
  INTEGER,INTENT(IN):: ind   !  control integer. 0 implics julian date,1 inplies days

!
 
  REAL(WP),DIMENSION(12):: a,w
  REAL(WP):: c1,c2
  REAL(WP):: ca
  REAL(WP):: ck
  REAL(WP):: dy
  REAL(WP):: fc
  REAL(WP):: fd
  REAL(WP):: fh
  REAL(WP):: fm
  INTEGER:: mon
  INTEGER:: i,n
  REAL(WP):: q
  REAL(WP):: wd
  REAL(WP):: z

!-------------------------------------------------------------------------------
  wd=wdi
  fd=fdi

  IF (ind) 1,1,5
1 IF(fd-HALF)2,2,3
2 fd=fd+HALF
  wd=wd-ONE
  GO TO 4

3 fd=FD-HALF
4 wd=wd-D50
5 wd=wd+ONE


  dy=365._WP 
  z=TWO
  n=0
  q=FOUR

6 wd=wd-dy
  IF(wd)10,10,7
7 n=n+1 
  z=z+ONE
  ck=q-z 
  IF (ck) 9,9,8
8 dy=365._WP
  fc=28. 
  go to 6

9 dy=366._WP 
  q=q+FOUR
  fc=29. 
  GO TO 6

10 wd=wd+dy
  a(1:12)=ZERO
  c1=31._WP
  c2=30._WP 
  DO 13 i=1,12
    a(i)=ONE
    ca=fc*a(2)+c1*(a(1)+a(3)+a(5)+a(7)+a(8)+a(11)+a(12))+ &
               c2*(a(4)+a(6)+a(9)+a(11))
    w(i)=wd-ca
    IF (w(i)) 12,12,13
12  IF (i-1) 15,15,16
15  mon=1
    GO TO 14
16  mon=i-1
    wd=w(mon) 
    mon=mon+1
    GO TO 14

13 CONTINUE   ! end of loop

14 n=n+50 
  x(1)=n 
  x(2)=mon
  x(3)=wd 
  fh=fd*24._WP
  n=fh
  x(4)=n 
  fm=(fh-x(4))*60._WP
  n=fm
  x(5)=n 
  x(6)=(fm-x(5))*60._WP
  RETURN 
END Subroutine JulCal   ! ------------------------------------------------------

!+
FUNCTION JulianToCalendar(a) RESULT(b)
! ------------------------------------------------------------------------------
! PURPOSE - Given a Julian date, produce a character string (length=19) of the 
!  Gregorian calendar date.
  REAL(WP),INTENT(IN):: a
  
  CHARACTER(LEN=19):: b,buffer
  REAL(WP):: whole,fractional
  REAL(WP),DIMENSION(6)::e
  CHARACTER(LEN=*),PARAMETER:: FMT = '(I2.2,":",I2.2,":",I2.2,I3,A3,I4)'
  INTEGER,DIMENSION(6):: v
  CHARACTER(LEN=36),PARAMETER:: MTAB = "JanFebMarAprMayJunJulAugSepOctNovDec"
!-------------------------------------------------------------------------------
  whole=AINT(a)
  fractional=a-whole
  IF (fractional==HALF) fractional=HALF + 1E-5_WP
  CALL JULCAL(e,whole,fractional,0)
  v=INT(e)
  WRITE(buffer,FMT) v(4),v(5),v(6),v(3),MTAB(3*v(2)-2:3*v(2)), 1900+v(1)
  b=buffer
  RETURN
END FUNCTION JulianToCalendar   ! ----------------------------------------------

END Module CalendarProcedures   ! ==============================================
