

INTERPLANETARY MISSION PLANNER                                /tomars/readme.txt

The files for this program are in the directory tomars 
  readme.txt    this file of general description
  code.zip      the source code
  cases.zip     some test cases
  tnd5104.pdf   NASA TN D-5104 by Richard Green
  tnd5985.pdf   NASA TN D-5985 by James Kibler
     
This program is a "work in progress" and is not ready for general release.
I have included it so those who have a special interest may see
the original code plus my modifications to date. This code compiles and runs,
although I show a number of discrepancies between my output and that
of the documents.
 
