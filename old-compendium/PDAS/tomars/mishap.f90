INCLUDE 'space.f90'
!+
MODULE MainEarthToMarsProcedures
! ------------------------------------------------------------------------------
! PURPOSE - 
USE InterplanetaryProcedures
IMPLICIT NONE

CONTAINS

!+
SUBROUTINE Main(inputFile,outputFile,debugFile)
! ------------------------------------------------------------------------------
! PURPOSE - 
  INTEGER,INTENT(IN):: inputFile,outputFile,debugFile

  REAL(WP),DIMENSION(6):: e   ! first calendar date of launch period.
                              ! e(1)=calendar year (2 digits)
                              ! e(2)=calendar month
                              ! e(3)=calendar day 
                              ! e(4)=hours; e(5)=minutes; e(6)=seconds.
  REAL(WP),DIMENSION(6):: xm  ! first calencar date of arrival period.
                              ! xm(1..6) same as for launch.

  REAL(WP),DIMENSION(6):: e1,xm1
  REAL(WP),DIMENSION(6):: dbst,deor,xland

  REAL(WP):: ae
  REAL(WP):: afjd,awjd, awdi,afdi
  REAL(WP):: ang1,ang2,ang3,ang4
  REAL(WP):: az
  REAL(WP):: b
  REAL(WP):: br,bt, bdr,bdt
  REAL(WP):: c3  ! vis-viva injection energy, km**2/s**2
  REAL(WP):: c3max  ! constraint on vis-viva injection energy c3
  REAL(WP):: capw
  REAL(WP):: cbw
  REAL(WP):: datee,datem
  REAL(WP):: day1,day2  ! initial, final values of days
  REAL(WP):: days  ! stay time prior to deorbit, days
  REAL(WP):: dbrad
  REAL(WP):: dbf,dbw
  REAL(WP):: dcom
  REAL(WP):: decep,dechp
  REAL(WP):: delcom,delsom
  REAL(WP):: deltv
  REAL(WP):: delvmax
  REAL(WP):: dla  ! geocentric declination of the launch asymptote, degrees
  REAL(WP):: dlamax  ! constraint on dla
  REAL(WP):: dm
  REAL(WP):: dof,dow
  REAL(WP):: dpa
  REAL(WP):: durear,dursun
  REAL(WP):: ee
  REAL(WP):: ewdi,efdi
  REAL(WP):: efjd,ewjd
  REAL(WP):: eh
  REAL(WP):: elonp
  REAL(WP):: elp  ! declination of impact, degrees
  REAL(WP):: elp1,elp2  ! initial, final values of elp
  REAL(WP):: eprad
  INTEGER:: errCode   ! added by RLC
  REAL(WP):: etc,ete,ets
  REAL(WP):: ex,ey,ez
  REAL(WP):: fd
  REAL(WP):: fe,fh
  REAL(WP):: fpa  ! flight path angle at entry, degrees
  REAL(WP):: gee  ! sun lighting angle, degrees
  REAL(WP):: gee1,gee2  ! initial, final values of gee
  REAL(WP):: gra,gda
  REAL(WP):: ha  ! height of apoapsis, km
  REAL(WP):: helang
  REAL(WP):: hp  ! height of periapsis, km
  REAL(WP):: hprad
  INTEGER:: i,j
  INTEGER:: i1,i2,i3,i4,i5
  INTEGER:: iad  ! arrival period size
  INTEGER:: ild  ! launch period size
  INTEGER:: ie,is
  INTEGER:: ijd  ! launch date increment
  INTEGER:: jjd   ! arrival date increment
  INTEGER:: inc  ! =1 for posigrade hyperbola; =2 for retrograde
  INTEGER:: isearch  ! parametric-analysis control integer
                     ! =1 for parametric analysis
                     ! =0 for standard run
  INTEGER:: jday
  INTEGER:: jelp
  INTEGER:: jgee
  INTEGER:: jper
  INTEGER:: jxi
  INTEGER:: kday = 1
  INTEGER:: kelp = 1
  INTEGER:: key1  ! =0 for minimal output 
                  ! =1 for extended output
                  ! =2 for parametric analysis output.
  INTEGER:: key2
  INTEGER:: key3  ! landing point control integer
                  ! =1 descending node, p.m. lighting
                  ! =2 ascending node, p.m. lighting
                  ! =3 descending node, a.m. lighting
                  ! =4 ascending node, a.m. lighting

  INTEGER:: key4  ! time-correction mode control integer
                  ! =1 for time correction
                  ! =0 for no correction
  INTEGER:: key5  ! broken-plane input control integer
                  ! =1 for broken plane input
                  ! =0 for standard input.
  INTEGER:: kgee = 1
  INTEGER:: kk
  INTEGER:: kper = 1  ! incremental value of per
  INTEGER:: kxi = 1
  INTEGER:: m
  REAL(WP):: oe
  REAL(WP):: oz
!!!  REAL(WP):: pa
  REAL(WP):: pd
  REAL(WP):: per  ! periapsis to landing point angle, degrees
  REAL(WP):: per1,per2  ! initial, final values of per, degrees
  REAL(WP):: plane
  REAL(WP):: probinc,probap,probper
  REAL(WP):: prodr,prodt, proder,prodet
  REAL(WP):: raep, rahp
  REAL(WP):: ral
  REAL(WP):: rap
  REAL(WP):: rentry  ! radius at entry, km.
  REAL(WP):: rex,rey,rez
  REAL(WP):: rm
  REAL(WP):: rpmin  ! minimum radius of periapsis of hyperbola, km
  REAL(WP):: rs  ! radius of mars, km.
  REAL(WP):: rx,ry,rz
  REAL(WP):: speclon  ! specified longitude of impact, degrees
  REAL(WP):: taa,tal
  REAL(WP):: tadeorb  ! true anomaly of deorbit, degrees
  REAL(WP):: taein,taeout
  REAL(WP):: tasin,tasout
  REAL(WP):: tdest  ! estimated time from deorbit to landing, days
                    ! =0 yields computed time from deorbit to landing
  REAL(WP):: tearin
  REAL(WP):: timdbst,timdeor,timland
  REAL(WP):: tpere,tperh
  REAL(WP):: tsunin
  REAL(WP):: tt
  REAL(WP):: tx,ty,tz
  REAL(WP):: tex,tey,tez
  REAL(WP):: u  ! gravitational constant for Mars, km**3/s**2
  REAL(WP):: vdbe,vdbh
  REAL(WP):: vdeorb
  REAL(WP):: vexcess
  REAL(WP):: vhp  ! hyperbolic excess velocity at Mars, km/s
  REAL(WP):: vhpmax  ! constraint on hyperbolic excess velocity
  REAL(WP):: vxd,vyd,vzd
  REAL(WP):: vxe,vye,vze
  REAL(WP):: vxh,vyh,vzh

  REAL(WP):: we
  REAL(WP):: wnd
  REAL(WP):: wz
  REAL(WP):: xeq,yeq,zeq
  REAL(WP):: xi  ! inclination of elliptic orbit at Mars, degrees
  REAL(WP):: xi1,xi2  ! initial, final values of xi
  REAL(WP):: xitw
  REAL(WP):: xj2  ! oblateness coefficient for Mars, non-dimensional
  REAL(WP):: xlf,xlw
  REAL(WP):: xmeq,ymeq,zmeq
  REAL(WP):: xs,ys,zs
  REAL(WP):: zac,zae,zap
  REAL(WP):: zi

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(A,5ES15.5)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(A,6F12.4)'

  CHARACTER(LEN=*),PARAMETER:: HEAD875 = " LAUNCH ARRIVAL  C3      DLA"// &
   "DELTAV F DBST F DBST DBST T1M SO EO TSUNIN   "// &
   "DURSUN  TASIN TASOUT    TEARIN  DUREAR  TAEIN TAEOUT"
  CHARACTER(LEN=*),PARAMETER:: HEAD876 = &
    "    DATE    DATE    ELIPSE HYPERB FROM PER"

  NAMELIST/INPT/e,xm,ild,iad,ijd,jjd,c3max,dlamax,vhpmax,delvmax, &
    per,elp,speclon,gee,xi,days,ha,hp,rs,rpmin,tadeorb,fpa,rentry,xj2, &
    u,inc,key1,key3,key4,key5,isearch,tdest

  NAMELIST/PARAM/per1,per2,kper,elp1,elp2,kelp, &
    gee1,gee2,kgee,xi1,xi2,kxi,day1,day2,kday

  NAMELIST/BPDATA/c3,dla,vhp,dpa,rap,xm,e

!-------------------------------------------------------------------------------

1 READ(UNIT=inputFile,IOSTAT=errCode,NML=INPT)
  IF (errCode<0) THEN
    WRITE(*,*) "No more input"
    RETURN
  END IF
  WRITE(UNIT=debugFile,NML=INPT)
  IF (errCode /= 0) THEN
    WRITE(*,*) 'Bad data in namelist INPT'
    RETURN
  END IF

  IF (ISEARCH==1) THEN
    READ(UNIT=inputFile,NML=PARAM)
    WRITE(UNIT=debugFile,NML=PARAM)
  END IF

  WRITE(outputFile,650)
  IF (KEY1==0) THEN  ! only a header at top of page
    WRITE(outputFile,*) HEAD875
    WRITE(outputFile,'(A/)') HEAD876
  END IF

  IF (KEY5.NE.1) GO TO 21
24 READ(inputFile,BPDATA)
   WRITE(debugFile,BPDATA)
21 CONTINUE

  CALL CALJUL(ewjd,efjd,wnd,fd,e) 
  CALL CALJUL(awjd,afjd,wnd,fd,xm) 
  WRITE(debugFile,FMT2) 'E  ',e
  WRITE(debugFile,FMT2) 'E  ',ewjd,efjd,wnd,fd
  WRITE(debugFile,FMT2) 'XM ',xm
  WRITE(debugFile,FMT2) 'XM ',awjd,afjd,wnd,fd

  WRITE(debugFile,'(A,4I6)') ' control:', ild,ijd, iad,jjd

  DO 700 i=1,ild,ijd    ! begin big loop on launch dates
    WRITE(outputFile,500)
    DO 710 j=1,iad,jjd   ! begin big loop on arrival dates
    datee=REAL(i-1)+ewjd+efjd 
    datem=REAL(j-1)+awjd+afjd
    WRITE(debugFile,*) "Inside loop 710, i,j=", i,j, key1,key5

    ewdi=REAL(INT(datee))    ! was IFIX(DATEE) 
    efdi=datee-ewdi +.0000001_WP
    awdi=REAL(INT(datem))   ! was IFIX(DATEM) 
    afdi=datem-awdi     +.0000001_WP
    CALL JULCAL(e1,ewdi,efdi,0)
    CALL JULCAL(xm1,awdi,afdi,0)
    WRITE(debugFile,FMT2) 'E1  ',e1
    WRITE(debugFile,FMT2) 'E1  ',ewdi,efdi
    WRITE(debugFile,FMT2) 'XM1 ',xm1
    WRITE(debugFile,FMT2) 'XM1 ',awdi,afdi


    IF (KEY5.EQ.1)GO TO 22
    IF (key1==0) THEN
      CALL Plug(datee,datem,c3,dla,vhp,dpa,rap,kk)
    ELSE
      CALL Plug1(datee,datem,c3,dla,vhp,dpa,rap,tt,ral,dcom,probper,probap, &
        probinc,tal,taa,helang,gda,gra,zap,ets,zae,ete,zac,etc,kk)
    END IF
    WRITE(debugFile,*) "End of Plugs,kk=",kk

    IF (kk==0) CYCLE !GO TO 710
    WRITE(debugFile,'(A,6F12.3)') "Limit tests", c3,c3max, dla,dlamax, vhp,vhpmax
    IF (C3.GT.C3MAX) CYCLE  ! GO TO 710
    IF (DLA.GT.DLAMAX) CYCLE  ! GO TO 710
    IF (VHP.GT.VHPMAX) CYCLE  ! GO TO 710
    GO TO 23
 22 CONTINUE 
 23 CONTINUE
 

    IF (ISEARCH==0) THEN
      jper=1
      jelp=1
      jgee=1
      jxi =1
      jday=1
    ELSE
      jper=1+INT(ABS(per1-per2))
      jelp=1+INT(ABS(elp1-elp2))
      jgee=1+INT(ABS(gee1-gee2))
      jxi =1+INT(ABS( xi1- xi2))
      jday=1+INT(ABS(day1-day2))
    END IF

    IF (KEY1==2) WRITE(outputFile,900) e1(2),e1(3),e1(1),e1(4),e1(5),e1(6), &
      xm1(2),xm1(3),xm1(1),xm1(4),xm1(5),xm1(6),datee,datem

    DO 720 i1=1,jper,kper 
      DO 730 i2=1,jelp,kelp 
        DO 740 i3=1,jgee,kgee
          DO 750 i4=1,jxi,kxi 
            DO 760 i5=1,jday,kday
              IF (isearch==1) THEN
                PER=PER1+REAL(I1-1)
                ELP=ELP1+REAL(I2-1)
                GEE=GEE1+REAL(I3-1)
                XI = XI1+REAL(I4-1)
                DAYS=DAY1+REAL(I5-1)
              END IF

              key2=1
              WRITE(debugFile,FMT2) 'Innermost loop, per,elp,gee,xi,days', per,elp,gee,xi,days
              CALL COMPEL(per,elp,gee,xi,capw,xitw,datem, ha,hp,ae,ee,days,rs,key2,key3,elonp)
              WRITE(debugFile,*) 'Return from CompEl, key2,key3', key2,key3
              eprad=hp + rs
              IF (key2==0) GO TO 711

              CALL Fudge(xj2,xi,ae,ee,delcom,delsom,pd,xitw,capw,oe,we,u,days,rs)

              IF (key1==0) THEN
                m=10
                CALL Deboost(ae,ee,xi,we,oe,fe,u,dpa,rap,vhp,rpmin,inc, &
                  az,eh,zi,wz,oz,fh,bt,br,deltv,tpere,m)
              ELSE
                m=1
                CALL Dbst1(ae,ee,xi,we,oe,fe,u,dpa,rap,vhp,rpmin,inc,az,eh,zi, &
                  wz,oz,fh,bt,br,deltv,tpere,tperh,b,hprad,dbrad,vxh,vyh,vzh,vxe,vye,vze, &
                  vxd,vyd,vzd,dechp,rahp,decep,raep,vdbh,vdbe,plane,m)
              END IF
              WRITE(debugFile,FMT1) 'After Deboost,Dbst; bt,br=', bt,br
              

! ******TRANSFORMATION OF BT,BR FROM EQUATORIAL TO ECLIPTIC.
              WRITE(debugFile,FMT1) 'Transformation, dpa,rap', dpa,rap
              xs=COS(dpa*DEG2RAD)*COS(rap*DEG2RAD)
              ys=COS(dpa*DEG2RAD)*SIN(rap*DEG2RAD)
              zs=SIN(dpa*DEG2RAD)
              WRITE(debugFile,FMT1) 'Transformation, xs,ys,zs', xs,ys,zs 
              CALL RECEQ(DATEM, ZERO,ZERO,ONE, XEQ,YEQ,ZEQ)
              WRITE(debugFile,FMT1) 'Transformation, xeq,yeq,zeq', xeq,yeq,zeq
              CALL REQMEQ(DATEM, XEQ,YEQ,ZEQ, XMEQ,YMEQ,ZMEQ, DM,RM)
              WRITE(debugFile,FMT1) 'Transformation, xmeq,ymeq,zmeq', xmeq,ymeq,zmeq
              WRITE(debugFile,FMT1) 'Transformation, dm,rm', dm,rm
              CALL CROSS(XS,YS,ZS, XMEQ,YMEQ,ZMEQ, TX,TY,TZ,PRODT)
              WRITE(debugFile,FMT1) 'Transformation, tx,ty,tz,prodt', tx,ty,tz,prodt
              CALL CROSS(XS,YS,ZS, TX,TY,TZ, RX,RY,RZ,PRODR)
              WRITE(debugFile,FMT1) 'Transformation, rx,ry,rz,prodr', rx,ry,rz,prodr
              CALL CROSS(XS,YS,ZS, ZERO,ZERO,ONE, TEX,TEY,TEZ,PRODET)
              WRITE(debugFile,FMT1) 'Transformation, tex,tey,tez,prodet', tex,tey,tez,prodet
              CALL CROSS(XS,YS,ZS, TEX,TEY,TEZ, REX,REY,REZ,PRODER)
              WRITE(debugFile,FMT1) 'Transformation, rex,rey,rez,prodt', rex,rey,rez,proder
              CALL Dot(tx,ty,tz, tex,tey,tez, ang1)
              CALL Dot(tx,ty,tz, rex,rey,rez, ang2)
              CALL Dot(rx,ry,rz, tex,tey,tez, ang3)
              CALL Dot(rx,ry,rz, rex,rey,rez, ang4)
              WRITE(debugFile,FMT1) 'Transformation, ang1,ang2,ang3,ang4', ang1,ang2,ang3,ang4
              BDT=BT
              BDR=BR
              BT=BDT*COS(ANG1*DEG2RAD) + BDR*COS(ANG2*DEG2RAD) 
              BR=BDT*COS(ANG3*DEG2RAD) + BDR*COS(ANG4*DEG2RAD) 
              WRITE(debugFile,FMT1) 'Transformation, bdt,bdr,bt,br', bdt,bdr,bt,br
! ****** END OF TRANSFORMATION.

              VEXCESS=DELVMAX-DELTV
              
              IF(DELTV.GT.DELVMAX) GO TO 711

              CALL SEESE(DATEM,PD,AE,EE,XI,WE,OE,U,RS,EX,EY,EZ,IS,IE,TSUNIN, &
                DURSUN,TASIN,TASOUT,TEARIN,DUREAR,TAEIN,TAEOUT,DELSOM,DELCOM,DAYS)

              IF ((KEY1.NE.1).OR.(KEY4.NE.1)) GO TO 17
              CALL EVENTS(ELONP,SPECLON,AE,EE,FE,DAYS,PD,DATEM,PER,FPA,RENTRY,RS, &
                TDEST,U,TADEORB,TIMLAND,TIMDEOR,TIMDBST,VDEORB,KK)
              IF (KK.EQ.0) GO TO 711
              dbw=AINT(timdbst)   ! was IFIX(TIMDBST) 
              dow=AINT(timdeor)   ! was IFIX(TIMDEOR) 
              xlw=AINT(timland)   ! was IFIX(TIMLAND) 
              dbf=timdbst-dbw 
              dof=timdeor-dow 
              xlf=timland-xlw
              CALL Julcal(dbst,dbw,dbf,0) 
              CALL Julcal(deor,dow,dof,0) 
              CALL Julcal(xland,xlw,xlf,0)
              GO TO 20
              17 CONTINUE
              speclon=elonp
              tadeorb=ZERO
              vdeorb=ZERO
              20 CONTINUE


              SELECT CASE(key1) ! write the output, depending on value of key1
                CASE(0)
                  WRITE(outputFile,800) e1(1),e1(2),e1(3), xm1(1),xm1(2),xm1(3), &
                    c3,dla,deltv,fe,fh,tpere,is,ie,tsunin,dursun,tasin,tasout, &
                    tearin, durear,taein,taeout
                CASE(1)
                  IF (KEY4==1) THEN !GO TO 18 
                    WRITE(outputFile,899) E1(2),E1(3),E1(1),E1(4),E1(5),E1(6), &
                      XM1(2),XM1(3),XM1(1),XM1(4),XM1(5),XM1(6), &
                      DBST(2),DBST(3),DBST(1),DBST(4),DBST(5),DBST(6), &
                      DEOR(2),DEOR(3),DEOR(1),DEOR(4),DEOR(5),DEOR(6), &
                      XLAND(2),XLAND(3),XLAND(1),XLAND(4),XLAND(5),XLAND(6), &
                      DATEE,DATEM,TIMDBST,TIMDEOR,TIMLAND
                    CALL PrintOneDate(outputFile, "LAUNCH  DATE", e1,    datee)
                    CALL PrintOneDate(outputFile, "ARRIVAL DATE", xm1,   datem)
                    CALL PrintOneDate(outputFile, "DEBOOST TIME", dbst,  timdbst)
                    CALL PrintOneDate(outputFile, "DEORBIT TIME", deor,  timdeor)
                    CALL PrintOneDate(outputFile, "LANDING TIME", xland, timland)
                  ELSE
                    WRITE(outputFile,900) e1(2),e1(3),e1(1),e1(4),e1(5),e1(6), &
                    xm1(2),xm1(3),xm1(1),xm1(4),xm1(5),xm1(6), datee,datem
                  END IF

                  WRITE(outputFile,901)DLA,RAL,C3,TT,DPA,RAP,VHP,GDA,GRA,DCOM, &
                    ZAP,ETS,ZAE,ETE,ZAC,ETC,PROBPER,PROBAP,PROBINC, &
                    TAL,TAA,HELANG,B,BT,BR 
                   CALL PrintOutput901(outputFile, dla,ral,c3,tt,dpa,rap,vhp,  &
                    gda,gra,dcom,zap,ets,zae,ete,zac,etc,                      &
                    probper,probap,probinc,tal,taa,helang,b,bt,br )

                  WRITE(outputFile,902) AZ,EH,ZI,OZ,wZ,FH,TPERH,HPRAD,DECHP,RAHP,vDBH,VXH,VYH,VZH
                  CALL PrintOutput902(outputFile, az,eh,zi,oz,wz,fh,tperh,hprad,dechp,rahp,vdbh, &
                    vxh,vyh,vzh)
                  WRITE(outputFile,903) AE,EE,XI,OE,WE,FE,TPERE,TADEORB,VDEORB, &
                    EPRAD,DECEP,RAEP,PD,VDBE,VXE,VYE,VZE
                  CALL PrintOutput903(outputFile, ae,ee,xi,oe,we,fe,tpere,tadeorb,vdeorb, &
                    eprad,decep,raep,pd,vdbe,vxe,vye,vze)
                  WRITE(outputFile,904)DELTV,VEXCESS,DBRAD,PER,VXD,VYD,VZD,PLANE 
                  CALL PrintOutput904(outputFile, deltv,vexcess,dbrad,per,vxd,vyd,vzd,plane)
                  WRITE(outputFile,905)IS,TSUNIN,DURSUN,TASIN,TASOUT,IE,TEARIN,DUREAR,TAEIN,TAEOUT
                  CALL PrintOutput905(outputFile, is,tsunin,dursun,tasin,tasout, &
                                           ie,tearin,durear,taein,taeout)
                  CALL PrintOutput906(outputFile, speclon,elp,gee,days)

              CASE(2)
                WRITE(outputFile,910)PER,ELP,GEE,XI,DAYS,DELTV
             END SELECT 

!!!              600 CONTINUE
            760 END DO
          750 END DO
        740 END DO
      730 END DO
    720 END DO
  711 CONTINUE
  710 END DO
700 END DO
    IF (KEY5.EQ.1)GO TO 24
    GO TO 1


500 FORMAT("0")
650 FORMAT("1")
800 FORMAT(1X,6F3.0,2F6.2,1X,F5.3,2F6.1,1X,F9.2,2I3,2F9.2,2F8.2,2F9.2,2F8.2)

899 FORMAT(20X,"LAUNCH DATE",13X,"ARRIVAL DATE",/,"DEBOOST TIME",12X, &
      "DEORBIT TIME",12X,"LANDING TIME",/, &
      " CALENDAR",6X,6F3.0,6X,6F3.0,6X,6F3.0,6X,6F3.0,6X,6F3.0,/, &
      " JULIAN",5X,F18.2,6X,F18.2,6X,F18.2,6X,F18.2,6X,F18.2,/)
900 FORMAT(21X,"LAUNCH DATE",13X,"ARRIVAL DATE",/, &
           " CALENDAR",7X,6F3.0,7X,6F3.0,/, &
           " JULIAN",7X,F18.2,7X,F18.2,/)
901 FORMAT(" INTERPLANETARY FLIGHT PARAMETERS",// &
      5X,"DLA =",E16.8,5X, "RAL =",E16.8,5X,"C3 =",E16.8,5X,"TRIP TIME =",E16.8,/ &
      5X,"AREO. DEC. S-VECTOR =",E16.8, 5X,"AREO. R.A. S-VECTOR =",E16.8, &
      5X,"HYPER. EXCESS VEL. =",E16.8,/ &
      5X,"GEO. DEC. S-VECTOR =",E16.8, 7X,"GE0. R.A. S-VECTOR =",E16.8, &
      5X,"COMUNICATION DIST. =",E16.8,/ &
      5X,"ZAP =",E16.8, 5X,"ETS =",E16.8, 5X,"ZAE =",E16.8, &
      5X,"ETE =", E16.8,/ &
      5X,"ZAC =",E16.8, 5X,"ETC =",E16.8,/ &
      5X,"PROBE PERIHELION =",E16.8,10X,"PROBE APHELION =",E16.8, &
      7X,"PROBE INCLINATION= ",E16.8,/ &
      5X,"LAUNCH TRUE ANOM. =",E16.8, 5X,"ARRIVAL TRUE ANOM. =",E16.8, &
      5X,"HELIO. ANGLE TRAVEL =",E16.8,/ &
      5X,"B-VECTOR MAGNITUDE =",E16.8,15X,"B DOT T =",E16.8,17X,"B DOT R =",E16.8,/)

902 FORMAT(" ELEMENTS AND DEBOOST PARAMETERS - HYPERBOLA",// &
      5X,"A =", E16.8, 5X,"E =",E16.8, 5X,"I =",E16.8, 5X,"CAP.OMEGA =",E16.8, &
      5X,"OMEGA =",E16.8,/ 5X,"DBST TRUE ANOM. =",E16.8, 7X,"DBST TIME =",E16.8,/ &
      5X,"PER. RADIUS =",E16.8, 5X,"PER. DEC. =",E16.8, 6X,"PER. R.A. =",E16.81/ &
      5X,"V AT DBST =",E16.8, 6X,"VX AT DBST =",E16.8, &
      5X,"VY AT DBST =",E16.8, 5X,"VZ AT DBST =",E16.8,/)

903 FORMAT(" ELEMENTS AND DEBOOST PARAMETERS - ELLIPSE",// &
      5X,"A =",E16.8, 5X,"E =",E15.8, 5X,"I =",E16.8, &
      5X,"CAP.OMEGA =",E16.8, 5X, "OMEGA =",E16.8,/ &
      5X,"DBST TRUE ANOM. =",E16.8, 7X,"DBST TIME =",E16.8, &
      5X,"DORB.T.A. =",E16.8,5X,"VDORB =",E16.8,/ &
      5X,"PER. RADIUS =",E16.8, 5X,"PER. DEC. =",E16.8, &
      6X,"PER. R.A. =",E16.8, 9X,"PERIOD =",E16.8,/ &
      5X,"V AT DBST =",E16.8, 6X,"VX AT DBST =",E16.8, &
      5X,"VY AT DBST =",E16.8, 5X,"VZ AT DBST =",E16.8,/)

904 FORMAT(" DEBOOST MANEUVER PARAMETERS",// &
      5X,"DELTA V =",E16.8, 5X, "EXCESS DELTA V =",E16.8, &
      5X,"RADIUS =",E16.8,5X,"PER =",E16.8,/ &
      5X,"VX =",E16.8,13X,"VY =",E16.8,17X,"VZ =",E16.8, &
      9X,"PLANE CHANGE =",E16.8,/)

905 FORMAT(" OCCULTATION PARAMETERS",// &
      5X,"1ST ORBIT, SUN=",I3,8X,"TIME, SUN =",E16.8, &
      7X,"DURATION, SUN =",E16.8,/ &
      5X,"TRUE ANOM., SUN IN =",E16.8, &
      7X,"TRUE ANOM., SUN) OUT =",E16.8,/ &
      5X,"1ST ORBIT, EARTH =",I3, 5X,"TIME, EARTH =",E16.8, &
      5X,"DURATION, EARTH =",E16.8,/ &
      5X,"TRUE ANOM., EARTH IN =",E16.8, &
      5X,"TRUE ANOM., EARTH OUT =",E16.8,/)

!!!906 FORMAT(" LANDING POINT PARAMETERS",// &
!!!      5X,"LONGITUDE =",E16.8, 5X,"LLATITUDE =",E16.8, &
!!!      5X,"SUN LIGHTING ANGLE",E16.8, 5X,"DAYS =",F3.0,/"1")

910 FORMAT(" PER=",ES10.3,"  ELP=",ES10.3,"  GEE=",ES10.3,"  XI =",ES10.3, &
      "  DAYS =",F6.1,"  DELTA V=",E11.4)
END Subroutine Main   ! --------------------------------------------------------

!+
SUBROUTINE PrintOneDate(efu, title, cal,jul)
! ------------------------------------------------------------------------------
! PURPOSE - Print one date, in both calendar and Julian
  INTEGER,INTENT(IN):: efu   ! unit for output
  CHARACTER(LEN=*),INTENT(IN):: title
  REAL(WP),DIMENSION(6),INTENT(IN):: cal
  REAL(WP),INTENT(IN):: jul

  CHARACTER(LEN=19):: buffer
  CHARACTER(LEN=*),PARAMETER:: FMT = '(A, 6I3, F12.2,3X,A19)'
!-------------------------------------------------------------------------------
  buffer=JulianToCalendar(jul)
  WRITE(efu,FMT) title, &
    INT(cal(2)),INT(cal(3)),INT(cal(1)),INT(cal(4)),INT(cal(5)),INT(cal(6)), jul, buffer
  RETURN
END Subroutine PrintOneDate   ! ------------------------------------------------

!+
SUBROUTINE PrintOutput901(efu, dla,ral,c3,tt,dpa,rap,vhp,gda,gra,dcom, &
                    zap,ets,zae,ete,zac,etc,probper,probap,probinc, &
                    tal,taa,helang,b,bt,br)
! ------------------------------------------------------------------------------
! PURPOSE - Print the interplanetary flight parameters
  INTEGER,INTENT(IN):: efu   ! external file unit for output
  REAL(WP),INTENT(IN):: dla,ral,c3,tt,dpa,rap,vhp,gda,gra,dcom, &
                    zap,ets,zae,ete,zac,etc,probper,probap,probinc, &
                    tal,taa,helang,b,bt,br               

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,F12.5)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(1X,A,ES12.3, F7.3)'
!-------------------------------------------------------------------------------
  WRITE(efu,'(/A)') "  INTERPLANETARY FLIGHT PARAMETERS"
  WRITE(efu,FMT1) 'Declination of launch asymptote, deg.=',dla
  WRITE(efu,FMT1) 'Right ascension of launch asymptote, deg.=',ral
  WRITE(efu,FMT1) 'Vis-Viva injection for Earth-Mars trajectory, (km/s)**2=',c3
  WRITE(efu,FMT1) 'Trip time, days=',tt
  WRITE(efu,FMT1) 'Areocentric declination of S-vector, deg.=',dpa
  WRITE(efu,FMT1) 'Areocentric right ascension of S-vector, deg.=',rap
  WRITE(efu,FMT1) 'Hyperbolic excess velocity, km/s=',vhp
  WRITE(efu,FMT1) 'Geocentric declination of S-vector, deg.=',gda
  WRITE(efu,FMT1) 'Geocentric right ascension of S-vector, deg.=',gra
  WRITE(efu,FMT2) 'Mars center to Earth center at arrival, km., A.U.=', &
    dcom, dcom/AU
  WRITE(efu,FMT1) 'ZAP=',zap
  WRITE(efu,FMT1) 'ETS=',ets
  WRITE(efu,FMT1) 'ZAE=',zae
  WRITE(efu,FMT1) 'ETE=',ete
  WRITE(efu,FMT1) 'ZAC=',zac
  WRITE(efu,FMT1) 'ETC=',etc
  WRITE(efu,FMT2) 'Probe perihelion=',probper, probper/AU
  WRITE(efu,FMT2) 'Probe aphelion=',probap, probap/AU
  WRITE(efu,FMT1) 'Probe inclination, deg.=',probinc
  WRITE(efu,FMT1) 'Launch true anomaly, deg.=',tal
  WRITE(efu,FMT1) 'Arrival true anomaly, deg.=',taa
  WRITE(efu,FMT1) 'Heliocentric angle travel, deg.=',helang
  WRITE(efu,FMT1) 'B-vector magnitude=',b
  WRITE(efu,FMT1) 'B-dot-T=',bt
  WRITE(efu,FMT1) 'B-dot-R=',br
 RETURN
END Subroutine PrintOutput901   ! ----------------------------------------------

!+
SUBROUTINE PrintOutput902(efu, az,eh,zi,oz,wz,fh,tperh,hprad,dechp,rahp,vdbh, &
  vxh,vyh,vzh)
! ------------------------------------------------------------------------------
! PURPOSE - 
  INTEGER,INTENT(IN):: efu   ! external file unit for output
  REAL(WP),INTENT(IN):: az,eh,zi,oz,wz,fh
  REAL(WP),INTENT(IN):: tperh,hprad,dechp,rahp,vdbh,vxh,vyh,vzh

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,3F12.5)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(1X,A,ES12.3, F7.3)'
!-------------------------------------------------------------------------------
  WRITE(efu,'(//A/)') "   ELEMENTS AND DEBOOST PARAMETERS - HYPERBOLA"
  WRITE(efu,FMT1) 'Semimajor axis=', az
  WRITE(efu,FMT1) 'Eccentricity=', eh
  WRITE(efu,FMT1) 'Inclination, deg.=', zi
  WRITE(efu,FMT1) 'Right ascension of ascending node, deg.', oz
  WRITE(efu,FMT1) 'Argument of periapsis, deg.=', wz
  WRITE(efu,FMT1) 'Deboost true anomaly, deg.=', fh
  WRITE(efu,FMT1) 'Deboost time=', tperh
  WRITE(efu,FMT1) 'Periapsis radius=', hprad
  WRITE(efu,FMT1) 'Periapsis declination, deg.=', dechp
  WRITE(efu,FMT1) 'Periapsis right declination, deg.=', rahp
  WRITE(efu,FMT1) 'Velocity at deboost=', vdbh
  WRITE(efu,FMT1) 'Aerocentric components of velocity=', vxh,vyh,vzh
  RETURN
END Subroutine PrintOutput902   ! ----------------------------------------------

!+
SUBROUTINE PrintOutput903(efu, ae,ee,xi,oe,we,fe,tpere,tadeorb,vdeorb, &
                    eprad,decep,raep,pd,vdbe,vxe,vye,vze)
! ------------------------------------------------------------------------------
! PURPOSE -
  INTEGER,INTENT(IN):: efu   ! external file unit for output
  REAL(WP),INTENT(IN):: ae,ee,xi,oe,we,fe,tpere,tadeorb,vdeorb
  REAL(WP),INTENT(IN):: eprad,decep,raep,pd,vdbe,vxe,vye,vze

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,F12.5)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(1X,A,ES12.3, F7.3)'
  CHARACTER(LEN=*),PARAMETER:: FMT3 = &
    '(1x,"Vx =",F12.5,3x,"Vy =",F12.5,3x,"Vz =",F12.5)'

!-------------------------------------------------------------------------------
  WRITE(efu,'(//A/)') "   ELEMENTS AND DEBOOST PARAMETERS - ELLIPSE"
  WRITE(efu,FMT1) 'Semimajor axis=', ae
  WRITE(efu,FMT1) 'Eccentricity=', ee
  WRITE(efu,FMT1) 'Inclination, deg.=', xi
  WRITE(efu,FMT1) 'Right ascension of ascending node, deg.=', oe
  WRITE(efu,FMT1) 'Argument of periapsis, deg.=', we
  WRITE(efu,FMT1) 'True anomaly at deboost, deg.=', fe
  WRITE(efu,FMT1) 'Time at deboost=', tpere
  WRITE(efu,FMT1) 'True anomaly of deorbit point on ellipse, deg.', tadeorb
  WRITE(efu,FMT1) 'Velocity change required for deorbit, km/s=', vdeorb
  WRITE(efu,FMT1) 'Periapsis radius, km=', eprad
  WRITE(efu,FMT1) 'Periapsis declination, deg.=', decep
  WRITE(efu,FMT1) 'Periapsis right ascension, deg.=', raep
  WRITE(efu,FMT1) 'Period, Earth days=', pd
  WRITE(efu,FMT1) 'Velocity at deboost, km/s=', vdbe
  WRITE(efu,FMT1) vxe,vye,vze
  RETURN
END Subroutine PrintOutput903   ! ----------------------------------------------

!+
SUBROUTINE PrintOutput904(efu, deltv,vexcess,dbrad,per,vxd,vyd,vzd,plane)
! ------------------------------------------------------------------------------
! PURPOSE -
  INTEGER,INTENT(IN):: efu   ! external file unit for output
  REAL(WP),INTENT(IN):: deltv,vexcess,dbrad,per,vxd,vyd,vzd,plane

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,3F12.5)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(1X,A,ES12.3, F7.3)'
  CHARACTER(LEN=*),PARAMETER:: FMT3 = &
    '(1x,"Vx =",F12.5,3x,"Vy =",F12.5,3x,"Vz =",F12.5)'
!-------------------------------------------------------------------------------
  WRITE(efu,'(//A/)') "   DEBOOST MANEUVER PARAMETERS"
  WRITE(efu,FMT1) 'Delta-v, velocity increment required for deboost=', deltv
  WRITE(efu,FMT1) 'excess delta v =', vexcess
  WRITE(efu,FMT1) 'radius on conics at deboost point=',dbrad
  WRITE(efu,FMT1) 'true anomaly of landing point beneath the ellipse, deg.=',per
  WRITE(efu,*) '      positive if landing before periapsis, negative if after'
  WRITE(efu,FMT1) 'Aerocentric components of delta-V',vxd,vyd,vzd
  WRITE(efu,FMT1) 'Plane change =', plane
  RETURN
END Subroutine PrintOutput904   ! ----------------------------------------------

!+
SUBROUTINE PrintOutput905(efu, is,tsunin,dursun,tasin,tasout, &
                               ie,tearin,durear,taein,taeout)
! ------------------------------------------------------------------------------
! PURPOSE -
  INTEGER,INTENT(IN):: efu   ! external file unit for output
  INTEGER,INTENT(IN):: is
  REAL(WP),INTENT(IN):: tsunin,dursun,tasin,tasout
  INTEGER,INTENT(IN):: ie
  REAL(WP),INTENT(IN):: tearin,durear,taein,taeout

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,F12.5)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(1X,A,ES12.3, F7.3)'
  CHARACTER(LEN=*),PARAMETER:: FMT3 = '(1X,A,I3,8X,A,F12.5)'
!-------------------------------------------------------------------------------
  WRITE(efu,'(//A/)')"   OCCULTATION PARAMETERS"
  WRITE(efu,FMT3) "1st orbit, Sun=", is, "time, Sun =", tsunin
  WRITE(efu,FMT1) "Duration, Sun =", dursun
  WRITE(efu,FMT1) "True anomaly, Sun in=", tasin
  WRITE(efu,FMT1) "True anomaly, Sun out=", tasout
  WRITE(efu,FMT3) "1st orbit, Earth =", ie, "time, Earth =", tearin
  WRITE(efu,FMT1) "Duration, earth =", durear
  WRITE(efu,FMT1) "True anomaly, Earth in =", taein
  WRITE(efu,FMT1) "True anomaly, Earth out =", taeout
  RETURN
END Subroutine PrintOutput905   ! ----------------------------------------------

!+
SUBROUTINE PrintOutput906(efu, speclon,elp,gee,days)
! ------------------------------------------------------------------------------
! PURPOSE -
  INTEGER,INTENT(IN):: efu   ! external file unit for output
  REAL(WP),INTENT(IN):: speclon,elp,gee,days

  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,F12.5)'
!-------------------------------------------------------------------------------
  WRITE(efu,'(//A/)') "   LANDING POINT PARAMETERS"
  WRITE(efu,FMT1) "Longitude, degrees =",speclon
  WRITE(efu,FMT1) "Latitude, degrees =",elp
  WRITE(efu,FMT1) "Sun lighting angle, degrees",gee
  WRITE(efu,FMT1) "Days =",days

  RETURN
END Subroutine PrintOutput906   ! ----------------------------------------------
           

END Module MainEarthToMarsProcedures   ! =======================================



!+
PROGRAM MISHAP
! ------------------------------------------------------------------------------
! PURPOSE - 
USE MainEarthToMarsProcedures
IMPLICIT NONE

  CHARACTER(LEN=*),PARAMETER,DIMENSION(3):: AUTHORS = (/         &
    " James F. Kibler, NASA Langley Research Center           ", &
    " Richard N. Green, NASA Langley Research Center          ", &
    " Ralph L. Carmichael, Public Domain Aeronautical Software" /)
  CHARACTER(LEN=15):: dateTimeStr
  INTEGER:: errCode
  CHARACTER(LEN=80):: fileName
  CHARACTER(LEN=*),PARAMETER:: GREETING = &
    "Earth-Mars Mission-Analysis Program.   NASA TN D-5985"
  INTEGER,PARAMETER:: IN=1, OUT=2, DBG=3
  CHARACTER(LEN=*),PARAMETER:: MODIFIER = " "  ! put your name here if you mod it
  CHARACTER(LEN=*),PARAMETER:: VERSION = "0.2 (31 May 2008)"
!-------------------------------------------------------------------------------
  WRITE(*,*) GREETING
  WRITE(*,*) 'Version '//VERSION 
  WRITE(*,'(A)') AUTHORS
  IF(MODIFIER .NE. ' ') WRITE(*,*) ' Modified by '//MODIFIER 


  dateTimeStr=GetDateTimeStr()
  OPEN(UNIT=DBG, FILE='mishap.dbg', STATUS='REPLACE', ACTION='WRITE')
  WRITE(DBG,*) 'Running mishap, version '//VERSION
  WRITE(DBG,*) 'Current date and time: '//dateTimeStr

  fileName="" 
  errCode=1                                                       
  CALL Get_Command(fileName)
  WRITE(DBG,*) "File name from command line: "//Trim(fileName)
  IF (Len_Trim(fileName)>0) THEN
    OPEN(UNIT=IN, FILE=fileName, STATUS='OLD', &
      IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
    IF (errCode/=0) THEN
      OPEN(UNIT=IN, FILE=Trim(fileName)//'.inp', STATUS='OLD', &
        IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      IF (errCode /= 0) THEN
        OPEN(UNIT=IN, FILE=Trim(fileName)//'.nml', STATUS='OLD', &
          IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      END IF
    END IF
  END IF

  IF (errCode /= 0) THEN
    DO                 
      WRITE(*,*) 'Enter the name of the input file: ' 
      READ(*,'(A)') fileName 
      IF (fileName == ' ') STOP 
      OPEN(UNIT=IN, FILE=fileName, STATUS='OLD', IOSTAT=errCode,     &
            ACTION='READ', POSITION='REWIND')
      IF (errCode == 0) EXIT
      OPEN(UNIT=IN, FILE=Trim(fileName)//'.inp', STATUS='OLD', &
        IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      IF (errCode == 0) EXIT
      OPEN(UNIT=IN, FILE=Trim(fileName)//'.nml', STATUS='OLD', &
        IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      IF (errCode == 0) EXIT
      WRITE(*, '(A)' ) ' Unable to open this file. Try again.' 
    END DO
  END IF 
  INQUIRE(UNIT=IN, NAME=fileName)
  WRITE(DBG,*) 'Reading from '//Trim(fileName)
  WRITE(*,*) 'Reading from '//Trim(fileName)
                                                                        
  OPEN(UNIT=OUT, FILE='mishap.out', STATUS='REPLACE',ACTION='WRITE') 
  WRITE(OUT,*) 'Running Earth-Mars mission analysis program, version '//VERSION
  WRITE(OUT,*) 'Current date and time: '//dateTimeStr

                                                                        
  CALL Main(IN,OUT,DBG)
  WRITE(*,*) "Normal termination of the Earth-Mars mission analysis program."
  STOP

! CONTAINS
! INCLUDE '\myfiles\mainlib\laheyfix.f90'


END Program Mishap   ! =========================================================

