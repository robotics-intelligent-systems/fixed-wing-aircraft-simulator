INCLUDE 'space.f90'

!+
MODULE VamoosProcedures
! ------------------------------------------------------------------------------
! PURPOSE - 
USE EarthToMarsProcedures
IMPLICIT NONE
!-------------------------------------------------------------------------------

CONTAINS

!+
SUBROUTINE Main(inputFile,outputFile,debugFile)
! ------------------------------------------------------------------------------

  INTEGER,INTENT(IN):: inputFile,outputFile,debugFile

  REAL(WP),DIMENSION(6):: g,b, sunb
  REAL(WP),DIMENSION(3,3):: rpqw

  REAL(WP):: a  ! semimajor axis
  REAL(WP):: alt1,alt2
  REAL(WP):: beta
  REAL(WP):: bfirst  ! first...
  REAL(WP):: bstep
  REAL(WP):: blast
  
  REAL(WP):: csdel,ssdel
  REAL(WP):: cx,cy,cz   ! unit vector from the planet towards Canopus
  REAL(WP):: decc, dece, decs
  REAL(WP):: delv
  REAL(WP):: jd 
  REAL(WP):: e
  INTEGER:: errCode   ! added by RLC
  REAL(WP):: ex,ey,ez   ! unit vector from the planet towards the Earth

  REAL(WP):: dec1,dec2
  REAL(WP):: f1,f2
  REAL(WP):: fjd  ! fractional part of Julian date
  CHARACTER(LEN=*),PARAMETER:: FMT1 = '(1X,A,3ES15.4)'
  CHARACTER(LEN=*),PARAMETER:: FMT2 = '(1X,A,3F15.4)'
  REAL(WP):: ha  ! apoapsis altitude of the elliptic orbit about planet
  REAL(WP):: hp  ! periapsis altitude "

  INTEGER:: i, ib1,ib2,ib3
  INTEGER:: ibody  ! planetselector,  =2 Venus; =4 Mars
  INTEGER:: iya,ima,ida, iyb,imb,idb
  INTEGER:: j,k
  INTEGER:: kk

  REAL(WP):: o
  REAL(WP):: odot

  REAL(WP):: pdec,pra
  REAL(WP):: period   ! period in hours

  REAL(WP):: ra1,ra2
  REAL(WP):: rac
  REAL(WP):: rae
  REAL(WP):: ras

  REAL(WP):: rs   ! radius of the planet
  REAL(WP):: sdel
  REAL(WP):: sun1 ! first sun angle
  REAL(WP):: sun2 
  REAL(WP):: sun3 
  REAL(WP):: sun4 
  REAL(WP):: sun5 
  REAL(WP):: sun6 

  REAL(WP):: svdece  ! declination of the incoming hyperbolic asymptote at the
                     ! planet in the Earth equinox and equator coordinate system
  REAL(WP):: svdecp  ! declination of the incoming hyperbolic asymptote at the
                     ! planet in the planet equinox and equator coordinate system
  REAL(WP):: svp
  REAL(WP):: svrae   ! right ascension of the incoming hyperbolic asymptote at the
                     ! planet in the Earth equinox and equator coordinate system
  REAL(WP):: svrap
  REAL(WP):: sx,sy,sz  ! unit vector from the planet towards the sun

  REAL(WP):: t1,t2
  REAL(WP):: thrust
  REAL(WP):: time  

  REAL(WP):: tlast  ! last time
  REAL(WP):: tocc
  REAL(WP):: tstep 

  REAL(WP):: u  ! gravitational constant of the planet
  REAL(WP):: vae ! velocity at apoapsis of the elliptical orbit about the planet
  REAL(WP):: vinf
  REAL(WP):: vpe  ! velocity at periapsis of the elliptical orbit about the planet
  REAL(WP):: vph  ! velocity at periapsis of the hyperbolic orbit
  REAL(WP):: vp,vq,vw
  REAL(WP):: w
  REAL(WP):: wdot
  REAL(WP):: wjd   ! whole part of the Julian date
  REAL(WP):: xi
  REAL(WP):: xj20  ! second zonal harmonic of the planet
  REAL(WP):: xn
  REAL(WP):: xsvp,ysvp,zsvp
  REAL(WP):: zap

  NAMELIST /CASE/ jd,svdece,svrae,vinf,ha,hp,bfirst,blast,bstep,tlast,tstep, &
    xj20,ibody,sun1,sun2,sun3,sun4,sun5,sun6 


!-------------------------------------------------------------------------------

600 CONTINUE
  tlast=ZERO   ! set default values for input quantities
  tstep=ZERO
  bfirst=ZERO
  bstep=ZERO
  blast=ZERO
  time=ZERO
  xj20=ZERO
  svdece=ZERO
  svrae=ZERO
  sun1=ZERO
  sun2=ZERO
  sun3=ZERO
  sun4=ZERO
  sun5=ZERO
  sun6=ZERO

  READ(UNIT=inputFile,IOSTAT=errCode,NML=CASE)
  IF (errCode < 0) THEN
    WRITE(*,*) "No more data"
    WRITE(debugFile,*) "No more data"
    RETURN
  END IF
  IF (errCode > 0) THEN
    WRITE(*,*) "Error reading namelist CASE"
    WRITE(debugFile,*) "Error reading namelist CASE"
    STOP
  END IF

  WRITE(debugFile,CASE)

  sunb(1)=sun1 
  sunb(2)=sun2 
  sunb(3)=sun3 
  sunb(4)=sun4
  sunb(5)=sun5 
  sunb(6)=sun6

  SELECT CASE(ibody)
    CASE(2)  ! we are going to Venus
      u=MUVENUS
      rs=VENUS_RADIUS
    CASE(4)  ! we are going to Mars
      u=MUMARS
      rs=MARS_RADIUS
  END SELECT

  wjd=AINT(jd)
  fjd=jd-wjd
  IF (fjd==HALF) fjd=HALF+0.00001_WP
  CALL Julcal(g,wjd,fjd, 0)
  iya=INT(g(1))  ! year
  ima=INT(g(2))  ! month 
  ida=INT(g(3))  ! dat

 2 CONTINUE   ! possible looping?
  CALL JulCal(b,wjd,fjd,0)
  iyb=INT(b(1))
  imb=INT(b(2))
  idb=INT(b(3))

  WRITE(outputFile,101) jd, iyb,imb,idb, iya,ima,ida

  CALL VectorV(outputFile, jd,decs,ras,dece,rae,decc,rac, &
    sx,sy,sz, ex,ey,ez, cx,cy,cz, ibody)

  IF (time > 0.01_WP) GO TO 7
  SELECT CASE(ibody)
    CASE(2)  ! Venus
      CALL ReqVeq(jd, Xcos(svdece)*Xcos(svrae), Xcos(svdece)*Xsin(svrae), &
        Xsin(svdece), xsvp,ysvp,zsvp ,svdecp,svrap)
    CASE(4)  ! Mars
      CALL ReqMeq(jd, Xcos(svdece)*Xcos(svrae), Xcos(svdece)*Xsin(svrae), &
        Xsin(svdece), xsvp,ysvp,zsvp, svdecp,svrap)
  END SELECT

  IF (bstep < 0.001) THEN
    ib1=1000
    ib2=1000
    ib3=1000
  ELSE
    ib1=INT(bfirst)
    ib2=INT(blast)
    ib3=INT(bstep)
  END IF
7 CONTINUE

  WRITE(debugFile,*) 'Beginning loop 11', ib1,ib2,ib3
  DO 11 i=ib1,ib2,ib3
    beta=REAL(i)
    IF (i==1000) beta=bfirst
    IF (ABS(Xsin(beta))< ABS(Xsin(svdecp))) GO TO 11

!... Calculate the orbital elements
    CALL Orbit(svdecp,svrap,vinf,ha,hp,beta, a,e,xi,w,o,pdec,pra,u,rs)
    WRITE(debugFile,FMT1) 'After Orbit, a,e,xi', a,e,xi
    WRITE(debugFile,FMT1) 'After Orbit, w,o', w,o
    WRITE(debugFile,FMT1) 'After Orbit, pdec,pra', pdec,pra
    WRITE(debugFile,FMT1) 'After Orbit, u,rs', u,rs


!... Perturb the orbital elements omega and cap-omega
    IF (time > 0.01_WP) THEN
      xn=SQRT(u/a**3)  ! period
      odot=(THREE/TWO)*xj20*xn*(rs/a)**2/(ONE-e*e)**2*Xcos(xi)
      wdot=THREE*xj20*xn*(rs/a)**2/(ONE-e*e)**2*(ONE-(FIVE/FOUR)*Xsin(xi)**2)
      w=w+(wdot*time*24*3600)*RAD2DEG
      o=o+(odot*time*24*3600)*RAD2DEG
      pdec=ASIN(Xsin(w)*Xsin(xi))*RAD2DEG
      ssdel=Xtan(pdec)/Xtan(xi)
      csdel=ssdel/Xtan(w)/Xcos(xi)
      sdel=ATAN2(ssdel,csdel)*RAD2DEG
      pra=Angle(o+sdel)
    END IF

    SELECT CASE(ibody)
      CASE(2)
        WRITE(outputFile,102) beta,time
      CASE(4)
        WRITE(outputFile,103) beta,time
    END SELECT

!... Calculate the output parameters ...
    CALL RxyzPqw(ZERO,ZERO,ZERO,xi,w,o, rpqw,vp,vq,vw)
    CALL Dot(rpqw(2,1),rpqw(2,2),rpqw(2,3),sx,sy,sz,thrust)
    CALL Dot(Xcos(svdecp)*Xcos(svrap),Xcos(svdecp)*Xsin(svrap), Xsin(svdecp), &
      sx,sy,sz,zap)
    svp=ACOS(u/(u+(rs+hp)*vinf**2))*RAD2DEG

    vpe=SQRT(u*(rs+ha)/a/(rs+hp))
    vph=SQRT(vinf**2 + TWO*u/(rs+hp))
    delv=vph-vpe
    vae=vpe*((ONE-e)/(ONE+e))
    period=TWOPI*SQRT(a**3/u)/3600   ! in hours

    WRITE(outputFile,FMT1) 'svdece,svrae', svdece,svrae
    WRITE(outputFile,FMT1) 'vinf', vinf
    WRITE(outputFile,FMT1) 'svdecp,svrap', svdecp,svrap
    WRITE(outputFile,FMT2) 'Altitude, apoapsis,periapsis (km)', ha,hp
    WRITE(outputFile,FMT2) 'Julian date', jd
    WRITE(outputFile,FMT2) 'Declination, right ascension (Planet)...', pdec,pra
    WRITE(outputFile,FMT2) 'px,py,pz', rpqw(1,1:3)
    WRITE(outputFile,FMT2) 'qx,qy,qz', rpqw(2,1:3)
    WRITE(outputFile,FMT2) 'wx,wy,wz', rpqw(3,1:3)
    WRITE(outputFile,FMT1) 'thrust', thrust
    WRITE(outputFile,FMT1) 'semi-major axis', a
    WRITE(outputFile,FMT2) 'eccentricity', e
    WRITE(outputFile,FMT2) 'inclination, deg. ....', xi
    WRITE(outputFile,FMT1) 'w.....', w
    WRITE(outputFile,FMT1) 'o.....', o
    WRITE(outputFile,FMT2) 'Declination, right ascension (Sun)....', decs,ras
    WRITE(outputFile,FMT2) 'Declination, right ascension (Earth)..', dece,rae
    WRITE(outputFile,FMT2) 'Sun unit vector (x,y,z) .........', sx,sy,sz
    WRITE(outputFile,FMT2) 'Earth unit vector (x,y,z) .......', ex,ey,ez
    WRITE(outputFile,FMT2) 'Period of orbit about planet, hours ....', period
    WRITE(outputFile,FMT2) 'Canopus unit vector (x,y,z) ......', cx,cy,cz
    WRITE(outputFile,FMT2) 'angle at planet between Sun vector and'
    WRITE(outputFile,FMT2) '  incoming hyperbolic asymptote (S-vector)...', zap
    WRITE(outputFile,FMT2) 'angle between incoming hyperbolic asymptote'
    WRITE(outputFile,FMT2) '       and vector from plante to periapsis...', svp
    WRITE(outputFile,FMT1) 'Velocity at periapsis of elliptic orbit', vpe
    WRITE(outputFile,FMT1) 'Velocity at apoapsis of elliptic orbit', vae
    WRITE(outputFile,FMT1) 'Velocity at periapsis on hyperbola...', vph
    WRITE(outputFile,FMT2) 'Deboost velocity, km/s ....', delv
    WRITE(outputFile,FMT1) 'Second zonal harmonic of the planet... ', xj20

!... Calculate and write imagery data ...
    IF (sun1 > 0.01_WP) CALL Sun(outputFile,sx,sy,sz,sunb,a,e,xi,w,o,rpqw,u,rs,1)

    CALL Occult(a,e,xi,w,o,u,rs,sx,sy,sz,tocc,t1,alt1,f1,dec1,ra1, &        ! sun
                                              t2,alt2,f2,dec2,ra2, kk)
    WRITE(outputFile,105) tocc,t1,f1,alt1,dec1,ra1,t2,f2,alt2,dec2,ra2


    CALL Occult(a,e,xi,w,o,u,rs,ex,ey,ez,tocc,t1,alt1,f1,dec1,ra1, &       ! earth
                                              t2,alt2,f2,dec2,ra2, kk)
    WRITE(outputFile,106) tocc,t1,f1,alt1,dec1,ra1,t2,f2,alt2,dec2,ra2


    CALL Occult(a,e,xi,w,o,u,rs,cx,cy,cz,tocc,t1,alt1,f1,dec1,ra1, &      ! canopus
                                              t2,alt2,f2,dec2,ra2, kk)
    WRITE(outputFile,107) tocc,t1,f1,alt1,dec1,ra1,t2,f2,alt2,dec2,ra2

11 END DO

  IF (tstep < 0.001_WP) GO TO 600   ! go back and read in a new case
  IF (time >= tlast) GO TO 600
  time=time+tstep
  wjd=wjd+AINT(tstep)
  fjd=fjd+MOD(tstep,ONE)
  jd=wjd+fjd
  GO TO 2

101 FORMAT(////" Julian date  ", F20.1// &
  22X,"year     month   day"// &
  22X,"calendar date  ",I4,5X,I3,7X,I3// &
  22X "arrival  date  ",I4,5X,I3,7X,I3)

102 FORMAT(////14x," beta       =",F9.4,4X, &
  "(venus equator,venus equinox)"/14X," time past arrival = ",F8.1,"     days")

103 FORMAT(////14X," beta       =",f9.4,4x, &
  "(mars equator, mars equinox)"/14x," time past arrival = ",F8.1,"     days")

104 FORMAT(//" svcece=",ES16.8/ "   svrae =", ES16.8/ "     vinf =", ES16.8/ &
  "   svdecp=", ES16.8/ &
  "   svrap =", ES16.8/ " ha    =", ES16.8/ " hp   =", ES16.8/ "   jd =", ES16.8/ &
  "   plat =", ES16.8/ "    plong =", ES16.8/ " px  =", ES16.8/  "  py      =", ES16.8/ &
  "   pz =", ES16.8/ "  qx = ", ES16.8/ " qy  =", ES16.8/ "  qz =", ES16.8/ &
  "   wx =", ES16.8/ "  wy = ", ES16.8/ "  wz =", ES16.8/ &
  "   velsun=", ES16.8/ &
  "   sma=", ES16.8/ "   ecc = ", ES16.8/ "  inc = ", ES16.8/ " argper=", e16.8/ &
  "   argncd=", ES16.8/ " decsjn=", ES16.8/ " rasun = ", ES16.8/ "deceth=", ES16.8/ &
  "  raeth =", ES16.8/ " xsun =", ES16.8/ " ysun =", ES16.8/ " zsun =", e16.8/ &
  "   xearth=", ES16.8/ " yearth=", ES16.8/ " zearth=", ES16.8/ "period=", ES6.8/ &
  "   xcanps=", ES16.8/ " ycanps=", ES16.8/ "  zcanps=", ES16.8/ "  zap =", ES16.8/ &
  "  svp=", ES16.8/ "  vpe  =", ES16.8/ "  vae =", ES16.8/ "  vph = ", ES16.8/ &
  "  delv = ", ES16.8/ " xj20 =", ES16.8)

105 FORMAT(/" sun occultaticn time", 15X, F11.2/ &
  " enter sun occultation",14x,5F11.2/ &
  " exit  sun occultation",14x,5F11.2)

106 FORMAT(/" earth occultation time",13x,F11.2/ &
  " enter earth occultation",12x,5F11.2/ &
  " exit earth occultation",13x,5F11.2)

107 FORMAT(/" canopus occultation time",11x,F11.2/ &
  " enter canopus occultation",10x,5F11.2/ &
  " exit canopus cccultation",11x,5F11.2)

800 continue
  RETURN
END Subroutine Main   ! --------------------------------------------------------




!+
SUBROUTINE Sun(efu,sx,sy,sz,sunb,a,e,xi,w,o,rpqw,u,rs,kk)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the positicns in orbit which correspond to various lighting 
! angles and writes output data. 
! NOTE - Calls subroutines SunBand, Concar, CarSph, and Tconic.

  INTEGER,INTENT(IN):: efu
  REAL(WP),INTENT(IN):: sx,sy,sz ! components of the unit vector 
                                 ! from the planet toward the sun
  REAL(WP),DIMENSION(6),INTENT(IN):: sunb  !(i-6) - lighting angles. 
        ! (angle between the sun vector and the spacecraft radius vector
  REAL(WP),INTENT(IN OUT):: a,e ! semimajor axis, eccentricity
  REAL(WP),INTENT(IN OUT):: xi,w,o ! inclinatich. argument of periapsis, longitude of ascending
!       ncce
  REAL(WP),DIMENSION(3,3),INTENT(IN OUT):: rpqw ! rotational matrix from the xyz to the pqw coordinate system
  REAL(WP),INTENT(IN):: u,rs ! gravitational ccnstant and radius of the planet
  INTEGER,INTENT(IN):: kk  ! control integer. 0 implies that xi,w,o are input. 1 implies
!       that rpqw is input.

  REAL(WP):: angrt   ! used?
  REAL(WP):: az  ! azimuth
  REAL(WP):: dec1, dec2
  REAL(WP):: dx,dy,dz
!  REAL(WP):: 
  REAL(WP):: fpa1,fpa2
  REAL(WP):: h1,h2
  INTEGER:: i
  INTEGER:: itype1,itype2
  REAL(WP):: r1,r2
  REAL(WP):: ra1,ra2
  REAL(WP):: t1,t2
  REAL(WP):: ta1,ta2
  REAL(WP):: v1,v2
  REAL(WP):: voh1,voh2
  REAL(WP):: x,y,z
!-------------------------------------------------------------------------------
  angrt=SQRT(u/a**3)

  WRITE(efu,105)

  DO 10 i=1,6
    IF (sunb(i) < 0.01) RETURN

    CALL SunBand(sx,sy,sz, sunb(i),xi,w,o,rpqw,ta1,ta2,itype1,itype2,kk)
    IF (itype1==0) GO TO 10

    CALL ConCar(a,e,xi,w,o,ta1, x,y,z,dx,dy,dz,u)
    CALL CarSph(x,y,z, dx,dy,dz, r1,ra1,dec1,v1,fpa1,az)
    CALL Tconic(u,e,a,ta1,t1)
    t1=t1/60
    h1=r1-rs
    voh1=v1*Xcos(fpa1)/h1
    SELECT CASE(itype1)
      CASE(1)
        WRITE(efu,101) sunb(i),t1,ta1,h1,dec1,ra1,voh1 
      CASE(2)
        WRITE(efu,102) sunb(i),t1,ta1,h1,dec1,ra1,voh1 
      CASE(3)
        WRITE(efu,103) sunb(i),t1,ta1,h1,dec1,ra1,voh1 
      CASE(4)
        WRITE(efu,104) sunb(i),t1,ta1,h1,dec1,ra1,voh1 
    END SELECT

    CALL ConCar(a,e,xi,w,o,ta2,x,y,z,dx,dy,dz,u)
    CALL CarSph(x,y,z, dx,dy,dz, r2,ra2,dec2,v2,fpa2,az)
    CALL Tconic(u,e,a,ta2,t2)
    t2=t2/60
    h2=r2-rs
    voh2=v2*Xcos(fpa2)/h2
    SELECT CASE(itype2)
      CASE(1)
        WRITE(efu,101) sunb(i),t2,ta2,h2,dec2,ra2,voh2 
      CASE(2)
        WRITE(efu,102) sunb(i),t2,ta2,h2,dec2,ra2,voh2 
      CASE(3)
        WRITE(efu,103) sunb(i),t2,ta2,h2,dec2,ra2,voh2 
      CASE(4)
        WRITE(efu,104) sunb(i),t2,ta2,h2,dec2,ra2,voh2 
    END SELECT
10 END DO

101 FORMAT(" sun angle", F5.1, " deg. and inc, s/c asc",5F11.2,F14.8)
102 FORMAT(" sun angle", F5.1, " deg. and inc, s/c dsc",5F11.2,F14.8)
103 FORMAT(" sun angle", F5.1, " deg. and dec, s/c asc",5f11.2,f14.8)
104 FORMAT(" sun angle", F5.1, " deg. and dec, s/c dsc",5f11.2,f14.8)
105 FORMAT(/41x,"ftime      t.a.    alt     dec     ra       v/h/")

800 return
END Subroutine Sun   ! ---------------------------------------------------------

SUBROUTINE SunBand(sx,sy,sz,psi,xi,w,o,rpqw,ta1,ta2,itype1,itype2, kk)
! ------------------------------------------------------------------------------
! PURPOSE - Get positicn in orbit which corresponds to a given lighting angle. 
! NOTE - Calls RxyzPqw and Qadrat

  REAL(WP),INTENT(IN):: sx,sy,sz ! unit vector from the planet toards the sun
  REAL(WP),INTENT(IN):: psi ! lighting angle. (angle between the sun vector and the
!       spacecraft racius vector)
  REAL(WP),INTENT(IN):: xi,w,o ! inclinatich, argument of periapsis, longitude of
!       ascending node
  REAL(WP),DIMENSION(3,3),INTENT(IN OUT):: rpqw ! rotational matrix from the xyz to 
!  the pqw coordinate system
  REAL(WP),INTENT(OUT):: ta1,ta2 ! first and second true anomalies ccrresponding to a
!       lighting angle
  INTEGER,INTENT(OUT):: itype1,itype2 ! cchlrol integer to indicate the conditions at ta1
!       and t42. 0 implies no solution. 1 implies psi increasing
!       spacecraft ascending. 2 implies p5i increasing, s/c
!       cescending. 3 implies psi decreasing, s/c ascending.
!       4 implies psi cecreasing, s/c descending.
  INTEGER,INTENT(IN):: kk !� ccatrcl integer. 0 implies that xi,w,o are input and rpqw is
!       output. 1 implies that rpqw is input.

  REAL(WP):: a,b,c
  REAL(WP):: asdes
  INTEGER:: num  ! number of real roots found by Qadrat
  REAL(WP):: sdpsi
  REAL(WP):: sf1,sf2
  REAL(WP):: sp,sq,sw
!-------------------------------------------------------------------------------
  itype1=0
  itype2=0

  IF (kk==0) THEN
    CALL RxyzPqw(sx,sy,sz, xi,w,o,rpqw,sp,sq,sw) 
  ELSE
    sp=rpqw(1,1)*sx + rpqw(1,2)*sy + rpqw(1,3)*sz 
    sq=rpqw(2,1)*sx + rpqw(2,2)*sy + rpqw(2,3)*sz 
    sw=rpqw(3,1)*sx + rpqw(3,2)*sy + rpqw(3,3)*sz
  END IF

  a=sq**4/sp**2 + TWO*sq**2 + sp**2
  b=-TWO*Xcos(psi)*(sq**3/sp**2 + sq) 
  c=sw**2 + sq**2/sp**2*Xcos(psi)**2 - Xsin(psi)**2 
  CALL qadrat(a,b,c,sf1,sf2,num)
  IF (num==0) RETURN

  IF (ABS(sf1)>ONE .OR. ABS(sf2)>ONE) RETURN

  ta1=RAD2DEG*atan2(sf1,(Xcos(psi)-sq*sf1)/sp)
  ta2=RAD2DEG*atan2(sf2,(Xcos(psi)-sq*sf2)/sp)

  sdpsi=(sp*sf1-sq*Xcos(ta1))/Xsin(psi) 
  asdes=rpqw(2,3)*Xcos(ta1)-rpqw(1,3)*sf1 
  IF (sdpsi.gt.ZERO .AND. asdes.gt.ZERO) itype1=1 
  IF (sdpsi.gt.ZERO .AND. asdes.lt.ZERO) itype1=2 
  IF (sdpsi.lt.ZERO .AND. asdes.gt.ZERO) itype1=3 
  IF (sdpsi.lt.ZERO .AND. asdes.lt.ZERO) itype1=4 

  sdpsi=(sp*sf2-sq*Xcos(ta2))/Xsin(psi) 
  asdes=rpqw(2,3)*Xcos(ta2)-rpqw(1,3)*sf2 
  IF (sdpsi.gt.ZERO .AND. asdes.gt.ZERO) itype2=1 
  IF (sdpsi.gt.ZERO .AND. asdes.lt.ZERO) itype2=2 
  IF (sdpsi.lt.ZERO .AND. asdes.gt.ZERO) itype2=3 
  IF (sdpsi.lt.ZERO .AND. asdes.lt.ZERO) itype2=4

  RETURN
END Subroutine Sunband   ! -----------------------------------------------------



!+
SUBROUTINE VectorV(efu,jd, decs,ras, dece,rae, decc,rac, sx,sy,sz, ex,ey,ez, &
  cx,cy,cz, ibody)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the positicn cf the Sun, Earth, and Cancpus in the 
!  planetocentric, planet equator, coordinate system and writes data. 
! NOTE - Calls subroutines eearth, emars, evenus, preces, latlng, dot, receq,
!  recvec, and reqmeq.
  INTEGER,INTENT(IN):: efu
  REAL(WP),INTENT(IN):: jd  ! julian date at time of interest
  REAL(WP),INTENT(OUT):: decs,ras  ! declination anc right ascension cf the Sun.
  REAL(WP),INTENT(OUT):: dece,rae  ! declination and right ascension of the earth.
  REAL(WP),INTENT(OUT):: decc,rac  ! declinaticn and right ascension of Canopus.
  REAL(WP),INTENT(OUT):: sx,sy,sz  ! unit vector from the planet to the Sun.
  REAL(WP),INTENT(OUT):: ex,ey,ez  ! unit vector from the planet to the Earth.
  REAL(WP),INTENT(OUT):: cx,cy,cz  ! unit vector from the planet to Canopus.
  INTEGER,INTENT(IN):: ibody  ! control integer for planet. 2=Venus; 4=Mars


  REAL(WP):: cxe,cye,cze
  REAL(WP):: dxhe,dyhe,dzhe
  REAL(WP):: dxhp,dyhp,dzhp
  REAL(WP):: ehlat,ehlong
  REAL(WP):: exe,eye,eze
  REAL(WP):: esp,sep,spe
!!!  INTEGER:: num  ! number of real roots found by Qadrat
  REAL(WP):: pex,pey,pez
  REAL(WP):: phlat,phlong
  REAL(WP):: rse,rsp,rpe
  REAL(WP):: sex,sey,sez
  REAL(WP):: spx,spy,spz
  REAL(WP):: sxe,sye,sze
  REAL(WP):: xhe,yhe,zhe
  REAL(WP):: xhp,yhp,zhp
  REAL(WP):: xhpe,yhpe,zhpe
  REAL(WP):: xpe,ype,zpe
  REAL(WP):: xps,yps,zps
!-------------------------------------------------------------------------------

  CALL Eearth(jd, xhe,yhe,zhe, dxhe,dyhe,dzhe)
  CALL Preces(2432282.0_WP, -0.060340592_WP, 0.60342839_WP, -0.79513092_WP,jd,cxe,cye,cze)

  SELECT CASE(ibody)
    CASE(2)  ! Venus
      CALL Evenus(jd, xhp,yhp,zhp, dxhp,dyhp,dzhp)
    CASE(4)  ! Mars
      CALL Emars(jd, xhp,yhp,zhp, dxhp,dyhp,dzhp)
  END SELECT

  xhpe=xhe-xhp
  yhpe=yhe-yhp
  zhpe=zhe-zhp 

  rse=SQRT(xhe**2  + yhe**2  + zhe**2)
  rsp=SQRT(xhp**2  + yhp**2  + zhp**2)
  rpe=SQRT(xhpe**2 + yhpe**2 + zhpe**2)

  sex=xhe/rse 
  sey=yhe/rse 
  sez=zhe/rse 

  spx=xhp/rsp 
  spy=yhp/rsp 
  spz=zhp/rsp

  pex=xhpe/rpe 
  pey=yhpe/rpe 
  pez=zhpe/rpe 

  CALL LatLng(sex,sey,sez, ehlat,ehlong)
  CALL LatLng(spx,spy,spz, phlat,phlong)
  CALL Dot(sex,sey,sez,  spx, spy, spz,  esp)
  CALL Dot(sex,sey,sez,  pex, pey, pez,  sep)
  CALL Dot(spx,spy,spz, -pex,-pey,-pez,  spe)
  CALL Receq(jd, -spx,-spy,-spz, sxe,sye,sze)
  CALL Receq(jd,  pex, pey, pez, exe,eye,eze)

  SELECT CASE(ibody)
    CASE(2)  ! Venus
      CALL ReqVeq(jd, sxe,sye,sze, sx,sy,sz, decs,ras)    ! Sun
      CALL ReqVeq(jd, exe,eye,eze, ex,ey,ez, dece,rae)    ! Earth 
      CALL ReqVeq(jd, cxe,cye,cze, cx,cy,cz, decc,rac)    ! Canopus
    CASE(4)  ! Mars
      CALL ReqMeq(jd, sxe,sye,sze, sx,sy,sz, decs,ras)    ! Sun
      CALL ReqMeq(jd, exe,eye,eze, ex,ey,ez, dece,rae)    ! Earth
      CALL ReqMeq(jd, cxe,cye,cze, cx,cy,cz, decc,rac)    ! Canopus
  END SELECT

  xps=sx*rsp
  yps=sy*rsp
  zps=sz*rsp

  xpe=ex*rpe
  ype=ey*rpe
  zpe=ez*rpe

  SELECT CASE(ibody)
    CASE(2)  ! Venus
      WRITE(efu,101) xhp,yhp,zhp, phlat,phlong, xhe,yhe,zhe, ehlat,ehlong
      WRITE(efu,102) xps,yps,zps, decs,ras, xpe,ype,zpe, dece,rae 
      WRITE(efu,103) sx,sy,sz, ex,ey,ez, cx,cy,cz
      WRITE(efu,104) rpe,rse,rsp, sep,esp,spe
    CASE(4)  ! Mars
      WRITE(efu,105) xhp,yhp,zhp, phlat,phlong, xhe,yhe,zhe, ehlat,ehlong
      WRITE(efu,106) xps,yps,zps, decs,ras, xpe,ype,zpe, dece,rae 
      WRITE(efu,107) sx,sy,sz, ex,ey,ez, cx,cy,cz
      WRITE(efu,108) rpe,rse,rsp, sep,esp,spe
  END SELECT

101 FORMAT(/"Heliocentric ecliptic    (mean equinox of date)"/ &
  24X,"x (km)",8x,"y (km)",9x,"z (km)",9x,"latitude",7x,"longitude"/ &
  " venus",5x,5ES16.7//" earth",5x,5ES16.7)
102 FORMAT(/" aphrodiocentric    (venus equator, venus equinox)"/ &
  24x,"x (km)",8x,"y (km)",9x,"z (km)",6x," declination       right ascension"/ &
  " sun",7x,5ES16.7//" earth",5x,5ES16.7)
103 FORMAT(" unit vectors"//" sun     ", 3ES16.7/ &
                            " earth   ", 3ES16.7/ &
                            " canopus ", 3ES16.7)
104 FORMAT(/" venus-earth distance (km) = ", ES16.8/ &
            " sun-earth distance (km)   = ", ES16.8/ &
            " sun-venus distance (km)   = ", ES16.8/ &
            " venus-earth-sun angle     = ", ES16.8/ &
            " venus-sun-earth angle     = ", ES16.8/ &
            " earth-venus-sun angle     = ", ES16.8)
105 FORMAT(/" Heliocentric ecliptic      (mean equinox of date)"/ &
  24x,"x (km)",8x,"y (km)",9x,"z (km)",9x,"latitude",7x,"longitude"/ &
  " mars ",5x,5ES16.7/ &
  " earth",5x,5ES16.7)
106 FORMAT(/" areocentric        (mars equator, mars equinox)"/ &
  24x,"x (km)",8x,"y (km)",9x,"z (km)",9x," declination      right ascension"/ &
  " sun  ",5x,5ES16.7/ &
  " earth",5x,5ES16.7)
107 FORMAT(/" unit vectors"/&
  " sun    ", 3ES16.7/&
  " earth  ", 3ES16.7/ &
  " Canopus", 3ES16.7)
108 FORMAT(/" Mars-earth distance (km) = ", ES16.8/ &
  " sun-earth distance (km) = ", ES16.8/ &
  " sun-mars distance (km) = ", ES16.8/ &
  " mars-earth-sun angle = ", ES16.8/ &
  " mars-sun-earth angle = ", ES16.8/ &
  " earth-mars-sun angle = ", ES16.8)

  RETURN
END Subroutine VectorV   ! -----------------------------------------------------


END Module VamoosProcedures   ! ================================================


!+
PROGRAM Vamoos
!PURPOSE - Venus and Mars Orbital Occultation Simulator

!       THIS PROGRAM CALLS SUBROUTINES JULCAL, REQVEU, REQMEQ, REQPEQ, RECEQ,
!       RXVZFCW, RPCWXYZ, EEARTF, EVENUS, EMARS, SUN, SUNBAND, OCCULT, CONCAR,
!       CARSPF, TCCNIC, GACRAT, CUBIC, (.ARTIC, CROSS, DOT, ORBIT,
!       PRECES, LATLNG, EULER, VECTOR
 
! ------------------------------------------------------------------------------
! PURPOSE - 
USE VamoosProcedures
IMPLICIT NONE

  CHARACTER(LEN=*),PARAMETER,DIMENSION(2):: AUTHORS = (/         &
    " Richard N. Green, NASA Langley Research Center          ", &
    " Ralph L. Carmichael, Public Domain Aeronautical Software" /)
  CHARACTER(LEN=15):: dateTimeStr
  INTEGER:: errCode
  CHARACTER(LEN=80):: fileName
  CHARACTER(LEN=*),PARAMETER:: GREETING = &
    "Occultation and Imagery Program.   NASA TN D-5104"
  INTEGER,PARAMETER:: IN=1, OUT=2, DBG=3
  CHARACTER(LEN=*),PARAMETER:: MODIFIER = " "  ! put your name here if you mod
  CHARACTER(LEN=*),PARAMETER:: VERSION = "0.3 (14 June 2008)"
!-------------------------------------------------------------------------------
  WRITE(*,*) GREETING
  WRITE(*,*) 'Version '//VERSION 
  WRITE(*,'(A)') AUTHORS
  IF(MODIFIER .NE. ' ') WRITE(*,*) ' Modified by '//MODIFIER 

  dateTimeStr=GetDateTimeStr()
  OPEN(UNIT=DBG, FILE='vamoos.dbg', STATUS='REPLACE', ACTION='WRITE')
  WRITE(DBG,*) 'Running vamoos, version '//VERSION
  WRITE(DBG,*) 'Current date and time: '//dateTimeStr

  fileName="" 
  errCode=1                                                       
  CALL Get_Command(fileName)
  WRITE(DBG,*) "File name from command line: "//Trim(fileName)
  IF (Len_Trim(fileName)>0) THEN
    OPEN(UNIT=IN, FILE=fileName, STATUS='OLD', &
      IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
    IF (errCode/=0) THEN
      OPEN(UNIT=IN, FILE=Trim(fileName)//'.inp', STATUS='OLD', &
        IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      IF (errCode /= 0) THEN
        OPEN(UNIT=IN, FILE=Trim(fileName)//'.nml', STATUS='OLD', &
          IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      END IF
    END IF
  END IF

  IF (errCode /= 0) THEN
    DO                 
      WRITE(*,*) 'Enter the name of the input file: ' 
      READ(*,'(A)') fileName 
      IF (fileName == ' ') STOP 
      OPEN(UNIT=IN, FILE=fileName, STATUS='OLD', IOSTAT=errCode,     &
            ACTION='READ', POSITION='REWIND')
      IF (errCode == 0) EXIT
      OPEN(UNIT=IN, FILE=Trim(fileName)//'.inp', STATUS='OLD', &
        IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      IF (errCode == 0) EXIT
      OPEN(UNIT=IN, FILE=Trim(fileName)//'.nml', STATUS='OLD', &
        IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
      IF (errCode == 0) EXIT
      WRITE(*, '(A)' ) ' Unable to open this file. Try again.' 
    END DO
  END IF 
  INQUIRE(UNIT=IN, NAME=fileName)
  WRITE(DBG,*) 'Reading from '//Trim(fileName)
  WRITE(*,*) 'Reading from '//Trim(fileName)
                                                                        
  OPEN(UNIT=OUT, FILE='vamoos.out', STATUS='REPLACE',ACTION='WRITE') 
  WRITE(OUT,*) 'Running Occultation and Imagery program, version '//VERSION
  WRITE(OUT,*) 'Current date and time: '//dateTimeStr
                                                                        
  CALL Main(IN,OUT,DBG)
  WRITE(*,*) "Normal termination of the Occultation and Imagery program."
  STOP

! CONTAINS
! INCLUDE '\myfiles\mainlib\laheyFix.f90'

END Program Vamoos   ! =========================================================



