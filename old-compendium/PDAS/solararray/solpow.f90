!+
!  ORBITING SOLAR ARRAY SIMULATION MODEL
!
! PURPOSE - Solar arrays are becoming an increasingly important means of
! generating power for earth orbiting spacecraft. Currently, almost all
! unmanned earth satellites utilize solar array electrical power
! generation systems.
! Applications for solar arrays in the near future include providing power
! for space shuttle payloads and manned space stations. This computer
! program was developed to simulate the capabilities of earth orbiting
! arrays. The model used is based on an improved version of a
! finite-element radiation shape factor subprogram. The inherent
! simplicity and speed of the original subprogram has been augmented with
! an improved shadow evaluation technique to provide the user with an
! efficient array model.

! The program allows the characteristics of orbiting arrays to be
! evaluated with a minimum of user effort and computer cost. Input to
! the program consists of a brief description of the array and the
! orbital parameters.
! The orbital parameters are used to determine the direct solar radiation
! incident on the cells, incident solar radiation reflected to cells from
! the earth, and the shadowing of any cells. Once the amount of thermal
! radiation gained and lost by the array is known, the amount of power
! which can be generated and the temperature of the array is determined.

! AUTHORS - R.A.Proeschel and perhaps others, NASA Marshall Spaceflight
!            Center and Rockwell Science Center
!           Ralph L. Carmichael, Public Domain Aeronautical Software
! REVISION HISTORY                                                        
!   DATE  VERS PERSON  STATEMENT OF CHANGES
!   1977?  1.0   RAP   Release of COSMIC program MSC-18558
!  1Oct99  1.1   RLC   Tidy/Convert processing
! 15Oct99  1.2   RLC   Subroutines into module; no COMMON
! 12Dec99  1.3   RLC   Removed all deprecated constructs; runs in Elf



!+
MODULE Subs
! ---------------------------------------------------------------------------

IMPLICIT NONE
!----------------------------------------------------------------------------
                             ! CALLED BY
  PUBLIC::  Beta             ! main
  PUBLIC::  CpGlas, CpKap    ! main
  PUBLIC::  Crad             ! main
  PUBLIC::  Earth            ! main
  PUBLIC::  Ephem            ! main
  PUBLIC::  Eta              ! main
  PUBLIC::  Falbedo          ! main
  PUBLIC::  Linterp          ! Cpglas,Eta,Orbit
  PUBLIC::  Orbit            ! main
  PUBLIC::  PI               ! main and many others
  PRIVATE:: PtoR             ! Beta,Earth,Orbit,Sphere2
  PRIVATE:: Rscon            ! Ephem, Stdtm
  PRIVATE:: Rsfdx            ! Ephem
  PRIVATE:: RtoP             ! Beta,Orbit,Sphere2
  PUBLIC::  Shfac            ! main, Falbedo
  PUBLIC::  Sphere           ! main
  PUBLIC::  Sphere2          ! main
  PUBLIC::  Stdtm            ! main
  PRIVATE:: Tdelt            ! Trsfx
  PRIVATE:: Trsfdx           ! Stdtm

CONTAINS

!+
FUNCTION Beta(re,phi,ra) RESULT(bet)    ! a PURE function
! ---------------------------------------------------------------------------
! PURPOSE - Determination of solar beta angle (deg)
IMPLICIT NONE
  REAL,INTENT(IN),DIMENSION(:):: re   ! dimension is 3
  REAL,INTENT(IN):: phi   ! inclination (radians)
  REAL,INTENT(IN):: ra    ! right ascension (radians)

  REAL:: bet              ! solar beta angle (degrees)

  REAL:: r,theta          ! theta in radians
  REAL:: xa,ya,za
!----------------------------------------------------------------------------
  xa=0.0   ! orbital normal vector in orbit coordinates
  ya=0.0
  za=1.0

  CALL Rtop(ya,za,r,theta)   ! rotate for inclination
  theta=theta+phi
  CALL Ptor(ya,za,r,theta)

  CALL Rtop(xa,ya,r,theta)   ! rotate for right ascension
  theta=theta+ra
  CALL Ptor(xa,ya,r,theta)

  CALL Rtop(ya,za,r,theta)   ! rotate for earth's inclination
  theta=theta-0.409274
  CALL Ptor(ya,za,r,theta)

  bet=ACOS((re(1)*xa + re(2)*ya)/SQRT(re(1)*re(1)+re(2)*re(2)))-PI(0.5)
  bet=bet*180./PI(1.)                    ! radians to degrees
  RETURN
END Function Beta   ! -------------------------------------------------------

!+
FUNCTION Cpglas(temperature) RESULT(f)
! ---------------------------------------------------------------------------
! PURPOSE - Heat capacity of fused silica glass  BTU/(LB-R)
IMPLICIT NONE
  REAL,INTENT(IN):: temperature

  REAL:: f   ! heat capacity  BTU/(lb-R)

  REAL,DIMENSION(5),PARAMETER:: TR = (/260.,860.,1260.,1960.,2460./)
  REAL,DIMENSION(5),PARAMETER:: CP = (/0.103,0.23,0.261,0.285,0.285/)
!----------------------------------------------------------------------------
  f=Linterp(TR,CP,temperature)
  RETURN
END Function CpGlas   ! -----------------------------------------------------

!+
FUNCTION CpKap(temperature) RESULT(f)
! ---------------------------------------------------------------------------
! PURPOSE - Heat capacity of polyimide   btu/(lb-R)
IMPLICIT NONE
  REAL,INTENT(IN):: temperature  ! deg R
  REAL:: f   ! heat capacity of polyimide  BTU/(lb-R)
!----------------------------------------------------------------------------
  f=4.6304E-4*temperature
  RETURN
END Function CpKap   ! ------------------------------------------------------

!+
FUNCTION Crad(e1,e2,a1,a2,f12) RESULT(c)   ! a PURE function
! ---------------------------------------------------------------------------
! PURPOSE - something about cosmic radiation
IMPLICIT NONE
  REAL,INTENT(IN):: e1,e2   ! emittances of cell front and back
  REAL,INTENT(IN):: a1,a2   ! areas ??
  REAL,INTENT(IN):: f12     ! ??? 
  REAL:: c
!----------------------------------------------------------------------------
  c=0.0
  IF (f12 > 0.0) c=1.714E-9*a1/(1.0/e1 -1.0 + 1.0/f12 + a1*(1.0/e2 -1.0)/a2)
  RETURN
END Function Crad   ! -------------------------------------------------------

!+
SUBROUTINE Earth(dateUT,x,y,tp,th,p)
! ---------------------------------------------------------------------------
! PURPOSE - Heliocentric position of earth on a given date
! called by main program
IMPLICIT NONE
  REAL,INTENT(IN):: dateUT   ! date of year (U.T.)
  REAL,INTENT(OUT):: x,y   ! heliocentric coordinates (ft)
  REAL,INTENT(IN),DIMENSION(:):: tp,th   ! position arrays form Ephem
  REAL,INTENT(IN):: p   ! period

  REAL,PARAMETER:: A=4.90744E11  ! semi-major axis of earth orbit, ft.
  REAL,PARAMETER:: E=0.016718    ! eccentricity of earth orbit

  REAL:: dtNode
  INTEGER:: jd
  REAL:: r,theta
  REAL:: xn
!-----------------------------------------------------------------------

  jd=INT(dateUT)   ! truncates dateUT for day only

!  GO TO (1,2,3,4,5,6,7,8,9,10,11,12), jd
  SELECT CASE(jd)
    CASE(1)
      xn=100.*(dateUT-1.0)-1.0
    CASE(2)
      xn=100.*(dateUT-2.0)+30.0
    CASE(3)
      xn=100.*(dateUT-3)+58.242
    CASE(4)
      xn=100.*(dateUT-4)+89.242
    CASE(5)
      xn=100.*(dateUT-5)+119.242
    CASE(6)
      xn=100.*(dateUT-6)+150.242
    CASE(7)
      xn=100.*(dateUT-7)+180.242
    CASE(8)
      xn=100.*(dateUT-8)+211.242
    CASE(9)
      xn=100.*(dateUT-9)+242.242
    CASE(10)
      xn=100.*(dateUT-10)+272.242
    CASE(11)
      xn=100.*(dateUT-11)+303.242
    CASE(12)
      xn=100.*(dateUT-12)+333.242
  END SELECT

  xn=24.0*xn   ! days to hours
  dtNode=p*Linterp(th,tp,4.49854199) - 6381.091333
  xn=xn+dtNode
  IF (xn > p)   xn=xn-p
  IF (xn < 0.0) xn=xn+p
  theta=Linterp(tp,th,xn/p)+1.78464317
  r=A*(1.-E*E)/(1.0+E*COS(theta))   ! Eq. 1.29 in van de Kamp
  CALL Ptor(x,y,r,theta)   ! returns x,y for output

  RETURN
END Subroutine Earth   ! ----------------------------------------------------

!+
SUBROUTINE Ephem(a,e,w,p,tp,theta)
! ---------------------------------------------------------------------------
! PURPOSE - Create a table of orbital position as a function of time.
!   Analyzes one complete period with theta going from 0 to 2pi in equal
!   increments. The time required for each step is computed by numerical
!   solution of the differential equation of orbital motion. The time for
!   one complete period is computed and returned as p. The array tp is then
!   normalized by this quantity and thus goes from 0 to 1.

IMPLICIT NONE
  REAL,INTENT(IN):: a    ! semimajor axis (ft)
  REAL,INTENT(IN):: e    ! eccentricity
  REAL,INTENT(IN):: w    ! mass of central body (lb)
  REAL,INTENT(OUT):: p   ! orbital period (hrs)
  REAL,INTENT(OUT),DIMENSION(:):: tp      ! fraction of orbital period
  REAL,INTENT(OUT),DIMENSION(:):: theta   ! angle from periapsis (radians)

  REAL:: c
  REAL:: ds
  REAL:: dst   ! ds/dtheta
  REAL:: dth
  REAL:: f
  INTEGER:: jrks
  INTEGER:: j,n
  REAL:: st    ! time
  REAL:: sts
  REAL:: th    ! angle (radians)
!-----------------------------------------------------------------------
  n=MIN(SIZE(tp), SIZE(theta))
  dth=PI(2.)/REAL(n-1)             ! never changes
  c=((1.0-e*e)**1.5)/PI(2.0)         ! never changes 
  jrks=1
  st=0.0
  th=0.0
  DO J=1,n
    tp(j)=st
    theta(j)=th
   10 f=1.0+e*COS(th)
    dst=c/(f*f)
    CALL Rsfdx(st,dst,ds,dth,sts,jrks) ! changes ds and sts
    CALL Rscon(th,dth,jrks)            ! changes th and increments jrks 
    IF (jrks > 1) GO TO 10
  END DO

  f=1.0/tp(n)
  tp(:)=f*tp(:)
  p=PI(2.)*(a**1.5)/(3600.*SQRT(1.068E-9*w))   ! what is 1.068E-9 ?
  RETURN
END Subroutine Ephem   ! ----------------------------------------------------

!+
FUNCTION Eta(t,xlife) RESULT(f)
! ---------------------------------------------------------------------------
! PURPOSE - Solar cell efficiency/temp function ??
! called by main program
IMPLICIT NONE
  REAL,INTENT(IN):: t, xlife

  REAL:: f   ! efficiency
  REAL,DIMENSION(4),PARAMETER:: TR = (/366.,542.,600.,618./)
  REAL,DIMENSION(4),PARAMETER:: ETAR = (/0.16949,0.11637,0.09902, 0.09362/)
!----------------------------------------------------------------------------
  f=(1.0+0.036*(1.0-xlife))*Linterp(TR,ETAR,t)
  RETURN
END Function Eta   ! --------------------------------------------------------

!+
FUNCTION Falbedo(xa,aa,vna,na,xe,ae,vne,fes,ne) RESULT(f)   ! a PURE function
! ---------------------------------------------------------------------------
! PURPOSE - Shape factor of the sun as reflected off the earth
! called by main program
IMPLICIT NONE
  REAL,INTENT(IN),DIMENSION(:,:):: xa
  REAL,INTENT(IN),DIMENSION(:)::   aa
  REAL,INTENT(IN),DIMENSION(:,:):: vna
  INTEGER,INTENT(IN)::             na
  REAL,INTENT(IN),DIMENSION(:,:):: xe
  REAL,INTENT(IN),DIMENSION(:)::   ae
  REAL,INTENT(IN),DIMENSION(:,:):: vne
  REAL,INTENT(IN),DIMENSION(:)::   fes
  INTEGER,INTENT(IN)::             ne

  REAL,DIMENSION(1):: ao
  REAL:: f
  INTEGER:: j
  REAL,DIMENSION(3,1):: vno  ! ditto
  REAL,DIMENSION(3,1):: xo   ! was 1-D in original
!----------------------------------------------------------------------------

  f=0.0
  DO j=1,ne
    IF (fes(j)==0.0) CYCLE
    xo(1:3,1)=xe(1:3,j)
!    xo(1,1)=xe(1,j)
!    xo(2,1)=xe(2,j)
!    xo(3,1)=xe(3,j)
    vno(1:3,1)=vne(1:3,j)
!    vno(1,1)=vne(1,j)
!    vno(2,1)=vne(2,j)
!    vno(3,1)=vne(3,j)
    ao(1)=ae(J)
    f=f + fes(j)*Shfac(xa,aa,vna,na, xo,ao,vno,1, xo,ao,vno,1)
  END DO
  RETURN
END Function Falbedo   ! ----------------------------------------------------

!+
FUNCTION Linterp(xtab,ytab,x) RESULT(y)
! ---------------------------------------------------------------------------
! PURPOSE - Linear interpolation
IMPLICIT NONE
  REAL,INTENT(IN),DIMENSION(:):: xtab,ytab
  REAL,INTENT(IN):: x

  REAL:: y

  INTEGER:: i,j,k
  REAL:: theta
!----------------------------------------------------------------------------
  i=1
  j=SIZE(xtab)
  IF (j<=1) THEN
    y=YTAB(1)
    RETURN
  END IF
    
  DO
    IF (j <= i+1) EXIT
    k=(i+j)/2
    IF (x < xtab(k)) THEN
      j=k
    ELSE
      i=k
    END IF
  END DO
  theta=(x-xtab(i))/(xtab(i+1)-xtab(i))
  y=theta*ytab(i+1) + (1.0-theta)*ytab(i)
 
  RETURN
END Function Linterp   ! ----------------------------------------------------

!+
SUBROUTINE Orbit(xo,yo,time,x,y,z,tp,th,p,a,e,ra,phi,omega)
! ---------------------------------------------------------------------------
! PURPOSE - Heliocentric coordinates of an earth satellite. Computes
!   x,y,z at a given time.
! called by main program
IMPLICIT NONE
  REAL,INTENT(IN):: xo,yo               ! heliocentric location of earth (ft)
  REAL,INTENT(IN):: time                ! time from perigee (hr)
  REAL,INTENT(OUT):: x,y,z              ! satellite coordinates (ft)
  REAL,INTENT(IN),DIMENSION(:):: tp,th  ! position arrays from ephem
  REAL,INTENT(IN):: p                   ! orbital period (hr)

  REAL,INTENT(IN):: a,e,ra,phi,omega
!  COMMON /ORBC/a,e,ra,phi,omega

!  INTEGER:: j,n
  REAL:: r,theta
!----------------------------------------------------------------------------
!  j=2
!  n=100

  theta=Linterp(tp,th,time/p)
!  theta=Yrdr(time/p,tp,th,j,n)         ! Location in orbit coordinates
  r=a*(1.-e*e)/(1.+e*COS(theta))

  theta =theta+omega                   ! Rotate for argument of perigee
  CALL Ptor(x,y,r,theta)
  z=0.0

  CALL Rtop(y,z,r,theta)               ! Rotate for inclination
  theta=theta+phi
  CALL Ptor(y,z,r,theta)

  CALL Rtop(x,y,r,theta)               ! Rotate for right ascension
  theta=theta+ra
  CALL Ptor(x,y,r,theta)

  CALL Rtop(y,z,r,theta)               ! Rotate for earth's inclination
  theta=theta-0.409274
  CALL Ptor(y,z,r,theta)

  x=x+xo                               ! Translate for earth's position
  y=y+yo

  RETURN
END Subroutine Orbit   ! ----------------------------------------------------

!+
FUNCTION PI(x) RESULT(f)
! ---------------------------------------------------------------------------
!PURPOSE - FUNCTION SIMULATING ROCKWELL SCCLIB ROUTINE "PI"    
IMPLICIT NONE
  REAL,INTENT(IN):: x
  REAL:: f
!----------------------------------------------------------------------------
  f=3.14159265*x
  RETURN
END Function PI   ! ---------------------------------------------------------

!+
SUBROUTINE Ptor(x,y,r,th)
! ---------------------------------------------------------------------------
! PURPOSE - Polar to rectangular conversion
IMPLICIT NONE
  REAL,INTENT(OUT):: x,y
  REAL,INTENT(IN):: r,th   ! theta in radians
!----------------------------------------------------------------------------
  x=r*COS(th)
  y=r*SIN(th)
  RETURN
END Subroutine Ptor   ! -----------------------------------------------------

!+
SUBROUTINE Rscon(xc,dxc,kc)
! ---------------------------------------------------------------------------
! PURPOSE - Control procedure for Runge-Kutta integration
! called by Ephem and Stdtm. Gets called AFTER Rsfdx or Trsfdx
IMPLICIT NONE
  REAL,INTENT(IN OUT):: xc
  REAL,INTENT(IN):: dxc
  INTEGER,INTENT(IN OUT):: kc
!----------------------------------------------------------------------------
  SELECT CASE(kc)
    CASE(1)
      xc=xc+0.5*dxc
      kc=2
    CASE(2)
      kc=3
    CASE(3)
      xc=xc+0.5*dxc
      kc=4
    CASE(4)
      kc=1
  END SELECT
  RETURN
END Subroutine Rscon   ! ----------------------------------------------------

!+
SUBROUTINE Rsfdx(y,dydx,dy,dx,ys,kr)
! ---------------------------------------------------------------------------
! PURPOSE - RUNGE-KUTTA INTEGRATION SUBROUTINE
! called by Ephem
IMPLICIT NONE
  REAL,INTENT(IN OUT):: y
  REAL,INTENT(IN):: dydx
  REAL,INTENT(IN OUT):: dy
  REAL,INTENT(IN):: dx
  REAL,INTENT(IN OUT):: ys
  INTEGER,INTENT(IN):: kr   ! Runge-Kutta index in {1,2,3,4}
!----------------------------------------------------------------------------
  SELECT CASE(kr)
    CASE(1)
      ys=y
      dy=dydx*dx
      y=ys+0.5*dy
    CASE(2)
      dy=dy+2.0*dydx*dx
      y=ys+0.5*dydx*dx
    CASE(3)
      dy=dy+2.0*dydx*dx
      y=ys+dydx*dx
    CASE(4)
      dy=(dy+dydx*dx)/6.0
      y=ys+dy
  END SELECT

  RETURN
END Subroutine Rsfdx   ! ----------------------------------------------------

!+
SUBROUTINE RTOP(X,Y,R,TH)
! ---------------------------------------------------------------------------
! PURPOSE - Rectangular to Polar Conversion. Like ATAN2 but returns angle in
!   [0,2pi] instead of [-pi,pi]
IMPLICIT NONE
  REAL,INTENT(IN):: x,y
  REAL,INTENT(OUT):: r,th  ! th in radians
!----------------------------------------------------------------------------
  R=SQRT(X*X+Y*Y)
  IF(R.GT.0.0) GO TO 10
  TH=0.0
  RETURN

10 IF (X < 0.0) GO TO 40
   IF (x==0.0) GO TO 30   ! 40,30,20

   IF (Y.LT.0.0) GO TO 25
   TH=ATAN(Y/X)
   RETURN

25 TH=PI(2.)-ATAN(0.-Y/X)
   RETURN

30 TH=PI(0.5)
   IF (Y.LT.0.0)  TH=PI(1.5)
   RETURN

40 IF (Y.LT.0.0) GO TO 45
   TH=PI(1.)+ATAN(Y/X)
   RETURN

45 TH=PI(1.)-ATAN(0.-Y/X)
   RETURN
END Subroutine RTOP   ! -----------------------------------------------------

!+
FUNCTION Shfac(x1,a1,vn1,n1,x2,a2,vn2,n2,xb,ab,vnb,nb) RESULT(f)  ! PURE
! ---------------------------------------------------------------------------
! PURPOSE - Shape factor function
! called by Falbedo and main program
IMPLICIT NONE
!
!          REVISION OF 1/5/79
!              ... BLOCKAGE EVALUATED AFTER COSINE EVALUATION
!
!
!               X=COORDINATE ARRAY FOR EACH NODAL PLANE
!               A=AREA OF EACH NODAL PLANE
!               VN=COMPONENTS OF UNIT NORMAL VECTOR FOR EACH NODAL PLANE
!               N=NUMBER OF NODAL PLANES IN EACH BODY
!               1,2=BODIES FOR SHAPE FACTOR COMPUTATION
!               B=  ALL SURFACES BETWEEN BODY 1 AND BODY 2
!                 (IF NO INTERVENING SURFACES,LET XB=X1,ETC AND NB=1)
!               SHFAC=BLACK BODY SHAPE FACTOR REFERENCED TO BODY 1
!
!      DIMENSION X1(3,N1),A1(N1),VN1(3,N1),X2(3,N2),A2(N2),VN2(3,N2),R(3)
!      DIMENSION XB(3,NB),AB(NB),VNB(3,NB),B(3)

  REAL,INTENT(IN),DIMENSION(:,:):: x1
  REAL,INTENT(IN),DIMENSION(:)::   a1
  REAL,INTENT(IN),DIMENSION(:,:):: vn1
  INTEGER,INTENT(IN)::             n1

  REAL,INTENT(IN),DIMENSION(:,:):: x2
  REAL,INTENT(IN),DIMENSION(:)::   a2
  REAL,INTENT(IN),DIMENSION(:,:):: vn2
  INTEGER,INTENT(IN)::             n2

  REAL,INTENT(IN),DIMENSION(:,:):: xb
  REAL,INTENT(IN),DIMENSION(:)::   ab
  REAL,INTENT(IN),DIMENSION(:,:):: vnb
  INTEGER,INTENT(IN)::             nb

  REAL:: f

  REAL:: a
  REAL,DIMENSION(3):: b
  REAL:: c
  REAL:: cos1,cos2
  REAL:: f1
  INTEGER:: i,j,k
  REAL,DIMENSION(3):: r
  REAL:: rdotn
  REAL:: rmag
  REAL:: s
  REAL:: xdotn

!----------------------------------------------------------------------------

  a=0.0
  f1=0.0

  DO j=1,n1
    a=a+a1(j)

    DO k=1,n2
      r(1:3)=x2(1:3,k)-x1(1:3,j)
      rmag=SQRT(SUM(r**2))   !               calculate radius vector
!      DO lr=1,3
!        r(lr)=x2(lr,k)-x1(lr,j)
!        rmag=rmag+(r(lr)*r(lr))
!      END DO
!      rmag=SQRT(rmag)


!               calculate cosines
      cos1=((vn1(1,j)*r(1))+(vn1(2,j)*r(2))+(vn1(3,j)*r(3)))/rmag
      cos2=0.-(((vn2(1,k)*r(1))+(vn2(2,k)*r(2))+(vn2(3,k)*r(3)))/rmag)

!       If a cosine is .LE.0.0,the surfaces don't see each other
      IF(cos1.LE.0.0 .OR. cos2.LE.0.0) CYCLE
!
!                   blockage evaluation
      DO i=1,nb
        b(1:3)=xb(1:3,i)-x1(1:3,j)   ! radius vector to blocking element
        xdotn=DOT_PRODUCT(b(1:3), vnb(1:3,i))
        rdotn=DOT_PRODUCT(r(1:3), vnb(1:3,i))
!        xdotn=b(1)*vnb(1,i)+b(2)*vnb(2,i)+b(3)*vnb(3,i)
!        rdotn=r(1)*vnb(1,i)+r(2)*vnb(2,i)+r(3)*vnb(3,i)
        IF (rdotn==0.0) CYCLE
        c=xdotn/rdotn
        IF (c <= 0.0 .OR. c >= 1.0) CYCLE
        s=SUM( (c*r(1:3)-b(1:3))**2)   ! Blockage surface vector magnitude
        IF (s .LE. ab(i)/2.0) GO TO 20
      END DO

      f1=f1 + cos1*cos2*a1(j)*a2(k)/(rmag*rmag)
   20 f1=f1
    END DO
  END DO

  f=f1/PI(a)   !

  RETURN
END Function Shfac   ! ------------------------------------------------------


!+
SUBROUTINE Sphere(xo,r,x,a,vn,phimin,phimax,n,na)
! ---------------------------------------------------------------------------
! PURPOSE - Generation of Shfac parameters for a sphere
IMPLICIT NONE
!
!!!      DIMENSION XO(3),X(3,520),A(520),VN(3,520)
!
  REAL,INTENT(IN),DIMENSION(3):: xo ! origin vector for center
  REAL,INTENT(IN):: r ! radius
  REAL,INTENT(OUT),DIMENSION(:,:):: x ! surface position tensor
  REAL,INTENT(OUT),DIMENSION(:):: a ! surface area vector
  REAL,INTENT(OUT),DIMENSION(:,:):: vn ! surface normal tensor
  REAL,INTENT(IN):: phimin,phimax ! limits of latitude
!                                     measured from pole (radians)
  INTEGER,INTENT(IN):: n ! number of latitude divisions
  INTEGER,INTENT(OUT):: na ! actual number of surface elements

  REAL:: area
  REAL:: cosp,sinp
  REAL:: da
  REAL:: dphi
  INTEGER:: i,j,k,m
  REAL:: phi
  REAL:: the
  REAL:: xm
!----------------------------------------------------------------------------
  na=0
  j=1
  phi=phimin
  dphi=(phimax-phimin)/n

  the=0.0
  DO k=1,n
    sinp=SIN(phi+dphi/2.)
    cosp=COS(phi+dphi/2.)
    da=dphi/sinp
    xm=Pi(2./da)+0.5
    m=xm
    da=Pi(2./m)
    the=the+da/2.
    area=r*r*da*(COS(phi)-COS(phi+dphi))
    na=na+m
    IF (na > SIZE(x,2)) THEN
      WRITE(*,*) "Subroutine Sphere"
      WRITE(*,*) "Choice of lat divisions produces excess element array size"
      WRITE(*,*) "Fatal error"
      STOP
    END IF
    DO i=1,m
      vn(1,j)=sinp*COS(the+da/2.)
      vn(2,j)=sinp*SIN(the+da/2.)
      vn(3,j)=cosp
      a(j)=area
      x(1:3,j)=xo(1:3)+r*vn(1:3,j)
      the=the+da
      j=j+1
    END DO          
    phi=phi+dphi
  END DO       
  RETURN
END Subroutine Sphere   ! ---------------------------------------------------

!+
SUBROUTINE Sphere2(xo,phi,ra,r,x,a,vn,phimin,phimax,n,na)
! ---------------------------------------------------------------------------
! PURPOSE - Generation of Shfac parameters in heliocentric coordinates
!   for a portion of earth between phimin and phimax
IMPLICIT NONE
!
!!!      DIMENSION XO(3),X(3,520),A(520),VN(3,520)

  REAL,INTENT(IN),DIMENSION(:):: xo
  REAL,INTENT(IN):: phi
  REAL,INTENT(IN):: ra
  REAL,INTENT(IN):: r
  REAL,INTENT(OUT),DIMENSION(:,:):: x
  REAL,INTENT(OUT),DIMENSION(:):: a
  REAL,INTENT(OUT),DIMENSION(:,:):: vn
  REAL,INTENT(IN):: phimin
  REAL,INTENT(IN):: phimax
  INTEGER,INTENT(IN):: n   
  INTEGER,INTENT(OUT):: na

  INTEGER:: j
  REAL:: rn
  REAL:: rx
  REAL:: td
  REAL,DIMENSION(3):: xc
!----------------------------------------------------------------------------
  xc(:)=0.0

! Calculate spherical surface tensors in sphere's coordinate system
  CALL Sphere(xc,r,x,a,vn,phimin,phimax,n,na)

  DO j=1,na            ! Transform to general coordinate system

    CALL Rtop(x(2,j), x(3,j), rx,td)           ! Rotate about x axis
    rn=SQRT(vn(2,j)*vn(2,j) + vn(3,j)*vn(3,j))
    td=td+phi
    CALL Ptor(x(2,j), x(3,j), rx,td)
    CALL Ptor(vn(2,j),vn(3,j),rn,td)

    CALL Rtop(x(1,j), x(2,j), rx,td)           ! Rotate about z axis
    rn=SQRT(vn(1,j)*vn(1,j) + vn(2,j)*vn(2,j))
    td=td+ra
    CALL Ptor(x(1,j), x(2,j), rx,td)
    CALL Ptor(vn(1,j),vn(2,j),rn,td)

    CALL Rtop(x(2,j), x(3,j), rx,td)           ! Rotate for earth's inclination
    rn=SQRT(vn(2,j)*vn(2,j) + vn(3,j)*vn(3,j))
    td=td-0.409274
    CALL Ptor(x(2,j), x(3,j), rx,td)
    CALL Ptor(vn(2,j),vn(3,j),rn,td)

!    x(1,j)=x(1,j)+xo(1)                      ! Translate
!    x(2,j)=x(2,j)+xo(2)
!    x(3,j)=x(3,j)+xo(3)
    x(1:3,j)=x(1:3,j)+xo(1:3)
  END DO
  RETURN
END Subroutine Sphere2   ! --------------------------------------------------

!+
SUBROUTINE Stdtm(c2,c4,cap,q,t,dt,time,jrks)
! ---------------------------------------------------------------------------
! PURPOSE - Stable numerical integration of solar array temperature
! called by main
IMPLICIT NONE
  REAL,INTENT(IN):: c2,c4,cap,q
  REAL,INTENT(IN OUT):: t     ! temperature, not time
  REAL,INTENT(IN):: dt
  REAL,INTENT(IN OUT):: time
  INTEGER,INTENT(IN OUT):: jrks   ! Runge-Kutta index, in {1,2,3,4}

!  REAL:: c,cap,q,t
!      COMMON /FTAC/c(25,25),cap(25),q(25),t(25)
  
  REAL:: b       !
!  REAL:: c2,c4
  REAL,SAVE:: dts
!  REAL:: t24,t44
  REAL,SAVE:: to      ! no initial setting !!!
  REAL,PARAMETER:: TEARTH=479.0, TSPACE=4.97, T24=TEARTH**4, T44=TSPACE**4
  REAL:: tss
! NOTE (RLC) - You may get a message indicating that to is used without
! being set. Don't worry - this routine is always called 4 times for each
! time step. When jrks=1 (the first time), then Trsfdx does not use the
! value of to, but it returns a value. Then, for jrks=2,3,4, Trsfdx uses
! that value of to. So it must be declared with an intent of IN OUT.
!----------------------------------------------------------------------------
!!!  to=0.0   ! RLC   maybe this will help
  
!  c2=c(1,2)
!  c4=c(1,4)
!  t24=t(2)**4   ! t(2) is temp of sun
!  t44=t(4)**4   ! t(4) is temp of space

  tss=SQRT(SQRT((q+c2*T24+c4*T44)/(c2+c4)))  ! a temperature
  b=-(tss*tss+t*t)*(tss+t)*(c2+c4)/cap
  WRITE(3,'(A,7ES13.4)') " In 1 Stdtm ", c2,c4,q, cap, tss,b, t
  CALL Trsfdx(t,b,tss,dts,dt,to,jrks)                ! changes t(1),dts,to
  CALL Rscon(time,dt,jrks)                    ! changes time and jrks, not dt
  WRITE(3,'(A,3F12.6)') " In 2 Stdtm", dts,to,t
  RETURN
END Subroutine StdTm   ! ----------------------------------------------------

!+
FUNCTION Tdelt(tss,to,b,delt) RESULT(td)   ! a PURE function
! ---------------------------------------------------------------------------
! PURPOSE - 
! called by Trsfx
IMPLICIT NONE
  REAL,INTENT(IN):: tss
  REAL,INTENT(IN):: to
  REAL,INTENT(IN):: b
  REAL,INTENT(IN):: delt

  REAL:: phi
  REAL:: td
!----------------------------------------------------------------------------
  phi=b*delt
  IF (phi < -25.0) THEN
    td=tss-to
  ELSE
    td=(tss-to)*(1.0-EXP(phi))
  END IF
  RETURN
END Function Tdelt   ! ------------------------------------------------------

!+
SUBROUTINE Trsfdx(t,b,tss,dts,delt,to,kr)
! ---------------------------------------------------------------------------
! PURPOSE - Transform/Runge-Kutta integration 
!   Developed by  R.A.Proeschel- 8/77 
!   Sucessive approximation version of 4/78 
! called by Stdtm
IMPLICIT NONE

  REAL,INTENT(OUT)::    t
  REAL,INTENT(IN)::     b
  REAL,INTENT(IN)::     tss
  REAL,INTENT(IN OUT):: dts
  REAL,INTENT(IN)::     delt
  REAL,INTENT(IN OUT):: to
  INTEGER,INTENT(IN)::  kr
  
  REAL:: dt
!----------------------------------------------------------------------------
  SELECT CASE(kr)
    CASE(1)
      to=t   ! only set to when kr==1
      dts=tdelt(1.0, 0.0, b, delt)
      t=to+tdelt(tss, to, b, delt/2.)
    CASE(2)
      dts=dts+2.0*tdelt(1.0, 0.0, b, delt)
      t=to+tdelt(tss, to, b, delt/2.)
    CASE(3)
      dt=tdelt(1.0, 0.0, b, delt)
      dts=dts+2.*dt
      t=to+(tss-to)*dt
    CASE(4)
      t=to+(tss-to)*(dts+tdelt(1.0, 0.0, b, delt))/6.
  END SELECT
  RETURN
END Subroutine Trsfdx   ! ---------------------------------------------------

END Module Subs   ! =========================================================

!+
PROGRAM SolarPowerArray
! ----------------------------------------------------------------------
! PURPOSE - Solar power array model
USE Subs
IMPLICIT NONE

!      COMMON /FTAC/C(25,25),CAP(25),Q(25),T(25)
 !                    T=initial TEMPERATURE OF ARRAY (R)
!      COMMON /ORBC/A,E,RA,PHI,OMEGA


!... parameters
  INTEGER,PARAMETER:: IN=1, OUT=2, DBG=3

  REAL,PARAMETER:: AEARTH = 4.90745E+11   ! semi-major axis (ft)
  REAL,PARAMETER:: BEARTH = 0.3   ! earth albedo
  REAL,PARAMETER:: EEARTH = 0.86  ! earth emittance  
  REAL,PARAMETER:: EE = 0.016718   ! eccentricity
  REAL,PARAMETER:: WS = 4.387E+30  ! weight (mass) of sun in pounds 
  REAL,PARAMETER:: WE = 1.317E+25  ! weight (mass) of earth in pounds
  REAL,PARAMETER:: RE = 2.08982E7  ! radius of earth
  REAL,PARAMETER:: RS = 2.28228E9  ! radius of sun
  REAL,PARAMETER:: SIGMA = 1.714E-9
  REAL,PARAMETER:: TSUN=10467.0    ! temperature of sun R

  CHARACTER(LEN=*),PARAMETER:: T202='(5X,"TIME",5X,'// &
   '"DIRECT  REFLECTED      TOTAL",7X,"TEMP",4X, "PWR GEN",6X,"EFFIC")'
  CHARACTER(LEN=*),PARAMETER:: T203= '(6X,"HR",9X,"KW",9X,"KW",9X,'// &
   '"KW",10X,"F",9X,"KW",8X,"PCT")'
  CHARACTER(LEN=*),PARAMETER:: T204= '(33X,"-BETA=",F6.2," DEG-")'

!... variables
  REAL:: a   ! semimajor axis (ft) of spacecraft orbit
  REAL:: a1  ! solar absorptance of cell side
  REAL:: a2  ! solar absorptance of back side
  REAL,DIMENSION(10):: aa
  REAL,DIMENSION(1)::  ad
  REAL,DIMENSION(520):: ae
  REAL:: alife  ! fraction of array design life
  REAL:: area   ! area of array (ft2)
  REAL,DIMENSION(520):: as

  REAL:: c2,c4
  REAL:: cap
  REAL:: dateUT   ! DATE OF THE YEAR (U.T.)
!                               EXAMPLE, 6.215=12 NOON ON JUNE 21 (U.T.)
  REAL:: dphi
  REAL:: dt
  REAL:: e   ! eccentricity of the spacecraft orbit

  REAL:: e1     ! emittance of cell side 
  REAL:: e2     ! emittance of back side

  INTEGER:: errCode
  REAL,DIMENSION(500):: et
  REAL:: fea,feb
  REAL,DIMENSION(520):: fes
  CHARACTER(LEN=132):: fileName
  REAL:: fsol

  INTEGER:: j
  INTEGER:: jrks    ! Runge-Kutta system index, in {1,2,3,4}
  INTEGER:: k

  INTEGER:: na   ! number of array elements
  INTEGER:: ne   ! number of earth latitude divisions
  INTEGER:: nea
  INTEGER:: nL   ! length of tables tml and pload 
  INTEGER:: ns   ! number of solar latitude divisions
  INTEGER:: nsa
  INTEGER:: nt   ! number of time steps per orbit


  REAL:: omega   ! argument of perigee of the spacecraft orbit

  REAL:: p,pe
  REAL:: phi    ! inclination of the spacecraft orbit
  REAL:: phimax,phimin
  REAL,DIMENSION(50):: pload ! table of load power level (KW) 
  REAL,DIMENSION(500):: ps
  REAL:: q
  REAL:: q2
  REAL,DIMENSION(500):: qarray
  REAL,DIMENSION(500):: qd,qr,qt
  REAL:: r
  REAL:: ra    ! right ascension of the spacecraft orbit

  REAL:: sbeta
  REAL:: sigae

  REAL:: t 
  REAL,DIMENSION(500):: ta
  REAL:: time
  CHARACTER(LEN=80):: title1,title2,title3
  REAL,DIMENSION(500):: tm
  REAL,DIMENSION(50):: tml   ! table of time for load power level  (hr)

  REAL,DIMENSION(100):: tp,th,tpe,the
  REAL,DIMENSION(3,1):: vd                    ! was XD(3),AD(1),VD(3)
  REAL,DIMENSION(3,10):: vnb
  REAL:: wsil   ! weight of fused silica (lb)
  REAL:: wpol   ! weight of polyimide substrate (lb)
  REAL:: wadh   ! weight of adhesive (lb)

  REAL:: x,y,z
  REAL,DIMENSION(3,10):: xa,vna  
  REAL,DIMENSION(3,1):: xd
  REAL,DIMENSION(3,520):: xe,vne  
  REAL,DIMENSION(3):: xeo,xo
!  REAL,DIMENSION(520):: xp,yp,zp
!  REAL,DIMENSION(500):: xr,yr,zr
  REAL,DIMENSION(3,520):: xs,vns
  REAL,DIMENSION(2):: xsol,ysol   ! ,zsol , never used

  NAMELIST /ARRAY/T,AREA,E1,E2,A1,A2,WSIL,WPOL,WADH,ALIFE
 
  NAMELIST /INPUT/dateUT,A,E,RA,PHI,OMEGA,NA,NE,NS,NT, &
        title1,title2,title3
  NAMELIST /LOAD/PLOAD,TML,NL

!----------------------------------------------------------------------------
  title1=' '
  title2=' '
  title3=' '
  c2=0.0
  c4=0.0
  cap=0.0
  q=0.0
  t=0.0
  jrks=1
      
!  t(2)=479.0      ! temperature of earth (R)
!  t(3)=10467.0    ! temperature of sun   (R) 
!  t(4)=4.97       ! temperature of cosmic background radiation (R)
     
  xeo(:)=0.0
  xo(:)=0.0

  DO
    WRITE(*,*) "Enter the name of the input file: "
    READ(*,'(A)') fileName
    IF (Len_Trim(fileName)==0) STOP
    OPEN(UNIT=IN, FILE=fileName, STATUS='OLD', &
      IOSTAT=errCode, ACTION='READ', POSITION='REWIND')
    IF (errCode==0) EXIT
    WRITE(*,*) "Unable to open this file. Try again."
  END DO

  ! output file
  OPEN(UNIT=OUT, FILE='solar.out', STATUS='REPLACE', &
    IOSTAT=errCode, ACTION='WRITE', POSITION='REWIND')
  IF (errCode==0) THEN
    WRITE(OUT,*) "Execution of SolPow with "//Trim(fileName)
  ELSE
    WRITE(*,*) "Unable to open output file"
    STOP
  END IF

  ! log file
  OPEN(UNIT=DBG, FILE='solar.log', STATUS='REPLACE', &
    IOSTAT=errCode, ACTION='WRITE', POSITION='REWIND')

  IF (errCode==0) THEN
    WRITE(DBG,*) "Execution of SolPow with "//Trim(fileName)
  ELSE
    WRITE(*,*) "Unable to open log file"
    STOP
  END IF

  READ(IN,ARRAY)

1  READ(IN,LOAD,IOSTAT=errCode)
  IF (errCode /= 0) STOP

  READ(IN,INPUT,IOSTAT=errCode)
  IF (errCode /= 0) STOP

  
! Determine orbital ephemerides

  CALL Ephem(AEARTH,EE,WS,pe,tpe,the)   ! ...earth ephemerides
  WRITE(DBG,*) "Earth Ephemerides, period=", pe
  WRITE(DBG,'(I4,2ES12.3)') (k,tpe(k),the(k),k=1,SIZE(tpe))

  CALL Ephem(a,e,we,p,tp,th)            ! ...solar array ephemerides
  WRITE(DBG,*) "Solar Array Ephemerides, period=", p
  WRITE(DBG,'(I4,2ES12.3)') (k,tp(k),th(k),k=1,SIZE(tp))

! Location of earth in heliocentric coordinates
  CALL Earth(dateUT,xeo(1),xeo(2),tpe,the,pe)   ! ... center
  WRITE(DBG,*) "Location of earth", xeo

! ... Surface elements seeing orbital track
  dphi=ACOS(RE/(a*(1.+e)))
  phimin=PI(0.5)-dphi
  phimax=PI(0.5)+dphi
  CALL Sphere2(xeo,phi,ra,re,xe,ae,vne,phimin,phimax,ne,nea)
  WRITE(DBG,*) "Sphere2 Results, ne,nea:", ne,nea
  WRITE(DBG, '(I4,7ES9.2)') (k,xe(:,k),ae(k),vne(:,k),k=1,ne)

!  Solar beta angle
  sbeta=Beta(xeo,phi,ra)
  WRITE(DBG,*) "Solar beta angle=", sbeta

! Solar model
  CALL Sphere(xo,rs,xs,as,vns,0.,pi(1.),ns,nsa)
  WRITE(DBG,*) "Sphere Results, ns,nsa:", ns,nsa
  WRITE(DBG, '(I4,7ES9.2)') (k,xs(:,k),as(k),vns(:,k),k=1,ns)

! Element shape factors for albedo calculation
  sigae=0.0
  DO j=1,nea
    xd(1,1)=xe(1,j)
    xd(2,1)=xe(2,j)
    xd(3,1)=xe(3,j)
    vd(1,1)=vne(1,j)
    vd(2,1)=vne(2,j)
    vd(3,1)=vne(3,j)
    ad(1)=ae(j)
    sigae=sigae+ad(1)
    fes(j)=Shfac(xd,ad,vd,1, xs,as,vns,nsa, xd,ad,vd,1)
  END DO
  WRITE(DBG,*) "Element shape factors"
  WRITE(DBG,'(5ES15.3)') fes(1:nea)
!... End of general setup. Now compute the temperature, etc.
! of the satellite as it travels thru one orbit.

  dt=p/nt
  j=1
  k=nt+1
  time=0.0
  jrks=1

!  Location of array in heliocentric coordinates
!  keep coming back here....
95  CALL Orbit(xeo(1),xeo(2),time,x,y,z,tp,th,p,a,e,ra,phi,omega)
  WRITE(DBG,'(A,I3,3ES11.3)') "Return from Orbit, jrks,x,y,z,=", jrks,x,y,z
  xa(1,1)=x
  xa(2,1)=y
  xa(3,1)=z
!  xr(j)=x   ! never used
!  yr(j)=y   ! never used
!  zr(j)=z   ! never used
  aa(1)=1.0
  r=SQRT(x*x+y*y+z*z)
  vna(1,1)=-x/r
  vna(2,1)=-y/r
  vna(3,1)=-z/r
  vnb(1:3,1) = -vna(1:3,1)
!  vnb(1,1)=0.-vna(1,1)
!  vnb(2,1)=0.-vna(2,1)
!  vnb(3,1)=0.-vna(3,1)
!
! Direct solar radiation
  fsol=Shfac(xa,aa,vna,na, xs,as,vns,nsa, xe,ae,vne,nea)
  qd(j)=fsol*SIGMA*area*TSUN**4

! Reflected radiation
  IF (qd(j) <= 0.0) THEN
    qr(j)=0.0
    q2=0.0
  ELSE
       ! Cell side
    qr(j)=Falbedo(xa,aa,vna,na,xe,ae,vne,fes,nea)*area*SIGMA*BEARTH*TSUN**4
       ! Back side
    q2=Falbedo(xa,aa,vnb,na,xe,ae,vne,fes,nea)*a2*area*SIGMA*BEARTH*TSUN**4
  END IF

! Terrrestial radiation
  fea=Shfac(xa,aa,vna,na, xe,ae,vne,nea, xd,ad,vd,1)
  feb=Shfac(xa,aa,vnb,na, xe,ae,vne,nea, xd,ad,vd,1)

! keep coming back here from below
96  WRITE(DBG,'(A,2F10.5)') "After 96, fea,feb=", fea,feb
  c2=Crad(e1,EEARTH,area,sigae,fea)+crad(e2,EEARTH,area,sigae,feb)

! Cosmic background radiation
  c4=Crad(e1,1.,area,1.,1.-fea-fsol) + Crad(e2,1.,area,1.,1.-feb)

  ps(j)=Linterp(tml(1:NL),pload(1:NL),time)
  et(j)=eta(t,alife)   ! Array efficiency
  qarray(j)=(qd(j)+qr(j))*et(j)/3412.76   ! Maximum array generation rate
  IF (ps(j) < qarray(j)) qarray(j)=ps(j)   ! Actual array generation rate

      ! Effective array heating rate
  q=(qd(j)+qr(j))*a1+q2-qarray(j)*3412.76-SIGMA*e1*area*fsol*t**4

      ! Heat capacity of array
  cap=wsil*Cpglas(t) + wpol*Cpkap(t) + wadh*0.35
  WRITE(DBG,'(A,3F12.5,I3)') 'heat capacity, eff=', cap, et(j), t, j

  IF (jrks==1) THEN
    tm(j)=time
    qd(j)=qd(j)/3412.76
    qr(j)=qr(j)/3412.76
    qt(j)=qd(j)+qr(j)
    et(j)=0.0
    IF (qt(j)/=0.0) et(j)=qarray(j)*100.0/qt(j)
    ta(j)=t-459.67          ! Rankine -> Fahrenheit
    IF (j>=nt+1) GO TO 100     ! this is the way out of this loop....
    j=j+1
  END IF

  CALL Stdtm(c2,c4,cap,q,t,dt,time,jrks)     ! changes time and jrks and t(1) in /FTAC/
  WRITE(DBG,'(A,4F12.6,I4)') "after Stdtm",dt,time,q,cap,jrks
  IF (jrks==1 .OR. jrks==3) THEN
    GO TO 96
  ELSE
    GO TO 95
  END IF

!!! END LOOP
100 WRITE(OUT,*) 'Converged'

! Output
  IF (Len_Trim(title1)>0)  WRITE(OUT,'(A)') Trim(title1)
  IF (Len_Trim(title2)>0)  WRITE(OUT,'(A)') Trim(title2)
  IF (Len_Trim(title3)>0)  WRITE(OUT,'(A)') Trim(title3)

  WRITE(OUT,T204) sbeta

  WRITE(OUT,*) "         number of earth elements=", nea
  WRITE(OUT,*) "         number of solar elements=", nsa
  WRITE(OUT,*) "         SOLAR IRRADIATION"
  WRITE(OUT,T202)
  WRITE(OUT,T203)

  WRITE(OUT,'(F9.4,6F11.4)') &
    (tm(j),qd(j),qr(j),qt(j),ta(j),qarray(j),et(j), j=1,nt+1)

! Earth surface arrays
!  xp(1:nea)=xe(1,1:nea)   ! never used
!  yp(1:nea)=xe(2,1:nea)   ! never used
!  zp(1:nea)=xe(3,1:nea)   ! never used

! Solar vector end points
  r=SQRT(xeo(1)*xeo(1) + xeo(2)*xeo(2))
  xsol(1)=xeo(1)
  xsol(2)=1.5*re*(0.-xsol(1)/r)+xsol(1)
  ysol(1)=xeo(2)
  ysol(2)=1.5*re*(0.-ysol(1)/r)+ysol(1)

  GO TO 1  ! look for more input; STOP on EOF

END Program SolarPowerArray   ! =============================================
