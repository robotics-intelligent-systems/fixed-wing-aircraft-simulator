

ORBITING SOLAR ARRAY SIMULATION MODEL                       \solararr\readme.txt

The files for this program are in the directory \solararr on the CD-ROM and in the
archive file solararr.zip that may be downloaded from the PDAS web site.

  readme.txt      this file of general description
  input.txt       instructions for preparing input
  notes.txt       some notes on my modernization of this program
  solpow.f90      the complete source code for the program
  case1.nml       a sample case
  case1.out       output produced by case1.nml
  case2.nml       another case with slightly different orbit
  case2.out       output produced by case2.nml
  msc18558.txt    the original program description from COSMIC
  original.src    the original copy of the source code (from COSMIC)


This is an interesting little program from the old COSMIC collection.
It isn't really aeronautical, but a modern aeronautical engineer 
should learn a bit about orbits and launch vehicles.
It defines a satellite in earth orbit at a specific day of the year and 
tracks an array of solar panels through a complete orbit around the
earth.
It works out the 3-D geometry of angles between the sun and satellite
and earth and comes up with an estimate of power.
It is so old that I am sure the conversion efficiencies and
weight estimates are out of date.
If you can send me some better numbers for these, I can make an
updated version someday.

This program was written at Rockwell International for NASA Marshall
Spaceflight Center. It was originally released as program MSC-18558.
The program offered here conforms to Fortran 90 standards. All subroutines
are in a module; all variables have declared types; all subroutine arguments 
have declared intent; and all code that used deprecated features of Fortran
95 have been recoded.

The student of astronautics can see the techniques for constructing an
ephemeris of an orbiting body, the calculation of black-body radiation from
the sun with corrections for reflection and shadowing of the earth, and the
radiation from the photoelectric array. The calculation of the temperature 
of the satellite is done by solution of the numerical solution of the 
differential equations of heat absorbtion and radiation.
