

VASCOMP II- V/STOL AIRCRAFT SIZING AND PERFORMANCE           /vascomp/readme.txt

The files for this program are in the directory vascomp 
  arc11433.txt    the original program description from COSMIC
  readme.txt      this file of general description
  original.src    the original copy of the source code (from COSMIC)
  vascomp.f90     the complete source code in modern Fortran
  case1.inp       input for a sample case
  case1.out       output for case1.inp


This program is a "work in progress" and is not ready for general release.
I have included it so those who have a special interest may see
the original code plus my modifications to date.
 
