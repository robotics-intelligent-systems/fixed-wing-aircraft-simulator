!!! changes 14 Dec 2008
! Rewrote all of the lines that called the COTAN intrinsic with
! appropriate calls to TAN.
!  When running with error checking enables, there are numerous error
!  messages in subroutine Climb concerning variables that are used
!  before they are set. I tried setting v11,v22,rct1,rct2,rcp1,rcp2
!  to 0.0 to avoid the messages. Looks as if tprop needed also.
! If you compile with Lahey LF95 without the -chk option, the program
! compiles successfully and runs the test case giving what looks
! like good answers. 
! Still dozens of messages about putting character data into reals.



!!!      PROGRAM MAIN(INPUT,OUTPUT,TAPE5=INPUT,TAPE6=OUTPUT)
      PROGRAM Main
!     ENTRY  MAIN    B-93    VASCOM  PROGRAM
!**** MEMBER NAME = B93TAMAN
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
      DIMENSION A0(3,15),A1(3,15),A2(3,15),BB0(16),BB1(16),BB2(16),     &
     & RJ3(10),CPOW3(10),ETAI3(10,10),RJ4(10),CPOW4(10),ETAI4(10,10),   &
     &AMACH(3),CLLL(15),CLGAM(16),CPOW33(20),CTI3(20),CTI4(20),         &
     & GMDD1(16),GAMD11(3,15)
      DIMENSION BNFUEL(10),BNLNG(10),BLKTIM(10)
      DIMENSION OO3(2,4)
!
!
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***         PAGE NO.24  ,LOC 2201 TO 2330 + SPACE(20)
!
      COMMON     GWIND(10) , GWP(10) , ATMIN7(10), CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10)  , TOWP(10), AN2M7(10) , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE N0.25  , LOC 2351 TO 2428  +  SPACE(22)
!
       COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6) ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(2450)   ! was 1
      COMMON/ODD/A0,A1,A2,BB0,BB1,BB2,RJ3,CPOW3,ETAI3,RJ4,CPOW4,        &
     & ETAI4,AMACH,CLLL,CLGAM,CPOW33,CTI3,CTI4,GMDD1,GAMD11
      COMMON/KAYDON/LL1,LL2,BHPRCR,BHPRTO
! **  KAYDON IS FOUND IN ENGSZ; VARIABLES ARE USED FOR PRINT PURPOSES
      COMMON/KAGS/ATGP(10),TINGP(10)
! **  KAGS IS PASSED TO PRFRP TO BE USED IN CALL ATMOS STATEMENT
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DATA OO3(1,2)/4H MAX/, OO3(2,2)/4HIMUM/
      DATA OO3(1,3)/4HMILI/, OO3(2,3)/4HTARY/
      DATA OO3(1,4)/4H NOR/, OO3(2,4)/4HMAL /
!
!     *** INITIALIZE  DATA
!     NOMINAL VALUES
!                        EVERY INPUT VAR. INIT. TO 0.
!                    *** EXCEPT CK1  THRU  CK21 TO 1.
!
!
!
      DO 2867 I=1,2450
 2867 DATA(I) = 0.0
      CK1 = 1.0
      CKFF = 1.0
      XMSMRT = 1.0
      SHPTO = 1.0
      CKRC = 2.0
      LL1 = 0
      LL2 = 0
      DO 21667  I=410,416
      DATA(I) = 1.0
      DATA(I+23) = 1.0
21667 DATA(I+49) = 1.0
      DATA(416)  = 0.0
      TOL=.01
      DLWFN1= 0.
      WG1   = 0.
      KKKONT = 0
      DO 1914 I = 1,10
      BNFUEL(I) = 0.0
      BNLNG(I) = 0.0
 1914 BLKTIM(I) = 0.0
 1771 CONTINUE
!     *** AT THIS POINT (RE)INITIALIZE AND THEN CALL LOADET
      WGA = SAVE(29)
      CTSIG = SAVE(30)
      IGOOF = 0
      IIIX = 0
      IAERO=0
      OPTIND = OPTWAS
      NEXT = 0
      IST = 0
      OPTOPT = 0.0
      INOPTH = 0
      SAVE(24)=1.
      DTOR     = .01745329
      RTOD     =57.2957795

!!! The following lines disabled 8 Nov 2005   RLC
!!!      CALL ERRSET(207,256,0,0)
!!!      CALL ERRSET(209,256,0,0)
!!!      CALL ERRSET(210,256,0,0)
!!!      CALL ERRSET(253,256,0,0)
!!!      CALL ERRSET(258,256,0,0)



      CALL LOADET (DATA,1)
      SAVE(29) = WGA
      SAVE(30) = CTSIG
      IF(CYCPRP.EQ.CYCPRL) GO TO 2868
      WRITE(6,2869) CYCPRP,CYCPRL
 2869 FORMAT(9X,'*** ERROR  THE USER REQUESTED PRIMARY ENGINE NO.',F9.3/ &
     &  9X,'       BUT THE INPUT DECK WAS SET UP TO USE NO.',F9.3)
      GO TO 1771
 2868 IF ((DNITFL.EQ.0.0).OR.(CYCLFP.EQ.CYCLFL)) GO TO 2872
      WRITE(6,2871) CYCLFP,CYCLFL
 2871 FORMAT(9X,'*** ERROR  THE USER REQUESTED LIFT ENGINE NO.',F9.3/    &
     &  9X,'       BUT THE INPUT DECK WAS SET UP TO USE NO.',F9.3)
      GO TO 1771
 2872 IF ((ETAIND.NE.1.0).OR.(CYPROP.EQ.PROPCY)) GO TO 2870
      WRITE(6,2873) CYPROP,PROPCY
 2873 FORMAT(9X,'*** ERROR  THE USER REQUESTED PROP TABLE NO.',F9.3/     &
     &  9X,'       BUT THE INPUT DECK WAS SET UP TO USE NO.',F9.3)
      GO TO 1771
 2870 IF((SGTIND(1).NE.0.).AND.(SGTIND(1).NE.100.).AND.(SGTIND(1).NE.5.)&
     &         )                                            GO TO 100
      IF (SGTIND(1).NE.5.)  GO TO  102
      IF((DESIND(1).EQ.2.).OR.(DESIND(1).GE.4.))            GO TO 100
  102 WRITE(6,1003)
 1003 FORMAT(9X,'ERROR *** THE FIRST SEGMENT INDICATOR OF A MISSION'/   &
     & 9X,'    CANNOT BE 0.,100.OR 5.(DESCIND=1,3) SEE USERS MANUAL')
                                                           GO TO  1771
  100 DO 11852   I=2,50
      KARA=0
      IF (SGTIND(I).EQ.100.)                                GO TO 11853
      IF(.NOT.((SGTIND(I).EQ.0.).AND.((SGTIND(I+1).EQ.0.).OR.(SGTIND(I+1&
     &).EQ.5.))))                                          GO TO    48
      JARA = I+1
      DO  4848  LARA=1,JARA
 4848 IF(SGTIND(LARA).EQ.5.) KARA = KARA + 1
      IF((DESIND(KARA).EQ.2.).OR.(DESIND(KARA).GE.4.))      GO TO 48
      WRITE(6,1003)
                                                           GO TO  1771
   48 IF(SGTIND(I).NE.5.)                                  GO TO 11852
      JARA = I
      DO 4849   LARA=1,JARA
 4849 IF(SGTIND(LARA).EQ.5.) KARA = KARA + 1
      IF((DESIND(KARA).EQ.2.).OR.(DESIND(KARA).GE.4.))      GO TO 11852
      IF(SGTIND(I-1).EQ.4.)                                GO TO 11852
      WRITE (6,1004)
 1004 FORMAT(9X,51H*** ERROR DESCENTS 1&3 MUST BE PRECEDED BY A CRUISE)
                                                           GO TO  1771
11852 END DO
11853 CONTINUE
      DO  500  I=1,10
      ATGP(I) = ATMIN7(I)
      ATMIND(I)    = ATMIN1(I)
      ATMIND(I+10) = ATMIN2(I)
      ATMIND(I+20) = ATMIN3(I)
      ATMIND(I+30) = ATMIN4(I)
      ATMIND(I+40) = ATMIN5(I)
      ATMIND(I+50) = ATMIN6(I)
      TIN(I)       = TIN1(I)
      TIN(I+10)    = TIN2(I)
      TIN(I+20)    = TIN3(I)
      TIN(I+30)    = TIN4(I)
      TIN(I+40)    = TIN5(I)
      TIN(I+50)    = TIN6(I)
      TINGP(I) = TIN7(I)
      DELH(I)      = DELH3(I)
      DELH(I+10)   = DELH5(I)
  500 END DO
      IF (ETAIND.NE.2.0) GO TO 67
      DO 69 I=1,16
      GMDD1(I) = BB0(I)+CLEYE*(BB1(I) + CLEYE*BB2(I))
   69 END DO
      DO 77 J=1,3
      DO 77 K=1,15
      GAMD11(J,K) = A0(J,K) + CLEYE*(A1(J,K) + CLEYE*A2(J,K))
   77 CONTINUE
   67 RLMC4 = DLMC4*DTOR
      AR   =DAM2
      CBARD=DAM3
      WGS  =DAM4
      ELF  =DAM5
      SF   =DAM6
      BHPP =DAM7
      TP   =DAM8
      TL   =DAM9
      SEE  =DAM10
      SHT = AAW11
      SVT = AAW12
      WG = WG00
      H  = H00
      R  = R00
      ST = ST00
      V  = 0.
      OPTWAS=OPTIND
      NETAP4=ETAP4N
      NTH    = THN       +.0001
      NTCL   = TCLN      +.0001
      NTCL2  = TCL2N     +.0001
      NTEM   = TENN      +.0001
      IWD   =  WDTIND    +.0001
      IN1   =  AN1IND    +.0001
      IN2   =  AN2IND    +.0001
      IN3   =  AN3IND    +.0001
      IQ    =  QIND      +.0001
      IRN   =  RNOIND    +.0001
      NMS   =  UMS       +.0001
      NTS   =  UNTS      +.0001
      NMW   =  UMW       +.0001
      NTW   =  UNTW      +.0001
      NM1   =  UNM1      +.0001
      NT1   =  UNT1      +.0001
      NM2   =  UNM2      +.0001
      NT2   =  UNT2      +.0001
      LWD   =  VWDIND    +.0001
      LN1   =  VN1IND    +.0001
      LN2   =  VN2IND    +.0001
      INDDRG = DRGIND    +.0001
      INDESZ = ESZIND    +.0001
      INDENG = ENGIND    +.0001
      INDLFT = DNITFL    +.0001
      KPRINT = TNIRPK + 0.01
      IVLMT = VLMIND + 0.01
      NGLIND = DNILGN + 0.01
      IOPTH = HOPTIN + 0.01
      LAVIND = DNIVAL + 0.01
      KKROPT = SPACE1(23) + 0.01
      WG1 = WG
      PI = 3.14159
      IF (OPTIND.EQ.3.0) GO TO 16
      IF(OPTIND.EQ.2.)                                     GO TO     1
      OPTION = 1.0
    2 CALL SIZTR(IST)
      CALL AERO(IAERO)
      IF(FIXIND.NE.1.0) GO TO 6666
      CALL ENGSZ
      IF(IST/2.NE.IIIX+1) GO TO 2
 6666 IF(NEXT.NE.0) GO TO 55
      WRITE(6,919) WG1,WFA,WFR
  919 FORMAT(T10,'WG = ',E12.6,2X,'WFA = ',E12.6,2X,'WFR = ',E12.6)
      IF(FIXIND.EQ.1.0) GO TO 4971
! **  BELOW IS THE CODE TO SIZE TRANSMISSIONS WITH FIXED SIZE ENGINES
! **  IN SIZING XMSN WITH FIXIND = 0 AND XMSND = 1, THE INPUT LOCATIONS
!     FOR ENGINE SIZING MUST BE FILLED OUT. THIS IS NECESSARY IN ORDER
!     FOR THE PROGRAM TO CALCULATE THE POWERS REQUIRED FOR TAKEOFF AND
!     CRUISE
      IF(ENGIND.EQ.1.0) GO TO 4971
      IF(ENGIND.EQ.2.0) GO TO 4973
! **  IF ENGIND = 2.0, THE TRANSMISSION CAN BE SIZED AT A SPECIFIED
!     FRACTION OF INSTALLED POWER (XMSND = 0), OR IF XMSND = 1, THE XMSN
!     WILL BE SIZED FOR THE TAKEOFF POWER REQUIRED
      IF(XMSND.EQ.1.0) GO TO 4972
      BHPXMS = BHPP * XMSMRT
      ANXMAX = AN2TO
      LL1 = 1
! **  LL1 IS AN INDICATOR USED FOR PRINTOUT PURPOSES IN PROPULSION DATA
! **  IT WAS ASSUMED THAT INSTALLED POWER WAS MORE CRITICAL AT HOVER
      GO TO 4971
 4972 HH = HES
      YALE22 = 1.0
      CALL ATMOS(HH,YALE22,TINY)
      DSHPRC = WG * VRCRC/(33000. * CKRC)
      BHPRTO=SQRT(SENE**3)*WG*SQRT(WGA)/(550.*S2RHO*SHPTO*ETAT*ETAP2)   &
     &   + (DSHPAC + DSHPRC)/(SHPTO)
      HH = HC
      CALL ATMOS(HH,YALE22,ATMIY)
      V = VC
      Q = 1.42636*RHO*V**2
      CL = WG/(SW*Q)
      EM = V/SA
      CALL DRAG
      P = AN2CR * AN2CR * VT
      CALL POWAVL(TMAX,0.0)
      BLP = SHPA
      IF(ETAIND.EQ.0.0) GO TO 2003
      TPROP = CD * SW * Q
      IF(ETAIND.EQ.2.0) GO TO 91
      REALJ = 1.689 * PI * V/P
      CCT=TPROP*WGA*PI**3/(0.009507*SIGMA*P**2*WG)
      IF(ETAIND.NE.3.0) GO TO 46
      EM = V/SA
      TOAD = TPROP * WGA/(WG*DELTA)
      HPADTH=XLKUP(EM,TOAD,XJP,NOXJP,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
      CCP = HPADTH * 5634966.0/(P/STHETA)**3
      GO TO 47
   46 CCP = XLKUP(REALJ,CCT,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
   47 IF(IX.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1009)
      IF(IX.NE.0.AND.ETAIND.EQ.3.0) WRITE(6,1005)
      IF(IY.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1010)
      IF(IY.NE.0.AND.ETAIND.EQ.3.0) WRITE(6,1006)
 1009 FORMAT(9X,'THIS ERROR IS IN THE J PART OF THE PROPELLER POWER',   &
     &' COEFFICIENT TABLE DUE TO FIXED SIZE ENGINES - SUBROUTINE MAIN')
 1005 FORMAT(9X,'THIS ERROR IS IN THE M PART OF THE FAN COEFFICIENT',   &
     &' TABLE DUE TO FIXED SIZE ENGINES - SUBROUTINE MAIN')
 1010 FORMAT(9X,'THIS ERROR IS IN THE CT PART OF THE PROPELLER POWER',  &
     &' COEFFICIENT TABLE DUE TO FIXED SIZE ENGINES - SUBROUTINE MAIN')
 1006 FORMAT(9X,'THIS ERROR IS IN THE FN PART OF THE FAN COEFFICIENT',  &
     &' TABLE DUE TO FIXED SIZE ENGINES - SUBROUTINE MAIN')
      ETAP = REALJ * CCT/CCP
      ETAP4 = ETAP
      GO TO 2010
   91 ITER = 0
      ETA = 0.85
      TPROP1 = TPROP
      BHPACR = (CD*SW*Q*V/(325.8*ETAT) + DSHPAC)/(DELTA*STHETA*BLP)
      BHPP4 = BHPP
   92 ITER = ITER + 1
      BHPP = BHPACR/ETA
      ETA1 = ETA
      TP1 = TPROP
      CALL THRUST(TPROP,1.0,ETAP)
      ETA2 = ETAP
      TP2 = TPROP
      IF(ABS(1.0 - TPROP/TPROP1).LE.0.005) GO TO 93
      ETA = ETAP
      IF(ITER.GT.1) ETA = ETA2 + (TPROP1 - TP2)*(ETA2-ETA1)/(TP2-TP1)
      IF(ITER.GT.25) GO TO 84
      GO TO 92
   93 BHPRCR = BHPP * DELTA * STHETA * BLP
      BHPP = BHPP4
      GO TO 2004
 2003 ETAP4 = XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      IF(M.NE.0) WRITE(6,1001)
 1001 FORMAT(22X,'THIS ERROR IS IN THE ETAP4-M TABLE ASSOCIATED WITH FIX&
     &ED SIZE ENGINES - SUBROUTINE MAIN')
 2010 BHPRCR = CD*SW*Q*V/(325.8*ETAT*ETAP4) + DSHPAC
 2004 BHPXMS = BHPRTO * XMSMRT
      ANXMAX = AN2TO
      LL1 = 1
      IF(BHPRCR/AN2CR.LE.BHPRTO/AN2TO) GO TO 4971
      BHPXMS = BHPRCR * XMSMRT
      ANXMAX = AN2CR
      LL2 = 2
      GO TO 4971
 4973 HH = HES
      YALE22 = 1.0
      CALL ATMOS(HH,YALE22,TINY)
      TPXMSN = TP * XMSMRT
      ANXMAX = AN2TO
      LL1 = 1
      IF(XMSND.EQ.0.0) GO TO 4971
      TPRTO = BETA*SENE**1.5*WG*WGA**0.5/(550.*ETAP2*ETAT*SHPTO*S2RHO)  &
     &  + BETA*(DSHPAC + DSHPRC)/SHPTO
      TPXMSN = XMSMRT * TPRTO
      LL1 = 1
 4971 CONTINUE
      CALL WGHTR
    3 IF(OPTIND.EQ.0) GO TO 55
      H   = H00
      ST  = ST00
      R   = R00
      V  = 0.
      IF (OPTOPT.EQ.1.0) OPTIND = 1.0
      CALL PRFRM
      IF(NEXT.NE.0)                                        GO TO    55
!     *** IF OPTIND.EQ.2.,PRINT IN EACH MISSION SUBTOUTINE
!     SAVE(25) = SGTIND COMING OUT OF PRFRM ALWAYS = 0. OR 100.
      IF (OPTOPT.EQ.1.0) GO TO 33
      IF (OPTIND.EQ.2.0.AND.SAVE(25).EQ.100.) GO TO 1921
      IF (OPTIND.EQ.2.0.AND.OPTOPT.EQ.0.0) GO TO 506
   33 IIIX = IIIX + 1
      IF(WFR.GT.0.)                                        GO TO  4899
      NEXT = 1
      WRITE(6,501)
  501 FORMAT(/9X, '***ERROR  WFR   ',                                    &
     & 'WEIGHT OF FUEL REQUIRED IS LESS THAN OR EQUAL TO ZERO.'/)
                                                            GO TO    55
 4899 IF((ABS(1.-WFA/WFR).LE.TOL).OR.(IIIX.GE.25))          GO TO     5
      DLWFN = WFA - WFR
      IF (IIIX.EQ.1) GO TO 319
      PD    = (DLWFN -DLWFN1) / (WG - WG1)
  319 IF (IIIX.EQ.1) PD = 0.7
      WG1   = WG
      WG    = WG - DLWFN / PD
      DLWFN1= DLWFN
      IF (OPTOPT.EQ.1.0) GO TO 19
      IF (WG.GT.0.)                                         GO TO     2
      IF (IGOOF.EQ.0)                                       GO TO 502
      WRITE(6,503)
  503 FORMAT(/9X,'***ERROR  FUEL AVAILABLE AND FUEL REQUIRED'/            &
     & ' DO NOT CONVERGE AT A POSITIVE GROSS WEIGHT')
      NEXT = 1
                                                           GO TO    55
  502 WG  = WG00 /2.
      IGOOF = 1
                                                           GO TO     2
   16 WOVSS = WGS
      WOVAS = WGA
      GO TO 17
    1 OPTION = 2.0
      GO TO 34
   19 WGS = WOVSS*(WG/WG00)
      WGA = WOVAS*(WG/WG00)
   17 WFA = WG - OWE1 - WPL
      WRITE(6,508) WG,WGS,WGA,WG00,WOVSS,WOVAS,WFA,WFR
  508 FORMAT(10X,'WG    = ',E14.7,5X,'WGS   = ',E14.7/10X,              &
     &  'WGA   = ',E14.7,5X,'WG00  = ',E14.7/10X,                       &
     &  'WOVSS = ',E14.7,5X,'WOVAS = ',E14.7/10X,                       &
     &  'WFA   = ',E14.7,5X,'WFR   = ',E14.7)
      OPTIND = 2.0
      OPTOPT = 1.0
   34 CALL AERO(IAERO)
      DEBAR = XI4 * ((BHPP/ENP)**.5)
                                                           GO TO     3
    5 IF (OPTOPT.EQ.0.0) GO TO 18
      OPTOPT = 0.0
      OPTIND = 2.0
      GO TO 3
   18 OPTIND = 2.0
      IF (IIIX.GE.25) WRITE(6,504) WFA,WFR,TOL
  504 FORMAT(/2X,'**********  THIS AIRCRAFT HAS NOT CONVERGED AFTER 25 ATTEMPTS.'/   &
     & ' THE WEIGHT OF FUEL AVAILABLE (WFA) = ',E14.7/                                &
     & 14X,'THE WEIGHT OF FUEL REQUIRED (WFR) = ',E14.7/                              &
     & 5X,'(WFA) MUST BE WITHIN ',F5.4, ' OF (WFR) FOR THE AIRCRAFT'/                 &
     & 14X,'TO CONVERGE. THIS TOLERANCE IS SET IN THE MAIN PROGRAM'/                  &
     & ' UNDER THE VARIABLE NAME TOL.')
   84 IF(ITER.GT.25) NEXT = 1
      IF(IIIX.GE.25) NEXT = 1
   55 WE = WP + WFC + WST + WFE
      OWE= WE + WFUL
      IF(FIXIND.EQ.0.0.AND.NEXT.EQ.1) WRITE(6,3254)
 3254 FORMAT(/22X,'THE NUMBER OF ITERATIONS FOR TRANSMISSION SIZING ',  &
     &'FOR FIXED SIZE ENGINES EXCEEDED 25'/22X,'SUBROUTINE MAIN, CASE', &
     &' TERMINATED')
      IF(NEXT.NE.0) WRITE(6,555)
  555 FORMAT(//9X,'THE PRECEEDING ERROR HAS CAUSED THIS CASE TO TERMINATE.'/ &
     & ' THE ERROR MESSAGE SHOULD INDICATE AT WHAT POINT IN THE PROGRAM'/     &
     & 5X,'THE ERROR OCCURRED.  THE FOLLOWING INFORMATION, ALONG WITH A'/    &
     & ' FLOW CHART, SHOULD ENABLE AN ASTUTE USER TO FIND HIS ERROR.'//       &
     & ' THE FOLLOWING ARE THE CURRENT VALUES CALCULATED IN SIZING'/         &
     & ' THEY ARE BY NO MEANS THE FINAL VALUES BUT RATHER THE VALUES'/       &
     & ' BEING USED AT THE TIME THE ERROR OCCURRED.' )
      IF (OPTWAS.EQ.3.0) GO TO 23
      GO TO 24
   23 WG = WG00
      WGS = WOVSS
      WGA = WOVAS
   24 CALL LOADET(SS,2)
      IF (OPTWAS.EQ.2.0.OR.OPTWAS.EQ.3.0) GO TO 5555
      IF (KKROPT.NE.0) WGSAVE = WG
      WRITE(6,9901) IIIX, WG, ELF, SWF ,SF
 9901 FORMAT(///7X40HS I Z E  D A T A  THIS RUN CONVERGED IN ,I2,11H ITE&
     &RATIONS//41X14HGROSS WEIGHT =,F8.0,4H  LB//16X,8HFUSELAGE/20X2HLF1&
     &3X,6HLENGTH29X,F10.1,5X2HFT/20X2HWF13X5HWIDTH30X,F10.1,5X2HFT/    &
     &                       20X2HSF13X11HWETTED AREA24X,F10.0,5X4HSQFT)
      WRITE(6,9902) AR, SW, B, CBARW, DLMC4, SLM, TCR, TCT, WGS
 9902 FORMAT(/16X4HWING/20X2HAR13X12HASPECT RATIO23XF10.2,5X/20X2HSW13X4&
     &HAREA31X,F10.1,5X4HSQFT/20X1HB14X4HSPAN31X,F10.1,5X2HFT/          &
     &20X 5HCBARW10X16HGEOM. MEAN CHORD19X,F10.1,5X2HFT/                &
     &20X10HLAMBDA C/45X19HQUARTER CHORD SWEEP16X,F10.1,5X3HDEG/        &
     &20X10HLAMBDA    5X19HTAPER RATIO        16X,F10.3,5X/             &
     &20X10H(T/C)R    5X19HROOT THICKNESS     16X,F10.3,5X/             &
     &20X10H(T/C)T    5X19HTIP  THICKNESS     16X,F10.3,5X/             &
     &20X10HWG/SW     5X19HWING LOADING       16X,F10.1,5X7HLB/SQFT)
      IF(WDMIND.NE.0..OR.ENGIND.NE.1.)WRITE(6,9903)CBARD
 9903 FORMAT(20X9HC BAR / D6X23HMEAN CHORD / PROP. DIA.12X,F10.3)
      WRITE(6,9904) ARHT,SHT,BHT,CBARHT,TCHT
 9904 FORMAT(/16X,9HHOR. TAIL/                                          &
     &20X10HARHT      5X17HASPECT RATIO     18X,F10.2/                  &
     &20X10HSHT       5X17HAREA             18X,F10.1,5X4HSQFT/         &
     &20X10HBHT       5X17HSPAN             18X,F10.1,5X2HFT/           &
     &20X10HCBARHT    5X17HMEAN CHORD       18X,F10.1,5X2HFT/           &
     &20X10H(T/C)HT   5X17HTHICKNESS / CHORD18X,F10.3)
      IF (HTIND.EQ.1.) WRITE(6,9804) ELTH
 9804 FORMAT(20X10HLTH       5X17HMOMENT ARM       18X,F10.1,5X2HFT)
      WRITE(6,9905) ARVT,SVT,BVT,CBARVT,TCVT
 9905 FORMAT(/16X10HVERT. TAIL/                                         &
     &20X10HARVT      5X17HASPECT RATIO     18X,F10.2/                  &
     &20X10HSVT       5X17HAREA             18X,F10.1,5X4HSQFT/         &
     &20X10HBVT       5X17HSPAN             18X,F10.1,5X2HFT/           &
     &20X10HCBARVT    5X17HMEAN CHORD       18X,F10.1,5X2HFT/           &
     &20X10H(T/C)VT   5X17HTHICKNESS / CHORD18X,F10.3)
      IF(VTIND.EQ.1.) WRITE(6,9805) ELTV
 9805 FORMAT(20X10HLTV       5X17HMOMENT ARM       18X,F10.1,5X2HFT)
      WRITE(6,9806) ELN,DBARN, SN
 9806 FORMAT(//16X20HPRIMARY ENG. NACELLE/                              &
     &20X10HLN        5X17HLENGTH           18X,F10.1,5X2HFT/           &
     &20X10HDBARN     5X17HMEAN DIAMETER    18X,F10.1,5X2HFT/           &
     &20X10HSN        5X17HWETTED AREA      18X,F10.1,5X4HSQRT//        &
     &16X17HLIFT ENG. NACELLE)
      IF(DNITFL.EQ.1.) GO TO 22367
      WRITE(6,9906)
 9906 FORMAT(35X31HNO LIFT PROPULSION SELECTED    )
      GO TO 22368
22367 ELNG=DBARLN*(ENL/ENC+EPSLON)
      WRITE(6,9907) ELNG,ELLN,DBARLN,SLN
 9907 FORMAT(20X3HLNG12X17HLENGTH           18X,F10.1,5X2HFT/           &
     &20X3HLLN12X17HDEPTH            18X,F10.1,5X2HFT/                  &
     &20X10HDBARLN    5X17HMEAN DIAMETER    18X,F10.0,5X2HFT/           &
     &20X10HSLN       5X17HWETTED AREA      18X,F10.0,5X4HSQFT)
22368 IF(ENGIND.EQ.1.) GO TO 22369
      WRITE(6,9908) D,SIGRP,WGA,CTSIG,ENR,BLDN
 9908 FORMAT(/16X9HPROPELLER/                                           &
     &20X10HD         5X25HDIAMETER                 10X,F10.1,5X2HFT/   &
     &20X10HSIGMA R/P 5X25HSOLIDITY                 10X,F10.3/          &
     &20X10HWG/A      5X25HDISC LOADING             10X,F10.1,5X7HLB/SQF&
     &T/20X,8HCT/SIGMA7X25HTHRUST COEFF. / SOLIDITY 10X,F10.3/          &
     &20X,2HNR13X17HNO. OF PROPELLERS,18X,F10.3/                        &
     &20X10HNO. BLADES,5X18HNO. OF BLADES/PROP,17X,F10.3)
      GO TO 22370
22369 WRITE(6,9909)
 9909 FORMAT(/16X9HPROPELLER//35X30HNO PROPELLER ON THIS AIRCRAFT )
22370 IF (FDMIND.NE.2.0) GO TO 9984
      RNLAVS = NLAVS
      IF (LAVIND.EQ.1) RNLAVS = ANLAVS
      CALL LOADET(SS,2)
      WRITE(6,9971)
 9971 FORMAT(///7X,41HP A S S E N G E R   S I Z I N G   D A T A//       &
     & 37X31HTOURIST             FIRST CLASS/37X31H-------             -&
     &----------)
      WRITE(6,9972) ANPXT,ANPX1,ANABT,ANAB1,ANISLT,ANISL1,              &
     & WSEATT,WSEAT1,PSEATT,PSEAT1,WAISLT,WAISL1
 9972 FORMAT(20X,17HNO. OF PASS.     ,F4.0,16X,F4.0/                    &
     &       20X,18HNO. ABREAST       ,F3.0,17X,F3.0/                   &
     &       20X,18HNO.OF AISLES      ,F3.0,17X,F3.0/                   &
     &       20X,18HUNIT SEAT WIDTH   ,F3.0,4H IN.,13X,F3.0,4H IN./     &
     &       20X,18HSEAT PITCH        ,F3.0,4H IN.,13X,F3.0,4H IN./     &
     &       20X,18HAISLE WIDTH       ,F3.0,4H IN.,13X,F3.0,4H IN.//)
      WRITE(6,9973) RNLAVS
 9973 FORMAT(24X,26HNUMBER OF LAVATORIES      ,F5.2)
      WRITE(6,9974) AGALLY
 9974 FORMAT(24X,25HGALLEY AREA              ,F5.1,8H SQ. FT.)
      WRITE(6,9976) ACL
 9976 FORMAT(24X,25HCLOSET AREA              ,F5.1,8H SQ. FT.)
      IF (PREC1.EQ.1) GO TO 9979
      WRITE(6,9977) DCDC
 9977 FORMAT(24X,25HCABIN DIAMETER           ,F5.1,33H IN.     *** FIRST&
     & CLASS CRITICAL)
      WRITE(6,9978) DBDB
 9978 FORMAT(24X,25HBODY DIAMETER            ,F5.1,33H IN.     *** FIRST&
     & CLASS CRITICAL//)
      GO TO 9980
 9979 WRITE(6,9981) DCDC
 9981 FORMAT(24X,25HCABIN DIAMETER           ,F5.1,35H IN.     *** TOURI&
     &ST CLASS CRITICAL)
      WRITE(6,9982) DBDB
 9982 FORMAT(24X,25HBODY DIAMETER            ,F5.1,35H IN.     *** TOURI&
     &ST CLASS CRITICAL//)
 9980 WRITE(6,9983) ELP,ELT,ELC,ELF
 9983 FORMAT(24X,25HNOSE SECTION LENGTH      ,F5.1,4H FT./              &
     &       24X,25HTAIL SECTION LENGTH      ,F5.1,4H FT./              &
     &       24X,25HCONST. DIA. LENGTH       ,F5.1,4H FT./              &
     &     22X,27HTOTAL FUSELAGE LENGTH      ,F5.1,4H FT.)
 9984 CALL LOADET(SS,2)
      WRITE(6,9910) GLF
 9910 FORMAT(///7X32HW E I G H T S  D A T A    IN LBS//                 &
     &20X3HGLF12X19HGUST LOAD    FACTOR15X,F10.3)
      WRITE(6,9912) WW,WHT,WVT,WB,WLG,WLES,WPES,DELWST,WST
 9912 FORMAT(/16X16HSTRUCTURES GROUP/                                   &
     &20X10HK8  WW    5X35HWING                               ,F10.0/   &
     &20X10HK9  WHT   5X35HHOR. TAIL                          ,F10.0/   &
     &20X10HK10 WVT   5X35HVERT. TAIL                         ,F10.0/   &
     &20X10HK11 WB    5X35HFUSELAGE                           ,F10.0/   &
     &20X10HK12 WLG   5X35HLANDING GEAR                       ,F10.0/   &
     &20X10HK13 WLES  5X35HLIFT ENGINE SECTION                ,F10.0/   &
     &20X10HK14 WPES  5X35HPRIMARY ENGINE SECTION             ,F10.0/   &
     &20X10HDELTA WST 5X35HSTRUCTURE WEIGHT INCREMENT         ,F10.0/   &
     &20X10HWST       5X35HTOTAL STRUCTURE WEIGHT             ,F10.0)
      WRITE(6,9911) WRP,WDS,WEL,WEP,WLEI,WPEI,WFSS,DELWP,WP
 9911 FORMAT(/16X16HPROPULSION GROUP/                                   &
     &20X10HK2  WR/P  5X35HROTOR OR PROP                      ,F10.0/   &
     &20X10HK3  WDS   5X35HDRIVE SYSTEM                       ,F10.0/   &
     &20X10HK4  WEL   5X35HLIFT  ENGINES                      ,F10.0/   &
     &20X10HK5  WEP   5X35HPRIMARY ENGINES                    ,F10.0/   &
     &20X10HK6  WLEI  5X35HLIFT ENGINE INSTALLATION           ,F10.0/   &
     &20X10HK7  WPEI  5X35HPRIMARY ENGINE INSTALLATION        ,F10.0/   &
     &20X10HK21 WFS   5X35HFUEL SYSTEM                        ,F10.0/   &
     &20X10HDELTA WP  5X35HPROPULSION GROUP WEIGHT INCREMENT  ,F10.0/   &
     &20X10HWP        5X35HTOTAL PROPULSION GROUP WEIGHT      ,F10.0)
      WRITE(6,9913) WCC,WUC,WH,WCFW,WSAS,WTM,DELWF2,WFC
 9913 FORMAT(/16X21HFLIGHT CONTROLS GROUP/                              &
     &20X10HK15 WCC   5X25HCOCKPIT CONTROLS         10X,F10.0/          &
     &20X10HK16 WUC   5X25HUPPER   CONTROLS         10X,F10.0/          &
     &20X10HK17 WH    5X25HHYDRAULICS               10X,F10.0/          &
     &20X10HK18 WFW   5X25HFIXED WING CONTROLS      10X,F10.0/          &
     &20X10HK19 WSAS  5X25HSAS                      10X,F10.0/          &
     &20X10HK20 WTM   5X25HTILT MECHANISM           10X,F10.0/          &
     &20X10HDELTA WFC 5X25HCONTROL WEIGHT INCREMENT 10X,F10.0/          &
     &20X10HWFC       5X25HTOTAL CONTROL WEIGHT     10X,F10.0)
      WRITE(6,511)    WFE
  511 FORMAT(/16X3HWFE16X25HWEIGHT OF FIXED EQUIPMENT10X,F10.0)
      WRITE(6,9914) WE,WFUL,OWE,WPL,WFA,WFW,WG
 9914 FORMAT(/16X2HWE17X12HWEIGHT EMPTY23X,F10.0//                      &
     &        16X4HWFUL15X17HFIXED USEFUL LOAD18X,F10.0//               &
     &        16X3HOWE16X22HOPERATING WEIGHT EMPTY13X,F10.0//           &
     &        16X3HWPL16X7HPAYLOAD28X,F10.0//                           &
     &16X5H(WF)A14X4HFUEL31X,F10.0,10X,5H(WF)W,10X,F10.0//              &
     &        16X2HWG17X12HGROSS WEIGHT23X,F10.0)
      CALL LOADET(SS,2)
      WRITE(6,9920) CYCPRP
 9920 FORMAT(///7X28HP R O P U L S I O N  D A T A/                      &
     &      16X28HPRIMARY PROPULSION CYCLE NO.,F9.3)
      IF(CYCPRP.GE.1..AND.CYCPRP.LE.9.) WRITE(6,9921)
      IF(CYCPRP.GE.10..AND.CYCPRP.LE.18.)WRITE(6,9922)
      IF(CYCPRP.GE.19..AND.CYCPRP.LE.45.)WRITE(6,9923)
 9921 FORMAT(24X17HTURBOSHAFT ENGINE)
 9922 FORMAT(24X17HTURBOJET   ENGINE)
 9923 FORMAT(24X17HTURBOFAN   ENGINE)
      WRITE(6,9924) ENP
 9924 FORMAT(/24X,F3.0,2X,7HENGINES/)
      IF(ENGIND.NE.0.) WRITE(6,9925) TP
      IF(ENGIND.NE.1.) WRITE(6,9926) BHPP
 9925 FORMAT(20X3HT*P12X32HMAX. STANDARD S.L. STATIC THRUST3X,F10.0,5X2H&
     &LB)
 9926 FORMAT(20X5HBHP*P10X30HMAX. STANDARD S.L. STATIC H.P.5X,F10.0,5X4H&
     &H.P.)
      YALE22 = 1.
      CALL ATMOS (HES,YALE22,TINY)
      FTIN = THETA * 518.69  - 459.69
      IF(FIXIND.EQ.1.) GO TO 22467
      WRITE(6,9934)
      GO TO 22499
22467 IF(DNITFL.EQ.0.) GO TO 22468
22470 YALE22 = 1.
      CALL ATMOS (HC,YALE22,ATMIY)
      FTINH = THETA * 518.69  - 459.69
      WRITE(6,9933) VC,HC,FTINH
      GO TO22499
22468 HVSIZ = SHPTO*100
      PWSET = 0
      IF (TMAX.EQ.TMIL) PWSET=1
      DO 2008 I = 1,2
      IF (PWSET-1.0) 2005,2006,2007
 2005 OO3(I,1) = OO3(I,2)
      GO TO 2008
 2006 OO3(I,1) = OO3(I,3)
      GO TO 2008
 2007 OO3(I,1) = OO3(I,4)
 2008 END DO
      IF(ESZIND.EQ.1.) GO TO 22469
      WRITE(6,9931) SENE, HVSIZ,(OO3(I,1),I=1,2),VRCRC,HES, FTIN, ENP0
      WRITE(6,9932)
      GO TO 22499
22469 IF((ENGIND.EQ.0.).AND.(BHPP2.GT.BHPP1)) GO TO 22470
      IF((ENGIND.NE.0.).AND.(TP2.GT.TP1))     GO TO 22470
      WRITE(6,9931) SENE, HVSIZ,(OO3(I,1),I=1,2),VRCRC,HES, FTIN, ENP0
 9931 FORMAT(/20X,'ENGINE SIZED FOR TAKEOFF AT T/W = ',F4.2,/20X,       &
     &F5.1,1X,'PERCENT,',2A4,1X,'POWER SETTING',/20X,'VERTICAL RATE OF',&
     &' CLIMB = ',F6.1,' FT/MIN',/20X,'H =',F7.0,' FT,',' TEMPERATURE', &
     &' =',F6.2,' DEG F,',/20X,'AND ',F6.3,'ENGINES INOPERATIVE.')
 9934 FORMAT(/20X30HENGINE SIZE WAS FIXED BY INPUT)
 9932 FORMAT( 20X30HNO CRUISE CONDITION SPECIFIED.)
 9933 FORMAT(/20X32HENGINE SIZED FOR CRUISE AT VC = ,F4.0, 7H KNOTS,/   &
     &        20X,5HHC = ,F6.0,19H FT, TEMPERATURE = ,F6.2, 7H DEG F.)
22499 CONTINUE
      IF(DNITFL.NE.0.) GO TO 22867
      WRITE(6,9950)
 9950 FORMAT(//16X29HNO LIFT ENGINE CYCLE SELECTED)
      GO TO 3967
22867 WRITE(6,9951) CYCLFP
      IF(CYCLFP.GE.46..AND.CYCLFP.LE.54.) WRITE(6,9952)
      IF(CYCLFP.GE.55..AND.CYCLFP.LE.81.) WRITE(6,9953)
      WRITE(6,9954) ENL, ENC
 9951 FORMAT(//16X21HLIFT ENGINE CYCLE NO.,F9.3)
 9952 FORMAT(24X20HLIFT TURBOFAN ENGINE)
 9953 FORMAT(24X20HLIFT FAN      ENGINE)
 9954 FORMAT(16X,F3.0,2X11HENGINES IN ,F3.0,9H CLUSTERS)
      WRITE(6,31767) TL
31767 FORMAT(20X3HT*L12X32HMAX. STANDARD S.L. STATIC THRUST3X,F10.0,5X2H&
     &LB)
      IF (FIXIND.NE.0.) GO TO 512
      WRITE(6,9955)
 9955 FORMAT(/16X30HENGINE SIZE WAS FIXED BY INPUT)
      GO TO 3967
!     SAVE(23) = 100. * DELTAY * TLP    FROM ENGSZ
  512 IF (INDLFT.EQ.1.AND.ENP.EQ.ENP0) SAVE(23) = 0.0
      HVSIZ = SHPTO * 100.0
      WRITE(6,9956) SENE,HVSIZ,SAVE(23)
 9956 FORMAT(/16X,'ENGINE SIZED FOR TAKEOFF WITH T/W = ',F4.2,'.,'/16X, &
     &F5.1,' PERCENT POWER,'/16X,'AUGMENTED BY PRIMARY PROPULSION OF ', &
     &F4.0,'PER CENT')
! **  VERTICAL RATE OF CLIMB FOR ENGINE SIZING WITH LIFT AND TURBOFAN
!     ENGINES IS PRINTED OUT UNDER THE TURBOFAN SIZING DATA AND NOT THE
!     LIFT ENGINE DATA. IF ONLY LIFT ENGINES ARE USED FOR TAKEOFF, A
!     HIGHER T/W SHOULD BE INPUT TO ACCOUNT FOR THE ADDITIONAL THRUST
!     NECESSARY TO CLIMB AT THE SPECIFIED VERTICAL RATE (LOC.0261)
!     SAVE(22) =TL  CALCULATED IN ENGSZ
!     SAVE(21) =TL1 CALCULATED IN ENGSZ
      IF(SAVE(22).GT.SAVE(21)) WRITE(6,9957) ENL0
      IF(SAVE(22).LT.SAVE(21)) WRITE(6,9958) ENP0
      IF(SAVE(22).EQ.SAVE(21)) WRITE(6,9959) ENP0, ENL0
 9957 FORMAT(/24X29HCRITICAL SIZING CONDITION IS ,F6.3,25H LIFT ENGINES &
     &INOPERATIVE)
 9958 FORMAT(/24X29HCRITICAL SIZING CONDITION IS ,F6.3,28H PRIMARY ENGIN&
     &ES INOPERATIVE)
 9959 FORMAT(/24X36HCRITICAL SIZING CONDITION IS EITHER ,F6.3,28H PRIMAR&
     &Y ENGINES INOPERATIVE/57X,3HOR ,F6.3,24HLIFT ENGINES INOPERATIVE)
 3967 CONTINUE
      IF(LL1.EQ.0) GO TO 2464
      PRHVM=ANXMAX/AN2TO*100
      XXMSMR=XMSMRT*100
      IF(XMSND.EQ.0.0)                                                  &
     &WRITE(6,10001)XXMSMR,PRHVM
      IF(XMSND.EQ.0.0) GO TO 2464
      IF(ENGIND.EQ.2.0) GO TO 3941
      PERBHP = BHPRTO
      IF(BHPRCR.GT.BHPRTO) PERBHP = BHPRCR
      IF(ABS((BHPRCR-BHPRTO)/PERBHP).LE.0.01) GO TO 10004
 3941 CONTINUE
      IF(XMSND.EQ.1.0.AND.LL1.EQ.1)                                     &
     &WRITE(6,10002)XXMSMR,HES,FTIN,PRHVM
      IF(XMSND.EQ.1.0.AND.LL2.EQ.2)                                     &
     &WRITE(6,10003) XXMSMR,VC,HC,FTINH,PRHVM
10001 FORMAT(/16X,'XMSN SIZED AT',F5.0,1X,                              &
     &'PERCENT OF TOTAL PRIMARY ENGINE INSTALLED POWER'/16X,            &
     &'(MAX.STANDARD S.L. STATIC H.P.),'F5.1,1X,                        &
     &'PERCENT HOVER RPM')
10002 FORMAT(/16X,'XMSN SIZED AT',F5.0 ,                                &
     &' PERCENT OF ROTOR HOVER POWER REQUIRED'/16X,                     &
     &'AT H=',F6.0,' FT,TEMP=',F6.2 ,' DEG.F.,',F5.1,1X,                &
     &' PERCENT HOVER RPM')
10003 FORMAT(/16X,'XMSN SIZED AT',F5.0,1X,                              &
     &'PERCENT OF ROTOR CRUISE POWER REQUIRED AT VC=',F4.0,1X,          &
     &'KT,',/16X,'HC=',F6.0,'FT, TEMP=',F6.2,' DEG.F., ',               &
     &F5.1,1X,'PERCENT HOVER RPM')
      GO TO 2464
10004 WRITE (6,10005)
10005 FORMAT(/16X,'POWER REQUIRED TO HOVER IS WITHIN 1 PERCENT OF'/     &
     & 16X,'CRUISE POWER REQUIRED AT DESIGN CONDITIONS')
      WRITE (6,10002) XXMSMR,HES,FTIN,PRHVM
 2464 CALL LOADET(SS,2)
      WRITE(6,9960)
 9960 FORMAT(///7X32HA E R O D Y N A M I C S  D A T A)
      WRITE(6,9961) FE, SWET, CBARF
 9961 FORMAT(20X,2HFE13X30HTOTAL EFFECTIVE FLATPLATE AREA5X,F10.3,5X4HSQ&
     &FT    /20X,4HSWET11X17HTOTAL WETTED AREA18X,F10.0,5X4HSQFT        &
     &      /20X,5HCBARF10X25HMEAN SKIN FRICTION COEFF.10X,F10.6)
      WRITE(6,9962)
 9962 FORMAT(/7X26HD R A G  B R E A K D O W N10X,9HIN  SQFT )
      WRITE(6,9963) FEW,FEF,FEVT,FEHT,FEN,FELN,DLTAFE
 9963 FORMAT(20X,3HFEW12X,7HWING FE28X,F10.3 /                          &
     &       20X,3HFEF12X11HFUSELAGE FE24X,F10.3/                       &
     &       20X,4HFEVT11X13HVERT. TAIL FE22X,F10.3/                    &
     &       20X,4HFEHT11X13HHOR.  TAIL FE22X,F10.3/                    &
     &       20X,3HFEN12X23HPRIMARY ENG. NACELLE FE12X,F10.3/           &
     &       20X,4HFELN11X20HLIFT ENG. NACELLE FE15X,F10.3/             &
     &       20X,8HDELTA FE7X14HINCREMENTAL FE21X,F10.3/)
                                                           GO TO  5555
!     SAVE(24) = NO. OF TIMES ENTERING PRFRM WITHIN ONE RUN.
  506 SAVE(24)  = SAVE(24) +1.0
      OPTION = 2.0
                                                           GO TO     3
 5555 WRITE(6,516)
  516 FORMAT(7X34HA E R O D Y N A M I C  C O E F F .)
      IF(DRGIND.EQ.0.) WRITE(6,518) SA1,SA2,SA3,SA4
  518 FORMAT(20X2HA148X,F10.5/20X2HA248X,F10.5/20X2HA348X,F10.5/20X2HA44&
     &8X,F10.5)
      WRITE(6,519) SA5,SA6,SA7,CLALPH
  519 FORMAT(20X2HA548X,F10.5/20X2HA648X,F10.5/20X2HA748X,F10.5/20X,8HCL&
     & ALPHA7X14H3-D LIFT SLOPE21X,F10.5,5X10HPER RADIAN)
      IF (OSWIND.EQ.1.) WRITE(6,520) SEE
  520 FORMAT(20X1HE14X15HOSWALD   FACTOR20X,F10.5)
      IF (OPTIND.EQ.0) GO TO 1771
      IF(NEXT.EQ.0)                                        GO TO     3
 1921 IF (KKROPT.EQ.0) GO TO 1771
      KKKONT = KKKONT + 1
      IF (KKKONT.LE.10) GO TO 1916
      WRITE(6,1915)
 1915 FORMAT(10X,'NUMBER OF CASES REQ. EXCEEDS 10,NO OUTPUT GENERATED')
      GO TO 1771
 1916 BNFUEL(KKKONT) = WF
      BNLNG(KKKONT) = R
      BLKTIM(KKKONT) = ST
      RUDY = BHPP
      IF (ENGIND.NE.0.) RUDY = TP
      IF(KKROPT.EQ.2) WRITE(2)                                          &
     & WRP,WDS,WEL,WEP,WLEI,WPEI,WFSS,DELWP,WP,WW,WHT,WVT,WB,           &
     & WLG,WLES,WPES,DELWST,WST,WCC,WUC,WH,WCFW,WSAS,WTM,DELWF2,        &
     & WFC,WFE,WE,WFUL,OWE,WPL,WFA,WGSAVE,ENP,CYCPRP,RUDY,ENL,          &
     & TL,VC,BNLNG,BNFUEL,BLKTIM
      GO TO 1771
      END
      SUBROUTINE AERO(IAERO)
!**** MEMBER NAME = B93TAERO
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***          PAGE NO. 24  LOC.2201 TO 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
 1001 FORMAT(22X,34HTHIS ERROR IS IN THE CL,CDWI TABLE)
      IAERO = IAERO + 1
      RLMC4 =  DLMC4 * DTOR
      YALE05=(1.-SLM)/(1.+SLM)
      T     =  SIN(RLMC4)/COS(RLMC4)
! ***  SAVE(26) = SEC(SQUARED) LAMBDA ,C/2
      SAVE(26) = 1.0 +(T -YALE05/AR)**2
      IF(OPTIND.NE.2.) GO TO 100
      B=SQRT(AR*WG/WGS)
      SW=WG/WGS
      SIGS = AF*BLDN/4900.
  100 IF(DRGIND.EQ.1.) GO TO 16
      IF((OPTIND.EQ.2.).OR.(WDMIND.NE.0.)) GO TO 12
      IF(IAERO.NE.1)   GO TO 15
   12 DLMPS  = ATAN( T -4./AR *(XCPS -.25)* YALE05) * RTOD
      DLMTCX = ATAN( T - 4./AR*(XCTCM-.25) *YALE05)*RTOD
      SA2   = -.33* (.65 -XCPS)*(1. +.0033*(4.*DLMPS- 3.*DLMTCX))
      RLMLE = ATAN( T + YALE05/AR)
      FK    = 1./(1. + YALE05 /AR * 4.* SLM**2)
   15 TC   =((TCR -SWF/B *(TCR-TCT))*(1.-SWF/B *(1.-SLM))+ SLM * TCT)   &
     &     / (1.+SLM -SWF/B * (1.-SLM))
      SA1=(1. + .0033 * (4.*DLMPS - 3.*DLMTCX)) *                       &
     &     (1. -1.4  * TC - .06*(1.-XCPS)) -.0368
      SA3   = (1.5 - 2. * FK**2 * SIN(RLMLE)**2)* TC**(5./3.)
      SA4   = .75 * TC
   16 IF(OPTIND.EQ.2.) GO TO 209
      REF   = RELI * ELF
      FFRE  =  (ALOG10(REF) / 7.0)**(-2.6)
      GO TO 17
  209 IF(OSWIND.EQ.1.) SEE = .94 - .045 * AR**.68
      SA5   = DELCD
      SA6   = CKW
      SA7   = 1./(3.1415926 * SEE * AR)
      ARPI   = 3.141593 * AR
      CLALPH = ARPI /(1.+SQRT((ARPI/CSLALF)**2*(SAVE(26)-.75*EMM0**2)+  &
     &1.0))
      RETURN
   17 REW   = RELI * CBARW
      FWRE  =  (ALOG10(REW) / 7.0)**(-2.6)
      FNRE  = 0.
      IF(CKN.EQ.0.) GO TO 219
      REN   = RELI * ELN
      FNRE  =  (ALOG10(REN) / 7.0)**(-2.6)
  219 FVTRE = 0.
      IF(CBARVT.EQ.0.) GO TO 229
      REVT  = RELI * CBARVT
      FVTRE =  (ALOG10(REVT)/ 7.0)**(-2.6)
  229 FHTRE =0.
      IF(CBARHT.EQ.0.) GO TO 239
      REHT  = RELI * CBARHT
      FHTRE =  (ALOG10(REHT)/ 7.0)**(-2.6)
  239 FLNRE =0.
      IF(CKLN.EQ.0.) GO TO 249
      RELN  = RELI * DBARLN * (ENL/ENC + EPSLON)
      FLNRE = 0.
      IF(RELN.EQ.0.) GO TO 249
      FLNRE =  (ALOG10(RELN)/ 7.0)**(-2.6)
  249 IF(OSWIND.EQ.1.) SEE = .94 - .045 * AR**.68
      SA5   = (.00287 * CKF*SF+DELFE) *FFRE/SW +CKVT*SVT/SW*CDVTI*FVTRE &
     &      +CKHT*SHT/SW*CDHTI*FHTRE + CKN*SN/SW*CDNI*FNRE +CKLN*SLN/SW*&
     &       CDLNI*FLNRE +DELCD
      SA6   =  CKW * FWRE
      SA7   =  1. /(3.1415926 * SEE * AR)
      ARPI = 3.141593 *AR
      CLALPH=ARPI/(1.+SQRT((ARPI/CSLALF)**2*(SAVE(26)-.75*EMM0**2)+1.))
      DLMELE = RLMLE * RTOD
      IF (DNITFL.EQ.0.0)  SLN = 0.0
      IF (CKN.EQ.0.) SN=0.
      SWET= SF + SN + SLN + 2.*(SVT +SHT +SW) -2.*CBARW *SWF *(1. -.5*  &
     &      SWF/B + .5*SLM *SWF/B) /(1.+SLM) + DLSWSW * SW
      FEW = SW* XLINT(TBCL1,TBCDWI,0.,NTCL,M) * CKW
      FEW=FEW*FWRE
      IF(M.NE.0)WRITE(6,1001)
      FEF =(.00287 *CKF * SF + DELFE) * FFRE
      FEVT= CKVT * SVT * CDVTI * FVTRE
      FEHT= CKHT * SHT * CDHTI * FHTRE
      FEN = CKN  * SN  * CDNI  * FNRE
      FELN= CKLN * SLN * CDLNI * FLNRE
      DLTAFE= DELCD * SW
      FE= FEW +FEF +FEVT +FEHT +FEN +FELN +DLTAFE
      CBARF= FE / SWET
      RETURN
      END
      SUBROUTINE ATMOS(HH,ATM333,TIN333)
!**** MEMBER NAME = B93TATMO
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***          PAGE NO. 24   LOC. 2201 TO 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      TIN33 = TIN333
      INDATM = ATM333
      IF (INDATM.EQ.0) TIN33 = 0.0
      IF(INDATM - 1) 1,1,2
    1 THETA = .7519 + TIN33 / 518.69
    6 DELTA = .22336 * EXP((36089.- HH)/20786.)
      IF(HH.GT.36089.) GO TO 4
      THETA = 1. - .000006875 * HH + TIN33 / 518.69
      DELTA = (1. - .000006875 * HH)**5.2561
    4 SIGMA = DELTA / THETA
      STHETA = SQRT(THETA)
      SA     = 661.7 * STHETA
      RHO    = .0023769 * SIGMA
      S2RHO  = SQRT( 2.*RHO)
      RETURN
    2 THETA  = XLINT(TBH,TBTHE,HH,NTH,M)
      IF(M.NE.0) WRITE (6,3000)
 3000 FORMAT(22X,34HTHIS ERROR IS IN THE H-THETA TABLE/)
      IF(HH.GT.36089.) GO TO 6
      DELTA = (1. - .000006875 * HH)**5.2561
      GO TO 4
      END
      FUNCTION BIV(XARG,YARG,XTAB,YTAB,ZTAB,NXARG,NYARG,NXI,NYJ)
!**** MEMBER NAME = B93TBIV
      DIMENSION ZTAB(NXI,NYJ),XTAB(1),YTAB(1)
      VAL(A,B,C,D)=(A*(A-B-C)+B*C)/(D*(D-B-C)+B*C)
!     XTAB AND YTAB DIMENSIONS ARE DUMMY BUT Z IS CRITICAL
!     IT MUST AGREE WITH THE DIMENSION IN THE CALLING PROGRAM
!     LOCATE THE X VALUE
!     IS XARG LESS THAN FIRST VALUE IN TABLE
    1 IF (XARG-XTAB(1))2,3,3
!     YES-USE FIRST 3 VALUES IN TABLE
    2  J=1
      GO TO 9
!     NO-FIND WHICH 2 ARGUMENTS XARG IS BETWEEN
    3 DO 5 I=2,NXARG
      K=I
    4 IF(XARG-XTAB(I))6,6,5
    5 END DO
!     ARE WE AT END OF TABLE
    6 I=K
      IF(I-NXARG) 8,7,8
!     YES-USE LAST 3 TABLE VALUES
    7 J=NXARG-2
      GO TO 9
!     NO-USE 3 SURROUNDING VALUES
    8 J=I-1
!     FIND WHERE YARG LIES IN TABLE
!     IS YARG LESS THAN FIRST VALUE OF TABLE
    9 IF (YARG-YTAB(1))10,11,11
!     YES-USE FIRST 3 VALUES IN TABLE
   10 L=1
      GO TO 17
!     NO-FIND WHERE YARG LIES
   11 DO 13 I=2,NYARG
      K=I
   12 IF (YARG-YTAB(I))14,14,13
   13 END DO
!     ARE WE AT END OF TABLE
   14 I=K
      IF(I-NYARG) 16,15,16
!     YES-USE LAST 3 VALUES IN TABLE
   15 L=NYARG-2
      GO TO 17
!     NO-USE 3 SURROUNDING VALUES
   16 L=I-1
!     SET UP FOR INTERPOLATION
   17 JJ=J+1
      JJJ=J+2
      LL=L+1
      LLL=L+2
!     EVALUATE FY1(X)
   18 FY11=VAL(XARG,XTAB(JJ),XTAB(JJJ),XTAB(J))
      FY12=VAL(XARG,XTAB(J),XTAB(JJJ),XTAB(JJ))
      FY13=VAL(XARG,XTAB(J),XTAB(JJ),XTAB(JJJ))
      FY1=FY11*ZTAB(J,L)+FY12*ZTAB(JJ,L)+FY13*ZTAB(JJJ,L)
!     EVALUATE FY2(X)
   19 FY2=FY11*ZTAB(J,LL)+FY12*ZTAB(JJ,LL)+FY13*ZTAB(JJJ,LL)
!     EVALUATE FY3(X)
   20 FY3=FY11*ZTAB(J,LLL)+FY12*ZTAB(JJ,LLL)+FY13*ZTAB(JJJ,LLL)
!     EVALUATE F(Y)-FINAL ANSWER
   21 BIV=(VAL(YARG,YTAB(LL),YTAB(LLL),YTAB(L))*FY1)+                   &
     &(VAL(YARG,YTAB(L),YTAB(LLL),YTAB(LL))*FY2)+                       &
     &(VAL(YARG,YTAB(L),YTAB(LL),YTAB(LLL))*FY3)
      RETURN
      END
      BLOCK DATA
!**** MEMBER NAME = B93TBLKD
      DIMENSION A0(3,15),A1(3,15),A2(3,15),BB0(16),BB1(16),BB2(16),     &
     & RJ3(10),CPOW3(10),ETAI3(10,10),RJ4(10),CPOW4(10),ETAI4(10,10),   &
     &AMACH(3),CLLL(15),CLGAM(16),CPOW33(20),CTI3(20),CTI4(20),         &
     & GMDD1(16),GAMD11(3,15)
      COMMON/ODD/A0,A1,A2,BB0,BB1,BB2,RJ3,CPOW3,ETAI3,RJ4,CPOW4,        &
     & ETAI4,AMACH,CLLL,CLGAM,CPOW33,CTI3,CTI4,GMDD1,GAMD11
      DATA A0/ 90.,90.,90.,7.0392,10.2148,11.3227,4.8350,8.3106,        &
     & 11.3355,3.2218,5.4623,8.3676,2.7551,4.0458,6.5856,2.481,         &
     & 3.9439,5.3862,2.4521,3.6769,5.2054,2.8149,3.8766,6.1902,         &
     & 3.8725,4.5901,8.153,5.6653,6.1044,10.1745,8.5799,8.9031,         &
     & 13.0822,12.25,12.2042,16.5344,17.0496,17.0398,20.8089,           &
     & 21.8332,22.784,25.6453,31.7062,28.7851,33.5049/
      DATA A1/ 0.0,0.0,0.0,1.9949,2.4433,40.9515,-4.1639,-22.6338,      &
     & -14.062,-1.7030,-14.9997,-12.2425,-2.5322,-9.9837,-9.574,        &
     & -4.5422,-13.0524,-8.9808,-5.4949,-11.7146,-9.3153,-7.092,        &
     & -12.0044,-14.7567,-10.861,-13.8756,-25.0375,-16.2691,-18.2607,   &
     & -30.7342,-24.8115,-26.0958,-33.2211,-33.6185,-29.4588,-34.9378,  &
     & -43.061,-37.3809,-40.6314,-47.8821,-47.3791,-49.145,-49.6246,    &
     & -57.8217,-77.6449/
      DATA A2/ 0.0,0.0,0.0,61.2416,86.4731,21.9481,19.2195,56.0,        &
     & 62.0142,8.291,35.3636,36.0889,7.0366,23.0606,25.9573,7.3774,     &
     & 22.0028,18.6439,7.4251,17.3803,16.4063,8.3401,16.0882,23.2672,   &
     & 11.4678,17.2451,39.117,15.8093,21.8349,51.0509,22.6773,30.7056,  &
     & 58.1494,28.7271,34.1515,64.3529,33.8798,43.697,76.3927,33.6322,  &
     & 55.5455,94.4326,26.4923,68.2121,144.4176/
      DATA BB0/ 90.,2.68,1.9141,1.513,1.5304,1.9611,2.7089,3.8237,      &
     & 5.051,7.3796,9.13,12.397,17.169,22.392,28.85,38.2/
      DATA BB1/ 0.0,-5.4836,-2.9393,-2.7523,-3.0648,-4.5374,            &
     & -6.8293,-10.0965,-12.558,-18.5171,-19.35,-11.944,-10.883,        &
     & -5.948,0.0,0.0/
      DATA BB2/ 0.0,13.5125,8.012,5.8118,4.8132,5.3846,6.8072,          &
     & 9.3267,10.7333,15.8002,15.5,6.818,7.879,5.606,0.0,0.0/
      DATA RJ3/0.2,1.0,1.2,1.4,1.8,2.2,2.6,3.0,3.4,4.2/
      DATA CPOW3/0.0,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.75,0.9/
      DATA ETAI3/10*1.0,0.74,0.937,0.954,0.9662,0.980,0.9862,0.9895,    &
     & 0.9912,0.9924,0.9962,0.621,0.896,0.920,0.939,0.961,0.973,0.9793, &
     & 0.9827,0.985,0.9906,0.467,0.830,0.866,0.892,0.928,0.949,0.960,   &
     & 0.9664,0.9715,0.9794,0.342,0.772,0.819,0.851,0.896,0.926,0.9415, &
     & 0.9486,0.956,0.9683,0.262,0.718,0.770,0.810,0.868,0.9044,0.9235, &
     & 0.935,0.944,0.9575,0.206,0.666,0.728,0.773,0.839,0.882,0.9064,   &
     & 0.9204,0.9305,0.9456,0.168,0.615,0.684,0.736,0.816,0.8615,0.8885,&
     & 0.906,0.918,0.9345,0.126,0.541,0.615,0.679,0.779,0.830,0.8633,   &
     & 0.8848,0.8988,0.9175,0.096,0.471,0.547,0.620,0.743,0.803,0.8385, &
     & 0.863,0.880,0.900/
      DATA RJ4/0.2,1.0,1.2,1.4,1.8,2.2,2.6,3.0,3.4,4.2/
      DATA CPOW4/0.0,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.75,0.9/
      DATA ETAI4/10*1.0,0.668,0.935,0.959,0.968,0.9822,0.9875,0.9906,   &
     & 0.9925,0.9938,0.9956,0.542,0.898,0.929,0.946,0.9655,0.9755,0.981,&
     & 0.9846,0.987,0.9905,0.405,0.834,0.878,0.904,0.9355,0.954,0.964,  &
     & 0.9705,0.975,0.982,0.311,0.784,0.834,0.869,0.9095,0.9345,0.9484, &
     & 0.9575,0.9646,0.9736,0.252,0.735,0.793,0.835,0.886,0.916,0.933,  &
     & 0.9445,0.9535,0.966,0.204,0.690,0.753,0.8025,0.862,0.8974,0.918, &
     & 0.932,0.9436,0.9585,0.170,0.646,0.714,0.772,0.8415,0.8794,0.902, &
     & 0.9195,0.933,0.9513,0.135,0.582,0.657,0.727,0.810,0.853,0.8796,  &
     & 0.9007,0.918,0.940,0.110,0.520,0.601,0.683,0.781,0.828,0.860,    &
     & 0.883,0.904,0.928/
      DATA AMACH/0.7,0.8,0.9/
      DATA CLLL/ 0.0,0.05,0.1,0.15,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,     &
     & 1.0,1.1,1.2/
      DATA CLGAM/0.0,0.15,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,          &
     & 1.2,1.4,1.6,1.8,2.0/
      DATA CPOW33/ 0.0,0.005,0.01,0.02,0.03,0.04,0.06,0.08,0.12,0.16,   &
     & 0.20,0.24,0.28,0.32,0.36,0.40,0.44,0.48,0.52,0.60/
      DATA CTI3/0.0,0.0323,0.0523,0.082,0.1065,0.127,0.163,0.194,       &
     &0.2475,0.2945,0.338,0.3773,0.4102,0.4368,0.4637,0.4912,0.517,     &
     & 0.539,0.5616,0.5958/
      DATA CTI4/0.0,.0345,.0545,.0838,0.1073,0.128,0.166,0.201,0.2556,  &
     & 0.3026,0.3462,0.3859,0.4208,0.4544,0.4871,0.514,0.539,0.5592,    &
     & 0.5824,0.6354/
      END
      SUBROUTINE CHGFW (IFUEL)
!**** MEMBER NAME = B93TCHGF
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24 LOC.2201 TO 2330  + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  OO(2,3)
      DATA  OO(1,2)/4H ADD/ ,OO(2,2)/4H    /
      DATA  OO(1,3)/4H REM/ ,OO(2,3)/4HOVE /
      DO 2004 I=1,2
      IF (DLTAWF(IFUEL))  2002,2001,2001
 2001 OO(I,1)  = OO(I,2)
      GO TO 2004
 2002 OO(I,1)  = OO(I,3)
 2004 END DO
      AAWA  = ABS(DLTAWF(IFUEL))
 9000 FORMAT(/7X16HCHANGE  FUEL,   ,2A4,1X,F7.0,2X,3HLB.)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,                                     &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,                &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT)               &
     &)
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0)
      IF (OPTIND.EQ.2.)                                                 &
     &WRITE(6,9000) OO(1,1),OO(2,1),AAWA
      IF(OPTIND.EQ.2.)                                                  &
     &WRITE(6,9001)
      IF(OPTIND.EQ.2.)                                                  &
     &WRITE(6,9002)ST,R,WF,W,H
      STORWF(IFUEL)= WF
      STRWFL(IFUEL)= WFL
      WF = 0.
      WFL=0.
      W = W + DLTAWF(IFUEL)
      IF(OPTION.EQ.2.0.AND.WGTIND.EQ.1.0) GO TO 10
      IF(W.GT.WG) W = WG
   10 ST = ST + STFW(IFUEL)
      IF(OPTIND.EQ.2.)                                                  &
     &WRITE(6,9002)ST,R,WF,W,H
      RETURN
      END
      SUBROUTINE CHGPL(ICHGPL)
!**** MEMBER NAME = B93TCHGP
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24  LOC. 2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  OO(2,3)
      DATA  OO(1,2)/4H ADD/ ,OO(2,2)/4H    /
      DATA  OO(1,3)/4H REM/ ,OO(2,3)/4HOVE /
      DO 2004 I=1,2
      IF (DELWPL(ICHGPL)) 2002,2001,2001
 2001 OO(I,1)  = OO(I,2)
      GO TO 2004
 2002 OO(I,1)  = OO(I,3)
 2004 END DO
      AAWA  = ABS(DELWPL(ICHGPL))
 9000 FORMAT(/7X16HCHANGE PAYLOAD, ,2A4,1X,F7.0,2X,3HLB.)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,                                     &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,                &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT)               &
     &)
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0)
      IF (OPTIND.EQ.2.)                                                 &
     &WRITE(6,9000) OO(1,1),OO(2,1),AAWA
      IF(OPTIND.EQ.2.)                                                  &
     &WRITE(6,9001)
      IF(OPTIND.EQ.2.)                                                  &
     &WRITE(6,9002)ST,R,WF,W,H
      W=W+DELWPL(ICHGPL)
      IF(OPTION.EQ.2.0.AND.WGTIND.EQ.1.0) GO TO 10
      IF(W.GT.WG) W = WG
   10 ST = ST + STPW(ICHGPL)
      IF(OPTIND.EQ.2.)                                                  &
     &WRITE(6,9002)ST,R,WF,W,H
      RETURN
      END
      SUBROUTINE CLIMB(ICLIMB)
!**** MEMBER NAME = B93CLIMB
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24  LOC. 2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25.  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      COMMON CLCLCL(10),CDCDCD(10),ALDALD(10),ELLELL(10),DEEDEE(10),    &
     & FFF(10),BHPBHP(10),TPTPTP(10),CCPCCP(10),CCTCCT(10),AJAJAJ(10),  &
     & PPP(10)
      DIMENSION DATA(1)
      DIMENSION OO1(3,4),OO3(2,4)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  EO(7)
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
      NAMELIST/NCLIMB/INDATM,DELTAH,LC1,LC2,LC3,LC4,V2,RGAM1,RGAM3,EM,Q,&
     &CL,PHI,ENGIND,BLP,BHPA,RCPOW,TLP,TA,RC1,RGAM2,RCTETA,RGAM4,THETAF,&
     &GAMMA,RGAMMA,SFC,F,TSFC,V,RC,EAS,PEHF,PETF,H,NQUO,YALE22,H9,R,W,V1&
     &,ST,WF,KCLIMB,V11,V22,V33,RCP1,RCP2,RCP3,RCT1,RCT2,RCT3           &
     &,RCTA,RCPA,VRCTA,VRCPA,KKK,TPEA,EM,NPLIM,SHPA,WSHPA
 9000 FORMAT(/7X10HCLIMB  TO ,F6.0,2X,10HFT. WITH  ,3A4,2X,3HAT ,       &
     &    2A4,2X,13HENGINE RATING)
 9003 FORMAT(/7X41HCLIMB TO OPT. ALT. FOR NEXT CRUISE WITH  ,           &
     & 3A4,2X,3HAT ,2A4,2X,                                             &
     &28HENGINE RATING, MAXIMUM ALT. ,F6.0,4H FT.)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,19X4HMACH,  &
     &9X5HTHETA,                                                        &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X3HEAS,5X4HMACH,3X3HDIV,3X5HGAMMA,4X2H-F,5X    &
     &3HR/C,                                                            &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,25X5H(DEG),2X5H(DEG),4X5H(FPM))
 9004 FORMAT(          /9X2HCL,8X2HCD,8X3HL/D,7X4HLIFT,6X4HDRAG,3X9HFUEL&
     & FLOW   ,2X3HBHP,9X6HTHRUST,3X2HCP,6X2HCT,6X1HJ,5X4HVTIP,2X4HETAP &
     &/39X5H(LBS),5X5H(LBS),2X8H(LBS/HR),16X5H(LBS),25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,2XF6.1,3XF5.3,2XF5.3,2XF5.1,2XF5.1,3XF6.0)
 5224 FORMAT(//2X5HERROR100(1H*)//)
 1003 FORMAT(9X,38HINSUFFICIENT POWER AVAILABLE FOR CLIMB)
      PART(XXX,X111,X222,X333,Y111) = (((XXX-X111) * (XXX-X222)) /      &
     &    ((X333-X111) * (X333-X222))) * Y111
      BO2A(V11,V22,V33,U1,U2,U3) = (V11**2*(U2-U3)-V22**2*(U1-U3)+V33**2&
     &*(U1-U2))/(2.*(-V11*(U2-U3)+V22*(U1-U3)-V33*(U1-U2)))
      ADEN(W1,W2,W3,Z1,Z2,Z3) = Z3*(W2-W1)+Z2*(W1-W3)+Z1*(W3-W2)
      DATA  OO1(1,2)/4H MAX/ ,OO1(2,2)/4HIMUM/ ,OO1(3,2)/4H R/C/
      DATA  OO1(1,3)/4HCONS/ ,OO1(2,3)/4HTANT/ ,OO1(3,3)/4H EAS/
      DATA  OO1(1,4)/4HMACH/ ,OO1(2,4)/4H NUM/ ,OO1(3,4)/4HBER /
      DATA  OO3(1,2)/4H MAX/ ,OO3(2,2)/4HIMUM/
      DATA  OO3(1,3)/4HMILI/ ,OO3(2,3)/4HTARY/
      DATA  OO3(1,4)/4H NOR/ ,OO3(2,4)/4HMAL /
      DATA TAS/4H TAS/
   
      tprop=0.0
      v11=0.0
      v22=0.0
      rct1=0.0
      rct2=0.0
      rcp1=0.0
      rcp2=0.0   ! added by RLC 13Dec2008.  case1.inp fails with RCP2 =NaN
      YLS2 = 1.0
      ADELTN = ENCLMB(ICLIMB)
      PI = 3.14159
      IBOTOM = 0
      NOCPP = CPPNO + 0.1
      ICHEKH = 0
      ICHEKT = 0
      HOPTWF = -1000.
      IWRITE = 0
      INDIC = 0
      INDEX4 = INDEX3
      WFOOO = WF
      WFTOTO = 0.0
      DO 2004 I=1,3
      IF (CLMIND(ICLIMB).EQ.4.0) GO TO 2002
      IF (CLMIND(ICLIMB)-2.0) 2001,2002,2003
 2001 OO1(I,1) = OO1(I,2)
      GO TO 2004
 2002 OO1(I,1) = OO1(I,3)
      GO TO 2004
 2003 OO1(I,1) = OO1(I,4)
 2004 END DO
      IF (CLMIND(ICLIMB).EQ.4.0) OO1(3,1) = TAS
      DO 2008 I=1,2
      IF (POWCLI(ICLIMB)-1.0) 2005,2006,2007
 2005 OO3(I,1) = OO3(I,2)
      GO TO 2008
 2006 OO3(I,1) = OO3(I,3)
      GO TO 2008
 2007 OO3(I,1) = OO3(I,4)
 2008 END DO
      IF (OPTIND.NE.2.0) GO TO 312
      IF (INOPTH.NE.1)                                                  &
     &WRITE(6,9000) HMAX(ICLIMB),(OO1(I,1),I=1,3),(OO3(I,1),I=1,2)
      IF (INOPTH.EQ.1)                                                  &
     & WRITE(6,9003) (OO1(I,1),I=1,3),(OO3(I,1),I=1,2),HMAX(ICLIMB)
      WRITE(6,9001)
      IF (KPRINT.EQ.1) WRITE(6,9004)
  312 KCLIMB = ICLIMB
      INDATM         = ATMIND(ICLIMB + 20)
      IF (ENGIND.NE.0.0) ETAP = 1.0
      IF (ETAIND.EQ.0.0.AND.ENGIND.EQ.0.0) ETAP = ETAP3
      DELTAH = 0.
      CLL = TBCL1(NTCL)
      IF((DRGIND.EQ.1.).AND.(CLL.GT.TBCL2(NTCL2))) CLL=TBCL2(NTCL2)
      AN2MAX = AN2M3(ICLIMB)
    1 CALL ATMOS (H,ATMIND(ICLIMB + 20),TIN(ICLIMB + 20))
      VL  = 1.10 * SQRT(W/(SW *1.42636 * RHO * CLL))
      IF(VL.LT.50.) VL = 50.
      VL = VL + 10.
      LC1=0
      LC2=0
      LC3=0
      KKK = 0
      IF(CLMIND(ICLIMB).EQ.4.0) GO TO 236
      IF (CLMIND(ICLIMB)-2.0) 288,211,212
  210 V2 = VMAX
      V2 =V2/10. + 1.001
      NV2 =V2
      V2 = 10 * NV2
      IF(ETAIND.EQ.0.0.OR.ETAIND.EQ.2.0)GO TO 2
      V = V2
  446 Q = 1.42636*RHO*V**2
      EM = V/SA
      IF (POWCLI(ICLIMB)-1.0) 546,547,548
  546 TPS = TMAX
      GO TO 549
  547 TPS = TMIL
      GO TO 549
  548 TPS = TNRP
  549 CALL POWAVL(TPS,EM)
      CL = W/(SW*Q)
      CALL DRAG
      IF (NEXT.NE.0) RETURN
      CD = CD + DCLIMB(ICLIMB)
      TT3 = CD*SW*Q
      P = A2STR*VT
      V2 = V
      VMAX = V2
      CCT = (PI**3)*TT3*WGA/(0.009507*SIGMA*WG*P**2)
      IF(ETAIND.EQ.1.0)GO TO 447
      TOAD=CCT*(P/STHETA)**2/3261.21
      IF(TOAD.LE.CPPROP(NOCPP))GO TO 2
      GO TO 448
  447 IF(CCT.LE.CPPROP(NOCPP)) GO TO 2
  448 V=V-10.
      IF (V.LT.VL) GO TO 206
      GO TO 446
  211 V2  = EMACH(ICLIMB) / SQRT(SIGMA)
      GO TO 288
  236 V2 = EMACH(ICLIMB)
      GO TO 288
  212 EM  = EMACH(ICLIMB)
      V2 = EM * SA
  288 VMAX = VM0 / SQRT(SIGMA)
      IF (VMAX.GT.(SA*EMM0)) VMAX = SA *EMM0
      IF (IVLMT.EQ.0.OR.H.GT.10000.)GO TO 310
      VLMT = 250./SQRT(SIGMA)
      IF (VMAX.LE.VLMT) GO TO 310
      INDIC = 1
      VMAX = VLMT
  310 IF (CLMIND(ICLIMB).EQ.1.0) GO TO 210
      IF (VMAX.LT.V2) V2 = VMAX
      EM = V2/SA
    2 RGAM1  = 0.
      RGAM3  = 0.
      EM  = V2 / SA
    3 Q      = 1.42636 * RHO * V2**2
      CL = (ADELTN + 1.0)*W/(Q*SW)
      IF (ADELTN.EQ.0.0) CL = CL*COS(RGAM1)
      PHI    = 1.
      IF (DELTAH.NE.0.0) PHI = 1.0 +V2/11.278 * (V2-V1)/DELTAH
      CALL DRAG
      CD = CD + DCLIMB(ICLIMB)
      IF(NEXT.NE.0)RETURN
      IF (POWCLI(ICLIMB)-1.0) 200,201,202
  200 TPS = TMAX
      GO TO 204
  201 TPS = TMIL
      GO TO 204
  202 TPS = TNRP
  204 IF (ENGIND.NE.0.) GO TO 4
      CALL  POWAVL(TPS,EM)
      TKE = TPEA
      KE = NPLIM + 1
      BLP  = SHPA
      IF (ETAIND.NE.0.0) GO TO 61
      BHPA   = BLP * DELTA * STHETA * BHPP
      RCPOW = 101.34 * (325.6365*ETAP3*(ETAT*BHPA-DSHPAC)-CD*SW*Q*V2)/W
! **  ACCESSORY POWER IS SUBTRACTED FROM THE THRUST X VELOCITY TERM IN
!     THE ABOVE EQUATION. THIS CALCULATION REDUCES THE CLIMB RATE (FPM)
      GO TO 5
   61 V = V2
      CALL THRUST(TPROP,YLS2,ETAP)
      RCPOW = 101.34*V*((TPROP -CD*SW*Q)/W)
      GO TO 5
    4 CALL  THRAVL(TPS,EM)
      TKE = TPEA
      KE = NPLIM + 1
      TLP  = SHPA
      TA     = TLP * DELTA * TP
      RCPOW = 101.34 * V2 * (TA-CD * SW * Q)/ W
    5 IF (RCPOW.GE.(101.34*V2    ))  GO TO 7
      IF ((RCPOW.GE.0.).OR.(LC1.NE.0)) GO TO 6
      IF (CLMIND(ICLIMB).NE.1.0) GO TO 206
      V2     = V2 - 10.
      RC1 = RCPOW
      IF(V2.GE.VL ) GO TO 2
  206 WRITE(6,5224)
      WRITE(6,1003)
      WRITE(6,5224)
      WRITE(6,NCLIMB)
      NEXT=1
      IF (INOPTH.EQ.1) NEXT = 0
      IF (H.GE.1000.) H = H - 1000.
      RETURN
    6 RGAM2  = ASIN(RCPOW /(101.34 * V2     ))
      IF((ABS(RGAM2-RGAM1)* RTOD).LE.0.1)              GO TO    7
      RGAM1  = RGAM2
      GO TO 3
    7 CL = (ADELTN + 1.0)*W/(Q*SW)
      IF (ADELTN.EQ.0.0) CL = CL*COS(RGAM3)
      RCTETA =101.34*V2*SIN((THEMAX(ICLIMB)-ALPHL0+EYEW)*.0174533 -CL/CL&
     &ALPH)
      RGAM4  = ASIN(RCTETA /(101.34*V2))
      IF((ABS(RGAM4-RGAM3)* RTOD).LE..1)               GO TO    8
      RGAM3  = RGAM4
      GO TO 7
    8 IF (CLMIND(ICLIMB).NE.1.0) GO TO 220
      IF (KKK-1)  9,30,20
    9 LC1 = LC1 + 1
      RCP3   = RCP2
      RCP2   = RCP1
      RCP1   = RCPOW
      RCT3   = RCT2
      RCT2   = RCT1
      RCT1   = RCTETA * PHI
      V33    = V22
      V22    = V11
      V11    = V2
      IF  (LC1.GE.3)  GO TO  16
      IF ((LC1.EQ.1).AND.(RCT1.LE.RCP1)) GO TO 10
      IF ((LC1.EQ.2).AND.(RCP2.GT.RCP1)) GO TO 12
      IF (V2.LT.VL)  LC4= 1
      IF (V2.LT.VL)  GO TO 30
      V2 = V2 -5.
      GO TO 2
   10 LC4  = 0
      IF (V2.GT.VMAX) V11 = VMAX
      V2   = V11
      KKK  = 2
      GO TO 2
   12 LC4  = 1
      IF (V22.GT.VMAX) V22 = VMAX
      V2   = V22
      KKK  = 1
      GO TO 2
   16 IF (RCT1.LT.RCP1)  GO TO 18
      IF (RCP1.LE.RCP2)  GO TO 40
      IF (V2.LT.VL)  LC4= 1
      IF (V2.LT.VL)  GO TO 30
      V2 = V2 -5.
      GO  TO 2
   18 RCTA  = PART(V2,V22,V33,V11,RCT1) + PART(V2,V11,V33,V22,RCT2) +   &
     &        PART(V2,V11,V22,V33,RCT3)
      RCPA  = PART(V2,V22,V33,V11,RCP1) + PART(V2,V11,V33,V22,RCP2) +   &
     &        PART(V2,V11,V22,V33,RCP3)
      IF (RCPA.LE.RCTA) GO TO 19
      V2 = V2 +.2
      GO  TO 18
   19 RCTA  = RCPA
      VRCTA = V2
      VTEST = ADEN(V11,V22,V33,RCP1,RCP2,RCP3)
      IF (VTEST.GE.0.0) GO TO 45
      GO  TO 42
   40 RCTA  = RCP1
      VRCTA = V11
   42 VRCPA = -BO2A(V11,V22,V33,RCP1,RCP2,RCP3)
      IF (VRCPA.GE.VRCTA) GO TO 46
   45 LC4 = 0
      V2  = VRCTA
      GO TO 50
   46 LC4 = 1
      V2  = VRCPA
   50 KKK  = 1
!     WRITE(6,NCLIMB)
      GO TO 2
   20 CL     = CLALPH*((THEMAX(ICLIMB) - ALPHL0+EYEW)*.01745329-RGAM4)
      THETAF = THEMAX(ICLIMB)
      GAMMA = RGAM4 * RTOD
      RGAMMA= RGAM4
      CALL DRAG
      CD = CD + DCLIMB(ICLIMB)
      IF(NEXT.NE.0)RETURN
      V   = V2
      RC  = RCTETA
  221 IF (ENGIND.NE.0.) GO TO 21
      IF (ETAIND.NE.0.0) GO TO 62
      BHPA   = W * PHI * (RCTETA /101.34 + CD * SW * Q * V2 /(W * PHI)) &
     &       /(325.8 * ETAP3 * ETAT)  +  DSHPAC/ETAT
! **  BHPA IS ACTUALLY POWER REQUIRED
      BLP    = BHPA /(BHPP * DELTA * STHETA)
      SHPR = BLP
      GO TO 63
   62 V= V2
      TPROP = CD*SW*Q + PHI*RCTETA*W/(101.54*V2)
      CALL POWER(TPROP,YLS2,ETAP)
! **  DSHPAC IS ACCOUNTED FOR IN THE POWER SUBROUTINE
   63 CALL POWREQ(EM)
      F  = WSHPR * DELTA * STHETA * BHPP
      TKE = TPEA
      KE = 6
      CALL POWAVL(TMAX,EM)
      PEHF  = SHPR / SHPA
! **  SHPR ALREADY REFLECTS THE ACCESSORY POWER
      GO TO 23
   21 TA   = W * PHI * RCTETA /(101.34 * V2) + CD * SW * Q
      TLP    = TA /(DELTA * TP)
      SHPR = TLP
      CALL  THRREQ(EM)
      F  = WSHPR * DELTA * STHETA * TP
      TKE = TPEA
      KE = 6
      CALL THRAVL(TMAX,EM)
      PEHF  = SHPR / SHPA
   23 EAS=V * SQRT(SIGMA)
      IF (INOPTH.NE.1) GO TO 238
      R1 = R + DELTAH/(6076.1*TAN(RGAMMA))
      IF (R1.GE.RMAX(INDEX3)) GO TO 2968
      A2STO = A2STR
      RO = R
      VO = V
      TKEO = TKE
      KEO = KE
      EOO = EO(KE)
      EMO = EM
      EMDO = EMD
      CLOO = W*COS(RGAMMA)/(SW*Q)
      CDOO = CD
      QOO = Q
      AN2MXO = AN2MAX
      SHPRO = SHPR
      DELTAO = DELTA
      STHETO = STHETA
      TPROPO = TPROP
      SIGMAO = SIGMA
      TAO = TA
      WFO = WF
      FO = F
      IF (CRSIND(INDEX4)-2.) 232,233,234
  232 CALL CRUS1(INDEX4)
      GO TO 235
  233 CALL CRUS2(INDEX4)
       GO TO 235
  234 CALL CRUS3(INDEX4)
  235 WFCR = (RMAX(INDEX3) - RO)/EN
      WFTOT = WFCR + WFO - WFOOO
      R = RO
      V = VO
      TKE = TKEO
      KE = KEO
      EO(KE) = EOO
      EM = EMO
      EMD = EMDO
      CL = CLOO
      CD = CDOO
      Q = QOO
      F = FO
      AN2MAX = AN2MXO
      SHPR = SHPRO
      DELTA = DELTAO
      STHETA = STHETO
      TPROP = TPROPO
      SIGMA = SIGMAO
      TA = TAO
      WF = WFO
      A2STR = A2STO
      IF (NEXT.NE.0) GO TO 243
      IF (IWRITE.NE.0.OR.WFTOTO.EQ.0.0) GO TO 237
      IF (IVLMT.NE.0.AND.H9.GT.10000.) ICHEKH = ICHEKH + 1
      IF (WFTOT.GE.WFTOTO) GO TO 241
      IF (ICHEKH.EQ.0.OR.IBOTOM.EQ.0) GO TO 237
      IF (WFTOT.LT.WFTT10.AND.OPTIND.EQ.2.0) GO TO 257
      WFTOTO = WFTOT
      GO TO 2967
  241 IF (ICHEKH.EQ.0) GO TO 2968
      IF (ICHEKH.GT.1.AND.IBOTOM.EQ.0) GO TO 2968
      IF (ICHEKH.NE.1) GO TO 245
      WFTT10 = WFTOTO
      R10K = R10
      W10K = W10
      WF10K = WF10
      H10K = H10
      ST10K = ST10
      R11K = R
      W11K = W
      WF11K = WF
      H11K = H
      ST11K = ST
      V11K = V2
      WFTOTO = WFTOT
      IBOTOM = 1
      GO TO 2967
  243 IF (IBOTOM.EQ.0) GO TO 251
      NEXT = 0
  245 IF (WFTOTO.LT.WFTT10) GO TO 247
  246 R = R10K
      W = W10K
      H = H10K
      WF = WF10K
      ST = ST10K
      RETURN
  247 IF (OPTIND.NE.2.) GO TO 251
      HOPTWF = H10
      INOPTH = 0
      GO TO 249
  257 ICHEKT = 1
      IBOTOM = 0
  249 IWRITE = 1
      H10 = H10K
      R10 = R10K
      W10 = W10K
      WF10 = WF10K
      ST10 = ST10K
      H = H11K
      R = R11K
      W = W11K
      WF = WF11K
      ST = ST11K
      DELTAH = 0.0
      INDIC = 0
      GO TO 1
  251 IF (NEXT.NE.0) NEXT = 0
 2968 R = R10
      H = H10
      W = W10
      ST = ST10
      WF = WF10
      RETURN
  237 IF (ICHEKT.EQ.2.AND.WFTOT.GE.WFTOTO) GO TO 2968
      IF (ICHEKT.EQ.1) ICHEKT = 2
      WFTOTO = WFTOT
  238 IF (OPTIND.NE.2.0) GO TO 2967
      WRITE(6,9002) ST,R,WF,W,H,V,TKE,EO(KE),PEHF,EAS,EM,EMD,GAMMA,     &
     &THETAF,RC
      PETF = PEHF
      CL = W*COS(RGAMMA)/(Q*SW)
      IF (KPRINT.EQ.1) CALL SCRIBE(0,ETAP,YLS2)
 2967 IF (H.GE.HMAX(ICLIMB).AND.IBOTOM.EQ.0) RETURN
      IF (H.LT.HMAX(ICLIMB)) GO TO 253
      IF (WFTOT.GE.WFTT10) GO TO 246
      IF (OPTIND.NE.2.) RETURN
      IBOTOM = 0
      INOPTH = 0
      GO TO 249
  253 IF (IBOTOM.EQ.0) GO TO 255
      IF (INOPTH.EQ.1) GO TO 255
      IF (H.LT.HOPTWF) GO TO 255
      RETURN
  255 NQUO = H/DELH(ICLIMB) + 0.05
      YALE22 = NQUO + 1
      H9  =  YALE22 * DELH(ICLIMB)
      IF(HMAX(ICLIMB).LT.H9) GO TO 25
      DELTAH = H9 - H
   24 R10 = R
      H10 = H
      W10 = W
      ST10 = ST
      WF10 = WF
      H = H9
      R    = R + DELTAH/(6076.1 * SIN(RGAMMA)/COS(RGAMMA))
      W      = W - DELTAH* F /(60.*RC)
      V1     = V2
      ST  = ST + DELTAH /(RC * 60.)
      WF     = WF+ DELTAH* F /(60.*RC)
      IF (INDIC.NE.1) GO TO 1
      IF (H9.LE.10000.) GO TO 1
      DELTAH = 0.0
      INDIC = 0
      GO TO 1
   25 DELTAH = HMAX(ICLIMB) - H
      H9     = HMAX(ICLIMB)
      GO TO 24
  220 IF (RCTETA * PHI.GT.RCPOW)  GO TO 225
      RC  = RCTETA
      GAMMA = RGAM4 * RTOD
      RGAMMA= RGAM4
      THETAF=THEMAX(ICLIMB)
      V = V2
      GO TO 221
  225 RC    = RCPOW    /PHI
      RGAM2  = ASIN(RC    /(101.34 * V2     ))
      GAMMA = RGAM2 * RTOD
      RGAMMA = RGAM2
      THETAF = GAMMA + ALPHL0-EYEW + RTOD *W*COS(RGAM2)/(SW*Q*CLALPH)
      V = V2
      GO TO 230
   30 IF (LC4.EQ.1) GO TO 32
      THETAF = THEMAX(ICLIMB)
      GAMMA = RGAM4 * RTOD
      RGAMMA= RGAM4
      RC = RCTETA
      V=V2
      GO TO 230
   32 RC = RCPOW / PHI
      RGAM2  = ASIN(RC    /(101.34 * V2     ))
      GAMMA = RGAM2 * RTOD
      RGAMMA = RGAM2
      THETAF= ALPHL0 - EYEW + (RGAM2 + W*COS(RGAM2)/(Q*SW*CLALPH))*RTOD
      V   = V2
  230 IF (ENGIND.NE.0.0) GO TO 231
      F  = WSHPA * DELTA * STHETA * BHPP
      PEHF = 1.0
      IF (POWCLI(ICLIMB).EQ.0.) GO TO 23
      CALL POWAVL(TMAX,EM)
      PEHF = BLP / SHPA
! **  BLP ALREADY INCLUDES DSHPAC
      GO TO 23
  231 F  = WSHPA * DELTA * STHETA * TP
      PEHF = 1.0
      IF (POWCLI(ICLIMB).EQ.0.) GO TO 23
      CALL THRAVL(TMAX,EM)
      PEHF = TLP / SHPA
      GO TO 23
      END
      SUBROUTINE CRUS1  (ICRUS )
!**** MEMBER NAME = B93CRUS1
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10),TKETKE(10),EOEOEO(10),HHH(10),ETAETA(10)
      COMMON CLCLCL(10),CDCDCD(10),ALDALD(10),ELLELL(10),DEEDEE(10),    &
     & FFF(10),BHPBHP(10),TPTPTP(10),CCPCCP(10),CCTCCT(10),AJAJAJ(10),  &
     & PPP(10)
      COMMON/HAL/INDSGT,KE
! **  HAL IS PASSED AMOUNG CRUS1,3, PRFRM, PRFRP, LOITR
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  EO(7),OO3(2,4)
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
      NAMELIST/NCRUS1/DELBHP,DELB,DELT,DELT1,ITAB,YLS2,YLS1,INDATM,H, V,&
     &   Q,EM,CL,CD,ENGIND,ETAP4,BLP,BHPSUP,BHPA,BHPR,LC1,LC3,B1,DELV,SF&
     &C,F,EN,EAS,PEHF,PETF,RRR,STSTST,WWW,WFWFWF,VVV,ENENEN,EASEAS,EMEME&
     &M,EMDEMD,PEHFPE,IC2D,IRMAX,R,NQUO,YALE22,R9,RMAX,DELR1,ST,WF,W,B2,&
     &TLP,TSUBP,TA,TR,T1,TSFC,T2,EMPTY1,IFUDGE,                         &
     &BHPR1,BHPR2,BHPA1,BHPA2,ETAP1,ETAP2,TPROP1,TPROP2
 9000 FORMAT(/7X11HCRUISE AT  ,2A4,2X,13HENGINE RATING,10X,13HTEMPERATUR&
     &E =,F6.1,1X,5HDEG.F)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,19X4HMACH,  &
     &3X5HSPEC.,4X,4HETAP,                                              &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X3HEAS,5X4HMACH,3X3HDIV,4X5HRANGE,4X,4HPROP,   &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,26X6H(NMPP))
 9004 FORMAT(          /9X2HCL,8X2HCD,8X3HL/D,7X4HLIFT,6X4HDRAG,3X9HFUEL&
     & FLOW   ,2X3HBHP,9X6HTHRUST,3X2HCP,6X2HCT,6X1HJ,5X4HVTIP          &
     &/39X5H(LBS),5X5H(LBS),2X8H(LBS/HR),16X5H(LBS),25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,2XF6.1,3XF5.3,2XF5.3,3XF6.5,3XF5.3)
 9005 FORMAT( 7XF6.3,5XF6.4,4XF6.3,4XF7.0,2XF7.0,2XF7.0,2XF7.0,         &
     & 6XF7.0,2XF6.4,1XF6.4,1XF6.3,3XF5.1/)
      DATA  OO3(1,2)/4H MAX/ ,OO3(2,2)/4HIMUM/
      DATA  OO3(1,3)/4HMILI/ ,OO3(2,3)/4HTARY/
      DATA  OO3(1,4)/4H NOR/ ,OO3(2,4)/4HMAL /
      RMAX1 = RMAX(ICRUS)
      KOLIN = 0
      IFUDGE = ICRUS
      DO 2008 I=1,2
      IF (POWCRI(ICRUS)-1.0)  2005,2006,2007
 2005 OO3(I,1) = OO3(I,2)
      GO TO 2008
 2006 OO3(I,1) = OO3(I,3)
      GO TO 2008
 2007 OO3(I,1) = OO3(I,4)
 2008 END DO
      IF(NEXT.NE.0) GO TO 1210
      INDATM = ATMIND(ICRUS + 30)
      CALL ATMOS(H,ATMIND(ICRUS + 30),TIN(ICRUS + 30))
 1210 TEMP = THETA * 518.69  -  459.69
      IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 312
      IF(INDSGT.EQ.11) GO TO 312
      WRITE(6,9000) (OO3(I,1),I=1,2),TEMP
      WRITE(6,9001)
      IF (KPRINT.EQ.1) WRITE(6,9004)
  312 IF (ENGIND.NE.0.0) ETAP = 1.0
      AN2MAX  =  AN2M4(ICRUS)
      DELBHP=10.
      DELB  =  .01
      DELT  =10.
      DELT1 =  .01
      ITAB = 0
      YLS2=(ENP - ENPSD(ICRUS))/ ENP
      YLS1=1./YLS2
      IF (NEXT.EQ.0) GO TO 250
      NEXT = 0
      KOLIN = 1
      LC3  = 1
      VSAVE = V
      IF (ENGIND.EQ.0.) GO TO 255
      GO TO 256
  250 CONTINUE
      LC1=0
    1 V   = VM0 / SQRT(SIGMA)
      LC3=0
      IF(V.GT.(SA * EMM0)) V= SA * EMM0
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 700
      IF(INDSGT.EQ.11) GO TO 700
      VLMT = 250./SQRT(SIGMA)
      IF (V.GT.VLMT) V = VLMT
  700 Q   = 1.42636 * RHO * V**2
      EM  = V / SA
      CL  = W /(SW*Q)
      LC3=LC3 + 1
      CALL DRAG
      IF(NEXT.NE.0)RETURN
      CD  = CD + DLCDCR(ICRUS)
      IF (POWCRI(ICRUS)-1.0) 200,201,202
  200 TPS = TMAX
      GO TO 204
  201 TPS = TMIL
      GO TO 204
  202 TPS = TNRP
  204 IF (ENGIND.NE.0.) GO TO 4
      IF (ETAIND.NE.0.0) GO TO 26
      ETAP4  = XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      ETAP = ETAP4
      IF(M.NE.0)WRITE(6,1001)
      GO TO 28
   26 TPROP = CD*SW*Q
      CALL POWER(TPROP,YLS2,ETAP)
      BHPR = SHPR*BHPP*DELTA*STHETA*YLS2
! **  DSHPAC IS ACCOUNTED FOR IN THE POWER SUBROUTINE
   28 CALL POWAVL(TPS,EM)
      TKE = TPEA
      KE  = NPLIM +1
      BLP = SHPA
      WSHPR  = WSHPA
      BHPSUP= BLP * BHPP * DELTA * STHETA
      BHPA=BHPSUP*YLS2
      IF (ETAIND.NE.0.0) GO TO 27
      BHPR = CD * SW * Q * V /(325.8 * ETAP4* ETAT) + DSHPAC
   27 IF (ABS(BHPA-BHPR).LE.DELBHP) GO TO 900
      IF(LC3.GE.2)GO TO 100
      IF(BHPA.GT.BHPR) GO TO 101
      BHPR1 = BHPR
      BHPA1 = BHPA
      ETAP1 = ETAP
      TPROP1 = TPROP
  255 B1  = BHPR -BHPA
  701 DELV = 10.
  702 V    = V - DELV
      IF (V.GT.100.) GO TO 700
      GO TO 780
  101 BHPA = BHPR
      BHPSUP=BHPA*YLS1
! **  AT THIS POINT DSHPAC IS ALREADY ACCOUNTED FOR
      BLP=BHPSUP/(BHPP*DELTA*STHETA)
      SHPR   = BLP
      CALL  POWREQ(EM)
      TKE = TPEA
      KE = 6
  900 F = WSHPR*DELTA*STHETA*BHPP*YLS2
   99 EN=V/F
      IF (INOPTH.EQ.1) RETURN
      IF(OPTIND.EQ.1.) GO TO 11
      EAS=V*SQRT(SIGMA)
      IF(ENGIND.EQ.0.) GO TO 4256
      VQ=TSUBP/(DELTA*TP)
      CALL  THRAVL(TMAX,EM)
      GO TO 4257
 4256 VQ=BHPSUP/(DELTA*STHETA*BHPP)
      CALL POWAVL(TMAX,EM)
 4257 ANICE = SHPA
      PETF=VQ/ANICE
      PEHF=PETF
   11 IF(INDSGT.EQ.11) RETURN
      ITAB = ITAB + 1
      IF (ITAB.GT.10) GO TO 706
    2 RRR(ITAB) = R
      STSTST(ITAB)= ST
      WWW(ITAB) = W
      WFWFWF(ITAB) = WF
      VVV(ITAB)    = V
      ENENEN(ITAB) = EN
      EASEAS(ITAB) = EAS
      EMEMEM(ITAB) = EM
      EMDEMD(ITAB) = EMD
      PEHFPE(ITAB) = PEHF
      TKETKE(ITAB)  = TKE
      EOEOEO(ITAB)  = EO(KE)
      ETAETA(ITAB) = ETAP
      HHH(ITAB) = H
      IF (KPRINT.EQ.1.AND.OPTIND.EQ.2.0) CALL SCRIBE(ITAB,ETAP,YLS2)
      IC2D=ITAB
      IRMAX=ICRUS
      IF (R.GE.RMAX1) GO TO 22267
      NQUO = R / DELR(ICRUS) + .05
      YALE22 = NQUO + 1
      R9     = YALE22 * DELR(ICRUS)
      IF (RMAX1.LT.R9) GO TO 3
      DELR1 = R9 - R
  709 R   = R9
      ST = ST + DELR1 / V
      WF = WF + F * DELR1/V
      W   =W - F * DELR1/ V
      V = VSAVE
      LC3 = 0
      IF (KOLIN.EQ.1) GO TO 700
      GO TO 1
    3 DELR1 = RMAX1 - R
      R9 = RMAX1
      GO TO 709
  706 IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 314
      WRITE(6,9002) STSTST(1),RRR(1),WFWFWF(1),WWW(1),                  &
     &H,VVV(1),TKETKE(1),EOEOEO(1),PEHFPE(1),EASEAS(1),EMEMEM(1),       &
     &EMDEMD(1),ENENEN(1),ETAETA(1)
      IF (KPRINT.EQ.0) GO TO 314
      WRITE(6,9005) CLCLCL(1),CDCDCD(1),ALDALD(1),ELLELL(1),DEEDEE(1),  &
     & FFF(1),BHPBHP(1),TPTPTP(1),CCPCCP(1),CCTCCT(1),AJAJAJ(1),PPP(1)
      DO 13146 I = 1,9
      PPP(I) = PPP(I+1)
      CLCLCL(I) = CLCLCL(I+1)
      CDCDCD(I) = CDCDCD(I+1)
      ALDALD(I) = ALDALD(I+1)
      ELLELL(I) = ELLELL(I+1)
      DEEDEE(I) = DEEDEE(I+1)
      FFF(I) = FFF(I+1)
      BHPBHP(I) = BHPBHP(I+1)
      TPTPTP(I) = TPTPTP(I+1)
      CCPCCP(I) = CCPCCP(I+1)
      CCTCCT(I) = CCTCCT(I+1)
13146 AJAJAJ(I) = AJAJAJ(I+1)
  314 DO 13145 I = 1,9
      RRR(I) = RRR(I+1)
      STSTST(I) = STSTST(I+1)
      WFWFWF(I) = WFWFWF(I+1)
      ENENEN(I) = ENENEN(I+1)
      EASEAS(I) = EASEAS(I+1)
      EMEMEM(I) = EMEMEM(I+1)
      EMDEMD(I) = EMDEMD(I+1)
      PEHFPE(I) = PEHFPE(I+1)
      VVV(I)    = VVV(I+1)
      TKETKE(I)  =  TKETKE(I+1)
      EOEOEO(I)     = EOEOEO(I+1)
      ETAETA(I) = ETAETA(I+1)
      HHH(I) = HHH(I+1)
13145 WWW(I) = WWW(I+1)
      ITAB = ITAB - 1
      GO TO 2
  704 IF (B2.LT.0.) GO TO 705
      GO TO 770
  100 B2  = BHPR - BHPA
      BHPR2 = BHPR
      BHPA2 = BHPA
      TPROP2 = TPROP
      IF(ABS((B1 - B2)/ B2).LE.DELB) GO TO 770
      DELV = DELV * B2 /(B1 - B2)
      IF (DELV.LT.0.) GO TO 704
  705 B1  = B2
      BHPR1 = BHPR2
      BHPA1 = BHPA2
      ETAP1 = ETAP2
      TPROP1 = TPROP2
      GO TO 702
    4 CALL THRAVL(TPS,EM)
      TKE = TPEA
      KE  = NPLIM +1
      WSHPR = WSHPA
      TLP = SHPA
      TSUBP= TLP * DELTA * TP
      TA=TSUBP*YLS2
      TR  = CD * SW * Q
      IF(ABS(TA - TR).LE.DELT) GO TO 901
      IF(LC3.GE.2)GO TO 102
      IF(TA.GT.TR)GO TO 103
  256 T1  = TR -TA
      GO TO 701
  103 TA= TR
      TSUBP=TA*YLS1
      TLP=TSUBP/(DELTA*TP)
      SHPR = TLP
      CALL THRREQ(EM)
      TKE = TPEA
      KE = 6
  901 F = WSHPR*DELTA*STHETA*TP*YLS2
      GO TO 99
  711 IF (T2.LT.0.) GO TO 710
      GO TO 770
  102 T2= TR -TA
      IF (ABS((T1-T2)/T2).LE.DELT1) GO TO 770
      DELV = DELV * T2 /(T1 - T2)
      IF (DELV.LT.0.) GO TO 711
  710 T1  = T2
      GO TO 702
!     EMPTY1  STANDS FOR THE NEXT  SGTIND (SET IN PRFRM LOOP)
!          IF EMPTY1 = 5.(DESCENT) WE DO NOT PRINT TABLE UNTIL DSCNT RT.
22267 IF((OPTIND.NE.2.).OR.(EMPTY1.EQ.5.)) RETURN
      IF (INOPTH.EQ.1) RETURN
      DO 13147 I = 1,ITAB
      WRITE(6,9002) STSTST(I),RRR(I),WFWFWF(I),WWW(I),H,VVV(I),         &
     &TKETKE(I),EOEOEO(I),PEHFPE(I),EASEAS(I),EMEMEM(I),EMDEMD(I),      &
     & ENENEN(I),ETAETA(I)
      IF (KPRINT.EQ.1) WRITE(6,9005)  CLCLCL(I),CDCDCD(I),ALDALD(I),    &
     & ELLELL(I),DEEDEE(I),FFF(I),BHPBHP(I),TPTPTP(I),CCPCCP(I),        &
     & CCTCCT(I),AJAJAJ(I),PPP(I)
13147 END DO
      RETURN
  770 IF (INOPTH.EQ.1) GO TO 613
      WRITE(6,5224)
 5224 FORMAT(//2X5HERROR100(1H*)//)
      WRITE(6,1004)
      WRITE(6,5224)
      WRITE(6,NCRUS1)
      NEXT=1
      RETURN
  780 IF (INOPTH.EQ.1) GO TO 613
      WRITE(6,5224)
      WRITE(6,1004)
      WRITE(6,1044)
 1044 FORMAT(9X27HAT V GREATER THAN 150 KNOTS)
      WRITE(6,5224)
      WRITE(6,NCRUS1)
  613 NEXT = 1
      RETURN
 1001 FORMAT(22X35HTHIS ERROR IS IN THE M-ETAP4 TABLE )
 1009 FORMAT(5X,6E14.7)
 1004 FORMAT(/9X,39HINSUFFICIENT POWER AVAILABLE FOR CRUISE)
      END
      SUBROUTINE CRUS2(ICRUS )
!**** MEMBER NAME = B93TCRS2
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10),TKETKE(10),EOEOEO(10),HHH(10),ETAETA(10)
      COMMON CLCLCL(10),CDCDCD(10),ALDALD(10),ELLELL(10),DEEDEE(10),    &
     & FFF(10),BHPBHP(10),TPTPTP(10),CCPCCP(10),CCTCCT(10),AJAJAJ(10),  &
     & PPP(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  EO(7),OO3(2,4)
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
      NAMELIST/NCRUS2/ITAB,YLS2,YLS1,INDATM,    V,H,Q,EM,CL,CD,ENGIND,   &
     & ETAP4,BHPR,BLP,BHPSUP,BHPA,SFC,F,TR,TLP,TSUBP,TA,TSFC,EN,EAS,     &
     & PETF,PEHF,RRR,STSTST,WWW,WFWFWF,VVV,ENENEN,EASEAS,EMEMEM,         &
     & EMDEMD,PEHFPE,IC2D,IRMAX,R,RMAX,NQUO,YALE22,R9,DELR1,ST,WF,W,     &
     & EMPTY1,IFUDGE
 9000 FORMAT(/7X11HCRUISE AT  ,F6.1,2X,22HKNOTS TAS, LIMITED BY ,2A4,2X,&
     &13HENGINE RATING,10X,13HTEMPERATURE =,F6.1,1X,5HDEG.F)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,19X4HMACH,  &
     &3X5HSPEC.,4X,4HETAP,                                              &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X3HEAS,5X4HMACH,3X3HDIV,4X5HRANGE,4X,4HPROP,   &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,26X6H(NMPP))
 9004 FORMAT(          /9X2HCL,8X2HCD,8X3HL/D,7X4HLIFT,6X4HDRAG,3X9HFUEL&
     & FLOW   ,2X3HBHP,9X6HTHRUST,3X2HCP,6X2HCT,6X1HJ,5X4HVTIP          &
     &/39X5H(LBS),5X5H(LBS),2X8H(LBS/HR),16X5H(LBS),25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,2XF6.1,3XF5.3,2XF5.3,3XF6.5,3XF5.3)
 9005 FORMAT( 7XF6.3,5XF6.4,4XF6.3,4XF7.0,2XF7.0,2XF7.0,2XF7.0,         &
     & 6XF7.0,2XF6.4,1XF6.4,1XF6.3,3XF5.1/)
      DATA  OO3(1,2)/4H MAX/ ,OO3(2,2)/4HIMUM/
      DATA  OO3(1,3)/4HMILI/ ,OO3(2,3)/4HTARY/
      DATA  OO3(1,4)/4H NOR/ ,OO3(2,4)/4HMAL /
      RMAX1 = RMAX(ICRUS)
      IFUDGE = ICRUS
      DO 2008 I=1,2
      IF (POWCRI(ICRUS)-1.0)  2005,2006,2007
 2005 OO3(I,1) = OO3(I,2)
      GO TO 2008
 2006 OO3(I,1) = OO3(I,3)
      GO TO 2008
 2007 OO3(I,1) = OO3(I,4)
 2008 END DO
 1001 FORMAT(22X35HTHIS ERROR IS IN THE M-ETAP4 TABLE )
 1006 FORMAT(19X,56HINSUFFICIENT POWER AVAILABLE FOR CRUISE AT DESIRED S&
     &PEED)
      INDATM = ATMIND(ICRUS + 30)
      CALL ATMOS(H,ATMIND(ICRUS + 30),TIN(ICRUS + 30))
      TEMP = THETA * 518.69  -  459.69
      IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 32
      WRITE(6,9000) VIN(ICRUS),(OO3(I,1),I=1,2),TEMP
      WRITE(6,9001)
      IF (KPRINT.EQ.1) WRITE(6,9004)
   32 IF (ENGIND.NE.0.0) ETAP = 1.0
      AN2MAX  =  AN2M4(ICRUS)
      ITAB=0
      YLS2 = (ENP - ENPSD(ICRUS))/ENP
      YLS1 = 1./YLS2
      V   =VIN(ICRUS)
      IF(V.GT.(VM0/SQRT(SIGMA))) V= VM0/SQRT(SIGMA)
      IF(V.GT.(SA*EMM0)) V= SA * EMM0
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 1
      VLMT = 250./SQRT(SIGMA)
      IF (V.GT.VLMT) V = VLMT
    1 Q   = 1.42636 * RHO * V**2
      EM  = V / SA
      CL  = W /(SW * Q)
      CALL DRAG
      IF(NEXT.NE.0) RETURN
      CD   = CD + DLCDCR(ICRUS)
      IF (POWCRI(ICRUS)-1.0)  200,201,202
  200 TPS = TMAX
      GO TO 204
  201 TPS = TMIL
      GO TO 204
  202 TPS = TNRP
  204 IF (ENGIND.NE.0.) GO TO 2
      IF (ETAIND.NE.0.0) GO TO 26
      ETAP4= XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      ETAP = ETAP4
      IF(M.NE.0) WRITE(6,1001)
      BHPR= CD * SW * Q * V /(325.8 *ETAP4* ETAT) + DSHPAC
      GO TO 28
   26 TPROP = CD*SW*Q
      CALL POWER(TPROP,YLS2,ETAP)
      BHPR = SHPR*BHPP*DELTA*STHETA*YLS2
! **  DSHPAC IS ACCOUNTED FOR IN THE POWER SUBROUTINE
   28 CALL POWAVL(TPS,EM)
      TKE = TPEA
      KE  = NPLIM +1
      BLP = SHPA
      BHPSUP = BLP * DELTA * STHETA * BHPP
      BHPA = BHPSUP * YLS2
      IF(BHPR.GT.BHPA) GO TO 3
      BHPA= BHPR
      BHPSUP= BHPA * YLS1
      BLP  = BHPSUP/(BHPP * DELTA * STHETA)
! **  BHPSUP INCLUDES DSHPAC DUE TO PRIOR CALCULATION OF BHPR
      SHPR  = BLP
      CALL POWREQ(EM)
      WSHPA  = WSHPR
      TKE  = TPEA
      KE   = 6
      F = WSHPA*DELTA*STHETA*BHPP*YLS2
      GO TO 99
    2 TR=CD*Q*SW
      CALL  THRAVL(TPS,EM)
      TKE = TPEA
      KE  = NPLIM +1
      TLP = SHPA
      TSUBP = TLP * TP * DELTA
      TA   = TSUBP * YLS2
      IF(TR.GT.TA) GO TO 3
      TA=TR
      TSUBP= TA * YLS1
      TLP  = TSUBP /(TP * DELTA)
      SHPR  = TLP
      CALL THRREQ(EM)
      WSHPA  = WSHPR
      TKE  = TPEA
      KE   = 6
      F = WSHPA*DELTA*STHETA*TP*YLS2
   99 EN=V/F
      IF (INOPTH.EQ.1) RETURN
      IF(OPTIND.EQ.1.) GO TO 11
      EAS=V*SQRT(SIGMA)
      IF(ENGIND.EQ.0.) GO TO 4256
      VQ=TSUBP/(DELTA*TP)
      CALL  THRAVL(TMAX,EM)
      GO TO 4257
 4256 VQ=BHPSUP/(DELTA*STHETA*BHPP)
      CALL  POWAVL(TMAX,EM)
 4257 ANICE  = SHPA
      PETF=VQ/ANICE
      PEHF=PETF
   11 ITAB = ITAB + 1
      IF (ITAB.GT.10)  GO TO 901
   22 RRR(ITAB) = R
      STSTST(ITAB)= ST
      WWW(ITAB) = W
      WFWFWF(ITAB) = WF
      VVV(ITAB)    = V
      ENENEN(ITAB) = EN
      EASEAS(ITAB) = EAS
      EMEMEM(ITAB) = EM
      EMDEMD(ITAB) = EMD
      PEHFPE(ITAB) = PEHF
      TKETKE(ITAB)  = TKE
      EOEOEO(ITAB)  = EO(KE)
      ETAETA(ITAB) = ETAP
      HHH(ITAB) = H
      IF (KPRINT.EQ.1.AND.OPTIND.EQ.2.0) CALL SCRIBE(ITAB,ETAP,YLS2)
      IC2D=ITAB
      IRMAX=ICRUS
      IF (R.GE.RMAX1) GO TO 22267
      NQUO = R / DELR(ICRUS) + .05
      YALE22 = NQUO + 1
      R9     = YALE22 * DELR(ICRUS)
      IF (RMAX1.LT.R9) GO TO 333
      DELR1 = R9 - R
  902 R  = R9
      ST  =ST +DELR1/V
      WF = WF + F * DELR1/V
      W   =W - F * DELR1/ V
      GO TO 1
  333 DELR1 = RMAX1 - R
      R9 = RMAX1
      GO TO 902
  901 IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 35
      WRITE(6,9002) STSTST(1),RRR(1),WFWFWF(1),WWW(1),                  &
     &H,VVV(1),TKETKE(1),EOEOEO(1),PEHFPE(1),EASEAS(1),EMEMEM(1),       &
     &EMDEMD(1),ENENEN(1),ETAETA(1)
      IF (KPRINT.EQ.0)GO TO 35
      WRITE(6,9005) CLCLCL(1),CDCDCD(1),ALDALD(1),ELLELL(1),DEEDEE(1),  &
     & FFF(1),BHPBHP(1),TPTPTP(1),CCPCCP(1),CCTCCT(1),AJAJAJ(1),PPP(1)
      DO 13146 I = 1,9
      PPP(I) = PPP(I+1)
      ALDALD(I) = ALDALD(I+1)
      CLCLCL(I) = CLCLCL(I+1)
      CDCDCD(I) = CDCDCD(I+1)
      ELLELL(I) = ELLELL(I+1)
      DEEDEE(I) = DEEDEE(I+1)
      FFF(I) = FFF(I+1)
      BHPBHP(I) = BHPBHP(I+1)
      TPTPTP(I) = TPTPTP(I+1)
      CCPCCP(I) = CCPCCP(I+1)
      CCTCCT(I) = CCTCCT(I+1)
13146 AJAJAJ(I) = AJAJAJ(I+1)
   35 DO 13145 I = 1,9
      RRR(I) = RRR(I+1)
      HHH(I) = HHH(I+1)
      STSTST(I) = STSTST(I+1)
      WFWFWF(I) = WFWFWF(I+1)
      ENENEN(I) = ENENEN(I+1)
      EASEAS(I) = EASEAS(I+1)
      EMEMEM(I) = EMEMEM(I+1)
      EMDEMD(I) = EMDEMD(I+1)
      PEHFPE(I) = PEHFPE(I+1)
      VVV(I)    = VVV(I+1)
      TKETKE(I)  =  TKETKE(I+1)
      EOEOEO(I)     = EOEOEO(I+1)
      ETAETA(I) = ETAETA(I+1)
13145 WWW(I) = WWW(I+1)
      ITAB = ITAB - 1
      GO TO 22
!     EMPTY1  STANDS FOR THE NEXT  SGTIND (SET IN PRFRM LOOP)
!          IF EMPTY1 = 5.(DESCENT) WE DO NOT PRINT TABLE UNTIL DSCNT RT.
22267 IF((OPTIND.NE.2.).OR.(EMPTY1.EQ.5.)) RETURN
      IF (INOPTH.EQ.1) RETURN
      DO 13147 I = 1,ITAB
      WRITE(6,9002) STSTST(I),RRR(I),WFWFWF(I),WWW(I),H,VVV(I),         &
     &TKETKE(I),EOEOEO(I),PEHFPE(I),EASEAS(I),EMEMEM(I),EMDEMD(I),      &
     & ENENEN(I),ETAETA(I)
      IF (KPRINT.EQ.1) WRITE(6,9005)  CLCLCL(I),CDCDCD(I),ALDALD(I),    &
     & ELLELL(I),DEEDEE(I),FFF(I),BHPBHP(I),TPTPTP(I),CCPCCP(I),        &
     & CCTCCT(I),AJAJAJ(I),PPP(I)
13147 END DO
      RETURN
    3 IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 33
 5224 FORMAT(//2X5HERROR100(1H*)//)
      WRITE(6,1006)
   33 NEXT = 1
      CALL CRUS1(ICRUS)
      RETURN
      END
      SUBROUTINE CRUS3(ICRUS)
!**** MEMBER NAME = B93TCRS3
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC. 2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10),  AN2M7(10) , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  ,  LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10),TKETKE(10),EOEOEO(10),HHH(10),ETAETA(10)
      COMMON CLCLCL(10),CDCDCD(10),ALDALD(10),ELLELL(10),DEEDEE(10),    &
     & FFF(10),BHPBHP(10),TPTPTP(10),CCPCCP(10),CCTCCT(10),AJAJAJ(10),  &
     & PPP(10)
      COMMON/HAL/INDSGT,KE
! **  HAL IS PASSED AMOUNG CRUS1,3, PRFRM, PRFRP, LOITR
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  EO(7)
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
      NAMELIST/NCRUS3/ITAB,YLS1,YLS2,INDATM,H,LC3,LC4,LC5,V,EM,Q,CL,CD,E&
     &TAP4,BLP,BHPSUP,BHPA,BHPR,SFC,F,TLP,TSUBP,TA,TR,TSFC,EN,EN1,V,EN2,&
     &EAS,PEHF,PETF,RRR,STSTST,WWW,WFWFWF,VVV,ENENEN,EASEAS,EMEMEM,EMDEM&
     &D,PEHFPE,IC2D,IRMAX,RMAX,R,NQUO,YALE22,R9,DELR1,ST,WF,W,EMPTY1,IFU&
     &DGE
 9000 FORMAT(/7X,'CRUISE AT BEST RANGE SPEED WITH HEADWIND OF ',F5.1,' K&
     &NOTS',10X,'TEMPERATURE =',F6.1,1X,'DEG.F')
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,19X4HMACH,  &
     &3X5HSPEC.,4X,4HETAP,                                              &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X3HEAS,5X4HMACH,3X3HDIV,4X5HRANGE,4X,4HPROP,   &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,26X6H(NMPP))
 9005 FORMAT(          /9X2HCL,8X2HCD,8X3HL/D,7X4HLIFT,6X4HDRAG,3X9HFUEL&
     & FLOW   ,2X3HBHP,9X6HTHRUST,3X2HCP,6X2HCT,6X1HJ,5X4HVTIP          &
     &/39X5H(LBS),5X5H(LBS),2X8H(LBS/HR),16X5H(LBS),25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,2XF6.1,3XF5.3,2XF5.3,3XF6.5,3XF5.3)
 9006 FORMAT( 7XF6.3,5XF6.4,4XF6.3,4XF7.0,2XF7.0,2XF7.0,2XF7.0,         &
     & 6XF7.0,2XF6.4,1XF6.4,1XF6.3,3XF5.1/)
      RMAX1 = RMAX(ICRUS)
      INDIC = 0
      V = VSAVE + 20.
      IFUDGE = ICRUS
      LC6 = 0
      PI = 3.14159
      NOCPP = CPPNO + 0.1
      CALL ATMOS(H,ATMIND(ICRUS+30),TIN(ICRUS+30))
      TEMP = THETA * 518.69  -  459.69
      WOVDEL = W/DELTA
      IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 41
      IF(INDSGT.EQ.11) GO TO 41
      IF (CRSIND(ICRUS).EQ.4.0.OR.CRSIND(ICRUS).EQ.6.0) GO TO 42
      WRITE(6,9000) VIN(ICRUS),TEMP
      GO TO 43
   42 WRITE(6,9003) VIN(ICRUS),TEMP
   43 IF (CRSIND(ICRUS).GT.4.0) WRITE(6,9004) WOVDEL
      WRITE(6,9001)
 9004 FORMAT(7X,'CRUISE-CLIMB AT CONSTANT W/DELTA = ',F8.0)
      IF (KPRINT.EQ.1) WRITE(6,9005)
   41 IF (ENGIND.NE.0.0) ETAP = 1.0
 9003 FORMAT(/7X,'CRUISE AT SPEED FOR 99 PER CENT BEST RANGE WITH HEADWI&
     &ND OF ',F5.1,' KNOTS',10X,'TEMPERATURE =',F6.1,1X,'DEG.F')
      AN2MAX  =  AN2M4(ICRUS)
      ITAB=0
      YLS2 = (ENP - ENPSD(ICRUS)) / ENP
      YLS1 = 1. / YLS2
      INDATM=ATMIND(ICRUS+30)
      LC1 = 0
      LC2 = 0
      LC3=0
      LC4=0
      LC5=0
      LC9 = 0
   52 VMAX = VM0/SQRT(SIGMA)
      IF (VMAX.GT.(SA*EMM0)) VMAX = SA*EMM0
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 1
      IF(INDSGT.EQ.11) GO TO 1
      VLMT = 250./SQRT(SIGMA)
      IF (VMAX.GT.VLMT) VMAX = VLMT
    1 IF (INOPTH.EQ.0.AND.INDIC.EQ.0) V = VMAX
      IF (INOPTH.EQ.1.AND.WFTOTO.EQ.0.0) V = VMAX
      IF (V.GT.VMAX) V = VMAX
      IF(ETAIND.EQ.1.0.OR.ETAIND.EQ.3.0) GO TO 31
      GO TO 2
   31 LC3 = LC3 + 1
      Q = 1.42636*RHO*V**2
      EM = V/SA
      CL = W/(SW*Q)
      CALL DRAG
      IF(NEXT.NE.0) RETURN
      CD = CD + DLCDCR(ICRUS)
      TT3 = CD*SW*Q
      P = A2STR*VT
      CCT = (PI**3)*TT3*WGA/(0.009507*SIGMA*WG*P**2)
      IF(ETAIND.NE.3.0) GO TO 447
      TOAD=CCT*(P/STHETA)**2/3261.21
      IF(TOAD.LE.CPPROP(NOCPP)) GO TO 224
      GO TO 448
  447 IF(CCT.LE.CPPROP(NOCPP)) GO TO 224
  448 V=V-10.
      IF (V.LE.100.) GO TO 11
      GO TO 31
    2 EM=V/SA
      Q=1.42636*RHO*V**2
  222 LC3=LC3 + 1
      CL=W/(SW*Q)
      CALL DRAG
      IF(NEXT.NE.0)RETURN
      CD  = CD + DLCDCR(ICRUS)
  224 IF (ENGIND.NE.0.0) GO TO 3
      IF (ETAIND.NE.0.0) GO TO 26
      ETAP4=XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      ETAP = ETAP4
      IF(M.NE.0)WRITE(6,1001)
   26 CALL POWAVL(TNRP,EM)
      TKE = TPEA
      KE  = NPLIM +1
      BLP  = SHPA
      BHPSUP= BLP * BHPP * DELTA * STHETA
      BHPA  = BHPSUP * YLS2
      IF (ETAIND.NE.0.0) GO TO 29
      BHPR=CD*SW*Q*V/(325.8*ETAP4*ETAT) + DSHPAC
      GO TO 28
   29 TPROP = CD*SW*Q
      CALL POWER(TPROP,YLS2,ETAP)
      BHPR = SHPR*BHPP*DELTA*STHETA*YLS2
! **  DSHPAC IS ACCOUNTED FOR IN THE POWER SUBROUTINE
   28 IF (BHPA.GE.BHPR) GO TO 4
      F = WSHPA*DELTA*STHETA*BHPP*YLS2
      IF (LC5.EQ.0) GO TO 62
      IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 6
      IF(INDSGT.EQ.11) GO TO 6
      IF (LC9.EQ.0) WRITE(6,92)
   92 FORMAT(10X,'+++ CAUTION - SPEED LIMITED BY POWER AVAILABLE AT SPEC&
     &IFIED POWER SETTING +++')
      LC9 = 1
      GO TO 6
   62 EN1 = 0.0
      GO TO 77
    3 CALL THRAVL(TNRP,EM)
      TKE = TPEA
      KE  = NPLIM +1
      TLP = SHPA
      TSUBP = TLP * DELTA * TP
      TA    = TSUBP * YLS2
      TR=CD*SW*Q
      IF(TA.GE.TR)GO TO 8
      F = WSHPA*DELTA*STHETA*TP*YLS2
      IF (LC5.EQ.0) GO TO 82
      IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 6
      IF(INDSGT.EQ.11) GO TO 6
      IF (LC9.EQ.0) WRITE(6,112)
  112 FORMAT(10X,'+++ CAUTION - SPEED LIMITED BY THRUST AVAILABLE AT SPE&
     &CIFIED POWER SETTING ')
      LC9 = 1
      GO TO 6
   82 EN1 = 0.0
      GO TO 77
    4 BHPA = BHPR
      BHPSUP= BHPA * YLS1
      BLP   = BHPSUP /(BHPP * DELTA * STHETA)
      SHPR = BLP
      CALL  POWREQ(EM)
      TKE = TPEA
      KE = 6
      F = WSHPR*DELTA*STHETA*BHPP*YLS2
      GO TO 9
    5 IF (CRSIND(ICRUS).NE.4.0.AND.CRSIND(ICRUS).NE.6.0) GO TO 6
      IF (LC6.EQ.0) GO TO 17
      IF (EN2.GT.EN3) GO TO 27
      GO TO 6
   17 EN3 = 0.99*EN1
      LC6 = 1
   27 V = V - 0.5
      GO TO 13
    6 EN = EN1
      VSAVE = V
      GO TO 10
    7 EN1 = (V - VIN(ICRUS))/F
   77 V  = V -10.0
      IF (INDIC.EQ.1) V = V + 5.
      GO TO 11
    8 TA = TR
      TSUBP = TA * YLS1
      TLP=TSUBP /(DELTA * TP)
      SHPR = TLP
      CALL  THRREQ(EM)
      TKE = TPEA
      KE = 6
      F = WSHPR*DELTA*STHETA*TP*YLS2
    9 LC5=1
      IF(LC3.LT.2)GO TO 7
      EN2 = (V - VIN(ICRUS))/F
      IF(EN2.GT.EN1) GO TO 12
      IF(LC4.NE.1)GO TO 13
      GO TO 5
   11 IF(V.GT.100.)GO TO 2
   33 IF (INOPTH.EQ.1) GO TO 613
      WRITE(6,5224)
      WRITE(6,1006)
      WRITE(6,NCRUS3)
 5224 FORMAT(//2X5HERROR100(1H*)//)
      WRITE(6,5224)
  613 NEXT = 1
      RETURN
   12 IF(LC4.NE.1)GO TO 16
   13 V=V+1.
      IF (V.GT.VMAX) V = VMAX
      IF (V.GE.VMAX) GO TO 6
      IF(ETAIND.EQ.0.0.OR.ETAIND.EQ.2.0) GO TO 32
      Q = 1.42636*RHO*V**2
      EM = V/SA
      CL = W/(SW*Q)
      CALL DRAG
      IF (NEXT.NE.0) RETURN
      CD = CD + DLCDCR(ICRUS)
      TT3 = CD*SW*Q
      P = A2STR*VT
      CCT = (PI**3)*TT3*WGA/(0.009507*SIGMA*WG*P**2)
      IF(ETAIND.NE.3.0) GO TO 449
      TOAD=CCT*(P/STHETA)**2/3261.21
      IF(TOAD.GT.CPPROP(NOCPP)) GO TO 33
      GO TO 450
  449 IF(CCT.GT.CPPROP(NOCPP)) GO TO 33
  450 CONTINUE
   32 EN1 = EN2
      LC4=1
      GO TO 11
   16 EN1=EN2
      V=V-10.
      IF (INDIC.EQ.1) V = V + 5.
      GO TO 11
   10 IF (INOPTH.EQ.1) RETURN
      IF(OPTIND.EQ.1.0) GO TO 111
      EAS=V*SQRT(SIGMA)
      IF(ENGIND.EQ.0.) GO TO 4256
      VQ=TSUBP/(DELTA*TP)
      CALL  THRAVL(TMAX,EM)
      GO TO 4257
 4256 VQ=BHPSUP/(DELTA*STHETA*BHPP)
      CALL  POWAVL(TMAX,EM)
 4257 ANICE  = SHPA
      PETF=VQ/ANICE
      PEHF=PETF
      IF(INDSGT.EQ.11) RETURN
  111 ITAB = ITAB + 1
      IF (ITAB.GT.10) GO TO 900
   22 RRR(ITAB) = R
      STSTST(ITAB)= ST
      WWW(ITAB) = W
      WFWFWF(ITAB) = WF
      VVV(ITAB)    = V
      ENENEN(ITAB) = EN
      EASEAS(ITAB) = EAS
      EMEMEM(ITAB) = EM
      EMDEMD(ITAB) = EMD
      PEHFPE(ITAB) = PEHF
      TKETKE(ITAB)  = TKE
      EOEOEO(ITAB)  = EO(KE)
      ETAETA(ITAB) = ETAP
      HHH(ITAB) = H
      IF (KPRINT.EQ.1.AND.OPTIND.EQ.2.0) CALL SCRIBE(ITAB,ETAP,YLS2)
      IC2D=ITAB
      IRMAX=ICRUS
      IF (LC1.EQ.1) GO TO 22267
      IF (LC2.EQ.1) GO TO 621
      NQUO = R / DELR(ICRUS) + .05
      YALE22 = NQUO + 1
      R9     = YALE22 * DELR(ICRUS)
      LC2 = 1
      DELR1 = R9 - R
      IF (R9.GT.RMAX1) GO TO 901
      GO TO 333
  621 R9 = R + DELR(ICRUS)
  622 IF (R9.GT.RMAX1) GO TO 901
      DELR1 = DELR(ICRUS)
      GO TO 333
  901 DELR1 = RMAX1 - R
      LC1 = 1
  333 R = R + DELR1
      ST = ST + DELR1/(V-VIN(ICRUS))
      WF = WF + DELR1/EN
      W = W - DELR1/EN
      LC3=0
      LC4=0
      LC5=0
      LC6 = 0
      INDIC = 1
      IF (CRSIND(ICRUS).EQ.3.0.OR.CRSIND(ICRUS).EQ.4.0) GO TO 222
      DELTA = W/WOVDEL
      IF (H.GT.36089.) GO TO 83
      H = (1.0 - DELTA**(1./5.2561))/6.875E-06
      GO TO 84
   83 H = 4931.4 - 20786.0*(ALOG(DELTA))
   84 CALL ATMOS(H,ATMIND(ICRUS+30),TIN(ICRUS+30))
      GO TO 52
  900 IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 907
      WRITE(6,9002) STSTST(1),RRR(1),WFWFWF(1),WWW(1),                  &
     &HHH(1),VVV(1),TKETKE(1),EOEOEO(1),PEHFPE(1),EASEAS(1),EMEMEM(1),  &
     &EMDEMD(1),ENENEN(1),ETAETA(1)
      IF (KPRINT.EQ.0)GO TO 907
      WRITE(6,9006) CLCLCL(1),CDCDCD(1),ALDALD(1),ELLELL(1),DEEDEE(1),  &
     & FFF(1),BHPBHP(1),TPTPTP(1),CCPCCP(1),CCTCCT(1),AJAJAJ(1),PPP(1)
      DO 13146 I = 1,9
      PPP(I) = PPP(I+1)
      CLCLCL(I) = CLCLCL(I+1)
      CDCDCD(I) = CDCDCD(I+1)
      ALDALD(I) = ALDALD(I+1)
      ELLELL(I) = ELLELL(I+1)
      DEEDEE(I) = DEEDEE(I+1)
      FFF(I) = FFF(I+1)
      BHPBHP(I) = BHPBHP(I+1)
      TPTPTP(I) = TPTPTP(I+1)
      CCPCCP(I) = CCPCCP(I+1)
      CCTCCT(I) = CCTCCT(I+1)
13146 AJAJAJ(I) = AJAJAJ(I+1)
  907 DO 13145 I = 1,9
      RRR(I) = RRR(I+1)
      STSTST(I) = STSTST(I+1)
      WFWFWF(I) = WFWFWF(I+1)
      ENENEN(I) = ENENEN(I+1)
      EASEAS(I) = EASEAS(I+1)
      EMEMEM(I) = EMEMEM(I+1)
      EMDEMD(I) = EMDEMD(I+1)
      PEHFPE(I) = PEHFPE(I+1)
      VVV(I)    = VVV(I+1)
      TKETKE(I)  =  TKETKE(I+1)
      EOEOEO(I)     = EOEOEO(I+1)
      ETAETA(I) = ETAETA(I+1)
      HHH(I) = HHH(I+1)
13145 WWW(I) = WWW(I+1)
      ITAB = ITAB - 1
      GO TO 22
!     EMPTY1  STANDS FOR THE NEXT  SGTIND (SET IN PRFRM LOOP)
!          IF EMPTY1 = 5.(DESCENT) WE DO NOT PRINT TABLE UNTIL DSCNT RT.
22267 IF((OPTIND.NE.2.).OR.(EMPTY1.EQ.5.)) RETURN
      IF (INOPTH.EQ.1) RETURN
      DO 13147 I = 1,ITAB
      WRITE(6,9002)  STSTST(I),RRR(I),WFWFWF(I),WWW(I),HHH(I),VVV(I),   &
     &TKETKE(I),EOEOEO(I),PEHFPE(I),EASEAS(I),EMEMEM(I),EMDEMD(I),      &
     & ENENEN(I),ETAETA(I)
      IF (KPRINT.EQ.1) WRITE(6,9006)  CLCLCL(I),CDCDCD(I),ALDALD(I),    &
     & ELLELL(I),DEEDEE(I),FFF(I),BHPBHP(I),TPTPTP(I),CCPCCP(I),        &
     & CCTCCT(I),AJAJAJ(I),PPP(I)
13147 END DO
      RETURN
 1001 FORMAT(22X35HTHIS ERROR IS IN THE M-ETAP4 TABLE )
 1006 FORMAT(/9X,39HINSUFFICIENT POWER AVAILABLE FOR CRUISE             &
     &       /9X,27HAT V GREATER THAN 100 KNOTS)
      END
      SUBROUTINE DRAG
!**** MEMBER NAME = B93TDRAG
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      IF(INDDRG.EQ.1) GO TO 1
      EMD = SA1 + SA2 * CL
      IF(EM.LE.EMD) GO TO 2
      IF(EM.LE.(EMD+((SA3+SA4*CL**2)/10.)**(1./3.)))  GO TO 3
      WRITE (6,1001)
 1001 FORMAT(9X,46HMACH NUMBER OUT OF RANGE FOR DRAG CALCULATIONS       &
     &      /9X,14HSET DRGIND = 1)
      NEXT=1
      RETURN
    3 DELCDM= 10. * (EM - EMD) ** 3
    4 CDWI  =  XLINT(TBCL1 ,TBCDWI,CL,NTCL ,M)
      IF(M.NE.0) WRITE(6,1002)
 1002 FORMAT(22X,34HTHIS ERROR IS IN THE CL,CDWI TABLE)
      CD  = SA5 + SA6*CDWI + CKW*DELCDM + SA7 * CL**2
      RETURN
    1 DELCDM=  XDLKP(CL,EM,TBCL2 ,TBEM ,TBCDM,NTCL2 ,NTEM  ,7     ,IERRX&
     &,IERRY)
      IF(IERRX.NE.0) WRITE(6,1003)
 1003 FORMAT(9X,64HTHIS ERROR IS IN THE  CL  PART OF THE COMPRESSIBILITY&
     & DRAG TABLE)
      IF(IERRY.NE.0) WRITE(6,1004)
 1004 FORMAT(9X,64HTHIS ERROR IS IN THE  M   PART OF THE COMPRESSIBILITY&
     & DRAG TABLE)
      EMD = 0.
      GO TO 4
    2 DELCDM = 0.
      GO TO 4
      END
      SUBROUTINE  DSCEX(IDSCNT)
!**** MEMBER NAME = B93TDSCX
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  OO3(2,3),EO(7)
      DATA  OO3(1,1)/4HCONS/ ,OO3(1,2)/4HTANT/ ,OO3(1,3)/4H EAS/
      DATA  OO3(2,1)/4HMACH/ ,OO3(2,2)/4H NUM/ ,OO3(2,3)/4HBER /
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
 8999 FORMAT(/7X15HDESCEND TO H = ,F6.0,10H FT. ,R = ,F8.2,10H N.MI. AT &
     &3A4)
 9000 FORMAT(/7X15HDESCEND TO H = ,F6.0, 8H FT. AT ,3A4)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,11X18HTURB.  ENG.   PETF,19X4HMACH,  &
     &9X5HTHETA,                                                        &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X3HEAS,5X4HMACH,3X3HDIV,3X5HGAMMA,4X2H-F,5X    &
     &3HR/S,                                                            &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,25X5H(DEG),2X5H(DEG),4X5H(FPM))
 9003 FORMAT(          /9X2HCL,8X2HCD,8X3HL/D,7X4HLIFT,6X4HDRAG,3X9HFUEL&
     & FLOW   ,2X3HBHP,9X6HTHRUST,3X2HCP,6X2HCT,6X1HJ,5X4HVTIP          &
     &/39X5H(LBS),5X5H(LBS),2X8H(LBS/HR),16X5H(LBS),25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,1X &
     &F6.3,2XF6.1,3XF5.3,2XF5.3,2XF5.1,2XF5.1,3XF6.0)
      IF ((DESIND(IDSCNT).EQ.5.0).OR.(DESIND(IDSCNT).EQ.6.0)) KOUT = 1
      IF ((DESIND(IDSCNT).EQ.7.0).OR.(DESIND(IDSCNT).EQ.8.0)) KOUT=2
      IF ( (OPTIND.EQ.2).AND.((DESIND(IDSCNT).EQ.6.) .OR.                &
     & (DESIND(IDSCNT).EQ.8.0))) WRITE(6,9000)                           &
     &  HMIN(IDSCNT),(OO3(KOUT,I),I=1,3)
      IF ((OPTIND.EQ.2) .AND.                                            &
     & ((DESIND(IDSCNT).EQ.7.).OR.(DESIND(IDSCNT).EQ.5.0)))              &
     & WRITE(6,8999) HMIN(IDSCNT),RMAX5(IDSCNT),(OO3(KOUT,I),I=1,3)
      IF (OPTIND.NE.2.0) GO TO 119
      WRITE(6,9001)
      IF (KPRINT.EQ.1) WRITE(6,9003)
  119 RTHMIN = THEMIN(IDSCNT)*DTOR
      INDIC1 = 0
      INDIC2 = 0
      YLS2 = 1.0
      RALFL0  = ALPHL0  * DTOR
      REYEW   = EYEW  * DTOR
      IPRT  =0
      DELTAH  = 0.0
      BHPR = 1.0
      ETAP = ETAP5
      LC1 = 0
      LC2 = 0
      IF (DESIND(IDSCNT).NE.5.0.AND.DESIND(IDSCNT).NE.7.0) GO TO 1
      IF (RMAX5(IDSCNT).EQ.R) GO TO 95
      GSTAR = ATAN((HMIN(IDSCNT) - H)/((RMAX5(IDSCNT) - R)*6076.1))
      GO TO 1
   95 GSTAR = -89.9*DTOR
    1 HH  = H
      INDATM  =  ATMIND(IDSCNT+40)
      CALL  ATMOS(HH,ATMIND(IDSCNT+40),TIN(IDSCNT+40))
      V2  = EAS5(IDSCNT) / (SIGMA**.5)
      EM  = V2 / SA
      IF (DESIND(IDSCNT).LE.6.)  GO TO 333
      V2  = EAS5(IDSCNT) * SA
      EM  = EAS5(IDSCNT)
  333 VMAX = VM0 / SQRT(SIGMA)
      IF (VMAX.GT.(SA*EMM0)) VMAX = SA *EMM0
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 116
      VLMT = 250./SQRT(SIGMA)
      IF (VMAX.LE.VLMT) GO TO 116
      VMAX = VLMT
      IF (INDIC2.EQ.0) INDIC1 = 1
  116 IF (VMAX.LT.V2) V2 = VMAX
      EM  = V2 / SA
    3 Q   = 1.42636 * RHO * V2**2
      PHI = 1.0
      IF (DELTAH.NE.0.)  PHI = 1.0 - V2*(V2-  V1 )/(11.278 * DELTAH)
      IF((DESIND(IDSCNT).EQ.6.).OR.(DESIND(IDSCNT).EQ.8.)) GO TO 20
!
!
      CL    =  W *COS(GSTAR) /(SW *Q)
      CALL  DRAG
      CD  =  CD  + DLCDDS(IDSCNT)
      IF (NEXT.NE.0)  RETURN
      IF (ENGIND.NE.0.) GO TO  40
      BSHPR = V2*(W*PHI*SIN(GSTAR)+CD*SW*Q)/(325.6365*ETAT*ETAP5)       &
     &   + DSHPAC/ETAT
      BSHPR  =  BSHPR /(BHPP *DELTA *STHETA)
      CALL  POWAVL(TFI,EM)
      TKE  = TPEA
      KE   = NPLIM + 1
      IF (SHPA.GT.BSHPR)  GO TO 60
      CALL  POWAVL(TNRP,EM)
      IF (SHPA.LT.BSHPR)  GO TO 200
      LC2  = 1
      GAMM2 = GSTAR
      GO TO 80
   20 GSTAR = 0.0
      IF (ENGIND.NE.0.) GO TO 25
      CALL  POWAVL(TFI,EM)
      TKE = TPEA
      KE  = NPLIM +1
      SHPR = SHPA
      GO TO 60
   25 CALL  THRAVL(TFI,EM)
      TKE  = TPEA
      KE   = NPLIM + 1
      SHPR = SHPA
      GO TO 120
   40 BFNR   =  W* PHI* SIN(GSTAR) + CD * SW *Q
      BFNR   = BFNR / (DELTA * TP)
      CALL  THRAVL(TFI,EM)
      TKE  = TPEA
      KE   = NPLIM + 1
      IF (SHPA.GT.BFNR)  GO TO 120
      CALL  THRAVL(TNRP,EM)
      IF (SHPA.LT.BFNR)  GO TO 200
      LC2 = 1
      GAMM2 =GSTAR
      GO TO  80
  120 LC1  = 1
      GAMM1 = GSTAR
      SHPR = SHPA
      BFNA  = SHPA * DELTA * TP
   42 CL    = W * COS(GAMM1) / (SW *Q)
      CALL  DRAG
      CD  =  CD  + DLCDDS(IDSCNT)
      GAMM2 = ASIN((BFNA -CD*SW*Q)/(W*PHI))
      IF((ABS(GAMM2-GAMM1)*RTOD).LE..1) GO TO 80
      GAMM1 = GAMM2
      GO TO 42
   60 LC1  = 1
      GAMM1 = GSTAR
      SHPR = SHPA
      BSHPA = SHPA * DELTA * STHETA * BHPP
   62 CL    = W * COS(GAMM1) / (SW *Q)
      CALL  DRAG
      CD  =  CD  + DLCDDS(IDSCNT)
      GAMM2 = ASIN((325.8*ETAT*ETAP5 * BSHPA/V2 -CD*SW*Q) /(W*PHI))
      IF ((ABS(GAMM2-GAMM1)*RTOD).LE..1)GO TO 80
      GAMM1 = GAMM2
      GO TO 62
   80 THETAF  = GAMM2 + RALFL0 - REYEW  + CL/CLALPH
      IF (THETAF.LT.RTHMIN) GO TO 85
      IF (ENGIND.NE.0.) GO TO 140
      GO TO 160
   85 LC1  = 1
      GAMM1  = GAMM2
   86 GAMM2  = RTHMIN  -RALFL0 +REYEW -CL/CLALPH
      IF ((ABS(GAMM2-GAMM1)*RTOD).LE..1) GO TO 90
      GAMM1  = GAMM2
      CL   = W *COS(GAMM1) /(SW*Q)
      GO TO  86
   90 IF (ENGIND.NE.0) GO TO 100
      BSHPR = V2*(CD*SW*Q+W*PHI*SIN(GAMM2))/(325.6365*ETAT*ETAP5)       &
     &  +  DSHPAC/ETAT
      BSHPR = BSHPR / (BHPP * DELTA * STHETA)
      IF (LC2.EQ.1) GO TO 92
      CALL  POWAVL(TNRP,EM)
      LC2  = 1
   92 IF (BSHPR.GT.SHPA) GO TO 200
      GO TO 160
  100 BFNR  = CD*SW*Q +W*PHI*SIN(GAMM2)
! **  DSHPAC DOES NOT HAVE TO BE ACCOUNTED FOR HERE SINCE THE
!     CALCULATION PERTAINS TO TURBOFAN,JET ENGINES
      BFNR  = BFNR / (TP * DELTA)
      IF (LC2.EQ.1) GO TO 102
      CALL  THRAVL(TNRP,EM)
      LC2  = 1
  102 IF (BFNR.GT.SHPA) GO TO 200
  140 IF (LC2.NE.1) GO TO 142
      SHPR  = BFNR
      CALL  THRREQ(EM)
      TKE  = TPEA
      KE  = 6
      WSHPA  = WSHPR
  142 F   = WSHPA * DELTA * STHETA * TP
      CALL THRAVL(TMAX,EM)
      GO  TO 170
  160 IF (LC2.NE.1) GO TO 162
      SHPR  = BSHPR
! **  DSHPAC IS ALREADY ACCOUNTED FOR IN BSHPR
      CALL  POWREQ(EM)
      TKE  = TPEA
      KE  = 6
      WSHPA  = WSHPR
  162 F   = WSHPA * DELTA * STHETA * BHPP
      CALL POWAVL(TMAX,EM)
  170 RS  = -101.34 * V2 * SIN(GAMM2)
      GAMMA  = GAMM2
      THETAF = GAMM2 + RALFL0 - REYEW + CL/CLALPH
      PEHF =  SHPR/SHPA
! **  SHPR INCLUDES DSHPAC AT THIS POINT
      PETF = PEHF
      V   = V2
      EAS = V2 *(SIGMA**.5)
      DG  = GAMMA * RTOD
      DTF = THETAF*RTOD
      IF (OPTIND.NE.2.0) GO TO 139
      WRITE(6,9002) ST,R,WF,W,H,V,TKE,EO(KE),PEHF,EAS,EM,EMD,DG,DTF,RS
      IF (KPRINT.EQ.1) CALL SCRIBE(0,ETAP,YLS2)
!
  139 IF (H.LE.HMIN(IDSCNT)) GO TO 190
      DELTAH = DELH5(IDSCNT)
      IF ((H-DELTAH).LT.HMIN(IDSCNT)) DELTAH = H-HMIN(IDSCNT)
      H = H - DELTAH
      V1 = V2
      R  =  R - DELTAH/(6076.1 * TAN(GAMMA))
      ST  = ST + DELTAH/(60.0 * RS)
      W  =  W - F *DELTAH /(60.*RS)
      WF =  WF+ F *DELTAH /(60.*RS)
      LC2 = 0
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 1
      IF (INDIC1.EQ.1) GO TO 1
      DELTAH = 0.0
      INDIC1 = 0
      INDIC2 = 1
      GO TO 1
  190 IF (LC1.NE.1) RETURN
      IF((DESIND(IDSCNT).EQ.6.).OR.(DESIND(IDSCNT).EQ.8.)) RETURN
!
      R  =  RMAX5(IDSCNT)
  999 FORMAT(10X,28HSPIRAL DESCENT PATH REQUIRED)
      IF (OPTIND.EQ.2.)  WRITE(6,999)
      RETURN
  200 WRITE(6,998)
  998 FORMAT(10X,64HDESCENT CONDITION IMPOSSIBLE, DESIRED FLIGHT PATH IS&
     & TOO SHALLOW)
      NEXT = 1
      RETURN
      END
      SUBROUTINE DSCNT(IDSCNT)
!**** MEMBER NAME = B93TDSNT
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC. 2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10),TKETKE(10),EOEOEO(10),HHH(10),ETAETA(10)
      COMMON CLCLCL(10),CDCDCD(10),ALDALD(10),ELLELL(10),DEEDEE(10),    &
     & FFF(10),BHPBHP(10),TPTPTP(10),CCPCCP(10),CCTCCT(10),AJAJAJ(10),  &
     & PPP(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  EO(7),OO3(2,3)
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,19X4HMACH,  &
     &9X5HTHETA,                                                        &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X3HEAS,5X4HMACH,3X3HDIV,3X5HGAMMA,4X2H-F,5X    &
     &3HR/S,                                                            &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,25X5H(DEG),2X5H(DEG),4X5H(FPM))
 9006 FORMAT(          /9X2HCL,8X2HCD,8X3HL/D,7X4HLIFT,6X4HDRAG,3X9HFUEL&
     & FLOW   ,2X3HBHP,9X6HTHRUST,3X2HCP,6X2HCT,6X1HJ,5X4HVTIP,2X4HETAP &
     &/39X5H(LBS),5X5H(LBS),2X8H(LBS/HR),16X5H(LBS),25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,1X &
     &F6.3,2XF6.1,3XF5.3,2XF5.3,2XF5.1,2XF5.1,3XF6.0)
 9005 FORMAT( 7XF6.3,5XF6.4,4XF6.3,4XF7.0,2XF7.0,2XF7.0,2XF7.0,         &
     & 6XF7.0,2XF6.4,1XF6.4,1XF6.3,3XF5.1/)
10001 FORMAT(9X,61HDESCENT CONDITION IMPOSSIBLE  R/S IS LESS THAN OR EQU&
     &AL TO 0.)
 9000 FORMAT(/7X15HDESCEND TO H = ,F6.0, 8H FT. AT ,3A4)
 8999 FORMAT(/7X15HDESCEND TO H = ,F6.0,10H FT. ,R = ,F8.2,10H N.MI. AT &
     &3A4)
      DATA  OO3(1,1)/4HMAX./ ,OO3(1,2)/4H SPE/ ,OO3(1,3)/4HED  /
      DATA  OO3(2,1)/4HFLIG/ ,OO3(2,2)/4HHT I/ ,OO3(2,3)/4HDLE /
      NAMELIST/NDSCNT/RTOL,RTHMIN,RALFL0,REYEW,HSTAR,IPRT,R,RSTAR,WSTAR,&
     &WFSTAR,IEX,RRR,STSTST,WWW,W,ST,WF,INDATM,GAM1,H,V2,Q,EM,CL,GAM2,RS&
     &,PHI,ENGIND,BLP,BHPCRU,BHPIDL,BHPTHE,TLP,TCRUS,TIDLE,TTHETA,BHP,TH&
     &ETAF,DTF,SFC,F,T,TSFC,GAMMA,DG,V,DELTAH,V1,V2,PEHF,PETF,EAS,DGAM2,&
     &DCLCLF,IFUDGE
      IFUDGE = IDSCNT
      YLS2 = 1.0
      INDIC1 = 0
      INDIC2 = 0
      ITAB = 0
      ETAP = ETAP5
      RTOL = 5.
      RTHMIN   = THEMIN(IDSCNT) * DTOR
      RALFL0   = ALPHL0 * DTOR
      REYEW    = EYEW   * DTOR
      HSTAR    = H
      IPRT=0
      IF ((DESIND(IDSCNT).EQ.2.0).OR.(DESIND(IDSCNT).EQ.4.0)) LC1 = 1
      IF ((DESIND(IDSCNT).EQ.2.0).OR.(DESIND(IDSCNT).EQ.4.0)) GO TO 1
      RSTAR=R
      WSTAR=W
      WFSTAR=WF
      LC1=0
      IEX=0
 7166 IEX = IEX + 1
      IF (IEX.LE.2) GO TO 7167
      IF (R.GE.RRR(1)) GO TO 7168
      WRITE(6,5224)
 5224 FORMAT(//2X5HERROR100(1H*)//)
      WRITE(6,24811)
24811 FORMAT(22X,47H*** ERROR *** THE RANGE NECESSARY TO DESCEND IS/    &
     &       22X,47HGREATER THAN THE RANGE OF THE TABLE CALCULATED /    &
     &       22X,50HIN CRUISE.  THIS MAY BE DUE TO A DELTA R IN CRUISE/ &
     &       22X,20HWHICH WAS TOO SMALL.)
      WRITE(6,5224)
      WRITE(6,NDSCNT)
      NEXT=1
      RETURN
 7167 IF (R.LT.RRR(1)) R = RRR(1)
 7168 W = XLINT(RRR,WWW,R,IC2D,M)
      IF(M.NE.0)WRITE(6,1089)
 1089 FORMAT(22X,45HTHIS ERROR IS IN THE RANGE VS WEIGHT TABLE   /      &
     &       22X,20HCREATED IN CRUISE   )
      ST=XLINT(RRR,STSTST,R,IC2D,M)
      IF(M.NE.0)WRITE(6,1090)
 1090 FORMAT(22X,40HTHIS ERROR IS IN THE RANGE VS TIME TABLE/           &
     &       22X,20HCREATED IN CRUISE   )
      H = XLINT(RRR,HHH,R,IC2D,M)
      IF (M.NE.0) WRITE(6,1091)
 1091 FORMAT(22X,'THIS ERROR IS IN THE RANGE VS ALTITUDE TABLE'/        &
     &    22X,'CREATED IN CRUISE')
      WF  = WFSTAR-W+WSTAR
      INDATM    = ATMIND(IDSCNT + 40)
    1 GAM1     = 0.
      LC6=0
      LC7=0
      HH       = H
      CALL ATMOS(HH,ATMIND(IDSCNT+40),TIN(IDSCNT+40))
      V2       = VM0 / SQRT(SIGMA)
      IF(V2.GE.(SA * EMM0)) V2    = SA * EMM0
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 2
      VLMT = 250./SQRT(SIGMA)
      IF (V2.LE.VLMT) GO TO 2
      V2 = VLMT
      IF (INDIC2.EQ.0) INDIC1 = 1
    2 Q        = 1.42636 * RHO * V2**2
      EM       = V2 / SA
      CL       = W * COS(GAM1) /(SW * Q)
      GAM2     = RTHMIN  -  RALFL0  + REYEW  -  CL / CLALPH
      IF((ABS(GAM2-GAM1)*RTOD).LE..1) GO TO 3
      GAM1     = GAM2
      GO TO 2
    3 CALL DRAG
      CD  =  CD  + DLCDDS(IDSCNT)
      IF(NEXT.NE.0)RETURN
      RS       = -V2 * SIN(GAM2) * 101.34
      IF(RS.GT.0.) GO TO 4
 3331 WRITE (6,5224)
      WRITE(6,10001)
      WRITE(6,5224)
      WRITE(6,NDSCNT)
      NEXT=1
      RETURN
    4 PHI      = 1.
      IF (INDIC1.EQ.1) GO TO 62
      IF(H.NE.HSTAR) PHI =1.-V2/11.278*(V2-V1)/DELTAH
   62 IF (ENGIND.NE.0) GO TO 100
      CALL  POWAVL(TFI,EM)
      TKE =TPEA
      KE  = NPLIM + 1
      SHPR = SHPA
      BLP = SHPA
      BHPCRU   = BLP * DELTA * STHETA  * BHPP
      BHPIDL   =          BHPCRU
      BHPTHE  = V2/(325.8*ETAT*ETAP5)*(CD*SW*Q -W*PHI*RS/               &
     &          (101.34 *V2))  +  DSHPAC/ETAT
! **  ACCESSORY POWER IS ADDED TO THE POWER REQ. FOR DESCENT
      GO TO 3880
  100 CALL  THRAVL(TFI,EM)
      TKE =TPEA
      KE  = NPLIM + 1
      SHPR = SHPA
      TLP = SHPA
      TCRUS    = TLP * TP * DELTA
      TIDLE    =          TCRUS
      TTHETA   = CD * SW * Q  -  W * PHI * RS /(101.34 *V2)
 3880 IF (DESIND(IDSCNT).GT.2.) GO TO 200
      IF(ENGIND.NE.0.) GO TO 110
      IF(BHPTHE.LT.BHPIDL)GO TO 5
      CALL POWAVL(TNRP,EM)
      BHPCRU = SHPA   * DELTA * STHETA * BHPP
      IF(BHPTHE.GT.BHPCRU) GO TO 3267
! **  IF POWREQ IS GREATER THAN POWAVL GO TO 3267
      BHP      = BHPTHE
      THETAF   = THEMIN(IDSCNT)
      DTF = THETAF
      GO TO 50
    5 RS   = 101.34 * V2 /(W * PHI) * (CD * SW * Q  -  325.6365 * ETAP5 &
     &          *(BHPIDL - DSHPAC) / V2)
! **  BHPIDL IS POWER AVAILABLE (NO XMSN EFFICIENCY NEEDED); ACCESSORY
!     POWER IS SUBTRACTED FROM THE THRUST X VELOCITY TERM IN THE RATE OF
!     SINK EQUATION (FPM)
      GAM2     = ASIN(-RS /(101.34 * V2))
      IF((ABS(GAM2-GAM1)*RTOD).GT..1) GO TO 7
      BHP      = BHPIDL
      THETAF   = GAM2 + RALFL0 - REYEW + CL/CLALPH
      DTF = THETAF * RTOD
      WSHPR  = WSHPA
      SHPR  = SHPA
      GO TO 51
    7 GAM1     = GAM2
      CL       = W * COS(GAM1) /(SW * Q)
      CALL DRAG
      CD  = CD + DLCDDS(IDSCNT)
      GO TO 5
   50 BLP   = BHP /(BHPP * DELTA * STHETA )
      SHPR  = BLP
      CALL POWREQ(EM)
      TKE =TPEA
      KE  = 6
   51 F   = WSHPR * DELTA * STHETA * BHPP
      GO TO 300
  110 IF(TTHETA.LT.TIDLE) GO TO 120
      CALL  THRAVL(TNRP,EM)
      TCRUS  = SHPA   * DELTA *          TLP
      IF(TTHETA.GT.TCRUS)  GO TO 3267
  130 T        = TTHETA
      THETAF   = THEMIN(IDSCNT)
      DTF = THETAF
      GO TO 150
  120 RS       = 101.34 * V2 /(W * PHI) * (CD * SW * Q  -  TIDLE)
      GAM2     = ASIN(-RS /(101.34 * V2))
      IF((ABS(GAM2-GAM1)*RTOD).GT..1) GO TO 121
      T        = TIDLE
      THETAF   = GAM2 + RALFL0 - REYEW + CL/CLALPH
      DTF = THETAF * RTOD
      WSHPR  = WSHPA
      SHPR  = SHPA
      GO TO 151
  121 GAM1     = GAM2
      CL       = W * COS(GAM1) /(SW * Q)
      CALL DRAG
      CD  =  CD  + DLCDDS(IDSCNT)
      IF(NEXT.NE.0) RETURN
      GO  TO  120
  150 TLP   = T /(DELTA * TP)
      SHPR = TLP
      CALL  THRREQ(EM)
      TKE =TPEA
      KE  = 6
  151 F   = WSHPR * DELTA * STHETA * TP
  300 GAMMA    = GAM2
      DG  = GAMMA  * RTOD
      V        = V2
      IF(LC1.EQ.1)GO TO 3926
 4027 IF(H.EQ.HMIN(IDSCNT))GO TO 3927
      IF(HMIN(IDSCNT).GT.(H-DELH5(IDSCNT))) GO TO 3928
      DELTAH= DELH5(IDSCNT)
      H=H-DELTAH
 3929 V1=V2
      R=R-DELTAH/(6076.1*SIN(GAMMA)/COS(GAMMA))
      ST = ST + DELTAH/(RS * 60.)
      W=W - F *DELTAH/(RS*60.)
      WF=WF+F *DELTAH/(RS*60.)
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 1
      IF (INDIC1.EQ.0) GO TO 1
      INDIC1 = 0
      INDIC2 = 1
      GO TO 1
 3928 DELTAH = H - HMIN(IDSCNT)
      H = HMIN(IDSCNT)
      GO TO 3929
 3927 IF(LC1.EQ.1)RETURN
      IF (ABS(R-RMAX5(IDSCNT)).LE.RTOL) LC1=1
      R  = RSTAR + RMAX5(IDSCNT) -R
      H = HSTAR
      RSTAR=R
      GO TO 7166
 3926 IF(OPTIND.NE.2.) GO TO 4027
      IPRT = IPRT + 1
      IF ((DESIND(IDSCNT).EQ.2.).OR.(DESIND(IDSCNT).EQ.4.)) GO TO 4026
      IF(IPRT.NE.1)GO TO 4026
      DO 4151 J=1,IC2D
      IF (RRR(J).GT.R) GO TO 4151
      KEO = J
      WRITE(6,9004) STSTST(J),RRR(J),WFWFWF(J),WWW(J),                  &
     &HHH(J),VVV(J),TKETKE(J),EOEOEO(J),PEHFPE(J),                      &
     & EASEAS(J),EMEMEM(J),EMDEMD(J),ENENEN(J),ETAETA(J)
      IF (KPRINT.EQ.0) GO TO 4151
      WRITE(6,9005) CLCLCL(J),CDCDCD(J),ALDALD(J),ELLELL(J),DEEDEE(J),  &
     & FFF(J),BHPBHP(J),TPTPTP(J),CCPCCP(J),CCTCCT(J),AJAJAJ(J),PPP(J)
 9004 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,2XF6.1,3XF5.3,2XF5.3,3XF6.5,3XF5.3)
 4151 END DO
      XTRA1T= XLINT(RRR,STSTST,R,IC2D,M)
      XTRA2W= XLINT(RRR,WFWFWF,R,IC2D,M)
      XTRA3W= XLINT(RRR,WWW   ,R,IC2D,M)
      XTRA4V= XLINT(RRR,VVV   ,R,IC2D,M)
      XTRATP  = XLINT(RRR,TKETKE,R,IC2D,M)
      XTRAEO  =  EOEOEO(KEO)
      XTRA5P= XLINT(RRR,PEHFPE,R,IC2D,M)
      XTRA6E= XLINT(RRR,EASEAS,R,IC2D,M)
      XTRA7M= XLINT(RRR,EMEMEM,R,IC2D,M)
      XTRA8D= XLINT(RRR,EMDEMD,R,IC2D,M)
      XTRA9N= XLINT(RRR,ENENEN,R,IC2D,M)
      XTRA0H = XLINT(RRR,HHH,R,IC2D,M)
      XTRA1E = XLINT(RRR,ETAETA,R,IC2D,M)
      WRITE(6,9004) XTRA1T,R,XTRA2W,XTRA3W,XTRA0H,XTRA4V,               &
     & XTRATP,XTRAEO,XTRA5P,XTRA6E,XTRA7M,XTRA8D,XTRA9N,XTRA1E
      IF (KPRINT.EQ.0) GO TO 4026
      XTR11N = XLINT(RRR,CLCLCL,R,IC2D,M)
      XTR12N = XLINT(RRR,CDCDCD,R,IC2D,M)
      XTR13N = XLINT(RRR,ALDALD,R,IC2D,M)
      XTR14N = XLINT(RRR,ELLELL,R,IC2D,M)
      XTR15N = XLINT(RRR,FFF,R,IC2D,M)
      XTR16N = XLINT(RRR,BHPBHP,R,IC2D,M)
      XTR17N = XLINT(RRR,TPTPTP,R,IC2D,M)
      XTR18N = XLINT(RRR,CCPCCP,R,IC2D,M)
      XTR19N = XLINT(RRR,CCTCCT,R,IC2D,M)
      XTR20N = XLINT(RRR,AJAJAJ,R,IC2D,M)
      XTR21N = XLINT(RRR,PPP,R,IC2D,M)
      XTR22N = XLINT(RRR,DEEDEE,R,IC2D,M)
      WRITE(6,9005) XTR11N,XTR12N,XTR13N,XTR14N,XTR22N,XTR15N,XTR16N,   &
     & XTR17N,XTR18N,XTR19N,XTR20N,XTR21N
 4026 IF (ENGIND-1.0) 2181,2182,2182
 2181 CALL POWAVL(TMAX,EM)
      GO TO 2183
 2182 CALL THRAVL(TMAX,EM)
 2183 PEHF  =  SHPR/SHPA
      PETF = PEHF
      EAS=V*SQRT(SIGMA)
      IF ((DESIND(IDSCNT).EQ.1.).OR.(DESIND(IDSCNT).EQ.2.0)) KOUT = 1
      IF ((DESIND(IDSCNT).EQ.3.).OR.(DESIND(IDSCNT).EQ.4.0)) KOUT = 2
      IF((IPRT.EQ.1).AND.((DESIND(IDSCNT).EQ.2.).OR.(DESIND(IDSCNT).EQ. &
     &4.0)))  WRITE(6,9000) HMIN(IDSCNT),(OO3(KOUT,I),I=1,3)
      IF((IPRT.EQ.1).AND.((DESIND(IDSCNT).EQ.1.0).OR.(DESIND(IDSCNT).EQ.&
     &3.0)))  WRITE(6,8999) HMIN(IDSCNT),RMAX5(IDSCNT),(OO3(KOUT,I),    &
     &I=1,3)
      IF (IPRT.EQ.1) WRITE(6,9001)
      IF (IPRT.EQ.1.AND.KPRINT.EQ.1) WRITE(6,9006)
      WRITE(6,9002) ST,R,WF,W,H,V,TKE,EO(KE),PEHF,EAS,EM,EMD,DG,DTF,RS
      IF (KPRINT.EQ.1) CALL SCRIBE(0,ETAP,YLS2)
      GO TO 4027
  200 IF(ENGIND.NE.0.) GO TO 250
      IF(BHPTHE.GT.BHPIDL) GO TO 210
      IF(LC6.EQ.0)GO TO 215
  205 V2=V2 + 1.
      LC7=1
      GO TO 2
  215 CL=W*COS(GAM1)/(SW*Q)
      CALL DRAG
      CD  =  CD  + DLCDDS(IDSCNT)
      IF(NEXT.NE.0) RETURN
      RS =101.34*V2*(CD*SW*Q-325.8*ETAT*ETAP5*BHPIDL/V2)/(W*PHI)
      GAM2=ASIN(-RS/(101.34*V2))
      IF((ABS(GAM2-GAM1)*RTOD).LE..1) GO TO 216
      GAM1=GAM2
      GO TO 215
  216 BHP= BHPIDL
      DGAM2=GAM2 * RTOD
      DCLCLF= CL/CLALPH * RTOD
      THETAF   =DGAM2 + ALPHL0 -  EYEW + DCLCLF
      DTF = THETAF
      WSHPR=WSHPA
      GO TO 51
  210 IF(LC7.EQ.1)GO TO 215
      V2= V2- 10.
      LC6=1
      GO TO 2
  250 IF(TTHETA.GT.TIDLE) GO TO 260
      IF(LC6.NE.0)GO TO 205
  270 CL=W* COS(GAM1)/(SW*Q)
      CALL DRAG
      CD  =  CD  + DLCDDS(IDSCNT)
      IF(NEXT.NE.0) RETURN
      RS=101.34*V2*(CD*SW*Q-TIDLE)/(W*PHI)
      GAM2=ASIN(-RS/(101.34*V2))
      IF((ABS(GAM2-GAM1)*RTOD).LE..1) GO TO 280
      GAM1=GAM2
      GO TO 270
  280 T=TIDLE
      DGAM2= GAM2 * RTOD
      DCLCLF= CL/CLALPH * RTOD
      THETAF   =DGAM2 + ALPHL0 -  EYEW + DCLCLF
      DTF = THETAF
      WSHPR=WSHPA
      GO TO 151
  260 IF(LC7.EQ.1)GO TO 270
      V2=V2-10.
      LC6=1
      GO TO 2
 3267 WRITE(6,5224)
      WRITE(6,10002)
      WRITE(6,5224)
10002 FORMAT(9X,79HDESCENT CONDITION IMPOSSIBLE  POWER/THRUST REQUIRED I&
     &S MORE THAN THAT AVAILABLE)
      WRITE(6,NDSCNT)
      NEXT = 1
      RETURN
      END
      SUBROUTINE ENGSZ
!**** MEMBER NAME = B93TENGZ
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      COMMON/KAYDON/LL1,LL2,BHPRCR,BHPRTO
! **  KAYDON IS PASSED TO MAIN FOR XMSN PRINT DATA
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
! *** ANXMAX PASSED TO WGHTR FOR DRIVE SYSTEM WEIGHT CALCULATION
!     THIS VALUE IS THE XMSN SIZING RPM CONDITION
      C2  = 0.0
      NOCTSG = CTSGNO + 0.01
      NOMTP  = TPMNO + 0.01
      NOCPP = CPPNO + 0.001
      NOXPJ = XPJNO + 0.01
      PI = 3.1415927
      HH=HES
      YALE22=1.
      INDATM=YALE22
      CALL ATMOS (HH,YALE22,TINY)
      AN2MAX  =  AN2TO
      VMN = VRCRC/(101.34 * SA)
! **  VMN = VERTICAL MACH NO. USED TO OBTAIN THRUST REQUIRED/AVAILABLE
!     FOR T/FAN VERTICAL CLIMB. ADDITIONAL THRUST IS REQUIRED TO CLIMB
!     AT VRCRC AND MOST FANS CLIMB WITH THEIR INLET IN THE AXIAL FLOW
!     DIRECTION. THEREFORE, BY SPECIFYING A MACH NO. THE THRAVL AND
!     THRREQ SUBROUTINES WILL DIVIDE THE HOVER THRUST BY THE LAPSE RATE.
!     FOR ENGINES WITH THEIR INLET PERPENDICULAR TO THE AIR FLOW (SUCH
!     AS VECTORED THRUST) THERE IS ONLY A SLIGHT INLET EFFICIENCY LOSS
!     UP TO VRC=1000 FT/MIN AND INLET EFF. VS. VRC WAS NOT ACCOUNTED FOR
      IF(INDLFT.EQ.0) GO TO 27
      DELTAY = DELTA
      CALL  THRAVL(TMAX,VMN)
! **  VMN ACCOUNTS FOR THE ADDITIONAL THRUST NECESSARY TO CLIMB AT VRCRC
      TLP = SHPA
      CALL  LIFAVL(TLMAX)
      TLL = FLIF * SHPTO
      HH=HC
      YALE22=1.
      INDATM=YALE22
      CALL ATMOS (HH,YALE22,ATMIY)
      Q   = 1.42636 * RHO * VC**2
      CL  = WG /(SW * Q)
      EM  = VC / SA
      CALL DRAG
      IF(NEXT.NE.0) RETURN
      AN2MAX  =  AN2CR
      IF (POWESI -1.0) 201,202,203
  201 TPS = TMAX
      GO TO 204
  202 TPS = TMIL
      GO TO 204
  203 TPS = TNRP
  204 CALL  THRAVL(TPS,EM)
      TLPPR = SHPA
      TP     = CD * SW * Q / (DELTA * TLPPR)
      TL     = ENL/(ENL-ENL0)*(WG*SENE- TP * DELTAY * TLP * ETAP2)      &
     &       /(DELTAY * ETAL * TLL)
      TL1    =(WG*SENE-((ENP-ENP0)/ENP)*TP*DELTAY*TLP*ETAP2)            &
     &       /(ETAL * DELTAY * TLL)
      SAVE(22) = TL
      SAVE(21) = TL1
      IF(TL1.GT.TL) TL=TL1
      SAVE(23) = 100. * DELTAY * TLP
      RETURN
   27 IF(INDENG.NE.0) GO TO 29
  300 CALL  POWAVL(TMAX,0.0)
      S12  = SHPA
      BLP  =  SHPA
      DSHPRC = WG * VRCRC/(33000. * CKRC * BLP * DELTA * STHETA)
! **  DSHPRC IS DELTA POWER NECESSARY TO CLIMB AT VRCRC FT/MIN;NOTE THAT
!     CKRC IS NORMALLY 2.0 BUT SHOULD BE LOWER FOR HIGH WG/A AND FANS
      SHPACC=DSHPAC/(BLP*DELTA*STHETA)
      P = A2STR*VT
      IF (ETAIND.EQ.0.0) GO TO 2001
      BHPP = S12
      V = 0.0
      YLS2 = (ENP - ENP0)/ENP
      TPROP = SENE*WG
      IF (ETAIND.EQ.2.0) GO TO 81
      REALJ = 0.0
      CCT=TPROP*WGA*PI**3/(0.009507*SIGMA*P**2*WG)
      IF(ETAIND.NE.3.0) GO TO 44
      TOAD = TPROP*WGA/(WG*DELTA)
      HPADTH=XLKUP(REALJ,TOAD,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
      CCP=5634966.0*HPADTH/(P/STHETA)**3
      GO TO 45
   44 TIPP = P/(SA * 1.689)
      ETAP = XLKUP(CTSIG,TIPP,CTOSIG,NOCTSG,TIPM,NOMTP,FMER,10,6,IX,IY)
! **  CTSIG IS EITHER INPUT OR CALCULATED IN SIZTR, DEPENDING ON PDMIND
   45 IF(IX.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1003)
      IF(IX.NE.0.AND.ETAIND.EQ.3.0) WRITE(6,1005)
 1003 FORMAT(9X,89HTHIS ERROR IS IN THE  J  PART OF THE PROPELLER POWER &
     &COEFFICIENT TABLE - SUBROUTINE ENGSZ)
 1005 FORMAT(9X,'THIS ERROR IS IN THE M PART OF THE FAN COEFFICIENT TABL&
     &E  -  SUBROUTINE ENGSZ')
      IF (IY.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1004)
      IF(IY.NE.0.AND.ETAIND.EQ.3.0) WRITE(6,1006)
 1004 FORMAT(9X,89HTHIS ERROR IS IN THE  CT PART OF THE PROPELLER POWER &
     &COEFFICIENT TABLE - SUBROUTINE ENGSZ)
 1006 FORMAT(9X,'THIS ERROR IS IN THE FN PART OF THE FAN COEFFICIENT TAB&
     &LE  -  SUBROUTINE ENGSZ')
      CCP = 0.798*CCT**1.5/ETAP
      BHPP1 = 0.009507*CCP*WG*P**3/                                     &
     & (550.*PI**4*WGA*ETAT*BLP*SHPTO*STHETA**3*YLS2)+(SHPACC+DSHPRC)/  &
     &    SHPTO
      GO TO 30
   81 ITER = 0
      ETA = 0.85
      TPROP1 = TPROP
      BHPP3 = (ENP/(ENP-ENP0))*SQRT(SENE**3)*WG*SQRT(WGA)/              &
     & (550.0*ETAT*S2RHO*DELTA*STHETA*BLP*SHPTO)+(SHPACC+DSHPRC)/SHPTO
   82 ITER = ITER + 1
      BHPP = BHPP3/ETA
      CALL THRUST(TPROP,YLS2,ETAP)
      IF (ABS(1.0-TPROP/TPROP1).LE.0.005) GO TO 83
      ETA = ETAP
      IF (ITER.GT.25) GO TO 84
      GO TO 82
   83 BHPP1 = BHPP
      GO TO 30
 2001 BHPP1 = (ENP/(ENP-ENP0))*SQRT(SENE**3)*WG*SQRT(WGA)               &
     &   /(550. *DELTA*STHETA*ETAT*ETAP2*S2RHO*BLP*SHPTO)               &
     &         +(SHPACC + DSHPRC)/SHPTO
      GO TO 30
   29 CALL  THRAVL(TMAX,VMN)
      TLP  =  SHPA
      IF (INDENG.EQ.2) GO TO 2002
      TP1   = (ENP/(ENP-ENP0))* SENE * WG /(DELTA*ETAP2*TLP*SHPTO)
      GO TO 807
 2002 TP1 = BETA*ENP*SENE**1.5*WG*WGA**0.5/(DELTA*                      &
     &      (ENP-ENP0) * 550. * ETAT * ETAP2 * SHPTO * S2RHO * TLP)     &
     &      +  BETA * (DSHPAC + DSHPRC)/(SHPTO * DELTA * TLP)
      GO TO 807
   30 BHPRTO = BHPP1*DELTA*STHETA*BLP
  807 IF(INDESZ.EQ.1.0.OR.XMSND.EQ.1.0) GO TO 500
      GO TO 32
  500 HH=HC
      YALE22=1.
      INDATM = YALE22
      CALL ATMOS (HH,YALE22,ATMIY)
      Q     = 1.42636 * RHO * VC**2
      CL    = WG /( SW * Q)
      EM    = VC / SA
      CALL DRAG
      IF(NEXT.NE.0) RETURN
      AN2MAX  =  AN2CR
      IF (POWESI -1.0) 211,212,213
  211 TPS = TMAX
      GO TO 214
  212 TPS = TMIL
      GO TO 214
  213 TPS = TNRP
  214 IF (INDENG.NE.0) GO TO 33
      CALL  POWAVL(TPS,EM)
      S22  = SHPA
      BLP  =  SHPA
      SHPACC=DSHPAC/(BLP*DELTA*STHETA)
      P = A2STR*VT
      IF (ETAIND.EQ.0.0) GO TO 2003
      TPROP = CD*SW*Q
      V = VC
      YLS2 = 1.0
      BHPP = S22
      IF (ETAIND.EQ.2.0) GO TO 91
      REALJ = 1.689*PI*V/P
      CCT = TPROP*WGA*PI**3/(0.009507*SIGMA*P*P*WG)
      IF(ETAIND.NE.3.0) GO TO 46
      EM=V/SA
      TOAD=TPROP*WGA/(WG*DELTA)
      HPADTH=XLKUP(EM,TOAD,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
      CCP=HPADTH*5634966.0/(P/STHETA)**3
      GO TO 47
   46 CCP=XLKUP(REALJ,CCT,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
   47 IF(IX.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1003)
      IF(IX.NE.0.AND.ETAIND.EQ.3.0) WRITE(6,1005)
      IF(IY.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1004)
      IF(IY.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1006)
      ETAP = REALJ*CCT/CCP
      BHPP2 = 0.009507*CCP*WG*P**3/                                     &
     & (550.*PI**4*WGA*ETAT*SHPA*STHETA**3)+SHPACC
      GO TO 2004
   91 ITER = 0
      ETA = 0.85
      TPROP1 = TPROP
      BHPP3 = CD*SW*Q*VC/(325.6365*ETAT*DELTA*STHETA*BLP)+SHPACC
   92 ITER = ITER + 1
      BHPP = BHPP3/ETA
      ETA1 = ETA
      TP1 = TPROP
      CALL THRUST(TPROP,1.0,ETAP)
      ETA2 = ETAP
      TP2 = TPROP
      IF (ABS(1.0-TPROP/TPROP1).LE.0.005) GO TO 93
      ETA = ETAP
      IF (ITER.GT.1) ETA = ETA2 + (TPROP1 - TP2)*(ETA2 - ETA1)/(TP2-TP1)
      IF (ITER.GT.25) GO TO 84
      GO TO 92
   93 BHPP2 = BHPP
      GO TO 2004
 2003 ETAP4 = XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      IF (M.NE.0) WRITE(6,1001)
 1001 FORMAT(22X,34HTHIS ERROR IS IN THE ETAP4-M TABLE)
      BHPP2 = CD*SW*Q*VC/(325.8*ETAT*ETAP4*DELTA*STHETA                 &
     &       * BLP )+SHPACC
 2004 BHPP = BHPP1
      C1  = 0.0
      BHPRCR = BHPP2*DELTA*STHETA*BLP
      IF (BHPP2.LT.BHPP1) GO TO 60
      BHPP = BHPP2
      C1  = 1.0
      GO TO 60
   33 CALL  THRAVL(TPS,EM)
      TLP  = SHPA
      TP2    = CD * SW * Q /(DELTA * TLP)
      TP   = TP1
      IF(TP2.GT.TP1) TP= TP2
      IF(TP.EQ.TP1)GO TO 1313
      IF(INDENG.EQ.2) WRITE (6,750)
      TPXMSN = TP
      LL1 = 0.0
      RETURN
 1313 IF(INDENG.EQ.2.AND.XMSND.EQ.1.0) WRITE (6,650)
      IF (INDENG.EQ.1) GO TO 1315
      TPXMSN = TP1*XMSMRT
      LL1 = 1.0
      ANXMAX = AN2TO
      RETURN
 1315 TPXMSN=TP
      LL1 = 0.0
      RETURN
   32 IF (INDENG.EQ.0) GO TO 58
      IF (INDENG.EQ.1) GO TO 1314
      IF (XMSND.EQ.1.0) WRITE (6,650)
      TP=TP1
      TPXMSN=TP1*XMSMRT
      LL1 = 1.0
      ANXMAX = AN2TO
      RETURN
 1314 TP=TP1
      TPXMSN=TP
      LL1 = 0.0
      RETURN
  650 FORMAT(22X,   '+++ERROR+++ TORQUE LIMIT OPTION USED NOT ',        &
     &'APPLICABLE TO CONVERTIBLE ENGINES.'/22X,'PROGRAM APPLIED XMSN ', &
     &'LIMIT AS SPECIFIED FRACTION OF INSTALLED TAKEOFF POWER.')
  750 FORMAT(22X,   '+++WARNING+++ CONVERTIBLE ENGINE SIZED FOR ',      &
     &'CRUISE. XMSN TORQUE LIMIT OPTION INPUT IGNORED.')
      RETURN
   58 BHPP = BHPP1
      C1  = 0.0
   60 IF (IRN.EQ.1) GO TO 600
  625 IF(XMSND.EQ.1.0)GO TO 700
      BHPXMS=BHPP*XMSMRT
      ANXMAX = AN2TO
      IF(BHPP.EQ.BHPP2) ANXMAX = AN2CR
      LL1 = 1.0
      RETURN
  700 BHPXMS=BHPRTO*XMSMRT
      ANXMAX = AN2TO
      LL1 = 1.0
      IF(BHPRCR/AN2CR.LE.BHPRTO/AN2TO) RETURN
      BHPXMS = BHPRCR*XMSMRT
      ANXMAX = AN2CR
      LL2 = 2.0
      RETURN
  600 DEBAR = XI4 * ((BHPP/ENP)**.5)
      IF (C1.EQ.0.0) GO TO 70
      IF (POWESI -1.0) 61,62,63
   61 TPS = TMAX
      GO TO 64
   62 TPS = TMIL
      GO TO 64
   63 TPS = TNRP
   64 CALL  POWAVL(TPS,EM)
      AAW6 = S22 / SHPA
      CALL ATMOS(HES,YALE22,TINY)
      GO TO 75
   70 HH  = HES
      YALE22 = 1.0
      CALL  ATMOS (HH,YALE22,TINY)
      AN2MAX  =  AN2CR
      CALL  POWAVL(TMAX,0.0)
      AAW6 = S12 / SHPA
   75 IF ((ABS(1.0-AAW6)).LE.0.01) GO TO 625
      AN2MAX  =  AN2TO
      C2 = C2 +1.0
      IF (C2.LT.25.)  GO TO 300
 1010 FORMAT(22X,91HERROR , THE NUMBER OF ITERATIONS IN THE ENGINE SIZIN&
     &G ROUTINE EXCEEDED 25 ,CASE  TERMINATED)
   84 WRITE(6,1010)
      NEXT = 1
      RETURN
      END
      SUBROUTINE  ENG1(TEA,AM)
!**** MEMBER NAME = B93TENG1
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC 2201 TO LOC 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      FAPN = 1.0
      KPN = 10
      KPR = 10
      IF (IN2.NE.2) GO TO 10
      A2STR = AN2MAX * A2MAX
      A2REF = XLKUP(AM,TEA,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IY)
 1001 FORMAT(9X,56HTHIS ERROR IS IN THE  M  PART OF THE N SUB 2 POWER TA&
     &BLE)
 1002 FORMAT(9X,56HTHIS ERROR IS IN THE  T  PART OF THE N SUB2 POWER  TA&
     &BLE)
      IF (IX.NE.0)  WRITE(6,1001)
      IF (IY.NE.0) WRITE(6,1002)
      A2OPT = A2STR /(A2REF * STHETA)
      FAPN = A2OPT *(2.0 - A2OPT)
      IF (FAPN.LT.0.0)  FAPN = 0.0
      DO 8  I=1,KPN
      IF (PN2(I).NE.0.0)  GO TO 9
    8 END DO
      GO TO 10
    9 FAPN = PARA(A2OPT,A2NO,PN2,KPN,IX)
 1007 FORMAT(9X,46HTHIS ERROR IS IN THE N SUB 2 CORRECTION FACTOR)
      IF (IX.NE.0) WRITE(6,1007)
   10 FAPR = 1.0
      IF (IRN.EQ.0) GO TO 20
      RRR3 =6346.8*DEBAR *((1.0+0.2*(AM**2))**1.74)*SIGMA /(THETA**0.26)
      A1REF = XLKUP(AM,TEA,AM1,NM1,TN1,NT1,AONE,6,8,IX,IY)
 1003 FORMAT(9X,56HTHIS ERROR IS IN THE  M  PART OF THE N SUB 2 POWER TA&
     &BLE)
 1004 FORMAT(9X,56HTHIS ERROR IS IN THE  T  PART OF THE N SUB 2 POWER TA&
     &BLE)
      IF (IX.NE.0) WRITE(6,1003)
      IF (IY.NE.0) WRITE(6,1004)
      RNE1 = RRR3  * A1REF
      FAPR = PARA(RNE1,PRN,RNE,KPR,IX)
 1008 FORMAT(9X,42HTHIS ERROR IS IN THE REYNOLDS NUMBER TABLE)
      IF (IX.NE.0) WRITE(6,1008)
   20 SHPA = XLKUP(AM,TEA,AMSHP,NMS,TSHP,NTS,SHPAV,6,8,IX,IY)
 1005 FORMAT(9X,56HTHIS ERROR IS IN THE  M  PART OF THE REFERED POWER TA&
     &BLE)
 1006 FORMAT(9X,56HTHIS ERROR IS IN THE  T  PART OF THE REFERED POWER TA&
     &BLE)
      IF (IX.NE.0) WRITE(6,1005)
      IF (IY.NE.0) WRITE(6,1006)
      SHPA  = SHPA * FAPN * FAPR
      RETURN
      END
      SUBROUTINE LIFAVL(TLPS)
!**** MEMBER NAME = B93TLAVL
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  ,  LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      NLIM = 0
      NTF  = 8
      NWF  = 8
      N1F  = 8
      N2F  = 8
      TEA  =  TLPS/THETA
      IF (LWD)  4,4,2
    2 FWREF =  PARA(TLMAX,TFW,FWDOT,NWF,IX)
 1001 FORMAT(9X48HTHIS ERROR IS IN THE LIFT ENGINE FUEL FLOW TABLE)
      IF (IX.NE.0) WRITE(6,1001)
      FWSHP = WLMAX * FWREF /(DELTA *STHETA)
      FWTEM = PARA(TEA,TFW,FWDOT,NWF,IX)
      IF (IX.NE.0) WRITE(6,1001)
      IF (FWSHP.GE.FWTEM)  GO TO 4
      TEWD  = PARA(FWSHP,FWDOT,TFW,NWF,IX)
      IF (IX.NE.0) WRITE(6,1001)
      IF (TEA -TEWD) 4,4,3
    3 TEA = TEWD
      NLIM = 1
    4 IF (LN1) 7,7,5
    5 A1REF = A1LMAX / STHETA
      A1TEM = PARA(TEA,TF1,FONE,N1F,IX)
      IF (IX.NE.0) WRITE(6,1002)
      IF (A1REF.GE.A1TEM)  GO TO 7
      TEN1 = PARA(A1REF,FONE,TF1,N1F,IX)
 1002 FORMAT(9X,47HTHIS ERROR IS IN THE LIFT ENGINE  N SUB 1 TABLE)
      IF (IX.NE.0) WRITE(6,1002)
      IF (TEA -TEN1) 7,7,6
    6 TEA = TEN1
      NLIM = 2
    7 IF (LN2) 10,10,8
    8 A2REF = AL2MAX / STHETA
      A2TEM = PARA(TEA,TF2,FTWO,N2F,IX)
      IF (IX.NE.0) WRITE(6,1003)
      IF (A2REF.GE.A2TEM)  GO TO 10
      TEN2  = PARA(A2REF,FTWO,TF2,N2F,IX)
 1003 FORMAT(9X,47HTHIS ERROR IS IN THE LIFT ENGINE  N SUB 2 TABLE)
      IF (IX.NE.0) WRITE(6,1003)
      IF (TEA -TEN2) 10,10,9
    9 TEA = TEN2
      NLIM = 3
   10 FLIF = PARA(TEA,TF,FAVL,NTF,IX)
 1004 FORMAT(9X,46HTHIS ERROR IS IN THE LIFT ENGINE  POWER  TABLE)
      IF (IX.NE.0) WRITE(6,1004)
      FWA  = PARA(TEA,TFW,FWDOT,NWF,IX)
      IF (IX.NE.0) WRITE(6,1001)
      FWA = FWA * CKFF
      NLLIM =  NLIM
      TLEA  =  TEA * THETA
      RETURN
      END
      FUNCTION XIBIV(XA,ZA,XTAB,NX,YTAB,NY,ZTAB,NXI,NYJ,IX,IZ)
!**** MEMBER NAME = B93TLIBV
      DIMENSION  XTAB(1),YTAB(1)
      DIMENSION ZTAB(NXI,NYJ),ZJ(20)
!     INVERSE DOUBLE TABLE PARABOLIC LOOK-UP      Y = FUNC( X,Z )
!         XA = FIRST INDEPENDENT VARIABLE, ONE DIMENSIONAL
!  ***    ZA = SECOND INDEPENDENT VARIABLE, TWO DIMENSIONAL            *
!         XTAB = TABLE OF FIRST INDEPENDENT VARIABLE
!         NX   = NO. OF ENTRIES IN XTAB
!         YTAB = TABLE OF DEPENDENT VARIABLE
!         NY   = NO. OF ENTRIES IN YTAB
!         ZTAB = TABLE OF SECOND INDEPENDENT VARIABLE
!         NXI  = X DIMENSION OF ZTAB
!         NYJ  = Y DIMENSION OF ZTAB
!         IX   = X ARGUMENT ERROR RETURN
!         IZ   = Z ARGUMENT ERROR RETURN
!  ***    NANS = NO. OF ANSWERS FOUND. ONLY THE FIRST Y VALUE FOUND    *
!              IS RETURNED.
!  ***    ZJ(30) IS A DUMMY ARRAY.DIMENSION 30 IS CRITICAL, IT MUST    *
!  ***         BE GREATER THAN OR EQUAL TO NY                          *
      PART(XXX,X111,X222,X333,Y111) = (((XXX-X111) * (XXX-X222)) /      &
     &    ((X333-X111) * (X333-X222))) * Y111
      BO2A(V11,V22,V33,U1,U2,U3) = (V11**2*(U2-U3)-V22**2*(U1-U3)+V33**2&
     &*(U1-U2))/(2.*(-V11*(U2-U3)+V22*(U1-U3)-V33*(U1-U2)))
      COA(V11,V22,V33,U1,U2,U3,U)= (V11**2*(U2*V33-U3*V22)-V22**2*(U1*V3&
     &3-U3*V11)+V33**2*(U1*V22-U2 *V11)-U*(-V11**2*(V22-V33)+V22**2*    &
     & (V11-V33)-V33**2*(V11-V22)))        /(V11*(U2-U3)-V22*(U1-U3)    &
     & +V33*(U1-U2))
      DENOM(V11,V22,V33,U1,U2,U3) = V11*(U2-U3)-V22*(U1-U3)+V33*(U1-U2)
      IX   = 0
      IZ   = 0
!     SEARCH XTAB FOR THE X ARGUMENT    (NO EXTRAPOLATION)
      IF (XA - XTAB(1)) 2,3,10
    2 IX = -1
    3 M  = 1
      GO TO 4
   10 IF (XA - XTAB(NX)) 15,12,11
   11 IX = 1
   12 M  = NX
      GO TO 4
   15 DO 16 I=2,NX
      II = I
      IF (XA - XTAB(I)) 18,17,16
   16 END DO
   17 M = II
    4 DO 5 J=1,NY
    5 ZJ(J) = ZTAB(M,J)
      GO TO 32
   18 CONTINUE
   20 IF (II-NX) 26,25,25
!  22 DX1 = XA - XTAB(II-1)
!     DX2 = XTAB(II) - XA
!     IF (DX2 - DX1)  26,26,25
   25 II = II-1
   26 X1 = XTAB(II-1)
      X2 = XTAB(II)
      X3 = XTAB(II+1)
      DO 30 J=1,NY
      Z1 = ZTAB(II-1,J)
      Z2 = ZTAB(II,J)
      Z3 = ZTAB(II+1,J)
   30 ZJ(J) = PART(XA,X2,X3,X1,Z1) + PART(XA,X1,X3,X2,Z2) +             &
     &    PART(XA,X1,X2,X3,Z3)
   32 NANS = 0
!     NOW WE HAVE  Y =FUNC(Z),BUT Y MAY BE MULTIVALUED
      NNN=NY-1
      DO 200 J=1,NNN
      IF(ZJ(J).LT.ZA.AND.ZA.LT.ZJ(J+1).OR.ZJ(J).GT.ZA.AND.ZA.GT.ZJ(J+1))&
     &GO TO 210
      IF(ZJ(J).EQ.ZA) GO TO 211
      IF(ZJ(J+1).EQ.ZA) GO TO 212
  200 END DO
      IF(ZA.GT.ZJ(NY).AND.ZJ(NY).GT.ZJ(1)) GO TO 201
      IF(ZA.LT.ZJ(NY).AND.ZJ(NY).LT.ZJ(1)) GO TO 201
      ANS=YTAB(1)
      IZ=-1
      GO TO 222
  201 ANS=YTAB(NY)
      IZ=1
      GO TO 222
  211 ANS=YTAB(J)
      GO TO 222
  212 ANS=YTAB(J+1)
      GO TO 222
  210 IF(ABS(ZA-ZJ(J)).LT.ABS(ZA-ZJ(J+1))) GO TO 213
      IF(J-NNN) 214,216,216
  214 JJ=J+1
      GO TO 215
  213 IF(J-1) 214,214,216
  216 JJ=J
  215 Z1=ZJ(JJ-1)
      Z2=ZJ(JJ)
      Z3=ZJ(JJ+1)
      Y1=YTAB(JJ-1)
      Y2=YTAB(JJ)
      Y3=YTAB(JJ+1)
      DNOM = DENOM(Y1,Y2,Y3,Z1,Z2,Z3)
      IF (ABS(DNOM).LE.1.0E-10) GO TO 84
      A=BO2A(Y1,Y2,Y3,Z1,Z2,Z3)
      B=COA(Y1,Y2,Y3,Z1,Z2,Z3,ZA)
      AN1 = -A-SQRT(A**2-B)
      AN2 = -A+SQRT(A**2-B)
      IF(YTAB(JJ-1).LT.AN1.AND.AN1.LT.YTAB(JJ+1)) NANS=NANS+1
      IF(YTAB(JJ-1).LT.AN2.AND.AN2.LT.YTAB(JJ+1)) NANS=NANS+1
      IF(NANS-1) 217,218,219
  217 WRITE(6,1009)
      IF(J-JJ) 230,231,231
  230 IF(AN1-YTAB(JJ-1)) 240,251,241
  240 IF(AN2-YTAB(JJ-1)) 250,241,251
  250 IF(AN1-AN2)241,241,251
  241 ANS=AN2
      GO TO 222
  251 ANS=AN1
      GO TO 222
  231 IF(AN1-YTAB(JJ+1)) 241,251,260
  260 IF(AN2-YTAB(JJ+1)) 251,241,270
  270 IF(AN1-AN2) 251,251,241
  218 ANS=AN1
      IF(YTAB(JJ-1).LT.AN2.AND.AN2.LT.YTAB(JJ+1)) ANS=AN2
      GO TO 222
  219 IF(J-JJ)220,221,221
  220 ANS=AN1
      IF(AN1.GT.YTAB(JJ)) ANS=AN2
      GO TO 222
  221 ANS=AN1
      IF(AN1.LT.YTAB(JJ)) ANS=AN2
      GO TO 222
   84 ANS = Y1 + (ZA - Z1)*(Y3 - Y1)/(Z3 - Z1)
 1001 FORMAT(9X,117H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. B&
     &OTH INDEPENDENT VARIABLES (X AND Z) ARE OUT OF RANGE OF THE TABLE)
 1002 FORMAT(9X,114H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. T&
     &HE FIRST INDEPENDENT VARIABLE (X) IS OUT OF RANGE OF THE TABLE)
 1003 FORMAT(9X,115H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. T&
     &HE SECOND INDEPENDENT VARIABLE (Z) IS OUT OF RANGE OF THE TABLE)
 1004 FORMAT(9X,56HTHE PROGRAM PICKED UP THE FIRST VALUE, Z(1) IN THE TA&
     &BLE)
 1005 FORMAT(9X,55HTHE PROGRAM PICKED UP THE LAST VALUE, Z(N) IN THE TAB&
     &LE)
 1006 FORMAT(9X,57HTHE PROGRAM PICKED UP THE FIRST VALUE, X(1), IN THE T&
     &ABLE)
 1007 FORMAT(9X,56HTHE PROGRAM PICKED UP THE LAST VALUE, X(N), IN THE TA&
     &BLE)
 1009 FORMAT(9X,105H****CAUTION***** THE PROGRAM HAD DIFFICULTY CONVERGI&
     &NG DURING REVERSE TABLE LOOKUP. CHECK THE TABLE RANGE)
  222 IF(IX.NE.0.AND.IZ.NE.0) GO TO 102
      IF(IX) 103,104,105
  103 WRITE(6,1002)
      WRITE(6,1006)
      GO TO 120
  105 WRITE(6,1002)
      WRITE(6,1007)
      GO TO 120
  104 IF(IZ) 106,120,108
  106 WRITE(6,1003)
      WRITE(6,1004)
      GO TO 120
  108 WRITE(6,1003)
      WRITE(6,1005)
      GO TO 120
  102 WRITE(6,1001)
  120 XIBIV = ANS
      RETURN
      END
      FUNCTION XLINT(XT,YT,X,NTAB,M)
!**** MEMBER NAME = B93TLINT
      DIMENSION XT( 1), YT( 1)
      XSAV=XT(1)
      N=1
      M=0
      IF(X-XT(1)) 1,2,3
    1 M=-1
    2 XLINT= YT(1)
      GO TO 25
    3 IF(X-XT(NTAB)) 4,5,6
    6 M=1
    5 N=NTAB
      XSAV= XT(NTAB)
      XLINT= YT(NTAB)
      GO TO 25
    4 IF(X-XSAV) 7,8,9
    8 XLINT= YT(N)
      GO TO 25
    7 I= N-1
   13 IF(X-XT(I)) 10,11,12
   11 N= I
      XSAV= XT(I)
      XLINT= YT(I)
      GO TO 25
   10 I = I-1
      GO TO 13
    9 I = N+1
   14 IF(X-XT(I)) 15,11,16
   16 I= I+1
      GO TO 14
   15 I=I-1
   12 N=I
      XSAV=XT(I)
      XLINT= YT(I)+(X-XT(I))*(YT(I+1)-YT(I))/(XT(I+1)-XT(I))
   25 IF(M) 30,31,32
   30 WRITE (6,300)
  300 FORMAT(/9X,97H***ERROR*** THE FOLLOWING VALUES MAY NOT BE ACCURATE&
     & THE INDEPENDENT VARIABLE WAS OUT OF RANGE OF/9X,81HTHE TABLE.  TH&
     &ESE VALUES WERE CALCULATED USING THE FIRST VALUE GIVEN IN THE TABL&
     &E)
   31 RETURN
   32 WRITE (6,301)
  301 FORMAT(/9X,97H***ERROR*** THE FOLLOWING VALUES MAY NOT BE ACCURATE&
     & THE INDEPENDENT VARIABLE WAS OUT OF RANGE OF/9X,81HTHE TABLE.  TH&
     &ESE VALUES WERE CALCULATED USING THE LAST  VALUE GIVEN IN THE TABL&
     &E)
      GO TO 31
      END
      SUBROUTINE LOADET (X,I)
!**** MEMBER NAME = B93TLOAD
      DIMENSION T(5), IDENT(20), X(1)
  917 FORMAT(/47X,17HV A S C O M P  II/18X,65HV/STOL AIRCRAFT SIZING & P&
     &ERFORMANCE COMPUTER PROGRAM    B-93    ,                          &
     &                         //15X76HTHE FOLLOWING IS A CARD BY CARD R&
     &EPRODUCTION OF THE INPUT DECK FOR THIS CASE//9X,58HLOC.  CORRESPON&
     &DS TO LOCATION NUMBER GIVEN ON INPUT SHEET / 9X,83HNUM   STANDS FO&
     &R THE NUMBER OF SEQUENTIAL INPUT VALUES STARTING WITH LOC. (MAX. =&
     &5)/ 9X,53HVAL   EQUALS VALUE FOR VARIABLE CORRESPONDING TO LOC./9X&
     &,4HVAL1,9X,5HVALUE,14X,26HCORRESPONDING TO LOC.+0001/9X,4HVAL2,9X,&
     &5HVALUE,14X,26HCORRESPONDING TO LOC.+0002/15X,4HETC.//11X,4HLOC.,7&
     &X,3HNUM,9X,3HVAL,15X,4HVAL1,14X,4HVAL2,14X,4HVAL3,14X,4HVAL4/)
  918 FORMAT(11X,I4,8X,I1,5X,5(G14.5,4X))
  999 FORMAT(/47X,17HV A S C O M P  II/18X,65HV/STOL AIRCRAFT SIZING & P&
     &ERFORMANCE COMPUTER PROGRAM    B-93    )
      DATA KLANK /4H    /
      IPRTIT = 0
!     IDTST(ON 360)= 1077952576         IDTST(ON 94)=-17997958192
      IDTST = KLANK
      GO TO (1,11),I
    1 READ(5,2)K,J,(T(L),L=1,5)
    2 FORMAT(I4,I1,5E14.7)
      GO TO (4,4,4,4,4,8,6,13,14),J
    4 CONTINUE
      IF(IPRTIT.NE.0)  GO TO 99
      WRITE(6,11111)
11111 FORMAT(1H1)
      WRITE(6,917)
      IPRTIT= 1
   99 DO 5  L=1,J
      N=K+L-1
    5 X(N)= T(L)
      WRITE (6,918) K,J,(X(IVY),IVY=K,N)
      GO TO 1
    6 READ(5,7)(IDENT(M),M=1,19)
    7 FORMAT(A4,2X,18A4)
      IF(IDENT(1)-IDTST) 8,10,8
    8 WRITE(6,9)
    9 FORMAT(5X,43H*** ERROR - NO TITLE CARD AFTER SEVEN CARD,/         &
     &       5X,44HCOLUMNS 1 THRU 6 ON TITLE CARD MUST BE BLANK/        &
     &        5X,45HOR THERE WAS A 6 IN COLUMN 5 OF AN INPUT CARD)
      GO TO 14
   10 IPAGE=0
   11 IPAGE=IPAGE+1
      WRITE(6,12) (IDENT(M),M=2,19),IPAGE
      IF(I.EQ.1) WRITE(6,917)
   12 FORMAT(1H11X18A4,6H PAGE I3//)
      IF(I.NE.1) GO TO113
      IPRTIT=1
      GO TO 1
  113 WRITE(6,999)
   13 RETURN
             !!! CALL EXIT
   14 STOP
      RETURN
      END
      FUNCTION XDLKP(XARG,YARG,XTAB,YTAB,ZTAB,NXARG,NYARG,M,IERRX,IERRY)
!**** MEMBER NAME = B93TLODK
      DIMENSION ZTAB(1),XTAB(1),YTAB(1)
      DIV(A,B,C) = (A-B)/(C-B)
      IERRX=0
      IERRY=0
      IF(XARG-XTAB(1))2,3,3
    2 IERRX=-1
      J=1
      GO TO 9
    3 DO 5 I=2,NXARG
      K=I
    4 IF(XARG-XTAB(I))6,6,5
    5 END DO
      IERRX = 1
    6 I=K
      IF(I-NXARG)8,7,8
    7 J=NXARG-1
      GO TO 9
    8 J=I-1
    9 IF(YARG-YTAB(1))10,11,11
   10 IERRY=-1
      L=1
      GO TO 17
   11 DO 13 I=2,NYARG
      K=I
   12 IF(YARG-YTAB(I))14,14,13
   13 END DO
      IERRY = 1
   14 I=K
      IF(I-NYARG)16,15,16
   15 L=NYARG-1
      GO TO 17
   16 L=I-1
   17 JJ=J+1
      LL=L+1
      M1=M*(L-1)+J
      M2=M*(LL-1)+J
   18 FY11=DIV(XARG,XTAB(JJ),XTAB(J))
      FY12=DIV(XARG,XTAB(J),XTAB(JJ))
      FY1=FY11*ZTAB(M1)+FY12*ZTAB(M1+1)
   19 FY2=FY11*ZTAB(M2)+FY12*ZTAB(M2+1)
      XDLKP=(DIV(YARG,YTAB(LL),YTAB(L))*FY1)+(DIV(YARG,YTAB(L),YTAB(LL))&
     &*FY2)
      IF(IERRX)21,22,23
   21 WRITE(6,20000)
      WRITE(6,21000)
   22 IF(IERRY)31,32,33
   23 WRITE(6,20000)
      WRITE(6,23000)
      GO TO 22
   31 WRITE(6,30000)
      WRITE(6,21000)
   32 RETURN
   33 WRITE(6,30000)
      WRITE(6,23000)
      GO TO 32
20000 FORMAT(/9X111H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. T&
     &HE FIRST  INDEPENDENT VARIABLE IS OUT OF RANGE OF THE TABLE)
21000 FORMAT(19X52HTHE PROGRAM PICKED UP THE  FIRST  VALUE IN THE TABLE)
23000 FORMAT(19X52HTHE PROGRAM PICKED UP THE  LAST   VALUE IN THE TABLE)
30000 FORMAT(/9X111H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. T&
     &HE SECOND INDEPENDENT VARIABLE IS OUT OF RANGE OF THE TABLE)
      END
      SUBROUTINE LOITR(ILOITR)
!**** MEMBER NAME = B93TLOIT
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      COMMON/HAL/INDSGT,KE
! **  HAL IS PASSED AMOUNG CRUS1,3, PRFRM, PRFRP, LOITR
      DIMENSION DATA(1)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  OO3(2,4),EO(7)
      DATA  OO3(1,1)/4H    /,OO3(1,2)/4H    /, OO3(1,3)/4H    /
      DATA  OO3(1,4)/4H    /,OO3(2,1)/4HFOR /, OO3(2,2)/4HRESE/
      DATA  OO3(2,3)/4HRVE /,OO3(2,4)/4HFUEL/
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
      NAMELIST/NLOITR/H,INDATM,W0,ST0,STMAX,CLL,VL,LC1,LC2,LC3,LC4,LC5,V&
     &MAX,V,Q,EM,CL,ENGIND,BHPR,YALE22,SFC,F2,F1,PEHF,PETF,EAS,DELTAT,WF&
     &,W,ST,DELWFL,WFL,DELV,TR,TSFC,BLP,BHPA,DELP1,VMIN,TLP,TA,IFUDGE
 9000 FORMAT(/7X12HLOITER  FOR ,F6.3,1X,5HHRS. ,4A4,10X,13HTEMPERATURE =&
     &,F6.1,1X,5HDEG.F)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,19X4HMACH,  &
     &3X4HFUEL,5X4HETAP,                                                &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X3HEAS,5X4HMACH,3X3HDIV,4X4HRATE,5X4HPROP,     &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,26X7H(LB-HR))
 9003 FORMAT(          /9X2HCL,8X2HCD,8X3HL/D,7X4HLIFT,6X4HDRAG,3X9HFUEL&
     & FLOW   ,2X3HBHP,9X6HTHRUST,3X2HCP,6X2HCT,6X1HJ,5X4HVTIP,5X4HETAP &
     &/39X5H(LBS),5X5H(LBS),2X8H(LBS/HR),16X5H(LBS),25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,2XF6.1,3XF5.3,2XF5.3,3XF6.0,3XF5.3)
 1001 FORMAT(9X,42HINSUFFICIENT POWER FOR STEADY LEVEL FLIGHT)
      IFUDGE = ILOITR
      M = 0
      PI = 3.14159
      WSR=1.0
      NOCPP = CPPNO + 0.1
      LTRIND  = DNIRTL(ILOITR)
      IF (RSW(ILOITR) .NE. 0.0) WSR = RSW(ILOITR)
      HH = H
      INDATM = ATMIND(ILOITR + 50)
      CALL ATMOS(HH,ATMIND(ILOITR + 50),TIN(ILOITR + 50))
      TEMP = THETA * 518.69  -  459.69
      IF (OPTIND.NE.2.0) GO TO 273
      IF(INDSGT.EQ.11) GO TO 273
      WRITE(6,9000) STL(ILOITR),(OO3(LTRIND,I),I=1,4),TEMP
      WRITE(6,9001)
      IF (KPRINT.EQ.1) WRITE(6,9003)
  273 AN2MAX = AN2M6(ILOITR)
      IF (ENGIND.NE.0.0) ETAP = 1.0
      IF (ENGIND.EQ.0.0.AND.ETAIND.EQ.0.0)                              &
     &  ETAP = XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      W0  = W
      ST0 = ST
      YLS2 = (ENP - ENPSDL(ILOITR))/ENP
      YLS1 = 1.0/YLS2
      STMAX= ST + STL(ILOITR)
      CLL = TBCL1(NTCL)
      IF((DRGIND.EQ.1.).AND.(CLL.GT.TBCL2(NTCL2))) CLL=TBCL2(NTCL2)
      VL = 1.1*SQRT((W*WSR)/(SW*1.42636*RHO*CLL))
      IF(VL.LT.50.) VL = 50.
      IF(VL.GT.150.)VL =150.
      IF(ETAIND.EQ.0.0.OR.ETAIND.EQ.2.0) GO TO 1
  173 Q = 1.42636*RHO*VL**2
      EM = VL/SA
      CL = W * WSR /(SW*Q)
      CALL DRAG
      IF (NEXT.NE.0) RETURN
      CD = CD + DLOITR(ILOITR)
      TT3 = CD*SW*Q
      P = A2STR*VT
      CCT = (PI**3)*TT3*WGA/(0.009507*SIGMA*WG*P**2)
      IF(ETAIND.NE.3.0) GO TO 450
      TOAD=CCT*(P/STHETA)**2/3261.21
      IF(TOAD.LE.CPPROP(NOCPP)) GO TO 1
      GO TO 451
  450 IF(CCT.LE.CPPROP(NOCPP)) GO TO 1
  451 VL=VL+5.0
      IF (VL.GE.200.) GO TO 1
      GO TO 173
    1 LC1=0
      LC2=0
      LC3=0
      LC4=0
      LC5=0
      LC6=0
      VMAX= VM0/SQRT(SIGMA)
      IF(VMAX.GT.(SA*EMM0)) VMAX= SA*EMM0
      IF (IVLMT.EQ.0.OR.H.GT.10000.) GO TO 36
      VLMT = 250./SQRT(SIGMA)
      IF (VMAX.GT.VLMT) VMAX = VLMT
   36 V = VMAX
      IF(ETAIND.EQ.0.0.OR.ETAIND.EQ.2.0)GO TO 2
  174 Q = 1.42636*RHO*V**2
      EM = V/SA
      CL = W * WSR /(SW*Q)
      CALL DRAG
      IF (NEXT.NE.0) RETURN
      CD = CD + DLOITR(ILOITR)
      TT3 = CD*SW*Q
      P = A2STR*VT
      VMAX = V
      CCT = (PI**3)*TT3*WGA/(0.009507*SIGMA*WG*P**2)
      IF(ETAIND.NE.3.0) GO TO 452
      TOAD=CCT*(P/STHETA)**2/3261.21
      IF(TOAD.LE.CPPROP(NOCPP)) GO TO 26
      GO TO 453
  452 IF(CCT.LE.CPPROP(NOCPP)) GO TO 26
  453 V=V-10.
      IF (V.LT.VL) GO TO 172
      GO TO 174
    2 Q   = 1.42636*RHO*V**2
      EM  = V / SA
      CL = W * WSR /(SW*Q)
      CALL DRAG
      CD = CD + DLOITR(ILOITR)
      IF (ETAIND.NE.0.0) GO TO 26
      IF(ENGIND.EQ.0.)                                                  &
     &ETAP4=XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      IF(M.NE.0)WRITE(6,1221)
 1221 FORMAT(22X35HTHIS ERROR IS IN THE M-ETAP4 TABLE )
   26 IF (LC3.EQ.0) GO TO 100
      IF(ENGIND.NE.0.)GO TO 200
      CALL POWAVL(TNRP,EM)
      IF (ETAIND.NE.0.0) GO TO 27
      BHPR= CD * SW * Q * V /(325.8 * ETAT * ETAP4) + DSHPAC
      YALE22= BHPR /(BHPP * DELTA * STHETA)
      YALE22 = YALE22*YLS1
      SHPR  = YALE22
      GO TO 28
   27 TPROP = CD*SW*Q
      CALL POWER(TPROP,YLS2,ETAP)
! **  DSHPAC IS ACCOUNTED FOR IN THE POWER SUBROUTINE
   28 CALL POWREQ(EM)
      TKE  = TPEA
      KE   =  6
      F2    = WSHPR* DELTA * STHETA * BHPP
      F2 = F2*YLS2
  300 IF(LC4.EQ.0) GO TO 400
      IF(F2.LT.F1) GO TO 500
      IF(LC4.EQ.3) GO TO 600
      IF(LC4.EQ.1) GO TO 600
  301 V   = V - 1.
      F1  = F2
      LC4=3
      GO TO 2
  600 IF(OPTIND.NE.2.) GO TO 7609
      IF (ENGIND-1.0) 701,703,703
  701 CALL POWAVL(TMAX,EM)
      GO TO 702
  703 CALL THRAVL(TMAX,EM)
  702 PEHF = SHPR /SHPA
      EAS = V *  SQRT(SIGMA)
      IF (ETAIND.EQ.0.0.AND.ENGIND.EQ.0.0) ETAP = ETAP4
      F = F1
      PETF = PEHF
      IF(INDSGT.EQ.11) RETURN
      WRITE(6,9002) ST,R,WF,W,H,V,TKE,EO(KE),PEHF,EAS,EM,EMD,F1,ETAP
      IF (KPRINT.EQ.1) CALL SCRIBE(0,ETAP,YLS2)
 7609 IF(ST.EQ.STMAX) GO TO 610
      IF(LC6.EQ.1) GO TO 610
      DELTAT= DELST(ILOITR)
      IF(STMAX.LT.(ST + DELST(ILOITR))) DELTAT= STMAX - ST
      IF(STMAX.LT.(ST + DELST(ILOITR))) LC6=1
      WF  = WF + F1 * DELTAT
      W   = W  - F1 * DELTAT
      ST  = ST + DELTAT
      GO TO 1
  610 IF(DNIRTL(ILOITR).EQ.1.)RETURN
      DELWFL=W0 - W
      WFL   =WFL+ DELWFL
      W   = W0
      ST  = ST0
      RETURN
  500 IF(LC4.EQ.3) GO TO 301
      V   = V + 10.
      F1  = F2
      LC4=2
      IF(V.LE.VMAX)GO TO 2
      V   = VMAX
      LC4=0
      LC5=1
      GO TO 2
  400 IF(LC5.EQ.0) GO TO 401
      DELV= -1.
      LC4=3
      GO TO 402
  401 DELV=  1.
      LC4=1
  402 V   = V + DELV
      F1  =F2
      GO TO 2
  200 TR  = CD * SW * Q
      YALE22= TR /(DELTA * TP)
      YALE22 = YALE22*YLS1
      CALL THRAVL(TNRP,EM)
      SHPR = YALE22
      CALL  THRREQ(EM)
      TKE  = TPEA
      KE   =  6
      F2   = WSHPR * DELTA * STHETA * TP
      F2 = F2*YLS2
      GO TO 300
  100 IF(ENGIND.NE.0.) GO TO 110
      CALL  POWAVL(TNRP,EM)
      BLP  = SHPA
      BHPA= BLP * BHPP * DELTA * STHETA
      BHPA = BHPA*YLS2
      IF (ETAIND.NE.0.0) GO TO 29
      BHPR= CD * SW * Q *V /(325.8 * ETAT * ETAP4) + DSHPAC
      GO TO 31
   29 TPROP = CD*SW*Q
      CALL POWER(TPROP,YLS2,ETAP)
      BHPR = SHPR*DELTA*STHETA*YLS2*BHPP
! **  DSHPAC IS ACCOUNTED FOR IN THE POWER SUBROUTINE
   31 DELP1 = BHPA - BHPR
  120 IF(LC2.EQ.0) GO TO 130
      IF(LC2.EQ.1) GO TO 140
      IF(DELP1.LT.0.) GO TO 150
      V   = V - 1.
      LC2=3
      GO TO 2
  150 IF(LC2.NE.2) GO TO 151
11150 V   = V + 10.
      LC2=2
      GO TO 2
  151 V   = V + 1.
  152 VMIN= V
      LC3=1
      GO TO 2
  140 IF(DELP1.LT.0.) GO TO 11150
      V = VL
      GO TO 152
  130 IF(LC1.EQ.0) GO TO 131
      IF(DELP1.LT.0.) GO TO 132
      V   = V + 1.
      LC1=2
      GO TO 2
  132 IF(LC1.EQ.1) GO TO 133
      V   = V - 1.
  134 VMAX= V
      V = VL
      LC2=1
      GO TO 2
  133 V   = V - 10.
      LC1=1
      IF(V.GE.VL)  GO TO 2
  172 WRITE(6,5224)
 5224 FORMAT(//2X5HERROR100(1H*)//)
      WRITE(6,1001)
      WRITE(6,5224)
      WRITE(6,NLOITR)
      NEXT=1
      RETURN
  131 IF(DELP1.LT.0.) GO TO 133
      GO TO 134
  110 CALL  THRAVL(TNRP,EM)
      TLP  = SHPA
      TA  = TLP * DELTA * TP
      TA = TA*YLS2
      TR  = CD  * SW * Q
      DELP1= TA - TR
      GO TO 120
      END
      FUNCTION XLKUP(XARG,YARG,XTAB,NX,YTAB,NY,ZTAB,NXTAB,NYTAB,IX,IY)
!**** MEMBER NAME = B93TLOKP
      DIMENSION  XTAB(1),YTAB(1)
      DIMENSION ZTAB(NXTAB,NYTAB)
!     DOUBLE TABLE  PARABOLIC  LOOK-UP        Z = FUNC( X,Y )
      IERRX = 0
      IERRY = 0
!     CHECK THE RANGE OF THE VARIABLE TABLES  (NO EXTRAPOLATION)
   10 IF (XARG - XTAB(1)) 12,13,13
   12 X = XTAB(1)
      IERRX = -1
      GO TO 18
   13 DO 15 I=2,NX
      IF (XARG - XTAB(I)) 16,16,15
   15 END DO
      X = XTAB(NX)
      IERRX = 1
      GO TO 18
   16 X = XARG
   18 IF (YARG - YTAB(1)) 20,21,21
   20 Y   = YTAB(1)
      IERRY = -1
      GO TO 26
   21 DO 23 I=2,NY
      IF (YARG - YTAB(I)) 24,24,23
   23 END DO
      Y = YTAB(NY)
      IERRY = 1
      GO TO 26
   24 Y = YARG
   26 IX =  IERRX
      IY =  IERRY
      XLKUP = BIV(X,Y,XTAB,YTAB,ZTAB,NX,NY,NXTAB,NYTAB)
!     ERROR  RETURNS
      IF (IERRX) 40,45,50
   40 IF (IERRY) 41,42,43
   41 WRITE (6,20000)
      WRITE (6,20010)
      GO TO 60
   42 WRITE (6,21000)
      WRITE (6,20020)
      GO TO 60
   43 WRITE (6,20000)
      WRITE (6,20030)
      GO TO 60
   45 IF (IERRY) 46,60,47
   46 WRITE (6,22000)
      WRITE (6,20040)
      GO TO 60
   47 WRITE (6,22000)
      WRITE (6,20050)
      GO TO 60
   50 IF (IERRY) 51,52,53
   51 WRITE (6,20000)
      WRITE (6,20060)
      GO TO 60
   52 WRITE (6,21000)
      WRITE (6,20070)
      GO TO 60
   53 WRITE (6,20000)
      WRITE (6,20080)
   60 RETURN
20000 FORMAT( 9X117H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. B&
     &OTH INDEPENDENT VARIABLES (X AND Y) ARE OUT OF RANGE OF THE TABLE)
21000 FORMAT( 9X114H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. T&
     &HE FIRST INDEPENDENT VARIABLE (X) IS OUT OF RANGE OF THE TABLE)
22000 FORMAT( 9X115H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. T&
     &HE SECOND INDEPENDENT VARIABLE (Y) IS OUT OF RANGE OF THE TABLE)
20010 FORMAT(19X69HTHE PROGRAM PICKED UP THE FIRST VALUES, X(1) AND Y(1)&
     &, IN BOTH TABLES)
20030 FORMAT(19X84HTHE PROGRAM PICKED UP THE FIRST VALUE, X(1), AND THE &
     &LAST VALUE, Y(N), IN THE TABLES)
20060 FORMAT(19X84HTHE PROGRAM PICKED UP THE LAST VALUE, X(N), AND THE F&
     &IRST VALUE, Y(1), IN THE TABLES)
20080 FORMAT(19X68HTHE PROGRAM PICKED UP THE LAST VALUES, X(N) AND Y(N),&
     & IN BOTH TABLES)
20020 FORMAT(19X57HTHE PROGRAM PICKED UP THE FIRST VALUE, X(1), IN THE T&
     &ABLE)
20070 FORMAT(19X56HTHE PROGRAM PICKED UP THE LAST VALUE, X(N), IN THE TA&
     &BLE)
20040 FORMAT(19X57HTHE PROGRAM PICKED UP THE FIRST VALUE, Y(1), IN THE T&
     &ABLE)
20050 FORMAT(19X56HTHE PROGRAM PICKED UP THE LAST VALUE, Y(N), IN THE TA&
     &BLE)
      END
      SUBROUTINE LIFREQ
!**** MEMBER NAME = B93TLREQ
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      NTF  = 8
      NWF  = 8
      TEA = PARA(FREQ,FAVL,TF,NTF,IX)
 1001 FORMAT(9X,46HTHIS ERROR IS IN THE LIFT ENGINE  POWER  TABLE)
      IF (IX.NE.0)  WRITE(6,1001)
      FWR  = PARA(TEA,TFW,FWDOT,NWF,IX)
 1002 FORMAT(9X,49HTHIS ERROR IS IN THE LIFT ENGINE  FUEL FLOW TABLE)
      IF (IX.NE.0) WRITE(6,1002)
      FWR = FWR * CKFF
      TLEA  =  TEA * THETA
      RETURN
      END
      FUNCTION PARA (ARG,XTAB,YTAB,NARG,IERR)
!**** MEMBER NAME = B93TPARA
      DIMENSION XTAB(1),YTAB(1)
      IERR = 0
!     A00PAR1
!PARABOLIC INTERPOLATION FUNCTION - Y=PARA(ARG,XTAB,YTAB,NARG)
!     FUNCTION PARA (ARG,XTAB,YTAB,NARG)
!     ARG=INDEPENDENT VARIABLE
!     XTAB=TABLE OF INDEPENDENT VARIABLES
!     YTAB=TABLE OF DEPENDENT VARIABLES
!     NARG=NO. OF ENTRIES IN THE TABLE
!     IS ARG. LESS THAN THE SMALLEST VALUE IN TABLE
    1 IF (ARG-XTAB(1))2,3,3
!     YES-USE FIRST 3 VALUES FOR EXTRAPALATION
    2 J=1
      ARG = XTAB(1)
 1001 FORMAT(9X,105H***ERROR THE FOLLOWING VALUES MAY NOT BE ACCURATE. T&
     &HE INDEPENDENT VARIABLE IS OUT OF RANGE OF THE TABLE.)
 1002 FORMAT(19X,50HTHE PROGRAM PICKED UP THE FIRST VALUE IN THE TABLE)
 1003 FORMAT(19X,50HTHE PROGRAM PICKED UP THE LAST  VALUE IN THE TABLE)
      WRITE(6,1001)
      WRITE(6,1002)
      IERR = 1
      GO TO 9
!     NO-FIND WHERE ARG LIES BETWEEN
    3 DO 5 I=2,NARG
    4 IF(ARG-XTAB(I))6,6,5
    5 END DO
    6 IF (I-NARG)8,7,7
!     IF LAST VALUE, USE LAST 3 ITEMS IN TABLE
    7 J=NARG-2
      IF (ARG -XTAB(NARG))  9,9,31
   31 ARG = XTAB(NARG)
      IERR = 1
      WRITE(6,1001)
      WRITE(6,1003)
      GO TO 9
!     OK-USE VALUE BEFORE AND 2 AFTER
    8 J=I-1
!     DO INTEROLATION
    9 PART1=(((ARG-XTAB(J+1))*(ARG-XTAB(J+2)))/((XTAB(J)-XTAB           &
     &(J+1))*(XTAB(J)-XTAB(J+2))))*YTAB(J)
   10 PART2 = (((ARG-XTAB(J))*(ARG-XTAB(J+2)))/((XTAB(J+1)-XTAB         &
     &(J))*(XTAB(J+1)-XTAB(J+2))))*YTAB(J+1)
   11 PART3=(((ARG-XTAB(J))*(ARG-XTAB(J+1)))/((XTAB(J+2)-               &
     &XTAB(J))*(XTAB(J+2)-XTAB(J+1))))*YTAB(J+2)
   12 PARA=PART1 + PART2 + PART3
      RETURN
      END
      SUBROUTINE POWAVL(TPS,AM)
!**** MEMBER NAME = B93TPAVL
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCT0(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  QARAY(20)
      NLIM = 0
      TEA = TPS/THETA
      IF (IWD)  4,4,2
    2 WREF = XLKUP(0.0,TMAX,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
 1001 FORMAT(9X,66HTHIS ERROR IS IN THE  M  PART OF THE REFERED FUEL FLO&
     &W POWER TABLE)
 1002 FORMAT(9X,66HTHIS ERROR IS IN THE  T  PART OF THE REFERED FUEL FLO&
     &W POWER TABLE)
      IF (IX.NE.0) WRITE(6,1001)
      IF (IY.NE.0) WRITE(6,1002)
      WSHP = WMAX * WREF / (DELTA * STHETA)
      WTEMP = XLKUP(AM,TEA,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
      IF  (IX.NE.0) WRITE(6,1001)
      IF  (IY.NE.0) WRITE(6,1002)
      IF (WSHP.GE.WTEMP) GO TO 4
      TEWD = XIBIV(AM,WSHP,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IZ)
 1003 FORMAT(9X,60HTHIS ERROR IS IN THE  W  PART OF THE REFERED FUEL FLO&
     &W TABLE)
      IF (IX.NE.0) WRITE(6,1001)
      IF (IZ.NE.0) WRITE(6,1003)
      IF (TEA - TEWD) 4,4,3
    3 TEA = TEWD
      NLIM = 1
    4 IF(IN1) 7,7,5
    5 A1REF = A1MAX / STHETA
      A1TEMP = XLKUP(AM,TEA,AM1,NM1,TN1,NT1,AONE,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IY.NE.0) WRITE(6,1010)
 1010 FORMAT(9X,57HTHIS ERROR IS IN THE  T  PART OF THE N SUB 1 POWER  T&
     &ABLE)
      IF (A1REF.GE.A1TEMP) GO TO 7
      TEN1 = XIBIV(AM,A1REF,AM1,NM1,TN1,NT1,AONE,6,8,IX,IZ)
 1004 FORMAT(9X,56HTHIS ERROR IS IN THE  M  PART OF THE N SUB 1 POWER TA&
     &BLE)
 1005 FORMAT(9X,56HTHIS ERROR IS IN THE  N1 PART OF THE N SUB 1 POWER TA&
     &BLE)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IZ.NE.0) WRITE(6,1005)
      IF (TEA - TEN1) 7,7,6
    6 TEA = TEN1
      NLIM = 2
    7 IF (IN3) 75,75,41
   41 A1REF = A3MAX*SQRT(1.0 + 0.2*AM**2.)
      A1TEMP = XLKUP(AM,TEA,AM1,NM1,TN1,NT1,AONE,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IY.NE.0) WRITE(6,1010)
      IF (A1REF.GE.A1TEMP) GO TO 75
      TEN1 = XIBIV(AM,A1REF,AM1,NM1,TN1,NT1,AONE,6,8,IX,IZ)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IZ.NE.0) WRITE(6,1005)
      IF (TEA.LE.TEN1) GO TO 75
      TEA = TEN1
      NLIM = 6
   75 CALL ENG1(TEA,AM)
      IF (IQ) 16,16,8
    8 IF (IN2 -1) 9,9,10
    9 A2REF = XLKUP(AM,TEA,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IY)
 1006 FORMAT(9X,56HTHIS ERROR IS IN THE  M  PART OF THE N SUB 2 POWER TA&
     &BLE)
 1007 FORMAT(9X,56HTHIS ERROR IS IN THE  T  PART OF THE N SUB 2 POWER TA&
     &BLE)
      IF (IX.NE.0) WRITE(6,1006)
      IF (IY.NE.0) WRITE(6,1007)
      A2STR = A2REF  * STHETA
      GO TO 110
   10 A2STR = AN2MAX * A2MAX
  110 Q2 = SHPA * DELTA * STHETA / A2STR
      IF (Q2 - QMAX) 15,15,11
   11 DO 14 I=1,NT2
      TII = TN2(I)
      IF(TII.LT.TSHP(1)) TII=TSHP(1)
      IF(TII.GT.TSHP(NTS)) TII=TSHP(NTS)
      CALL ENG1(TII,AM)
      IF (IN2 -1) 12,12,13
   12 A2REF = XLKUP(AM,TII,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1006)
      IF (IY.NE.0) WRITE(6,1007)
      A2STR = A2REF * STHETA
      GO TO 113
   13 A2STR = AN2MAX * A2MAX
  113 QARAY(I) = SHPA * DELTA * STHETA / A2STR
   14 END DO
      TEA = PARA(QMAX,QARAY,TN2,NT2,IX)
 1009 FORMAT(9X,41HTHIS ERROR IS IN THE TORQUE LIMIT LOOK-UP)
      IF (IX.NE.0) WRITE(6,1009)
      IF (IN2.EQ.2) GO TO 115
      A2REF = XLKUP(AM,TEA,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1006)
      IF (IY.NE.0) WRITE(6,1007)
      A2STR = A2REF * STHETA
  115 CALL ENG1(TEA,AM)
      NLIM = 4
   15 GO TO 20
   16 IF (IN2-1) 17,17,25
   17 A2REF = XLKUP(AM,TEA,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1006)
      IF (IY.NE.0) WRITE(6,1007)
      A2STR = A2REF * STHETA
   20 IF (IN2-1) 25,21,25
   21 IF (A2STR - A2MAX) 25,25,22
   22 A2REF = A2MAX  / STHETA
      TEA  = XIBIV(AM,A2REF,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IZ)
      A2STR = A2MAX
      NLIM = 3
 1008 FORMAT(9X,56HTHIS ERROR IS IN THE  N2 PART OF THE N SUB 2 POWER TA&
     &BLE)
      IF (IX.NE.0) WRITE(6,1006)
      IF (IZ.NE.0) WRITE(6,1008)
   25 CALL ENG1(TEA,AM)
      WSHPA = XLKUP(AM,TEA,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
      IF (IX.NE.0)  WRITE(6,1001)
      IF (IY.NE.0)  WRITE(6,1002)
      IF (WSHPA.LT.0.0)  WSHPA = 0.0
      WSHPA = WSHPA * CKFF
      TPEA  = TEA  * THETA
      NPLIM = NLIM
      RETURN
      END
      SUBROUTINE  PLOTXY(IPLOT)
!**** MEMBER NAME = B93TPLOT
      CALL LOADET(SS,2)
 1000 FORMAT(/44X36HA  PLOT  OF  X - Y  IN  3  DIMENSION)
 1001 FORMAT(///,5(40X,1HI/),40X1HI,42(1H.),1H*/40X1HI,41X,2H*.,/       &
     &40X1HI28X2H**10X3H* ./,40X1HI27X4H*  *8X4H*  ./37X4HZ  I26X       &
     &5H*   *7X5H*   ./,40X2HI.21X4H****5X1H*5X1H*4X1H./37X6HI  I .,    &
     &  19X1H*10X6H**   *4X1H./40X4HI  .17X1H*13X3H* *5X1H./37X       8H&
     &A  I   .15X1H*15X1H*6X1H./37X4HX  I4X1H.7X2H**4X1H*23X1H./       3&
     &7X4HI  I5X1H.5X7H*  *  *24X1H./37X4HS  I6X1H.3X1H*4X2H**25X       &
     &1H.)
 1002 FORMAT(  40X1HI7X3H. *32X1H./40X1HI8X1H*33X1H./4(40X1HI8X1H.      &
     &33X1H./),40X1HI50(1H-)/39X1H/9X1H.6X1H./35X4HS  /10X1H.5X1H.      &
     & 4X8HY - AXIS/34X4HI  /11X1H.4X1H./33X4HX  /12X1H.3X1H./32X       &
     &4HA  /13X4H.  ./34X1H/14X3H. ./30X4H/  /15X2H../32X1H/17(1H.)     &
     &  /28X4HX  //30X1H//29X1H//28X1H//)
      WRITE(6,1000)
      WRITE(6,1001)
      WRITE(6,1002)
      RETURN
      END
      SUBROUTINE POWER(TPROP,YLS2,ETAP)
!**** MEMBER NAME = B93TPOWR
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
      DIMENSION A0(3,15),A1(3,15),A2(3,15),BB0(16),BB1(16),BB2(16),     &
     & RJ3(10),CPOW3(10),ETAI3(10,10),RJ4(10),CPOW4(10),ETAI4(10,10),   &
     &AMACH(3),CLLL(15),CLGAM(16),CPOW33(20),CTI3(20),CTI4(20),         &
     & GMDD1(16),GAMD11(3,15)
       DIMENSION SHP(10),ETAPP(10),TPROPP(10)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
!
      COMMON     P         ,ANXMAX
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      COMMON/ODD/A0,A1,A2,BB0,BB1,BB2,RJ3,CPOW3,ETAI3,RJ4,CPOW4,        &
     & ETAI4,AMACH,CLLL,CLGAM,CPOW33,CTI3,CTI4,GMDD1,GAMD11
      NOCTSG = CTSGNO + 0.01
      NOMTP  = TPMNO + 0.01
      NOXPJ = XPJNO + 0.1
      NOCPP = CPPNO + 0.1
      PI = 3.14159
      TPROP1 = TPROP
      IT1 = 0
      ETAP = 0.8
      P1 = 0.0
      IF (IN2.EQ.2) A2STR=A2MAX*AN2MAX
      IF (IN2.EQ.2) P = A2STR*VT
   14 IF (V.EQ.0.0) GO TO 11
      SHPR = (1.689*V*TPROP1)/(550.0*ETAT*ETAP*DELTA*STHETA*            &
     & BHPP*YLS2)  +  DSHPAC/(DELTA * STHETA * BHPP)
   23 IF (IN2.EQ.2) GO TO 22
   15 TPS = TSHP(NTS)*THETA
      CALL POWAVL(TPS,EM)
      IF (SHPA.LT.SHPR) GO TO 25
      GO TO 12
   11 SHPR = TPROP1**1.5*SQRT(WGA/WG)/(550.0*ETAT*ETAP*DELTA*           &
     & STHETA*S2RHO*BHPP*YLS2)  +  DSHPAC/(DELTA * STHETA * BHPP)
      GO TO 23
   12 CALL POWREQ(EM)
      P = A2STR*VT
   22 IF (IT1.GT.25) GO TO 5
      IT1 = IT1 + 1
      IF (ETAIND.EQ.2.0) GO TO 13
      REALJ = 1.689*PI*V/P
      CCT = (PI**3)*TPROP1*WGA/(0.009507*SIGMA*WG*P**2)
      IF(ETAIND.NE.3.0) GO TO 300
      TOAD=CCT*0.009507*P**2/(THETA*PI**3)
      HPADTH=XLKUP(EM,TOAD,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IZ)
      CCP=HPADTH*550.*PI**4*STHETA**3/(0.009507*P**3)
      GO TO 301
  300 IF(REALJ.EQ.0.0) GO TO 712
      CCP=XLKUP(REALJ,CCT,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IZ)
      GO TO 301
  712 TIPP = P/(SA * 1.689)
      ETAP = XLKUP(CTSIG,TIPP,CTOSIG,NOCTSG,TIPM,NOMTP,FMER,10,6,IX,IZ)
  301 IF(IX.NE.0.AND.REALJ.GT.0.0) WRITE(6,1001)
 1001 FORMAT(9X,89HTHIS ERROR IS IN THE  J  PART OF THE PROPELLER POWER &
     &COEFFICIENT TABLE - SUBROUTINE POWER)
      IF (IZ.NE.0.AND.REALJ.GT.0.0) WRITE(6,1002)
 1002 FORMAT(9X,89HTHIS ERROR IS IN THE  CT PART OF THE PROPELLER POWER &
     &COEFFICIENT TABLE - SUBROUTINE POWER)
      IF(IX.NE.0.AND.REALJ.EQ.0.0) WRITE(6,1003)
 1003 FORMAT(9X,'THIS ERROR IS IN THE CT/SIGMA PART OF THE FIGURE',     &
     &' OF MERIT TABLE - SUBROUTINE POWER')
      IF(IZ.NE.0.AND.REALJ.EQ.0.0) WRITE(6,1004)
 1004 FORMAT(9X,'THIS ERROR IS IN THE MACH NO. PART OF THE FIGURE',     &
     &' OF MERIT TABLE - SUBROUTINE POWER')
      IF (REALJ.EQ.0.0) GO TO 17
      ETAP = REALJ * CCT/CCP
      SHPR = (1.689*V*TPROP1)/(550.*ETAT*ETAP*DELTA*STHETA*             &
     &BHPP*YLS2) + DSHPAC/(DELTA * STHETA * BHPP)
! **  ETAIND IS EQUAL TO 0.0 OR 1.0 FOR THIS CALCULATION
   24 IF (IN2.EQ.2) GO TO 6
      IF (ABS(1.0-P1/P).LE.0.01) GO TO 6
      P1 = P
      GO TO 15
   13 SHPA = SHPR
      CALL THRUST(TPROP,YLS2,ETAP)
      IF (ABS(1.0-TPROP/TPROP1).GT.0.005) GO TO 27
      IF (ABS(1.0-P1/P).LE.0.01) GO TO 6
   27 P1 = P
      IF (IT1.GT.10) GO TO 25
      GO TO 14
   17 CCP = 0.798*CCT**1.5/ETAP
      SHPR = TPROP1**1.5*SQRT(WGA/WG)/(550.*ETAT*ETAP*DELTA*            &
     &STHETA*S2RHO*BHPP*YLS2)  +  DSHPAC/(DELTA * STHETA * BHPP)
! **  AT THIS POINT IN THE SUBROUTINE DSHPAC IS A DIRECT ADDITION TO THE
!     POWER REQUIRED
      GO TO 24
    5 WRITE(6,10)
   10 FORMAT(10X,'J DOES NOT CONVERGE IN TWENTY FIVE ITERATIONS - SUBROU&
     &TINE POWER')
      GO TO 6
   25 TPS = TSHP(NTS)*THETA
      CALL POWAVL(TPS,EM)
      SHPMAX = SHPA
      A2S10 = A2STR
      SHPMIN = V*TPROP1 /(325.6365*ETAT*DELTA*BHPP*YLS2*STHETA) +DSHPAC/&
     &  (DELTA * STHETA * BHPP)
! **  SHPMIN IS BASED ON 100 PERCENT EFFICIENCY; THIS IS THE MINIMUM
!     POWER REQUIRED AND DOES INCLUDE DSHPAC
      IF (V.EQ.0.0) SHPMIN = TPROP1**1.5*SQRT(WGA/WG)/                  &
     & (550.*ETAT*DELTA*S2RHO*BHPP*YLS2*STHETA) + DSHPAC/(DELTA*STHETA* &
     &   BHPP)
      DELSHP = (SHPMAX - SHPMIN)/9.0
      DO 20 I=1,10
      XI = I
      SHP(I) = SHPMIN + DELSHP*(XI-1.0)
      SHPR = SHP(I)
      A2STR = A2S10
      IF (IN2.EQ.2) GO TO 21
      IF(I.NE.10) CALL POWREQ(EM)
   21 SHPA = SHPR
      CALL THRUST(TPROP,YLS2,ETAP)
      ETAPP(I) = ETAP
      TPROPP(I) = TPROP
      K = I
      IF (K.LT.3) GO TO 20
      IF (TPROP1.LT.TPROPP(K)) GO TO 215
   20 END DO
      SHPR = 1.1*SHPMAX
      GO TO 6
  215 ETAP = TABLE(TPROP1,TPROPP,ETAPP,K,2,M)
      SHPR = TABLE(TPROP1,TPROPP,SHP,K,2,M)
    6 RETURN
      END
      SUBROUTINE POWREQ(AM)
!**** MEMBER NAME = B93TPREQ
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  ARRAY(10)
      TII = XIBIV(AM,SHPR,AMSHP,NMS,TSHP,NTS,SHPAV,6,8,IX,IZ)
 1001 FORMAT(9X,56HTHIS ERROR IS IN THE  M  PART OF THE REFERED POWER TA&
     &BLE)
 1002 FORMAT(9X,56HTHIS ERROR IS IN THE  HP PART OF THE REFERED POWER TA&
     &BLE)
      IF (IX.NE.0) WRITE(6,1001)
      IF (IZ.NE.0) WRITE(6,1002)
      IF (IN2 -1)  1,1,4
    1 IF (IRN.EQ.0) GO TO 20
    4 DO 10 I=1,NTS
      TII = TSHP(I)
      CALL ENG1(TII,AM)
   10 ARRAY(I) = SHPA
      TII = PARA(SHPR,ARRAY,TSHP,NTS,IX)
 1010 FORMAT(9X,43HTHIS ERROR IS IN THE POWER REQUIRED LOOK-UP)
      IF (IX.NE.0) WRITE(6,1010)
   20 IF (IN2-1) 21,21,22
   21 A2REF = XLKUP(AM,TII,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1006)
 1006 FORMAT(9X,56HTHIS ERROR IS IN THE  M  PART OF THE N SUB 2 POWER TA&
     &BLE)
      IF (IY.NE.0) WRITE(6,1007)
 1007 FORMAT(9X,56HTHIS ERROR IS IN THE  T  PART OF THE N SUB 2 POWER TA&
     &BLE)
      A2STR = A2REF*STHETA
      GO TO 23
   22 A2STR = AN2MAX*A2MAX
   23 WSHPR = XLKUP(AM,TII,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
 1003 FORMAT(9X,60HTHIS ERROR IS IN THE  M  PART OF THE REFERED FUEL FLO&
     &W TABLE)
 1004 FORMAT(9X,60HTHIS ERROR IS IN THE  T  PART OF THE REFERED FUEL FLO&
     &W TABLE)
      IF (IX.NE.0) WRITE(6,1003)
      IF (IY.NE.0) WRITE(6,1004)
      IF (WSHPR.LT.0.0)  WSHPR  =0.0
      WSHPR = WSHPR * CKFF
      TPEA  = TII  * THETA
      RETURN
      END
      SUBROUTINE PRFRM
!**** MEMBER NAME = B93TPRFM
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      COMMON/HAL/INDSGT,KE
! **  HAL IS PASSED AMOUNG CRUS1,3, PRFRP, PRFRM, LOITR
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      KE = 0
      IF(OPTIND.EQ.2.) CALL LOADET(SS,2)
      IF(OPTIND.EQ.2.) WRITE(6,9001)
 9001 FORMAT(/15X,24HMISSION PERFORMANCE DATA/)
      DO 13 IAAA=1,11
   13 STORWF(IAAA)= 0.
      W  = WG
      WF = 0.
      WFL   =0.
      IF(SAVE(24).NE.1.) GO TO 91
      ITAXI = 0
      ITOHL = 0
      ICLIMB= 0
      ICRUS =0
      IDSCNT= 0
      ILOITR= 0
      IFUEL = 0
      ICHGPL= 0
      ITALT = 0
      IPLOT = 0
      IPRFRP = 0
      EMPTY1 = 0.
      I=0
   91 I=I+1
      INDSGT= SGTIND(I)
  100 INDEX = INDSGT
      GO TO (1,2,3,4,5,6,7,8,9,10,11),INDEX
    1 ITAXI = ITAXI  +  1
      CALL TAXI(ITAXI)
      GO TO 200
    2 ITOHL = ITOHL  +  1
      CALL TOHL(ITOHL)
      GO TO 200
    3 ICLIMB= ICLIMB +  1
      IF (IOPTH.EQ.0.OR.SGTIND(I+1).NE.4.) GO TO 101
      INOPTH = 1
      INDEX3 = ICRUS + 1
  101 CALL CLIMB(ICLIMB)
      INOPTH = 0
      GO TO 200
    4 ICRUS = ICRUS + 1
      INDCRS= CRSIND(ICRUS)
      EMPTY1 = SGTIND(I+1)
      IF (EMPTY1.NE.5.) GO TO 40
      IF((DESIND(IDSCNT+1).EQ.2.).OR.(DESIND(IDSCNT+1).GE.4.))EMPTY1=51.
   40 IF (INDCRS - 2) 41,42,43
   41 CALL CRUS1(ICRUS)
      GO TO 200
   42 CALL CRUS2(ICRUS)
      GO TO 200
   43 CALL CRUS3(ICRUS)
      GO TO 200
    5 IDSCNT= IDSCNT +  1
      IF  (DESIND(IDSCNT).GT.4.)  GO TO 50
      CALL DSCNT(IDSCNT)
      GO TO 200
   50 CALL DSCEX(IDSCNT)
      GO TO 200
    6 ILOITR= ILOITR +  1
      CALL LOITR(ILOITR)
      GO TO 200
    7 IFUEL = IFUEL + 1
      CALL CHGFW (IFUEL)
      GO TO 200
    8 ICHGPL= ICHGPL +  1
      CALL CHGPL(ICHGPL)
      GO TO 200
    9 ITALT = ITALT + 1
      IF (IOPTH.EQ.0.OR.SGTIND(I+1).NE.4.) GO TO 102
      IF (HFIN(ITALT).LT.1000.) GO TO 102
      INOPTH = 1
      INDEX3 = ICRUS + 1
  102 CALL TRALT(ITALT)
      INOPTH = 0
      GO TO 200
   10 IPLOT = IPLOT + 1
      CALL PLOTXY(IPLOT)
   11 IPRFRP = IPRFRP + 1
      CALL PRFRP(IPRFRP)
      INDSRV = INDSGT
  200 IF(NEXT.NE.0)RETURN
      I = I + 1
      INDSGT = SGTIND(I)
      IF ((INDSGT.NE.0).AND.(INDSGT.NE.100)) GO TO 100
      IF(IFUEL.EQ.0) GO TO 22
      WFS   =WF
      DO 21 L = 1, IFUEL
   21 WFS   =AMAX1(STORWF(L),WFS)
      IF(WFS.EQ.WF) GO TO 23
      DO 77 L=1,IFUEL
   77 IF(WFS.EQ.STORWF(L)) WFL= STRWFL(L)
      GO TO 23
   22 WFS = WF
   23 IF(INDSRV.EQ.11) GO TO 26
      WFR = CK1 * WFS + DELWF
      TOTFU = CK1 * WFS + DELWF
      RESFU =(CK1-1.)*WFS+DELWF+WFL
      SIMFU = TOTFU - RESFU
   26 SAVE(25) = INDSGT
      IF(INDSGT.EQ.0) SAVE(25)= SGTIND(I+1)
      IF(INDSRV.EQ.11) GO TO 27
      IF(OPTIND.EQ.2.) WRITE(6,9002) SIMFU, RESFU, TOTFU
 9002 FORMAT(//15X,23HMISSION FUEL REQUIRED =,F9.2                      &
     &        /15X,23HRESERVE FUEL REQUIRED =,F9.2                      &
     &        /15X,23HTOTAL   FUEL REQUIRED =,F9.2)
   27 INDSRV = 0
      IF (.NOT.(((OPTIND.EQ.2.).AND.(SAVE(25).EQ.100.)))) RETURN
      WRITE(6,5224)
 5224 FORMAT(//2X       100(1H*)//)
      WRITE(6,5225)
 5225 FORMAT(/2X 25HEND  OF  SUCCESSFUL  CASE/)
      WRITE(6,5224)
      RETURN
      END
      SUBROUTINE PRFRP(IPRFRP)
!**** MEMBER NAME = B93TPRFP
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11),STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC 2201 TO LOC 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON/HAL/INDSGT,KE
! **  HAL IS PASSED AMOUNG CRUS1,3, PRFRP, PRFRM, LOITR
      COMMON/KAGS/ATGP(10),TINGP(10)
! **  KAGS IS PASSED FROM MAIN TO BE USED IN CALL ATMOS STATEMENT
      DIMENSION EO(7)
      DATA EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
      DATA DASH /4H----/
      PEHF = 0.0
      LETF = 0.0
      PETF = 0.0
      EM = 0.0
      CT = 0.0
      CP = 0.0
      ADVJ = 0.0
      ETAPP = DASH
      BHPR = 0.0
      THRT = 0.0
 4971 FORMAT(//13X,'TOTAL',17X,'THRUST',11X,'FUEL FLOW   TURB.',4X      &
     &,'PETF',22X,'SPEC.',/5X,'TAS',5X,'FUEL FLOW',15X                  &
     &,'TO',6X,'EAS',4X,'PRIM. ENG',3X,'TEMP.',5X,'OR',5X,'MACH',5X     &
     &,'MACH',5X,'RANGE',6X,'BHP',8X,'FM',/5X,'(KTS)',3X,'(LBS/HR)',14X &
     &,'WEIGHT',4X,'(KTS)',3X,'(LBS/HR)',4X,'(R)',5X,'PEHF'             &
     &,13X,'DIV',6X,'(NMPP)',///5X,'CL',8X,'CD',8X,'L/D',7X,'LIFT',6X   &
     &,'DRAG',3X,'FUEL FLOW',5X,'J',6X,'LETF',5X,'CP',7X,'CT',6X,       &
     &'VTIP',7X,'NET',7X,'ETAP',/35X,'(LBS)',5X,'(LBS)',2X              &
     &,'LIFT ENG.',38X,'(FPS)',5X,'THRUST',5X,'PROP',/53X,'(LBS/HR)',//)
 4972 FORMAT(5X,F5.1,3X,F7.1,16X,F5.3,5X,F5.1,3X,F7.1,3X,F6.1           &
     &,3X,F5.3,3X,F5.3,4X,F5.3,4X,F7.6,4X,F6.0,4X,F5.3)
 4973 FORMAT(5X,F6.3,3X,F6.4,4X,F6.3,5X,F7.0,3X,F6.0,3X,F7.1,2X,F6.3    &
     &,4X,F5.3,3X,F6.4,3X,F6.4,3X,F5.1,4X,F8.0,4X,F5.3/)
 4974 FORMAT(6X,A4,5X,A4,6X,A4,6X,F7.0,4X,A4,4X,F7.1,3X,F6.3,3X,F5.3,   &
     &3X,F6.4,3X,F6.4,3X,F5.1,4X,F8.0,4X,A4/)
 4975 FORMAT(5X,F5.1,3X,F7.1,17X,A4,4X,F5.1,4X,F7.1,3X,F6.1,3X,F5.3,    &
     &3X,F5.3,4X,F5.3,4X,F7.6,4X,F6.0,4X,A4)
! **  4972 IS TOP LINE FOR HOVER; 4973 IS BOTTOM LINE FOR CRUISE
!     4974 IS BOTTOM LINE FOR HOVER; 4975 IS TOP LINE FOR CRUISE
      PI = 3.14159
      GWND = GWIND(IPRFRP)
      GPP = GWP(IPRFRP)
      CLP = CLWP(IPRFRP)
      DELFP = DELFEP(IPRFRP)
      ALT = AHOP(IPRFRP)
      TOVW = TOWP(IPRFRP)
      AN2MAX = AN2M7(IPRFRP)
      AN3MAX = AN2M8(IPRFRP)
      DLVP = DELVP(IPRFRP)
      VMXP = VMAXP(IPRFRP)
      W = GPP
      IF(GWND.EQ.2.0) GO TO 1
      W = WG + GPP
    1 A2STR = A2MAX*AN2MAX
      P = A2STR*VT
      LC1 = 0
      V = 0.0
      VH = 0.0
      VR = 0.0
      EAS = 0.0
      EN = 0.0
      CALL ATMOS(ALT, ATGP(IPRFRP), TINGP(IPRFRP))
      FTINGP = 518.69*THETA - 459.69
      EM = V/SA
      DELRTH = DELTA*STHETA
      WDEL = W/DELTA
      WRITE(6,1000) W, WDEL, ALT, DELRTH, FTINGP, DELTA, THETA
 1000 FORMAT(/16X,'GROSS WEIGHT =',F8.0,'  LB',13X,'W/DELTA =',F7.0,    &
     &'  LB'/16X,'ALTITUDE     =',F9.1,'  FT',12X,'DELTRTH = ',F5.3/16X &
     &,'TEMPERATURE  =',F9.3,' DEG.,F.',8X,'DELTA   = ',F5.3/55X,       &
     &'THETA   = ',F5.3)
      WRITE(6,4971)
      IF(ETAIND.EQ.0.0) ETAP = ETAP2
      IF(DNITFL.EQ.1.0) GO TO 19
      IF(INDENG.NE.0) GO TO 14
! *** IF ENGIND = 2. PROGRAM SETS ETAIND = 0; ETAP2 IS INPUT
      CALL POWAVL(TMAX,0.0)
      BLP = SHPA
      TKE = TPEA
      BHPSUP = BLP*BHPP*DELTA*STHETA
      KE = NPLIM + 1
   13 IF(ETAIND.NE.0.0) GO TO 29
      BHPPS = (TOWP(IPRFRP)**1.5)*WG*SQRT(WGA)                          &
     &/(550*ETAT*ETAP2*S2RHO) + DSHPAC
      BHPR = BHPPS
      IF(BHPSUP.GE.BHPPS) GO TO 11
! *** BHPSUP = POWER AVAILABLE  AND  BHPPS = POWER REQUIRED
      GO TO 28
   29 TPROP = TOWP(IPRFRP)*W
      IF(ENGIND.EQ.2.0) BHPP = TP/BETA
      CALL POWER(TPROP,1.0,ETAP)
! *** IF ETAIND.NE.0.0 ETAP CALCULATED IN POWER SUBROUTINE
      BHPR = BHPP*SHPR*DELTA*STHETA
! **  ACCESSORY POWER IS INCLUDED IN SHPR FROM THE POWER SUBROUTINE
      IF(BHPSUP.GE.BHPR) GO TO 11
! *** BHPSUP = POWER AVAILABLE  AND  BHPR = POWER REQUIRED
   28 WRITE(6,232)
  232 FORMAT(7X,'+++CAUTION+++ PEHF IS GREATER THAN 1.000')
   11 BHPPY = BHPSUP
      IF(ENGIND.EQ.0.0) GO TO 10
      TPY = BETA*BHPPY
! *** BHPPY = POWER AVAILABLE; BHPPS = POWER REQUIRED
      BHPSUP = BHPPS
      IF(ETAIND.EQ.0.0) GO TO 31
      TSUBP = BETA*BHPR
      GO TO 9916
   31 TSUBP = BETA*BHPPS
 9916 YALE22 = TSUBP/(DELTA*TP)
! *** TSUBP = PRIMARY ENGINE THRUST
      TLP = YALE22
      SHPR = YALE22
      CALL THRREQ(0.0)
      FPGP = WSHPR*DELTA*STHETA*TP
      TKE = TPEA
      KE = 6
      FLGP = 0.0
      GO TO 202
   10 IF(ETAIND.EQ.0.0) GO TO 30
      BHPSUP = BHPR
      GO TO 32
   30 BHPSUP = BHPPS
   32 YALE22 = BHPSUP/(BHPP*DELTA*STHETA)
      BLP = YALE22
      SHPR = YALE22
      CALL POWREQ(0.0)
      FPGP = WSHPR*DELTA*STHETA*BHPP
      TKE = TPEA
      KE = 6
      LETF = 0.0
      FLGP = 0.0
      GO TO 202
   14 CALL THRAVL(TMAX,0.0)
      TLP = SHPA
      TKE = TPEA
      KE = NPLIM + 1
      TSUBP = TLP*DELTA*TP
      IF(ENGIND.EQ.1.0) GO TO 9
      BHPSUP = TSUBP/BETA
      GO TO 13
    9 IF(TSUBP.LT.(TOWP(IPRFRP)*W/ETAP2)) GO TO 16
  161 TPY = TSUBP
      TSUBP = TOWP(IPRFRP)*W/ETAP2
      THRT = TSUBP
      GO TO 9916
   19 CALL LIFAVL(TLMAX)
      TLL = FLIF
      TSUBL = TLL*DELTA*TL
      IF(TSUBL.LT.(TOWP(IPRFRP)*W/ETAL)) GO TO 17
      LETF = TOWP(IPRFRP)*W/(TSUBL*ETAL)
      TSUBP = 0.0
      YALE22 = TSUBL/(DELTA*TL)
      TLL = YALE22
      FPGP = 0.0
      TKE = TFI
      KE = 1
      FREQ = YALE22
      CALL LIFREQ
      WDOTFL = FWR
      FLGP = WDOTFL*DELTA*STHETA*TL
      THRT = TSUBL
      GO TO 202
   17 LETF = 1.0
      CALL THRAVL(TMAX,0.0)
      TLP = SHPA
      TKE = TPEA
      KE = NPLIM + 1
      TSUBP = TLP*DELTA*TP
      YALE22 = (TOWP(IPRFRP)*W - TSUBL*ETAL)/ETAP2
      IF(TSUBP.LT.YALE22) GO TO 16
  162 TPY = TSUBP
      TSUBP = YALE22
      TLP = TSUBP/(DELTA*TP)
      SHPR = TLP
      CALL THRREQ(0.0)
      TKE = TPEA
      KE = 6
      FPGP = WSHPR*DELTL*STHETA*TP
      FREQ = TLL
      CALL LIFREQ
      WDOTFL = FWR
      FLGP = WDOTFL*TL*DELTA*STHETA
      THRT = TSUBP + TSUBL
      GO TO 202
   16 WRITE(6,233)
  233 FORMAT(7X,'+++CAUTION+++ PETF IS GREATER THAN 1.000')
      IF(DNITFL.EQ.1.0) GO TO 162
      GO TO 161
  202 FTOT = FPGP + FLGP
      IF(ENGIND.EQ.0.0) GO TO 253
      IF(TSUBP.EQ.0.0) PEHF = 0.0
      IF(TSUBP.EQ.0.0.AND.ENGIND.EQ.1.0) GO TO 3815
      PETF = TSUBP/TPY
      PEHF = PETF
      IF(ENGIND.EQ.2.0) GO TO 253
      GO TO 2044
  253 IF(ENGIND.EQ.0.0) PEHF = BHPSUP/BHPPY
      IF(ENGIND.EQ.2.0.AND.IN2.EQ.2) A2STR = A2MAX*AN2MAX
      P = A2STR*VT
 2044 CT = TOWP(IPRFRP)*W*PI**3*WGA/(0.009507*SIGMA*WG*P**2)
      CP = 0.798*CT**1.5/ETAP
      GO TO 3816
 3815 CP = 10000000.
      CT = 10000000.
      ADVJ = 10000000.
      P = 0.0
 3816 FM = ETAP
      ALTD = DASH
      CL = 0.0
      CALL DRAG
      EMDD = EMD
      CL = DASH
      CD = DASH
      ALIFT = W
      ADRAG = DASH
      ETAPP = DASH
      WRITE(6,4972) V,FTOT,TOVW,EAS,FPGP,TKE,PEHF,EM,EMDD,EN,BHPR,FM
      WRITE(6,4974) CL,CD,ALTD,ALIFT,ADRAG,FLGP,ADVJ,LETF,CP,CT,P,THRT, &
     &ETAPP
! **  THE FORTRAN CODE BELOW IS THE GENERAL PERF. FOR CRUISE ONLY
!     NOTE THAT THE CRUISE CODING IS TOTALLY SEPARATE FROM T/O CODING
! *** FIND HIGHEST CL VALUE, CLMAX, IN TABLE; TCLN=NO.VALUES IN TABLE
      P = A2MAX * AN3MAX * VT
      A2STR = A2MAX * AN3MAX
      CLMAX = 0.0
      NTCLN = TCLN
      DO 222 I = 1,NTCLN
      IF(TBCL1(I).GT.CLMAX) CLMAX = TBCL1(I)
  222 END DO
      VH = SQRT(W/(RHO*1.42636*CLMAX*SW))
      VINC = VH + 0.01
      V = VINC
! *** VR IS VH ROUNDED UP TO NEXT HIGHEST TEN VALUE; VH IS VELOCITY
!     AT CLMAX
      VR = 10.0*(AINT(VH/10.0) + 1.0)
    4 FLGP = 0.0
      IF(ENGIND.EQ.0.0) ADVJ = 1.689 * V * PI/P
      IF(ENGIND.NE.0.0) ETAP = 1.0
      AN2MAX = AN2M8(IPRFRP)
      ITAB = 0
      Q = 1.42636*RHO*V**2
      EM = V/SA
      CL = W/(SW*Q)
      CALL DRAG
      IF(NEXT.NE.0) RETURN
      EMDD = EMD
      CD = CD+DELFEP(IPRFRP)
      ADRAG = CD * Q * SW
      ALTD = W/ADRAG
      IF(ENGIND.NE.0.) GO TO 2
      IF(ETAIND.NE.0.0) GO TO 26
      ETAP4 = XLINT(TBEM5,TB8AP4,EM,NETAP4,M)
      ETAP = ETAP4
      IF(M.NE.0) WRITE(6,1001)
 1001 FORMAT(22X,'THIS ERROR IS IN THE M-ETAP4 TABLE-SUBROUTINE PRFRP')
      BHPR = CD*SW*Q*V/(325.8*ETAP4*ETAT) + DSHPAC
      GO TO 2670
   26 TPROP = CD*SW*Q
      YLS2 = 1.0
      CALL POWER(TPROP,YLS2,ETAP)
      BHPR = SHPR*BHPP*DELTA*STHETA
! **  ACCESSORY POWER IS INCLUDED IN THE POWER SUBROUTINE
! *** BELOW IS CODE YIELDING AVAILABLE POWER; THIS OPTION COMPARES P0WER
!     REQUIRED TO POWER AVAILABLE;  FOR PEHF AND FUTURE MODIFICATIONS
 2670 CALL POWAVL(TMAX,EM)
      TKE = TPEA
      KE = NPLIM+1
      BLP = SHPA
      BHPSUP = BLP*DELTA*STHETA*BHPP
      BHPA = BHPSUP
      IF(BHPR.GT.BHPA) WRITE(6,232)
      BHPA = BHPR
      BHPSUP = BHPA
      BLP = BHPSUP/(BHPP*DELTA*STHETA)
      SHPR = BLP
      CALL POWREQ(EM)
      WSHPA = WSHPR
      TKE = TPEA
      KE = 6
      FPGP = WSHPA*DELTA*STHETA*BHPP
      FTOT = FPGP + FLGP
      GO TO 99
    2 TR = CD*Q*SW
! *** THE FOLLOWING CODE CALCULATES THE THRUST AVAILABLE
      CALL THRAVL(TMAX,EM)
      TKE = TPEA
      KE = NPLIM + 1
      TLP = SHPA
      TSUBP = TLP*TP*DELTA
      TA = TSUBP
      IF(TR.GT.TA) WRITE(6,233)
      TA = TR
      TSUBP = TA
      TLP = TSUBP/(TP*DELTA)
      SHPR = TLP
      CALL THRREQ(EM)
      WSHPA = WSHPR
      TKE = TPEA
      KE = 6
      FPGP = WSHPA*DELTA*STHETA*TP
      FTOT = FPGP + FLGP
   99 EN = V/FTOT
      EAS = V*SQRT(SIGMA)
      THRT = TR
      IF(ENGIND.EQ.0.0) GO TO 4256
      VQ = TSUBP/(DELTA*TP)
      CALL THRAVL(TMAX,EM)
      GO TO 4257
 4256 VQ = BHPSUP/(DELTA*STHETA*BHPP)
      CALL POWAVL(TMAX,EM)
 4257 ANICE = SHPA
      PETF = VQ/ANICE
      PEHF= PETF
      ETAPP = ETAP
      FM = DASH
      TOVW = DASH
      LETF = 0.0
      IF(ENGIND.EQ.0.0) GO TO 1217
      BHPR = 0.0
      CT = 10000000.
      CP = 10000000.
      ADVJ = 10000000.
      P = 0.0
      GO TO 723
 1217 THRT = 0.0
      CP = 550. * BHPR * PI**4 * WGA /(4.0 * RHO * P**3 * WG)
      CT = ETAPP * CP/ADVJ
  723 WRITE(6,4975) V,FTOT,TOVW,EAS,FPGP,TKE,PEHF,EM,EMDD,EN,BHPR,FM
      WRITE(6,4973) CL,CD,ALTD,ALIFT,ADRAG,FLGP,ADVJ,LETF,CP,CT,P,THRT, &
     &ETAPP
      IF(V.NE.VINC) GO TO 552
      V = VR
      GO TO 4
  552 DVP = DLVP
      IF(V.LE.(VMXP-DVP)) GO TO 369
      IF(V.EQ.VMXP) GO TO 553
      DVP = VMXP - V
  369 V = V + DVP
      GO TO 4
  553 CONTINUE
! *** END OF POWER(SPEED) CALC;BEGIN SPEED(TEMP) AND TORQUE LIMIT CALC
      LQ5 = 0
      LQ6 = 0
      INDSGT = 11
      ICRUS = IPRFRP
      VIN(ICRUS) = 0.0
      ATMIND(ICRUS + 30) = ATGP(IPRFRP)
      DELR(ICRUS) = 2.0
      TIN(ICRUS + 30) = TINGP(IPRFRP)
      RMAX(ICRUS) = 2.0
      ENPSD(ICRUS) = 0.0
      AN2M4(ICRUS) = AN2M8(IPRFRP)
      DLCDCR(ICRUS) = DELFEP(IPRFRP)
      ILOITR = IPRFRP
      DNIRTL(ILOITR) = 1.0
      DELST(ILOITR) = 0.01
      ATMIND(ILOITR + 50) = ATGP(IPRFRP)
      STL(ILOITR) = 0.01
      TIN(ILOITR + 50) = TINGP(IPRFRP)
      ENPSDL(ILOITR) = 0.0
      AN2M6(ILOITR) = AN2M8(IPRFRP)
      DLOITR(ILOITR) = DELFEP(IPRFRP)
      RSW(ILOITR) = 1.0
      POWCRI(ICRUS) = 2.0
      CALL CRUS1(ICRUS)
      IF(NPLIM.EQ.4) VQLIM = V
      IF(NPLIM.EQ.4) LQ6 = 1
! **  NPLIM IS AN ENGINE OPERATING CODE WHICH IS PASSED FROM POWAVL
      IF(NEXT.EQ.1) LQ1 = 1
      IF(NPLIM.NE.4) GO TO 101
! **  NOTE; A TORQUE LIMIT CONDITION EXISTS EITHER WHEN OPERATING NII
!     IS GREATER THAN S.L.STD NIIMAX OR WHEN ENGINE CODE LIMITED
      PQ = A2STR * VT
      A2Q = A2STR
  101 VNRP = V
      SRNRP = EN
      KF = KE
      POWCRI(ICRUS) = 1.0
      CALL CRUS1(ICRUS)
      IF(NPLIM.EQ.4) VQLIM = V
      IF(NPLIM.EQ.4) LQ6 = 1
      IF(NEXT.EQ.1) LQ1 = 2
      IF(NPLIM.NE.4) GO TO 102
      PQ = A2STR * VT
      A2Q = A2STR
  102 VMIL = V
      SRMIL = EN
      KG = KE
      POWCRI(ICRUS) = 0.0
      CALL CRUS1(ICRUS)
      IF(NPLIM.EQ.4)VQLIM = V
      IF(NPLIM.EQ.4) LQ6 = 1
      IF(NEXT.EQ.1) LQ1 = 3
      IF(NPLIM.NE.4) GO TO 103
      PQ = A2STR * VT
      A2Q = A2STR
  103 VMXGP = V
      SRMAX = EN
      KH = KE
      CRSIND(ICRUS) = 3.0
      CALL CRUS3(ICRUS)
      VBR = V
      SRBR = EN
      KP = KE
      CRSIND(ICRUS) = 4.0
      CALL CRUS3(ICRUS)
      V99BR = V
      SR99BR = EN
      KS = KE
      CALL LOITR(ILOITR)
      VBE = V
      FFBE = F1
      KT = KE
      IF(QIND.GT.0.0) GO TO 24
      WRITE(6,1007)
      GO TO 2806
   24 IF(LQ6.NE.1) GO TO 45
      P = PQ
      A2STR = A2Q
      RPM = 60. * P/(PI*D)
      BHPQ = QMAX * BHPP * AN2TO * AN3MAX * A2MAX/ANXMAX
      XQLM = 275.*BHPQ*D/P
      WRITE(6,1008) VQLIM, P, RPM, BHPQ, XQLM
      GO TO 2806
   45 WRITE(6,1026)
 2806 IF(LQ1.EQ.1) GO TO 6630
      WRITE(6,1011) VMXGP, SRMAX, EO(KH)
 2807 IF(LQ1.EQ.2) GO TO 6631
      WRITE(6,1013) VMIL, SRMIL, EO(KG)
 2808 IF(LQ1.EQ.3) GO TO 6632
      WRITE(6,1015) VNRP, SRNRP, EO(KF)
 2809 WRITE(6,1017)VBR,SRBR,EO(KP),V99BR,SR99BR,EO(KS),VBE,FFBE,EO(KT)
 1007 FORMAT(//16X,'MAIN TRANSMISSION TORQUE LIMIT NOT APPLIED'//)
 1008 FORMAT(//16X,'MAIN TRANSMISSION TORQUE LIMIT (ALL ENGINES OPERATIN&
     &G) OCCURS AT'/48X,'V',15X,'= ',F6.1,1X,'KTAS'/48X,'MAIN ROTOR VTIP&
     & = ',F6.1,1X,'FT/SEC'/48X,'MAIN ROTOR RPM  = ',F6.1,/48X,'POWER',1&
     &1X,'= ',F6.0,1X,'SHP'/48X,'TORQUE',10X,'= ',F8.0,1X,'FT-LB'//)
 1011 FORMAT(//25X,'V(MAX PWR)',8X,'= ',F6.1,1X,'KTAS',2X,'SPEC.RANGE = &
     &',F6.4,1X,'NM/LB',2X,A4)
 1012 FORMAT(//25X,'INSUFFICIENT POWER AVAILABLE FOR CRUISE AT MAXIMUM E&
     &NGINE RATING')
 1013 FORMAT(25X,'V(MIL PWR)',8X,'= ',F6.1,1X,'KTAS',2X,'SPEC.RANGE = ',&
     &F6.4,1X,'NM/LB',2X,A4)
 1014 FORMAT(25X,'INSUFFICIENT POWER AVAILABLE FOR CRUISE AT MILITARY EN&
     &GINE RATING')
 1015 FORMAT(25X,'V(NRP)',12X,'= ',F6.1,1X,'KTAS',2X,'SPEC.RANGE = ',F6.&
     &4,1X,'NM/LB',2X,A4)
 1016 FORMAT(25X,'INSUFFICIENT POWER AVAILABLE FOR CRUISE AT NORMAL ENGI&
     &NE RATING')
 1017 FORMAT(25X,'V(BEST RANGE)',5X,'= ',F6.1,1X,'KTAS',2X,'SPEC.RANGE =&
     & ',F6.4,1X,'NM/LB',2X,A4,/25X,'V(99 PERCENT BR)',2X,'= ',F6.1,1X,'&
     &KTAS',2X,'SPEC.RANGE = ',F6.4,1X,'NM/LB',2X,A4,/25X,'V(BEST ENDURA&
     &NCE) = ',F6.1,1X,'KTAS',2X,'FUEL FLOW  = ',F6.0,1X,'LB/HR',2X,A4,/&
     &/)
 1026 FORMAT(//16X,'MAIN TRANSMISSION TORQUE LIMIT NOT EXCEEDED AT THESE&
     & FLIGHT CONDITIONS')
      GO TO 6310
 6630 WRITE(6,1012)
      GO TO 2807
 6631 WRITE(6,1014)
      GO TO 2808
 6632 WRITE(6,1016)
      GO TO 2809
 6310 RETURN
      END
      SUBROUTINE SCRIBE(ITAB,ETAP,YLS2)
!**** MEMBER NAME = B93TSCRI
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10),TKETKE(10),EOEOEO(10),HHH(10),ETAETA(10)
      COMMON CLCLCL(10),CDCDCD(10),ALDALD(10),ELLELL(10),DEEDEE(10),    &
     & FFF(10),BHPBHP(10),TPTPTP(10),CCPCCP(10),CCTCCT(10),AJAJAJ(10),  &
     & PPP(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      PI = 3.14159
      ELL = CL*Q*SW
      DEE = CD*Q*SW
      ALDLD = CL/CD
      IF (ENGIND.NE.0.0) BHPR = 0.0
      IF (ENGIND.NE.0.0) GO TO 241
      TPS = TMAX
      CALL POWAVL(TPS,EM)
      SHPR = SHPA*PEHF
      BHPR = SHPR*BHPP*DELTA*STHETA*YLS2
! **  PEHF IS DETERMINED FROM THE SUBROUTINE FROM WHICH SCRIBE WAS
!     CALLED. DSHPAC IS ALREADY INCLUDED IN BHPR (POWER REQ.); THE
!     ACCESSORY POWER HAS BEEN ADDED EITHER IN POWER OR PERF. SUBROUTINE
      TPROP = 325.6365*ETAP*ETAT*BHPR/V
      CALL POWREQ(EM)
      P = A2STR*VT
      PI3 = PI**3
      PP2 = P**2
      CCT = TPROP*PI3*WGA/(0.009507*SIGMA*WG*PP2)
      CCP = 550.0*PI*PI3*(STHETA**3)*BHPP*SHPR*ETAT*WGA*YLS2/           &
     & (0.009507*WG*PP2*P)
      REALJ = 1.689*PI*V/P
      GO TO 243
  241 TPS = TMAX
      CALL THRAVL(TPS,EM)
      TPROP = SHPA*PETF*DELTA*TP
      REALJ = 1000000.
      CCP = 1000000.
      CCT = 1000000.
  243 IF (ITAB.EQ.0) GO TO 242
      ELLELL(ITAB) = ELL
      DEEDEE(ITAB) = DEE
      FFF(ITAB) = F
      CLCLCL(ITAB) = CL
      CDCDCD(ITAB) = CD
      BHPBHP(ITAB) = BHPR
      CCPCCP(ITAB) = CCP
      CCTCCT(ITAB) = CCT
      AJAJAJ(ITAB) = REALJ
      TPTPTP(ITAB) = TPROP
      ALDALD(ITAB) = ALDLD
      PPP(ITAB) = P
      GO TO 245
  242 WRITE(6,9003) CL,CD,ALDLD,ELL,DEE,F,BHPR,TPROP,CCP,CCT,REALJ,     &
     & P,ETAP
 9003 FORMAT( 7XF6.3,5XF6.4,4XF6.3,4XF7.0,2XF7.0,2XF7.0,2XF7.0,         &
     &6XF7.0,2XF6.4,1XF6.4,1XF6.3,3XF5.1,4X,F5.3/)
  245 RETURN
      END
      SUBROUTINE SIZTR(IST)
!**** MEMBER NAME = B93TSIZR
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! **          PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      YALE22=1.
      NOCPP = CPPNO + 0.01
      NOXPJ = XPJNO + 0.01
      NOCTSG = CTSGNO + 0.01
      NOMTP = TPMNO + 0.01
      PI = 3.1415932
      INDATM = YALE22
      AN2MAX  =  AN2TO
      IST=IST+1
      IF(IST.NE.1) GO TO 3
      IF (FDMIND-1.0) 3,21,22
   21 DBAR = (HF + SWF)/2.
      ELP = DBAR*ELPD
      ELT = DBAR*ELTD
      ELF    = ELP + ELC + ELT
      SF     = DBAR * (2.5*ELP + 3.14*ELC + 2.1*ELT)
      GO TO 3
   22 AGALLY = AGLLEY
      ACL = (15.0*ANPX1 + 10.0*ANPXT)/144.
      WOV2T = 0.5*(ANABT*WSEATT + ANISLT*WAISLT) - 11.0
      WOV2 = 0.5*(ANAB1*WSEAT1 + ANISL1*WAISL1) - 11.0
      PREC1 = 0
      IF (WOV2T.GT.WOV2) PREC1 = 1
      IF (WOV2T.GT.WOV2) WOV2 = WOV2T
      IF (WOV2.GE.70.) RCRC = 10.0 + 1.011*WOV2
      IF (WOV2.LT.70.) RCRC = 27.7 + WOV2*(0.5 + 0.00369*WOV2)
      RBRB = RCRC/0.94
      DBDB = 2.0*RBRB
      DCDC = 2.0*RCRC
      IF (LAVIND.EQ.0)  NLAVS = (ANPX1 + ANPXT -20.)/40. + 1.0
      IF (LAVIND.EQ.1) NLAVS = ANLAVS + 0.01
      ALAVS =16.0*NLAVS
      IF (LAVIND.EQ.1) ALAVS = 16.0*ANLAVS
      IF (NGLIND.EQ.0) AGALLY = 0.27*ANPXT + 0.65*ANPX1
      AUTILS = ACL + ALAVS + AGALLY
      ELT = ELTD*RBRB/6.0
      ELP = ELPD*RBRB/6.0
      IF (ANISL1.GE.1.0) USAREA= 0.2*(ELP*(DCDC-ANISL1*WAISL1) +        &
     & ELT*(DCDC-ANISLT*WAISLT))/12.0
      IF (ANISL1.EQ.0.0) USAREA = 0.2*(ELP + ELT)*(DCDC - ANISLT*WAISLT)&
     &  /12.0
      IF (ANPXT.EQ.0.0) GO TO 223
      ELTC = ((ANPX1*PSEAT1*WSEAT1 + ANPXT*PSEATT*WSEATT + (AUTILS -    &
     & USAREA)*144.)/((ANPX1*PSEAT1/(ANPXT*PSEATT)*WSEAT1 + WSEATT)*    &
     & ANABT))/12.
      GO TO 224
  223 ELTC = 0.0
  224 IF (ANPX1.EQ.0.0) GO TO 225
      EL1C= ((ANPX1*PSEAT1*WSEAT1 + ANPXT*PSEATT*WSEATT                 &
     & + (AUTILS - USAREA)*144.)/((ANPXT*PSEATT/(ANPX1*PSEAT1)*WSEATT   &
     & + WSEAT1)*ANAB1))/12.
      GO TO 226
  225 EL1C = 0.0
  226 ELC = ELTC + EL1C
      ELF = ELP + ELT + ELC
      SWF = RBRB/6.
      DBAR = RBRB/6.
      SF = 2.0*PI*RBRB*ELC/12. + RBRB**2/144.*(10.*ELPD + 8.4*ELTD)
    3 IF (ENGIND.EQ.1.0) GO TO 34
      IF (PDMIND.EQ.2.0.OR.PDMIND.EQ.4.0) GO TO 31
      WGA = 4.0*WG/(3.141593*ENR*(D1**2))
      D = D1
      GO TO 32
   31 D = SQRT(4.0*WG/(3.141593*ENR*WGA))
   32 B = D*(ENR - (0.5*ENR - 1.0)*ZETA1 - ZETA2) + YCL
   34 IF (WDMIND.EQ.0.0) GO TO 5
      IF(WDMIND.EQ.2.) GO TO 22067
      CBARW  = D * CBARD
      SW     = B * CBARW
      AR     = B / CBARW
      WGS = WG / SW
      GO TO 7
22067 SW=WG / WGS
      CBARW = SW /B
      AR    = B / CBARW
      CBARD = CBARW / D
      GO TO 7
    5 SW     = WG / WGS
      B      = SQRT(AR*SW)
      CBARW  = SW / B
      IF(ENGIND.EQ.1.) GO TO 7
      CBARD  = CBARW / D
    7 IF(HTIND.EQ.1.) SHT = VBARH * SW * CBARW / ELTH
      IF(VTIND.EQ.1.) SVT = VBARV * SW * B / ELTV
      CBARHT = SQRT( SHT/ARHT )
      CBARVT = SQRT( SVT/ARVT )
      BHT    = SQRT( SHT*ARHT )
      BVT    = SQRT( SVT*ARVT )
      INDATM = YALE22
      CALL ATMOS (HES,YALE22,TINY)
      IF (FIXIND.EQ.0.) DEBAR = XI4 * ((BHPP/ENP)**.5)
      IF(FIXIND.EQ.0.) GO TO 12
      IF(IST.NE.1) GO TO 12
      IF(DNITFL.EQ.1.) GO TO 10
      IF(ENGIND.NE.0.) GO TO  9
      IF (ETAIND.EQ.2.0) GO TO 81
      IF(ETAIND.NE.0.0) GO TO 91
      S1 = (ENP/(ENP-ENP0))*(SENE**1.5 * WG * WGA**.5 /(550. * DELTA    &
     &    * STHETA * ETAT * ETAP2 * S2RHO))
      GO TO 93
   91 REALJ = 0.0
      TPROP = SENE*WG
      CCT = TPROP*WGA*PI**3/(0.009507*SIGMA*VT**2*WG)
      IF(ETAIND.NE.3.0) GO TO 52
      TOAD=TPROP*WGA/(WG*DELTA)
      HPADTH=XLKUP(REALJ,TOAD,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
      CCP=HPADTH*5634966.0/(VT/STHETA)**3
      GO TO 53
   52 TIPP = AN2TO * A2MAX * VT/(SA * 1.689)
      JCTSG = 0
      JCTSG = JCTSG + 1
      IF(JCTSG.EQ.1.AND.PDMIND.EQ.1.0)                                  &
     & CTSIG=9800.*WG/(RHO*(VT*AN2TO*A2MAX)**2*ENR*PI*D**2*AF*BLDN)
      IF(JCTSG.EQ.1.AND.PDMIND.EQ.2.0)                                  &
     &  CTSIG = 2450.*WGA/(RHO*(VT*AN2TO*A2MAX)**2*AF*BLDN)
      ETAP = XLKUP(CTSIG,TIPP,CTOSIG,NOCTSG,TIPM,NOMTP,FMER,10,6,IX,IY)
! **  NOTE ETAP LOOK-UP DEPENDS ON CT/SIGMA; THE FIRST ITERATION THROUGH
!     SIZTR DOES NOT DEFINE CT/SIGMA YET IF PDMIND = 1,2. THE ABOVE TWO
!     EQUATIONS CALCULATES CT/SIGMA WITH RESPECT TO THE INPUTS AVAILABLE
!     AT THIS TIME IN THE PROGRAM LOOP. THE SECOND ITERATION BYPASSES TH
!     E CALCULATIONS
   53 IF(ETAIND.NE.1.0) ETAP=0.798*CCT**1.5/CCP
      IF(ETAIND.EQ.1.0) CCP = 0.798 * CCT**1.5/ETAP
      IF (IX.NE.0.AND.ETAIND.EQ.3.0) WRITE(6,1003)
 1003 FORMAT(9X,'THIS ERROR IS IN THE  M  PART OF THE FAN POWER',       &
     &' COEFFICIENT TABLE - SUBROUTINE SIZTR')
      IF (IY.NE.0.AND.ETAIND.EQ.3.0) WRITE(6,1004)
 1004 FORMAT(9X,'THIS ERROR IS IN THE  FN PART OF THE FAN POWER',       &
     &' COEFFICIENT TABLE - SUBROUTINE SIZTR')
      IF(IX.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1005)
 1005 FORMAT(9X,'THIS ERROR IS IN THE CT/SIGMA PART OF THE FIGURE',     &
     &' OF MERIT TABLE - SUBROUTINE SIZTR')
      IF(IY.NE.0.AND.ETAIND.EQ.1.0) WRITE(6,1006)
 1006 FORMAT(9X,'THIS ERROR IS IN THE MACH NO. PART OF THE FIGURE',     &
     &' OF MERIT TABLE - SUBROUTINE SIZTR')
      YLS2 = (ENP-ENP0)/ENP
      P = VT
      S1 = 0.009507*CCP*WG*P**3/                                        &
     & (550.*PI**4*WGA*ETAT*STHETA**3*YLS2)
   93 DEBAR = XI4*((S1/ENP)**.5)
      CALL  POWAVL(TMAX,0.0)
  111 S2  = SHPA
      BHPP = S1/S2
      DEBAR = XI4 * ((BHPP/ENP)**.5)
      IF (IRN.EQ.0) GO TO 12
      CALL  POWAVL(TMAX,0.0)
      AAW  = S2 /SHPA
      IF (ABS(1.0-AAW).GT.0.01) GO TO 111
      GO TO 12
   81 ETA = 0.85
      YLS2 = (ENP-ENP0)/ENP
      S3 = (SENE**1.5*WG*WGA**0.5)/(550.0*DELTA*                        &
     & STHETA*ETAT*YLS2*S2RHO)
      TPROP1 = SENE*WG
      ITER = 0
      IF (PDMIND.LE.2.0) SIGS = AF*BLDN/4900.
      IF (PDMIND.GE.3.0) SIGS = 0.5*SENE*WGA/(RHO*VT**2*CTSIG)
   82 ITER = ITER + 1
   86 S1 = S3/ETA
      DEBAR = XI4*(S1/ENP)**0.5
      CALL POWAVL(TMAX,0.0)
   85 S2 = SHPA
      BHPP = S1/S2
      DEBAR = XI4*(BHPP/ENP)**0.5
      CALL THRUST(TPROP,YLS2,ETAP)
      IF (ABS(1.0-TPROP/TPROP1).LE.0.005) GO TO 83
      IF (ITER.GT.25) GO TO 84
      ETA = ETAP
      GO TO 82
   83 IF (IRN.EQ.0) GO TO 12
      CALL POWAVL(TMAX,0.0)
      AAW = S2/SHPA
      IF (ABS(1.0-AAW).LE.0.01) GO TO 12
      ITER = 0
      GO TO 82
    9 CALL  THRAVL(TMAX,0.0)
      TLP = SHPA
      IF(ENGIND.EQ.2.) GO TO 713
      TP     = (ENP/(ENP-ENP0)) * SENE * WG /(DELTA * ETAP2 * TLP)
      GO TO 12
  713 TP     = BETA * ENP/(ENP-ENP0) * SENE**1.5 * WG * WGA**.5         &
     &       /(550. * DELTA * ETAT * ETAP2 * S2RHO * TLP)
      GO TO 12
   10 CALL  THRAVL(TMAX,0.0)
      TLP = SHPA
      CALL  LIFAVL(TMAX)
      TLL = FLIF
      TP     = .25 * WG
      TL=ENL/(ENL-ENL0)*(WG*SENE-TP*DELTA*TLP*ETAP2)/(DELTA*ETAL*TLL)
      TL1=(WG*SENE-(ENP-ENP0)/ENP*TP*DELTA*TLP*ETAP2)/(DELTA*ETAL*TLL)
      IF(TL.LT.TL1) TL=TL1
   12 IF (CKLN.EQ.0.) GO TO 130
      DBARLN = XI1 * SQRT(TL/ENL)
      ELLN   = XI2 + XI3 * SQRT(TL/ENL)
      SLN = 2.*DBARLN*(ENL+EPSLON*ENC)*(ELLN+DBARLN)
      GO TO 13
  130 DBARLN =0.
      ELLN   = 0.
      SLN    = 0.
   13 IF (CKN.EQ.0.)  GO TO 140
      IF(ENGIND.NE.0.) GO TO 14
      DBARN =AZETA1* SQRT(BHPP/ENP)
      ELN   =AZETA2+ AZETA3* SQRT(BHPP/ENP)
      GO TO 15
   14 DBARN =AZETA1* SQRT(TP /ENP)
      ELN   =AZETA2+ AZETA3* SQRT(TP /ENP)
   15 SN     =ENP * 3.1415926 * DBARN * ELN
      GO TO 2057
  140 DBARN  = 0.
      ELN    = 0.
      SN     = 0.
 2057 IF (ENGIND.EQ.1.0) GO TO 23
      IF (PDMIND.EQ.3.0.OR.PDMIND.EQ.4.0) GO TO 42
      TAF = AF*BLDN
      SIGRP = TAF/2450.
      CTSIG = SENE*WGA/(RHO*SIGRP*VT**2)
      IF (FIXIND.EQ.0.0) GO TO 46
      IF (ENGIND.EQ.0.0) CTSIG = CTSIG/(A2STR*A2STR)
      GO TO 46
   42 SIGRP = SENE*WGA/(RHO*VT**2*CTSIG)
      CALL POWAVL(TMAX,0.0)
      IF (ENGIND.EQ.0.0) SIGRP = SIGRP/(A2STR*A2STR)
      TAF = 2450.*SIGRP
   46 SIGS = 0.5*SIGRP
      GO TO 23
   84 WRITE(6,1010)
 1010 FORMAT(22X,81HERROR, THE NUMBER OF ITERATIONS IN THE SIZTR ROUTINE&
     & EXCEEDED 25, CASE TERMINATED)
      NEXT = 1
   23 RETURN
      END
      FUNCTION TABLE (X,XTAB,YTAB,NXPTS,NORD,IER)
!**** MEMBER NAME = B93TABL
!     A00LANG
!     LAGRANGIAN INTERPOLATION FUNCTION
      DIMENSION XTAB(:), YTAB(:)
      IF (NORD.GE.NXPTS) GO TO 20
      L=MOD(NORD,2)
      IER = 0
      IF (XTAB(1).GT.XTAB(2)) GO TO 19
!     TABLE INCREASING
      NNN = NXPTS - 1
      DO  18  I = 1,NNN
          IF ((X.GT.XTAB(I)).AND.( X.LT.XTAB(I+1))) GO TO 17
          IF ( X.EQ.XTAB(I)) GO TO 16
   18     CONTINUE
!     ARGUMENT DOES NOT LIE IN TABLE
!     SET ERROR FLAG(IER) TO 3 AND RETURN THE LAST ENTRY IN THE TABLE AS
!     THE RESULT
   21 CONTINUE
      IER = 3
      TABLE = YTAB(NXPTS)
      IF  (X.LT.XTAB(1))   TABLE = YTAB(1)
      RETURN
!     ORDER GREATER THAN OR EQUAL TO NUMBER OF POINTS SET ERROR FLAG(IER
!      TO 2 AND RETURN 0.0 AS THE RESULT
   20 IER = 2
      TABLE =0.0
      RETURN
!     TABLE DECREASING
   19 NNN = NXPTS - 1
      DO  15  I = 1,NNN
          IF ((X.LT.XTAB(I)) .AND. (X.GT.XTAB(I+1))) GO TO 171
          IF ( X.EQ.XTAB(I)) GO TO 16
   15     CONTINUE
      GO TO 21
!     SET UP LIMITS ON PRODUCT
   17 CONTINUE
  180 J = I - NORD/2
      K = I + NORD/2
      IF (L.NE.0) K=K+1
!     PRODUCT FORMULA
   14 TABLE =0.0
!     TEST IF J AND K ARE BETWEEN 1 AND NXPTS
      IF (J.LE.0) GO TO 10
      IF ( K.GT.NXPTS ) GO TO 9
    8 DO 11 I = J,K
      ANUM =1.0
      ADEN =1.0
      DO 13 M= J,K
          IF ( M.EQ.I) GO TO 13
          ANUM = ANUM * (X-XTAB(M))
          ADEN = ADEN * (XTAB(I)-XTAB(M))
   13     CONTINUE
   11 TABLE = ANUM * YTAB(I) /ADEN + TABLE
      RETURN
   10 J= 1
      K= NORD+1
      GO TO 8
    9 J = J-1
      K= K-1
      IF (K.GT.NXPTS) GO TO 9
      GO TO 14
  171 CONTINUE
  182 J = I - NORD/2
      K = I + NORD/2
      IF ( L.NE.0 ) J= J+1
      GO TO 14
!     ARGUMENT IN TABLE
   16 TABLE = YTAB(I)
      RETURN
      END
      SUBROUTINE THRAVL(TPS,AM)
!**** MEMBER NAME = B93TTAVL
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2432  +  SPACE(18)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(10) ,  FMER(10,6),&
     &SPAC17(18)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
!
      NLIM = 0
      TEA  =  TPS/THETA
      IF (IWD) 4,4,2
    2 WREF = XLKUP(0.0,TMAX,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
 1001 FORMAT(9X,67HTHIS ERROR IS IN THE  M  PART OF THE REFERED FUEL FLO&
     &W THRUST TABLE)
 1002 FORMAT(9X,67HTHIS ERROR IS IN THE  T  PART OF THE REFERED FUEL FLO&
     &W THRUST TABLE)
      IF  (IX.NE.0) WRITE(6,1001)
      IF  (IY.NE.0) WRITE(6,1002)
      WSHP = WMAX * WREF / (DELTA * STHETA)
      WTEMP = XLKUP(AM,TEA,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
      IF  (IX.NE.0) WRITE(6,1001)
      IF  (IY.NE.0) WRITE(6,1002)
      IF (WSHP.GE.WTEMP) GO TO 4
      TEWD = XIBIV(AM,WSHP,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IZ)
 1003 FORMAT(9X,60HTHIS ERROR IS IN THE  W  PART OF THE REFERED FUEL FLO&
     &W TABLE)
      IF (IX.NE.0) WRITE(6,1001)
      IF (IZ.NE.0) WRITE(6,1003)
      IF (TEA-TEWD) 4,4,3
    3 TEA = TEWD
      NLIM = 1
    4 IF (IN1)  7,7,5
    5 A1REF = A1MAX / STHETA
      A1TEMP = XLKUP(AM,TEA,AM1,NM1,TN1,NT1,AONE,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IY.NE.0) WRITE(6,1010)
 1010 FORMAT(9X,57HTHIS ERROR IS IN THE  T  PART OF THE N SUB 1 THRUST T&
     &ABLE)
      IF (A1REF.GE.A1TEMP) GO TO 7
      TEN1 = XIBIV(AM,A1REF,AM1,NM1,TN1,NT1,AONE,6,8,IX,IZ)
 1004 FORMAT(9X,57HTHIS ERROR IS IN THE  M  PART OF THE N SUB 1 THRUST T&
     &ABLE)
 1005 FORMAT(9X,57HTHIS ERROR IS IN THE  N1 PART OF THE N SUB 1 THRUST T&
     &ABLE)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IZ.NE.0) WRITE(6,1005)
      IF (TEA-TEN1) 7,7,6
    6 TEA = TEN1
      NLIM = 2
    7 IF (IN3) 75,75,20
   20 A1REF = A3MAX*SQRT(1.0 + 0.2*AM**2.)
      A1TEMP = XLKUP(AM,TEA,AM1,NM1,TN1,NT1,AONE,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IY.NE.0) WRITE(6,1010)
      IF (A1REF.GE.A1TEMP) GO TO 75
      TEN1 = XIBIV(AM,A1REF,AM1,NM1,TN1,NT1,AONE,6,8,IX,IZ)
      IF (IX.NE.0) WRITE(6,1004)
      IF (IZ.NE.0) WRITE(6,1005)
      IF (TEA.LE.TEN1) GO TO 75
      TEA = TEN1
      NLIM = 6
   75 IF(IN2) 10,10,8
    8 A2REF = A2MAX / STHETA
      A2TEMP = XLKUP(AM,TEA,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IZ)
      IF (IX.NE.0) WRITE(6,1006)
      IF (IY.NE.0) WRITE(6,1011)
 1011 FORMAT(9X,57HTHIS ERROR IS IN THE  T  PART OF THE N SUB 2 THRUST T&
     &ABLE)
      IF (A2REF.GE.A2TEMP) GO TO 10
      TEN2 = XIBIV(AM,A2REF,AM2,NM2,TN2,NT2,ATWO,6,8,IX,IZ)
 1006 FORMAT(9X,57HTHIS ERROR IS IN THE  M  PART OF THE N SUB 2 THRUST T&
     &ABLE)
 1007 FORMAT(9X,57HTHIS ERROR IS IN THE  N2 PART OF THE N SUB 2 THRUST T&
     &ABLE)
      IF (IX.NE.0) WRITE(6,1006)
      IF (IZ.NE.0) WRITE(6,1007)
      IF (TEA -TEN2)  10,10,9
    9 TEA = TEN2
      NLIM  = 3
   10 SHPA = XLKUP(AM,TEA,AMSHP,NMS,TSHP,NTS,SHPAV,6,8,IX,IY)
 1008 FORMAT(9X,57HTHIS ERROR IS IN THE  M  PART OF THE REFERED THRUST T&
     &ABLE)
 1009 FORMAT(9X,57HTHIS ERROR IS IN THE  T  PART OF THE REFERED THRUST T&
     &ABLE)
      IF  (IX.NE.0) WRITE(6,1008)
      IF  (IY.NE.0) WRITE(6,1009)
      WSHPA = XLKUP(AM,TEA,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
      IF (IX.NE.0) WRITE(6,1001)
      IF (IY.NE.0) WRITE(6,1002)
      IF (WSHPA.LT.0.0)  WSHPA = 0.0
      WSHPA = WSHPA * CKFF
      TPEA  = TEA  * THETA
      NPLIM = NLIM
      RETURN
      END
      SUBROUTINE   TAXI (ITAXI)
!**** MEMBER NAME = B93TTAXI
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! ** VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***        PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      EQUIVALENCE(DATA(1),OPTIND)
      DIMENSION DATA(1)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  EO(7)
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
 9000 FORMAT(/7X10HTAXI  FOR ,F6.3,2X33HHRS. AT GROUND IDLE ENGINE RATIN&
     &G,10X,13HTEMPERATURE =,F6.1,6H DEG.F)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,            &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X4HLETF,                                       &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF)
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,3XF5.3)
      AN2MAX  = AN2M1(ITAXI)
      V   = 0.0
      HH = H
      INDATM = ATMIND(ITAXI)
      CALL ATMOS(HH,ATMIND(ITAXI),TIN(ITAXI))
      TEMP = THETA * 518.69  -  459.69
      IF(OPTIND.NE.2.) GO TO 395
      ST11 = ST
      WF11 =WF
      W11  = W
      IF(DNITFL.EQ.0.) LETF = 0.
      WRITE(6,9000)  DELTT(ITAXI),TEMP
      WRITE(6,9001)
  395 IF(DNITFL.EQ.1.) GO TO 3
      FL = 0.
      IF(ENGIND.EQ.0.) GO TO 2
    4 CALL  THRAVL(TGI,0.0)
      TKE  = TPEA
      KE   = NPLIM + 1
      TLP  = SHPA
      FP   = WSHPA * DELTA * STHETA * TP
      CALL  THRAVL(TMAX,0.0)
      PEHF = TLP / SHPA
    1 FT   = FP + FL * SKFL(ITAXI)
      WFT = FT * DELTT(ITAXI)
      W   = W - WFT
      WF  = WF +WFT
      ST  =  ST + DELTT(ITAXI)
      IF (OPTIND.NE.2.) GO TO 396
      WRITE(6,9002) ST11,R,WF11,W11,H,V,TKE,EO(KE),PEHF,LETF
      WRITE(6,9002) ST,R,WF,W,H,V,TKE,EO(KE),PEHF,LETF
  396 CONTINUE
      RETURN
    2 CALL  POWAVL(TGI,0.0)
      TKE  = TPEA
      KE   = NPLIM + 1
      BLP  =  SHPA
      FP   =  WSHPA * DELTA * STHETA * BHPP
      CALL  POWAVL(TMAX,0.0)
      PEHF =  BLP / SHPA
      GO TO 1
    3 CALL  LIFAVL(TLGI)
      TLL  = FLIF
      WDOTFL = FWA
      CALL  LIFAVL(TLMAX)
      LETF  = SKFL(ITAXI) * TLL / FLIF
      FL  = WDOTFL * DELTA * STHETA * TL
      GO TO 4
      END
      SUBROUTINE THRUST(TPROP,YLS2,ETAP)
!**** MEMBER NAME = B93TTHRT
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
      DIMENSION A0(3,15),A1(3,15),A2(3,15),BB0(16),BB1(16),BB2(16),     &
     & RJ3(10),CPOW3(10),ETAI3(10,10),RJ4(10),CPOW4(10),ETAI4(10,10),   &
     &AMACH(3),CLLL(15),CLGAM(16),CPOW33(20),CTI3(20),CTI4(20),         &
     & GMDD1(16),GAMD11(3,15)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10) , DELVP(10), VMAXP(10), &
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      COMMON/ODD/A0,A1,A2,BB0,BB1,BB2,RJ3,CPOW3,ETAI3,RJ4,CPOW4,        &
     & ETAI4,AMACH,CLLL,CLGAM,CPOW33,CTI3,CTI4,GMDD1,GAMD11
      NOXPJ = XPJNO + 0.1
      NOCPP = CPPNO + 0.1
      NOCTSG = CTSGNO + 0.01
      NOMTP  = TPMNO + 0.01
      PI = 3.14159
      P = A2STR*VT
      REALJ = 1.689*PI*V/P
      CCP = 550.*(PI**4)*(STHETA**3)*BHPP*YLS2*SHPA*ETAT*WGA/           &
     & (0.009507*WG  *P**3)
      ITER = 0
      IF (ETAIND.EQ.2.0) GO TO 7
      IF(ETAIND.NE.3.0)GO TO 9
      HPADTH = BHPP*YLS2*SHPA*ETAT*WGA/WG
      TOAD = XIBIV(EM,HPADTH,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
      CCT = PI**3*TOAD*THETA/(0.009507*P**2)
      GO TO 33
    7 IF (REALJ.EQ.0.0) GO TO 19
      EMH75 = EM*SQRT((0.75*PI/REALJ)**2 + 1.0)
      IF (EMH75.LT.0.70) EMH75 = 0.70
      GAMD2 = 1.0
      TANP = 4.0*REALJ/(3.0*PI)
      IF (BLDN.LE.3.0) ETAI = BIV(REALJ,CCP,RJ3,CPOW3,ETAI3,10,10,10,10)
      IF (BLDN.GE.4.0) ETAI = BIV(REALJ,CCP,RJ4,CPOW4,ETAI4,10,10,10,10)
      PHIT = ATAN(TANP/ETAI)
      SINP = SIN(PHIT)
      CLIK = 4.0*CCP/(SIGS*SINP*(COS(PHIT)+SINP*TANP)**2*(0.75*PI)**4)
      GAMD3 = ATAN(TANP*(CLIK/1.2-1.0)/ETAI)*RTOD
      IF (GAMD3.GT.1.0) GAMD2 = GAMD3
!!!   CL1 = CLIK/(1. + TAN(GAMD2*DTOR)*COTAN(PHIT))
      CL1 = CLIK/(1. + TAN(GAMD2*DTOR)/TAN(PHIT))
      GAMD1 = BIV(EMH75,CL1,AMACH,CLLL,GAMD11,3,15,3,15)
      DELGM1 = GAMD1 - GAMD2
!!!   CL2 = CLIK/(1.0 + TAN(GAMD1*DTOR)*COTAN(PHIT))
      CL2 = CLIK/(1.0 + TAN(GAMD1*DTOR)/TAN(PHIT))
   23 IF (CL2.LT.0.1) GO TO 52
      GAMD1 = BIV(EMH75,CL2,AMACH,CLLL,GAMD11,3,15,3,15)
      GAMD2 = ATAN(TAN(PHIT)*(CLIK/CL2 -1.0))*RTOD
      DELGM2 = GAMD1 - GAMD2
      IF (ABS(DELGM2).LE.0.02) GO TO 3
      ITER = ITER + 1
      IF (ITER.GT.20) GO TO 16
      CL3 = (CL1*DELGM2 - CL2*DELGM1)/(DELGM2 - DELGM1)
      CL1 = CL2
      CL2 = CL3
      DELGM1 = DELGM2
      GO TO 23
   52 DLGM10 = BIV(EMH75,0.1,AMACH,CLLL,GAMD11,3,15,3,15)-ATAN(TAN(PHIT)&
     &*(10.*CLIK - 1.0))*RTOD
      IF (DLGM10.GT.0.0) GO TO 50
      DLGM1 = DLGM10
      DO 30 I = 4,15
      CCL = CLLL(I)
      DLGM2 = BIV(EMH75,CCL,AMACH,CLLL,GAMD11,3,15,3,15)-ATAN(TAN(PHIT)*&
     &(CLIK/CCL-1.0))*RTOD
      IF (DLGM2.GT.0.0) GO TO 31
   30 END DO
      WRITE(6,7415)
 7415 FORMAT(9X,'PROGRAM HAD DIFFICULTY CONVERGING IN THE CL/GAMMA CALCU&
     &LATION , A CL OF 1.2 HAS ASSIGNED SUBROUTINE THRUST')
      CCCL = 1.2
      GO TO 32
   31 CCCL = CLLL(I)-(CLLL(I)-CLLL(I-1))*DLGM2/(DLGM2-DLGM1)
   32 GAMD1 = ATAN(TAN(PHIT)*(CLIK/CCCL-1.0))*RTOD
      GO TO 3
   50 DLGM5 = BIV(EMH75,0.05,AMACH,CLLL,GAMD11,3,15,3,15)-ATAN(TAN(PHIT)&
     &*(20.*CLIK-1.0))*RTOD
      IF (DLGM5.GT.0.0) GO TO 51
      DLGM2 = DLGM10
      I = 3
      DLGM1 = DLGM5
      GO TO 31
   51 GAMD5 = BIV(EMH75,0.05,AMACH,CLLL,GAMD11,3,15,3,15)
      GAMD10 = BIV(EMH75,0.1,AMACH,CLLL,GAMD11,3,15,3,15)
      DGD1CL = 40.*GAMD5 - (10.*GAMD10 + 2700.)
!!!   DGD2CL = -COTAN(PHIT)/CLIK
      DGD2CL = -1.0/(TAN(PHIT)*CLIK)
      IF (DGD1CL.LT.DGD2CL) GO TO 53
      ETAP = 0.5*ETAP
      CCT = ETAP*CCP/REALJ
      GO TO 21
   53 DDGDCL = DGD1CL - DGD2CL
      CCCL = -DDGDCL/(400.*DLGM5 - 20.*DDGDCL)
      GO TO 32
   19 IF (BLDN.LE.3.0) CTIND = XLINT(CPOW33,CTI3,CCP,20,M)
      IF (BLDN.GE.4.0) CTIND = XLINT(CPOW33,CTI4,CCP,20,M)
      IF (M.NE.0) WRITE(6,45)
   45 FORMAT(9X,52HTHIS ERROR IS IN THE CT/CP TABLE - SUBROUTINE THRUST)
      PHIT = ATAN(CCP/(0.75*PI*CTIND))
      SINP = SIN(PHIT)
      GAMD2 = 1.0
      CLIK = 4.0*CCP/((0.75*PI)**4*SIGS*SINP*COS(PHIT)**2)
      GAMD3 = ATAN(TAN(PHIT)*(CLIK/2.0-1.))*RTOD
      IF (GAMD3.GT.1.0) GAMD2 = GAMD3
!!!   CL1 = CLIK/(1. + TAN(GAMD2*DTOR)*COTAN(PHIT))
      CL1 = CLIK/(1. + TAN(GAMD2*DTOR)/TAN(PHIT))
      GAMD1 = TABLE(CL1,CLGAM,GMDD1,16,2,M)
      IF (M.NE.0) WRITE(6,47)
   47 FORMAT(9X,83HTHIS ERROR IS IN THE PROPELLER EQUIVALENT LIFT/DRAG P&
     &OLAR TABLE - SUBROUTINE THRUST    )
      DELGM1 = GAMD1 - GAMD2
!!!   CL2 = CLIK/(1.0 + TAN(GAMD1*DTOR)*COTAN(PHIT))
      CL2 = CLIK/(1.0 + TAN(GAMD1*DTOR)/TAN(PHIT))
   14 GAMD1 = TABLE(CL2,CLGAM,GMDD1,16,2,M)
      IF (M.NE.0) WRITE(6,47)
      GAMD2 = ATAN(TAN(PHIT)*(CLIK/CL2 - 1.0))*RTOD
      DELGM2 = GAMD1 - GAMD2
      IF (ABS(DELGM2).LE.0.02) GO TO 12
      ITER = ITER   + 1
      IF (ITER.GT.20) GO TO 16
      CL3 = (CL1*DELGM2 - CL2*DELGM1)/(DELGM2 - DELGM1)
      CL1 = CL2
      CL2 = CL3
      DELGM1 = DELGM2
      GO TO 14
!!!12 CCT = CCP*(COTAN(GAMD1*DTOR + PHIT)/(0.75*PI))
   12 CCT = (CCP/(0.75*PI))/TAN(GAMD1*DTOR + PHIT)
      ETAP = 0.798*CCT**1.5/CCP
      GO TO 21
    3 ETAP = TANP/TAN(GAMD1*DTOR + PHIT)
      CCT = ETAP*CCP/REALJ
      GO TO 21
    9 IF(REALJ.EQ.0.0.AND.ETAIND.EQ.1.0) GO TO 843
      CCT = XIBIV(REALJ,CCP,XPJ,NOXPJ,CPPROP,NOCPP,CTPROP,20,20,IX,IY)
      GO TO 33
  843 TIPP = P/(SA * 1.689)
      ETAP = XLKUP(CTSIG,TIPP,CTOSIG,NOCTSG,TIPM,NOMTP,FMER,10,6,IX,IY)
      CCT = ((CCP * ETAP)/0.798)**0.666667
      IF(IX.NE.0) WRITE(6,1005)
      IF(IY.NE.0) WRITE(6,1007)
      GO TO 127
   33 IF(IX.NE.0) WRITE(6,1001)
 1001 FORMAT(9X,70HTHIS ERROR IS IN THE  J  PART OF THE PROPELLER POWER &
     &COEFFICIENT TABLE)
      IF(IY.NE.0) WRITE(6,1003)
 1003 FORMAT(9X,70HTHIS ERROR IS IN THE  CP PART OF THE PROPELLER POWER &
     &COEFFICIENT TABLE)
 1005 FORMAT(9X,'THIS ERROR IS IN THE CT/SIGMA PART OF THE FIGURE',     &
     &' OF MERIT TABLE - SUBROUTINE THRUST')
 1007 FORMAT(9X,'THIS ERROR IS IN THE MACH NO. PART OF THE FIGURE',     &
     &' OF MERIT TABLE - SUBROUTINE THRUST')
  127 IF(REALJ.NE.0.0)  ETAP = REALJ*CCT/CCP
      IF(REALJ.EQ.0.0.AND.ETAIND.NE.1.0) ETAP = 0.798*CCT**1.5/CCP
   21 TPROP = 0.009507*SIGMA*WG*(P**2)*CCT/((PI**3)*WGA)
      GO TO 36
   16 WRITE(6,43)
   43 FORMAT(10X,65HGAMMA FAILED TO CONVERGE IN TWENTY ITERATIONS - SUBR&
     &OUTINE THRUST/)
   36 RETURN
      END
      SUBROUTINE TOHL (ITOHL)
!**** MEMBER NAME = B93TOHL
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      DIMENSION  EO(7)
      DATA  EO(1)/4H T  / ,EO(2)/4H W  / ,EO(3)/4H N1 /
      DATA  EO(4)/4H N2 / ,EO(5)/4H Q  / ,EO(6)/4H P  / ,EO(7)/4H C  /
      NAMELIST/NTOHL/LC1,STMAX,INDATM,H,DNITFL,ENGIND,BLP,BHPSUP,BHPPY,T&
     &SUBP,TPY,YALE22,TLP,TSFC,FP,FL,SFC,TLL,TSUBL,LETF,WDOTFL,FL,F,DLTT&
     &H,W,WF,ST,PEHF,PETF,IFUDGE
 8999 FORMAT(/7X,'TAKEOFF, HOVER, OR LAND AT PETF = ',F5.3,2X,'LETF = ',&
     &F5.3,' FOR ',F6.3,' HRS.',/7X,'VERTICAL RATE OF CLIMB =',F7.1,1X, &
     &'FT/MIN',12X,'TEMPERATURE =',F6.1,1X,'DEG.F')
 9000 FORMAT(/7X,'TAKEOFF, HOVER, OR LAND AT  T/W = ',F5.3,1X,'FOR',1X, &
     &F6.3,'.HRS.',/7X,'VERTICAL RATE OF CLIMB =',F7.1,1X,'FT/MIN',12X, &
     &'TEMPERATURE =',F6.1,1X,'DEG.F')
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,12X18HTURB.  ENG.   PETF,12X6HTHRUST &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,5X3HTAS,5X5HTEMP&
     &.,2X4HCODE,4X2HOR,5X4HLETF,6X2HTO,5X3H FM,9H     BHP ,8H   CT   , &
     & 7H  VTIP                                                         &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT),5X5H(KTS),3X  &
     &3H(R),11X4HPEHF,12X6HWEIGHT,25X5H(FPS))
 9002 FORMAT(7XF6.3,3XF8.2,3X,F7.1,3XF8.0,3XF6.0,3XF6.1,2XF6.1,2X,A4,2X &
     &F5.3,3XF5.3,4XF6.3,2XF5.3,1XF7.0,1XF6.4,2XF5.0)

      IFUDGE = ITOHL
      PI = 3.14159
      INDATM = ATMIND(ITOHL + 10)
      HH = H
      CALL ATMOS(HH,ATMIND(ITOHL + 10),TIN(ITOHL + 10))
      TEMP = THETA * 518.69  -  459.69
      IF (ETAIND.EQ.0.0) ETAP = ETAP2
      IF (OPTIND.NE.2.0) GO TO 27
      IF (TOLIND(ITOHL).EQ.3.0) GO TO 37
      WRITE(6,9000) ENT(ITOHL),STH(ITOHL),VRCTO(ITOHL),TEMP
      GO TO 38
   37 WRITE(6,8999) PFET2(ITOHL),FLET2(ITOHL),STH(ITOHL),VRCTO(ITOHL),  &
     &TEMP
   38 WRITE(6,9001)
      IF (ETAIND.EQ.1.0.AND.XPJ(1).EQ.0.0)                              &
     & WRITE(6,5226)
 5226 FORMAT(/2X'CAUTION, PROPELLER POWER TABLE INCLUDES ADVANCE RATIO',&
     &' EQUAL TO ZERO',/2X,'FOR THE STATIC THRUST CONDITION, FM IS',    &
     &' INPUT AS A FUNCTION OF CT/SIGMA AND MTIP')
      IF(ETAIND.GE.2.0.AND.XPJ(1).GT.0.0) WRITE(6,5227)
 5227 FORMAT(/2X,'CAUTION, STATIC THRUST NOT INCLUDED IN PROP DECK,',   &
     &' LOWEST ADVANCE RATIO USED')
   27 AN2MAX = AN2M2(ITOHL)
      ANT = ENT(ITOHL)
      LC1=0
      V   =  0.0
      EM=0.0
      STMAX = ST + STH(ITOHL)
      INDTOL  =  TOLIND(ITOHL)
      DSHPRC = W * VRCTO(ITOHL)/(33000. * CKRC)
      VMN = VRCTO(ITOHL)/(101.34 * SA)
! **  DSHPRC = DELTA POWER NECESSARY TO CLIMB VERTICALLY AT VRCTO
! **  VMN = VERTICAL MACH NO. FOR T/FAN ENGINES
  201 IF (INDTOL.NE.1) GO TO 300
! **  NOTE IF TOLIND=3 FUEL FLOW IS INDEPENDENT OF VERTICAL CLIMB RATE
      IF(DNITFL.EQ.1.) GO TO 19
      IF(ENGIND.NE.0.) GO TO 14
      CALL  POWAVL(TMAX,0.0)
      BLP  = SHPA
      TKE = TPEA
      KE  = NPLIM +1
      BHPSUP = BLP * BHPP * DELTA * STHETA
   13 IF (ETAIND.NE.0.0) GO TO 29
      BHPPS =ENT(ITOHL)**1.5*WG*SQRT(WGA) /(550.*ETAT*ETAP2*S2RHO)      &
     &    + DSHPAC + DSHPRC
      IF (BHPSUP.GE.BHPPS) GO TO 11
      GO TO 28
   29 TPROP = ENT(ITOHL)*W
      IF (ENGIND.EQ.2.0) BHPP = TP/BETA
      CALL POWER(TPROP,1.0,ETAP)
      BHPR = BHPP*SHPR*DELTA*STHETA + DSHPRC
! **  DSHPAC IS ACCOUNTED FOR IN THE POWER SUBROUTINE, BUT DELTA POWER
!     NECESSARY TO CLIMB AT VRCTO IS DIRECTLY ADDED TO POWER REQUIRED
      IF (BHPSUP.GE.BHPR) GO TO 11
   28 IF (OPTIND.EQ.2.0) WRITE(6,232)
 5224 FORMAT(//2X5HERROR100(1H*)//)
  232 FORMAT(7X,37HCAUTION **  PEHF   IS GREATER THAN 1.)
   11 BHPPY= BHPSUP
      IF(ENGIND.EQ.0.) GO TO 10
      TPY = BETA*BHPPY
      BHPSUP = BHPPS
      IF (ETAIND.EQ.0.0) GO TO 31
      TSUBP = BETA*BHPR
      GO TO 9916
   31 TSUBP = BETA*BHPPS
 9916 YALE22= TSUBP /(DELTA * TP)
      TLP = YALE22
      SHPR  = YALE22
      CALL  THRREQ(VMN)
      FP  = WSHPR * DELTA * STHETA * TP
      TKE  = TPEA
      KE   = 6
      FL  = 0.
      GO TO 202
   10 IF (ETAIND.EQ.0.0) GO TO 30
      BHPSUP = BHPR
      GO TO 32
   30 BHPSUP = BHPPS
   32 YALE22 = BHPSUP/(BHPP*DELTA*STHETA)
! **  DSHPAC IS ACCOUNTED FOR (ALONG WITH DSHPRC) IN BHPSUP
      BLP = YALE22
      SHPR  = YALE22
      CALL  POWREQ(0.0)
      FP  = WSHPR * DELTA * STHETA * BHPP
      TKE  = TPEA
      KE   = 6
      LETF  = 0.0
      FL  = 0.
      GO TO 202
   14 CALL  THRAVL(TMAX,VMN)
      TLP  =  SHPA
      TKE = TPEA
      KE  = NPLIM +1
      TSUBP = TLP * DELTA * TP
      IF(ENGIND.EQ.1.) GO TO 9
      BHPSUP= TSUBP / BETA
      GO TO 13
    9 IF(TSUBP.LT.(ENT(ITOHL) * W /ETAP2)) GO TO 16
  161 TPY = TSUBP
      TSUBP =      ENT(ITOHL) * W /ETAP2
      GO TO 9916
   19 CALL  LIFAVL(TLMAX)
      TLL  = FLIF
      TSUBL = TLL * DELTA * TL
      IF(TSUBL.LT.(ENT(ITOHL) * W / ETAL)) GO TO 17
      LETF= ENT(ITOHL) * W /(TSUBL * ETAL)
      TSUBL =      ENT(ITOHL) * W / ETAL
      TSUBP = 0.
      YALE22= TSUBL / (DELTA * TL)
      TLL = YALE22
      FP  = 0.
      TKE = TFI
      KE  = 1
      FREQ  = YALE22
      CALL  LIFREQ
      WDOTFL = FWR
      FL  = WDOTFL * DELTA * STHETA * TL
      GO TO 202
   17 LETF = 1.
      CALL  THRAVL(TMAX,VMN)
! **  NOTE IT IS ASSUMED THAT THE DELTA THRUST NECESSARY TO CLIMB
!     AT VRCTO IS COMING FROM THE PRIM ENGINES (WITH LETF EQUAL TO 1.0)
      TLP  = SHPA
      TKE = TPEA
      KE  = NPLIM +1
      TSUBP = TLP * DELTA * TP
      YALE22 =(ENT(ITOHL)*W - TSUBL*ETAL) / ETAP2
      IF(TSUBP.LT.YALE22) GO TO 16
  162 TPY = TSUBP
      TSUBP= YALE22
      TLP  = TSUBP /(TP * DELTA)
      SHPR  = TLP
      CALL  THRREQ(VMN)
! **  VMN IS USED TO OBTAIN THE CORRECT FUEL FLOW CORRESPONDING TO THE
!     DELTA THRUST REQUIRED TO CLIMB VERTICALLY AT VRCTO
      TKE  =  TPEA
      KE   =  6
      FP   =  WSHPR * DELTA * STHETA * TP
      FREQ =  TLL
      CALL  LIFREQ
      WDOTFL = FWR
      FL = WDOTFL * TL * DELTA * STHETA
      GO TO 202
   16 IF (OPTIND.EQ.2.)  WRITE(6,233)
  233 FORMAT(7X,37HCAUTION **  PETF   IS GREATER THAN 1.)
      IF(DNITFL.EQ.1.)  GO TO 162
      GO TO 161
  202 F  = FP + FL
      IF(OPTIND.EQ.2.) GO TO 203
  204 CONTINUE
      IF(LC1.EQ.1) RETURN
      IF((ST + DELTH(ITOHL)).GE.STMAX) GO TO 205
      DLTTH =DELTH(ITOHL)
      GO TO 206
  205 LC1=1
      DLTTH = STMAX - ST
  206 W  = W - F * DLTTH
      WF = WF+ F * DLTTH
      ST = ST + DLTTH
      GO TO 201
  203 IF(ENGIND.EQ.0.) GO TO 207
      IF(TSUBP.EQ.0.) PEHF =0.
      IF (TSUBP.EQ.0.0.AND.ENGIND.EQ.1.0) GO TO 2044
      PEHF = TSUBP / TPY
      IF (ENGIND.EQ.2.0) GO TO 253
      GO TO 2044
  207 PEHF = BHPSUP/ BHPPY
  253 IF (ENGIND.EQ.2.0.AND.IN2.EQ.2) A2STR = A2MAX*AN2MAX
      P = A2STR*VT
      CCT = ENT(ITOHL)*W*PI**3*WGA/(0.009507*SIGMA*WG*P**2)
 2044 WRITE(6,9002) ST,R,WF,W,H,V,TKE,EO(KE),PEHF,LETF,                 &
     & ANT,ETAP,BHPSUP,CCT,P
      GO TO 204
  300 PEHF =PFET2(ITOHL)
      IF (INDTOL.EQ.3) GO TO 350
      IF(.NOT.(DNITFL.EQ.0..OR.ENGIND.NE.1.))GO TO 310
      WRITE(6,5224)
      WRITE(6,301)
  301 FORMAT(10X,80HTAKEOFF AND LANDING OPTION NO. 2 IS NOT PERMITTED IF&
     & ENGIND = 0,2  OR LFTIND = 0)
      NEXT = 1
      RETURN
  310 CALL LIFAVL(TLMAX)
      CALL THRAVL(TMAX,VMN)
! **  NOTE THAT EQUAL POWER (THRUST/WEIGHT) IS COMING FROM LIFT AND PRIM
!     ENGINES. THE ADDITIONAL THRUST REQUIRED TO CLIMB AT VRCTO IS
!     COMING FROM THE PRIMARY ENGINES
      TKE = TPEA
      KE  = NPLIM +1
      TA = (SHPA* TP * ETAP2 + FLIF* TL * ETAL)* DELTA
      IF (TA.LT.(ENT(ITOHL)*W)) WRITE(6,233)
      PEHF = ENT(ITOHL)*W /TA
      SHPR = PEHF * SHPA
      FREQ = PEHF * FLIF
      LETF = PEHF
      CALL  THRREQ(VMN)
! **  VMN IS SPECIFIED TO YIELD THE CORRECT FUEL FLOW ASSOCIATED WITH
!     THE DELTA THRUST NECESSARY TO CLIMB AT VRCTO
      TKE  = TPEA
      KE  = 6
      FP  = WSHPR * DELTA * STHETA * TP
      CALL  LIFREQ
      FL  = FWR * DELTA * STHETA * TL
      F = FP +FL
      IF(OPTIND.EQ.2.)WRITE(6,9002)ST,R,WF,W,H,V,TKE,EO(KE),PEHF,LETF,  &
     &   ANT,ETAP
      GO TO 204
  350 IF(DNITFL.EQ.1.) GO TO 352
      LETF = 0.0
      ANL = 0.0
      FL = 0.0
      ETAL = 0.0
      IF (ENGIND.EQ.0.0)  GO TO 360
      GO TO 354
  352 LETF  =FLET2(ITOHL)
      CALL  LIFAVL(TLMAX)
      FREQ  = LETF * FLIF
      CALL  LIFREQ
      FL  = FWR * DELTA*STHETA * TL
      ANL  = FREQ * TL * DELTA * ETAL / W
  354 CALL  THRAVL(TMAX,0.0)
      TKE = TPEA
      KE  = NPLIM +1
      SHPR = PEHF * SHPA
      CALL  THRREQ(0.0)
      TKE  = TPEA
      KE  = 6
      FP   = WSHPR * DELTA * STHETA * TP
      IF (ENGIND.EQ.2.0) GO TO 370
      ANT        = SHPR * TP * DELTA * ETAP2 /W  + ANL
      GO TO 365
  360 CALL  POWAVL(TMAX,0.0)
! **  SINCE SPECIFIED POWER FRACTION IS WARRANTED, VMN WAS NOT USED IN
!     CALLING POWAVL OR POWREQ(FOR FUEL FLOW). HOWEVER, THE POWER THAT
!     IS PRINTED OUT IS THE CORRECT POWER ACCOUNTING FOR ACCESORY POWER
!     AND DELTA POWER FOR CLIMB AT VRCTO. THE FUEL FLOW PRINTED DOES
!     NOT INCLUDE EITHER OF THE ADDITIONAL POWERS REQUIRED, BUT THE FUEL
!     FLOW IS STILL AN ACCURATE REPRESENTATION OF THE OVERALL TOHL LEG
      TKE = TPEA
      KE  = NPLIM +1
      SHPR = PEHF * SHPA
      CALL  POWREQ(0.0)
      TKE  = TPEA
      FP   = WSHPR * DELTA * STHETA * BHPP
  363 IF (ETAIND.EQ.0.0) GO TO 364
      IF (ENGIND.EQ.2.0) GO TO 370
      SHPA = SHPR
      GO TO 371
  370 ANT = ((550.*SHPR*DELTA*TP*S2RHO*ETAP2*ETAT)                      &
     &  /(SQRT(WGA/WG)*BETA))**0.6667/W
      F = FP
      A2STR = 1.0
      IF (IN2.EQ.2) A2STR = A2MAX*AN2MAX
      P = A2STR*VT
      GO TO 40001
  371 CALL THRUST(TPROP,1.0,ETAP)
      ANT = TPROP/W
      GO TO 365
  364 ANT = ((37.92*ETAT*ETAP2*SHPR*BHPP)**.6666)*((WG/WGA)             &
     & **.3333)* DELTA / W
  365 F  = FP + FL
      P = A2STR*VT
40001 CCT = ANT*W*PI**3*WGA/(0.009507*SIGMA*WG*P**2)
      CCP = 0.798*CCT**1.5/ETAP
      BHPR = 1.77562E-07*CCP*SIGMA*WG*P**3/(WGA*ETAT) + DSHPAC + DSHPRC
! **  THE ABOVE POWER REFLECTS THE ADDITIONAL POWER NECESSARY FOR
!     ACCESSORY OPTIONS AND FOR VERTICAL RATE OF CLIMB.  THE FUEL FLOW
!     IS BASED ON THE ACTUAL OPERATING ENGINE POWER CORRESPONDING TO
!     PEHF X BHP* (MAX.S.L. POWER)
  366 IF(OPTIND.EQ.2.)WRITE(6,9002)ST,R,WF,W,H,V,TKE,EO(KE),PEHF,LETF,  &
     & ANT,ETAP,BHPR,CCT,P
      IF(LC1.EQ.1) RETURN
      IF((ST + DELTH(ITOHL)).GE.STMAX) GO TO 368
      DLTTH = DELTH(ITOHL)
      GO TO 369
  368 LC1 = 1
      DLTTH = STMAX - ST
  369 W  = W - F * DLTTH
      WF = WF+ F * DLTTH
      ST = ST + DLTTH
      GO TO 350
      END
      SUBROUTINE THRREQ(AM)
!**** MEMBER NAME = B93TTREQ
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      TEA = XIBIV(AM,SHPR,AMSHP,NMS,TSHP,NTS,SHPAV,6,8,IX,IZ)
 1001 FORMAT(9X,57HTHIS ERROR IS IN THE  M  PART OF THE REFERED THRUST T&
     &ABLE)
 1002 FORMAT(9X,62HTHIS ERROR IS IN THE  THRUST  PART OF THE REFERED THR&
     &UST TABLE)
      IF  (IX.NE.0)  WRITE(6,1001)
      IF  (IZ.NE.0)  WRITE(6,1002)
      WSHPR = XLKUP(AM,TEA,AMWD,NMW,TWD,NTW,WDOT,6,8,IX,IY)
 1003 FORMAT(9X,67HTHIS ERROR IS IN THE  M  PART OF THE REFERED FUEL FLO&
     &W THRUST TABLE)
 1004 FORMAT(9X,67HTHIS ERROR IS IN THE  T  PART OF THE REFERED FUEL FLO&
     &W THRUST TABLE)
      IF (IX.NE.0) WRITE(6,1003)
      IF (IY.NE.0) WRITE(6,1004)
      TPEA  = TEA  * THETA
      IF (WSHPR.LT.0.0)  WSHPR  =0.0
      WSHPR = WSHPR * CKFF
      RETURN
      END
      SUBROUTINE  TRALT(ITALT)
!**** MEMBER NAME = B93TTRLT
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***          PAGE NO. 24, LOC.2201 TO LOC. 2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***         PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
 9000 FORMAT(/7X,21HTRANSFER ALTITUDE TO ,F8.0,4H FT.)
 9001 FORMAT(/28X4HFUEL,17X5HPRES.,                                     &
     &/8X4HTIME,5X5HRANGE,6X4HUSED,6X6HWEIGHT,5X,4HALT.,                &
     &/8X5H(HRS),4X6H(N.M.),5X5H(LBS),5X6H(LBS.),5X4H(FT)               &
     &)
 9003 FORMAT(1H+,42X,'OPTIMUM FOR NEXT CRUISE')
 9002 FORMAT( 7XF6.3,3XF8.2,3X,F7.1,3XF8.0,2X,F8.0)
      INOPTG = 0
      WFTOTO = 0.0
      INDEX4 = INDEX3
      HOO = H
      HO = HFIN(ITALT)
      ICHEKH = 0
      IBOTOM = 0
      IF (OPTIND.NE.2.) GO TO 2
      IF (INOPTH.EQ.1) INOPTG = 1
   10 IF (OPTIND.NE.2.0.OR.INOPTH.EQ.1) GO TO 2
      WRITE(6,9000) HO
      IF (INOPTG.EQ.1) WRITE(6,9003)
      WRITE(6,9001)
      WRITE(6,9002) ST,R,WF,W,HOO
      WRITE(6,9002) ST,R,WF,W,HO
    2 IF (INOPTH.NE.1) GO TO 3
      EN = 0.0
      H = -1000.
    8 H = H + 1000.
      ENO = EN
      RO = R
      WFO = WF
      WO = W
      STO = ST
      H1 = H
      IF (IVLMT.EQ.1.AND.H.GT.10000.) ICHEKH = ICHEKH + 1
      IF (CRSIND(INDEX4) - 2.0) 4,5,6
    4 CALL CRUS1(INDEX4)
      GO TO 7
    5 CALL CRUS2(INDEX4)
      GO TO 7
    6 CALL CRUS3(INDEX4)
    7 R = RO
      WF = WFO
      W = WO
      ST = STO
      H = H1
      WFTOTO = 1.0
      IF (IBOTOM.EQ.1.AND.EN.GT.EN10K) IBOTOM = 0
      IF (NEXT.EQ.1) GO TO 9
      IF ((H+1000.).GT.HFIN(ITALT)) GO TO 12
      IF (ENO.EQ.0.0) GO TO 8
      IF (EN.GE.ENO) GO TO 8
    9 IF (ICHEKH.EQ.0) GO TO 14
      IF (ICHEKH.GT.1.AND.IBOTOM.EQ.0) GO TO 14
      IF (ICHEKH.NE.1) GO TO 15
      IBOTOM = 1
      EN10K = ENO
      H11K = H
      GO TO 8
   15 IF (EN10K.GE.ENO) H = H11K
   14 IF (H.GE.1000.)  H = H - 1000.
      IBOTOM = 0
   12 IF (IBOTOM.EQ.1) GO TO 15
      NEXT = 0
      INOPTH = 0
      HO = H
      GO TO 10
    3 H = HO
      RETURN
      END
      SUBROUTINE WGHTR
!**** MEMBER NAME = B93TWHTR
      REAL     LETF
      DIMENSION WWW(10),RRR(10),STSTST(10)
      DIMENSION STORWF(11), STRWFL(11)
! ****    INPUT  COMMON
!
!
! ****         PAGE NO. 1 , LOC 1  TO  LOC 76 , +SPACE(24)
!
      COMMON     OPTIND    ,TNIRPK    ,DRGIND    ,OSWIND    ,PDMIND    ,&
     &FDMIND    ,WDMIND    ,HTIND     ,VTIND     ,FIXIND    ,ENGIND    ,&
     &ESZIND    ,DNITFL    ,WG00      ,H00       ,R00       ,ST00      ,&
     &HOPTIN    ,VLMIND    ,EMM0      ,VM0       ,VDIV      ,EMLF      ,&
     &CK1       ,DELWF     ,CKFF             ,SGTIND(50)    ,SPACE1(24)
!
!
! ****         PAGE NO. 2 , LOC 101  TO  LOC  141 , +SPACE(9)
!
      COMMON     DAM2      ,DAM3      ,EYEW      ,TCR       ,TCT       ,&
     &DAM4      ,DLMC4     ,SLM       ,ARHT      ,SAH       ,ELTH      ,&
     &TCHT      ,VBARH     ,SLMH      ,AAW11     ,SR        ,YCL       ,&
     &ZETA1    ,ZETA2      ,DLSWSW    ,HF        ,DAM5      ,ELPD      ,&
     &ELTD      ,ELC       ,ELRW      ,DAM6      ,SWF       ,ARVT      ,&
     &ELTV      ,TCVT      ,VBARV     ,SLMV      ,AAW12     ,YMG       ,&
     &YP        ,YL        ,EPSLON    ,AZETA1    ,AZETA2    ,AZETA3    ,&
     &SKIP1(9)
!
!
! ****         PAGE NO. 3 , LOC 151  TO  LOC  166 , +SPACE(33)
!
      COMMON     DNILGN    ,AGLLEY    ,ANPX1     ,ANAB1     ,ANISL1    ,&
     &WSEAT1    ,PSEAT1    ,WAISL1    ,DNIVAL    ,ANLAVS    ,ANPXT     ,&
     &ANABT     ,ANISLT    ,WSEATT    ,PSEATT    ,WAISLT    ,SPACE2(33)
!
!
! ***          PAGE NOS. 4 THRU 7 , LOC 200  TO  LOC 262 , + SPACE(38)
!
      COMMON     ETAIND    ,CYCPRP    ,DAM7      ,DAM8      ,ENP       ,&
     &BETA  ,ETAT  ,HES  ,SENE  ,TINY  ,AN2TO  ,ENP0  ,ENL0            ,&
     &POWESI    ,HC        ,VC        ,ATMIY     ,AN2CR     ,CYCLFP    ,&
     &DAM9  ,ENL  ,ENC     ,SKP2      ,ENR       ,VT        ,WGA       ,&
     &D1        ,CTSIG     ,AF        ,BLDN      ,CLEYE     ,ETAL      ,&
     &ETAP2     ,ETAP3     ,ETAP5     ,TBEM5(10) ,ETAP4N    ,TB8AP4(10),&
     &CYPROP    ,XMSND     ,XMSMRT    , DSHPAC   ,SHPTO     ,VRCRC     ,&
     &CKRC      ,SPACE3(38)
!
!
! ***          PAGE NO. 8 , LOC 301  TO  LOC  384 , +SPACE(15)
!
      COMMON     CDVTI     ,CDHTI     ,CDNI      ,CDLNI    ,DELCD      ,&
     &DAM10     ,DELFE     ,TCLN      ,TENN      ,TCL2N    ,CKLN       ,&
     &CKW       ,CKN       ,CKF       ,CKVT      ,CKHT     ,TBCL1(8)   ,&
     &TBEM(5)   ,RELI      ,CSLALF    ,ALPHL0    ,XCPS     ,XCTCM      ,&
     &TBCDWI(8) ,TBCL2(7)  ,TBCDM(7,5)  , SPACE4(15)
!
!
! ***          PAGE NO. 9 , LOC 400  TO  LOC  475 , +SPACE(25)
!
      COMMON   OWE1    ,WFE    ,WFUL    ,WPL    ,SKCC    ,SKFW         ,&
     &SKH       ,SKSAS     ,SKTM      ,SKUC      ,CK15     ,CK16       ,&
     &CK17      ,CK18      ,CK19      ,CK20      ,THN      ,DELWF2     ,&
     &DELWP     ,DELWST    ,SKP       ,SKLES     ,SKLG     ,SKMG       ,&
     &SKTL      ,SKWF      ,SKWW      ,SKY       ,SKZ      ,SKPES      ,&
     &SKMT      ,SKNAC     ,SKLMT
      COMMON     CK8       ,CK9       ,CK10      ,CK11     ,CK12       ,&
     &CK13      ,CK14      ,TBH(10)   ,DELP      ,WC       ,YC         ,&
     &SKDS      ,SKFS      ,SKLEI     ,SKPEI     ,SKRP     ,SKVT       ,&
     &CK2       ,CK3       ,CK4       ,CK5       ,CK6      ,CK7        ,&
     &CK21      ,TBTHE(10) ,SPACE5(25)
!
!
! ***          PAGE NO. 10, LOC 501  TO  LOC  550 , +SPACE(50)
!
      COMMON     ATMIN1(10),DELTT(10) ,TIN1(10)  ,SKFL(10)  ,AN2M1(10) ,&
     &SPACE6(50)
!
!
! ***          PAGE NO. 11, LOC 601  TO  LOC  690
!
      COMMON     TOLIND(10),ATMIN2(10),PFET2(10) ,TIN2(10)  ,FLET2(10) ,&
     &ENT(10)    ,DELTH(10) , AN2M2(10) ,STH(10)
!
!
! ***          PAGE NO. 12, LOC 691  TO  LOC 800
!
      COMMON     CLMIND(10),EMACH(10),ATMIN3(10),DELH3(10),TIN3(10),    &
     &HMAX(10) ,POWCLI(10),THEMAX(10),AN2M3(10),DCLIMB(10),ENCLMB(10)
!
!
! ***          PAGE NO. 13  LOC 801  TO  LOC 900
!
      COMMON     CRSIND(10),VIN(10)   ,ATMIN4(10),DELR(10)  ,TIN4(10)  ,&
     &RMAX(10)  ,POWCRI(10),ENPSD(10) ,AN2M4(10) ,DLCDCR(10)
!
!
! ***          PAGE NO. 14  LOC 901  TO  LOC 1000
!
      COMMON     DESIND(10),EAS5(10)  ,ATMIN5(10),THEMIN(10),TIN5(10)  ,&
     &DELH5(10) ,RMAX5(10) ,HMIN(10)  ,AN2M5(10) ,DLCDDS(10)
!
!
! ***          PAGE NO. 15  LOC 1001  TO  LOC  1080,  +SPACE(20)
!
      COMMON    DNIRTL(10),DELST(10),ATMIN6(10),STL(10) , TIN6(10),     &
     &ENPSDL(10),AN2M6(10),DLOITR(10),RSW(10),SPAC11(10)
!
!
! ***          PAGE NO. 16  LOC 1101  TO  LOC  1151 , +SPACE(49)
!
      COMMON     DLTAWF(10),HFIN(10)  ,STFW(10)  ,DELWPL(10),STPW(10)  ,&
     &WGTIND    ,SPAC12(49)
!
!
! ***          PAGE NO. 17  LOC 1201  TO  LOC  1257 , +SPACE(43)
!
      COMMON   WDTIND   ,AN1IND   ,AN3IND   ,AN2IND   ,QIND   ,RNOIND  ,&
     &PRN(10)  ,VWDIND  ,VN1IND  ,VN2IND  ,WMAX  ,A1MAX  ,A3MAX        ,&
     &A2MAX     ,QMAX      ,RNE(10)   ,WLMAX     ,A1LMAX    ,AL2MAX    ,&
     &A2NO(10)  ,PN2(10)   ,SPAC13(43)
!
!
! ***          PAGE NOS. 18 + 19 , LOC 1301 TO LOC 1565 ,+SPACE(35)
!
      COMMON     CYCPRL    ,SK3       ,SK4       ,XI4      ,TGI        ,&
     &TFI       ,TNRP      ,TMIL      ,TMAX      ,UNTS     ,TSHP(8)    ,&
     &UMS       ,AMSHP(6)  ,SHPAV(6,8),UNTW      ,TWD(8)   ,UMW        ,&
     &AMWD(6)   ,WDOT(6,8) ,UNT1      ,TN1(8)    ,UNM1     ,AM1(6)     ,&
     &AONE(6,8) ,UNT2      ,TN2(8)    ,UNM2      ,AM2(6)   ,ATWO(6,8)  ,&
     &SPAC14(35)
!
!
! ***          PAGE NOS. 20 + 21 , LOC 1601 TO LOC 1672 , + SPACE(27)
!
      COMMON     CYCLFL    ,SK1       ,SK2       ,XI1      ,XI2        ,&
     &XI3       ,TLGI      ,TLMAX     ,TF(8)     ,FAVL(8)  ,TFW(8)     ,&
     &FWDOT(8)  ,TF1(8)    ,FONE(8)   ,TF2(8)    ,FTWO(8),   SPAC15(27)
!
!
! ***          PAGE NOS. 22 + 23 , LOC 1700  TO LOC 2142 + SAVE(58)
!
      COMMON    PROPCY,XPJNO,XPJ(20),CPPNO,CPPROP(20)  ,CTPROP(20,20),  &
     &SAVE(58)
!
!
! ***           PAGE NO. 24, LOC.2201 TO LOC.2330 + SPACE(20)
!
      COMMON     GWIND(10), GWP(10) , ATMIN7(10) , CLWP(10) , TIN7(10), &
     &DELFEP(10),AHOP(10) , TOWP(10), AN2M7(10)  , DELVP(10), VMAXP(10),&
     &AN2M8(10) ,VRCTO(10) ,SPAC16(20)
!
! **  VRCTO = VERTICAL RATE OF CLIMB FOR TOHL
!
! ***        PAGE NO.25  , LOC 2351 TO 2428  +  SPACE(22)
!
      COMMON     CTSGNO  ,  CTOSIG(10),  TPMNO , TIPM(6)  ,  FMER(10,6),&
     &SPAC17(22)
!
!
! ****         COMMON    MAIN
!
      COMMON     ATMIND(60),TIN(60)   ,DELH(20)  ,EMPTY1    ,WG        ,&
     &R         ,ST        ,H        ,OPTION
      COMMON     AR        ,CBARD     ,WGS       ,ELF       ,SF        ,&
     &BHPP      ,TP        ,TL        ,SEE
      COMMON     IC2D      ,IRMAX     ,RC        ,OPTWAS,PETF,PEHF,LETF
      COMMON     NEXT      ,DTOR      ,RTOD
      COMMON     NTH       ,NETAP4    ,INDDRG    ,NTCL      ,NTCL2
      COMMON     NTEM      ,INDLFT    ,INDENG    ,INDESZ
      COMMON    IVLMT   ,INOPTH   ,KPRINT   ,WFTOTO
!
!            ENGINE  ROUTINES                ENGINE ROUTINES
      COMMON     SHPA      ,WSHPA     ,SHPR      ,WSHPR     ,AN2MAX    ,&
     &A2STR     ,TPEA      ,NPLIM     ,FLIF      ,FWA       ,FREQ      ,&
     &FWR       ,TLEA      ,NLLIM
      COMMON     IWD       ,IN1       ,IN2       ,IQ        ,IRN       ,&
     &NMS       ,NTS       ,NMW       ,NTW       ,NM1       ,NT1       ,&
     &NM2      ,NT2       ,LWD      ,LN1       ,LN2        ,IN3
!
!                   AERO                      AERO
      COMMON     B         ,TC        ,RLMC4     ,DLMPS     ,DLMTCX
      COMMON     SA1       ,SA2       ,RLMLE     ,FK        ,SA3
      COMMON     SA4       ,REF       ,FFRE                 ,SA5
      COMMON     SA6       ,SA7       ,CLALPH    ,REW       ,FWRE
      COMMON     FNRE      ,REN       ,FVTRE     ,REVT      ,FHTRE
      COMMON     REHT      ,FLNRE     ,RELN      ,DLMELE
      COMMON     FEW       , FEF       ,FEVT      ,FEHT      ,FEN
      COMMON     FELN      ,DLTAFE     ,FE       ,CBARF     ,SWET
!
!                   ATMOS                     ATMOS
      COMMON     THETA     ,DELTA     ,SIGMA     ,STHETA    ,SA
      COMMON     RHO       ,S2RHO
!
!                   CHGFW                     CHGFW
      COMMON     STORWF    ,W         ,WFL       ,STRWFL
!
!                   CHGPL     NOTHING NEW IS CALCULATED
!
!                   CLIMB                     CLIMB
      COMMON     DELTAH    ,V2        ,VMAX      ,RGAM1     ,RGAM3
      COMMON     EM        ,Q         ,CL        ,PHI       ,BLP
      COMMON     BHPA      ,RCPOW     ,TLP       ,TA        ,RGAM2
      COMMON     RCTETA    ,RGAM4     ,RC2       ,RC1       ,V
      COMMON     THETAF    ,GAMMA     ,SFC       ,F         ,TSFC
      COMMON    V1      ,INDEX3      ,IOPTH
!     CRUS1, 2, AND 3                        CRUS1 ,2 AND 3
      COMMON     BHPR      ,B1        ,DELV      ,DELR1     ,RRR
      COMMON     STSTST    ,WWW       ,B2        ,TR        ,T1
      COMMON     T2        ,ETAP4     ,EN        ,EN1       ,EN2
!
!                   DRAG                      DRAG
      COMMON     EMD       ,DELCDM    ,CDWI      ,CD
!
!                   DSCNT                     DSCNT
      COMMON     HSTAR     ,RSTAR     ,WSTAR     ,WFSTAR    ,GAM1
      COMMON     GAM2      ,RS        ,BHPCRU    ,BHPIDL    ,BHPTHE
      COMMON     TCRUS     ,TIDLE     ,TTHETA               ,BHP
      COMMON     T
!
!                   ENGSZ                     ENGSZ
      COMMON     DELTAY    ,TLL       ,TLPPR     ,BHPP2     ,BHPXMS
      COMMON     BHPP1     ,SIGRP     ,TP1       ,TP2       ,TPXMSN
      COMMON     P         ,ANXMAX
!
!                   LOITR                     LOITR
      COMMON     W0        ,ST0       ,DELP1     ,VMIN      ,F2
      COMMON     F1        ,WF        ,WFS       ,WFR
!
!                   SIZTR                     SIZTR
      COMMON     DBAR      ,ELP       ,DEBAR     ,ELT
      COMMON     D         ,CBARW     ,SW        ,SHT
      COMMON     SVT       ,CBARHT     ,CBARVT   ,BHT       ,BVT
      COMMON     DBARLN    ,ELLN      ,SLN       ,DBARN     ,ELN
      COMMON    NLAVS   ,AGALLY   ,ACL      ,DBDB   ,DCDC   ,PREC1     ,&
     &NGLIND   ,LAVIND  ,SN       ,SIGS
!
!                   TAXI                      TAXI
      COMMON     FP        ,FT        ,FL        ,WFT       ,WDOTFL
!
!                   TOHL                      TOHL
      COMMON     BHPSUP    ,TSUBP     ,WFH
!
!                   WGHTR                     WGHTR
      COMMON     WEL       ,WEP       ,WRP       ,WDS       ,WPEI
      COMMON     WLEI      ,WP        ,WLG       ,WMG       ,WPES
      COMMON     WLES      ,FH        ,FV        ,WHT       ,WVT
      COMMON     GLF       ,ULF       ,CGAMMA    ,WFW       ,WR
      COMMON     RF        ,WW1       ,WM        ,Z         ,WW
      COMMON     WX        ,WB        ,WST       ,WCC       ,WUC
      COMMON     WH        ,WSAS      ,WTM       ,WFC       ,WFA   ,WFSS
      COMMON     WE        ,OWE       ,WCFW      ,WPSTAR
      COMMON     WFWFWF(10),VVV(10)   ,ENENEN(10),EASEAS(10),EMEMEM(10),&
     &EMDEMD(10),PEHFPE(10)
      DIMENSION DATA(1)
      EQUIVALENCE (DATA(1),OPTIND)
      EQUIVALENCE (ALPHL0,ALPHLO)
      LC1  = 0
      WFW  = 0.0
      LFTIND = DNITFL
      IF(LFTIND.EQ.0) GO TO 2
      WEL  = SK1 * TL  + SK2 * ENL
      WEP  = SK3 * TP  + SK4 * ENP
    1 WRP  = 0.
      WDS  = 0.
    9 WPEI = SKPEI * WEP
      WLEI = SKLEI * WEL
      WPSTAR= CK2 *WRP +CK3 *WDS +CK4 *WEL +CK5*WEP +CK6*WLEI +CK7*WPEI &
     &     + DELWP
      GO TO 4
    2 WEL    = 0.
      IF(INDENG.EQ.0) GO TO 3
      WEP    = SK3 * TP + SK4 * ENP
      IF(INDENG.EQ.1) GO TO 1
      BHPXMS = TPXMSN/BETA
    8 PDSXMS = A2MAX*ANXMAX*VT
! **  ANXMAX FOR XMSN SIZING WITH FIXIND=0.0 IS SET IN MAIN
      PRP = A2MAX*AN2TO*VT
! ***     ANXMAX IS CALCULATED IN ENGSZ
! ***     PRP USES MAXIMUM TIP SPEED; PRESUMED TO BE TAKEOFF
      WRP    = SKRP*((SR*D/2.)**.25*(BHPP*1.1/(100.*ENR))**.5*.011*PRP  &
     &       *(SIGRP*3.1415926*D**2/40.))**.67*ENR
      WDS    = SKDS  *(D * BHPXMS /(SKVT * PDSXMS))**.8
      WDS = WDS / 9.8105
      GO TO 9
    3 WEP    = SK3 * BHPP + SK4 * ENP
      GO TO 8
    4 SIGMA = (VM0 /(661.7 * EMM0))**1.61949
      IF (SIGMA.GE.0.53281) GO TO 20
      SIGMA  = 0.53281
      EM  = VM0 / 448.56
      V9  = VM0
      GO TO 25
   20 V9  = VM0
      IF (SIGMA.LE.1.0) GO TO 22
      V9  = 661.7 * EMM0
      SIGMA = 1.0
   22 EM  = EMM0
   25 D19 = 3.141593 * AR /(1.0 +SQRT((3.141593*AR /CSLALF)**2  *       &
     &    (SAVE(26) -EM**2) + 1.0))
      GLF  = 1.0  + 0.549 * V9 / (1.26*SIGMA*CBARW +6.216*WG/(SW*D19))
      ULF = GLF * 1.5
      IF(GLF.LE.EMLF) ULF = EMLF *1.5
      WPES = SKPES* WEP
      IF (SKPES.NE.0.0) GO TO 28
      WPES = 1.02 *( (WRP*(1.0+SKUC)+WDS+WEP*(1.0+SKPEI)+DELWP)/1000.*  &
     &    (2.0+SKLMT*SLN)*ULF*SKMT+0.5*ALOG10(VDIV)*SN*SKNAC +(DBARN**2 &
     &    * ENP) )
   28 WLG  =  SKLG * WG
      WMG  = SKMG * WLG
      WLES = SKLES* WEL
      FH   = WG * SKY *ELF *BHT *SKTL *(1.+2.*SLMH)/(1000000.*(1.+SLMH))
      FV   = WG * SKZ*(ELF+B)*BVT     *(1.+2.*SLMV)/(1000000.*(1.+SLMV))
      FV  = FV / 2.
      WHT  = 350.*(SHT*FH*ALOG10(VDIV)*(1.+SLMH)/(200.*ELTH*TCHT*CBARHT)&
     &    )**.54
      WVT  = 360.*((FV+SAH*FH/2.)*SVT*ALOG10(VDIV)*(1.+SLMV)/(200.*     &
     &    ELTV * TCVT * CBARVT))**.54
      CGAMMA = (SW/1000.)**1.25 * (1.+ULF*WG/(200.*SW))*(.01*AR/TCR+.3) &
     &       * (1.+ABS(SIN(RLMC4)))**.54 *(1.+.2*SLM)*(TCR/TCT)**.1
      WR  = 2.*(YP*(WEP*(1.+SKPEI)+WPES) +WEL*YL*(1.+SKLEI+SKLES) +     &
     &    WMG*YMG + WC*YC)
   99 RF  = (WR + .76 *WFW) / WG
      IF(RF.GT..4) RF = .4
      WW1    = 3830. * CGAMMA *(1.-RF)*WG /(WG + 3064.*CGAMMA)
      WM = (WEP*(1.+SKPEI)+WPES)*YP/(YP+.001) + WEL*(1.+SKLEI+SKLES)*YL &
     &    /(YL+.001) + WMG*YMG/(YMG+.001) +WC+WDS+WRP+DELWP+SKUC*WRP
      Z  = (2.*SLM + 1.)/(3.*(SLM+1.)) - SWF/B
   10 FOO=1.-WR/(2.*(WG-WW1-WFW)*Z)
      IF(FOO.LT..6)FOO=.6
      WW= SKWW*(FOO*(WG-WW1-WM-WFW)/10000.*SW/100.                      &
     &    *ALOG10(B/SWF)*SQRT((1.+SLM)/(2.*TCR))*SQRT(ULF)              &
     &    *ALOG10(VDIV)    * ALOG10(AR))**.585
      IF((ABS(WW1-WW)/WW).LE..01) GO TO 11
      WW1 = WW
      GO TO 10
   11 WX  = WG - WW - WFW - WM
      WB  = SKP * ((WX/10000.)**.7 * (SF/1000.) * SWF * (ELF + ELRW)**.5&
     &    *ALOG10(VDIV  )    * (DELP + 1.)**.2 * ULF**.3)**.508
      WST = CK8*WW + CK9*WHT + CK10*WVT + CK11*WB + CK12*WLG + CK13*WLES&
     &    + CK14*WPES + DELWST
      WCC = SKCC  * (WG / 1000.) ** .41
      WUC = SKUC  * (WRP)
      WH  = SKH   * (WRP/100.) ** .84
      WCFW= SKFW * WG
      WSAS= SKSAS
      WTM = SKTM  *  WG
      WFC = CK15*WCC + CK16*WUC +CK17*WH + CK18*WCFW + CK19*WSAS        &
     &     + CK20*WTM + DELWF2
      WFA = (WG-WPSTAR-WST-WFC-WFE-WFUL-WPL)/(1.+ SKFS*CK21)
      WFSS= SKFS * WFA
      WTEST = WFW
      WFW   = SKWF *(SW**1.5)
      IF (WFA.LT.WFW) WFW = WFA
      IF (ABS(WTEST-WFW).LE.10.) GO TO 60
      IF (LC1-1) 40,42,44
   40 W1  = WTEST
      A1  = WFW
      LC1 = 1
      GO  TO 99
   44 W1  = W2
      A1  = A2
   42 W2  = WTEST
      A2  = WFW
      WFW =(A2*W1 - A1*W2)/(W1-A1-W2+A2)
      LC1 = 2
      GO TO 99
   60 WP= WPSTAR + CK21*WFSS
      WRP   = CK2 * WRP
      WDS   = CK3 * WDS
      WEL   = CK4 * WEL
      WEP   = CK5 * WEP
      WLEI  = CK6 * WLEI
      WPEI  = CK7 * WPEI
      WFSS  = CK21* WFSS
      WW    = CK8 * WW
      WHT   = CK9 * WHT
      WVT   = CK10* WVT
      WB    = CK11* WB
      WLG   = CK12* WLG
      WLES  = CK13* WLES
      WPES  = CK14* WPES
      WCC   = CK15* WCC
      WUC   = CK16* WUC
      WH    = CK17* WH
      WCFW  = CK18* WCFW
      WSAS  = CK19* WSAS
      WTM   = CK20* WTM
      RETURN
      END
