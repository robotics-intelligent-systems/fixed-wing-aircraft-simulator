INCLUDE 'rogers.f90'
!+
PROGRAM Sample1
! ------------------------------------------------------------------------------
! PURPOSE - Reproduce one of the curves in Jones & Cohen showing the
!    variation of drag coefficient with Mach number for a delta wing.

! REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
!  6Jan98  1.0   RLC   Original coding

! REFERENCES -
!   1. Rogers, Arthur W. "Wave Drag of Arrow Wings with Modified
!      Double Wedge Section. Hughes Technical Note No. 568, 1 July 1958.
!   2. Jones, Robert T., and Cohen, Doris: "High Speed Wing Theory".
!      Princeton Aeronautical Paperbacks. Princeton University Press, 1960.

! NOTES - This example reproduces the curve for sweep=60 and b=0.2 and
!    a=0 (no trailing edge sweep) from Fig. A,15d on p.224 of Jones & Cohen.


  USE Rogers
  IMPLICIT NONE

  REAL,PARAMETER:: PI=3.14159265

  REAL:: cd
  REAL:: f1=0.2, f2=0.2
  INTEGER:: k
  REAL:: mach
  REAL:: tc = 0.05
  REAL:: tanLE,tanTE=0.0
!-------------------------------------------------------------------------------

  tanLE=TAN(60*PI/180)
  DO k=11,45
    mach=0.1*REAL(k)   ! Mach from 1.1 to 4.5
    cd=DragCoeff(mach, tc, tanLE,tanTE, f1,f2)
    WRITE(*, '(F6.1,F8.5)' ) mach,cd
  END DO
  STOP

END Program Sample1   ! =====================================================
