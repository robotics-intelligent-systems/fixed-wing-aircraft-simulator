

ROGERS - WAVE DRAG OF ARROW WINGS WITH MODIFIED               /rogers/readme.txt
   DOUBLE-WEDGE SECTION
                      

The files for this program are in the directory rogers 
  readme.txt      this file - general description
  rogers.f90      the source code of the subroutine package
  input.txt       how to use the code in rogers.f90
  eq.pdf          2 pages of equations from the original report

  sample1.f90     a sample program to check the subroutine
  sample1.out     expected results from running sample1
  fg.pdf          charts of the F and G functions 
  report.pdf      remake of charts from the original report 

The reference documents for this program may be accessed
from the web page https://www.pdas.com/rogersrefs.html. 


In many exercises in optimization problems involving supersonic vehicles, 
it is useful to have an expression for wave drag of a wing with straight 
leading and trailing edges,zero tip chord, and a simple airfoil section.
While this solution is not extremely difficult, it is perhaps beyond what 
can be expected of a student or preliminary design team looking for a 
quick answer.

Arthur Rogers wrote a report in 1958 based on a 1947 article in JAS by 
Allen E. Puckett and H.J. Stewart that gives a closed form solution for an arrow 
wing with a three-slope (hexagonal) section. This was based on the idea of
superimposing conical-flow solutions developed by Doris Cohen at NASA Ames. 
Rogers generalized the 1947 results to include a non-zero trailing edge sweep
angle and produced a number of charts that have been widely used as well as a 
clear description of the computational process. The equations are not difficult 
to program, but some care must be taken because there are several fractions 
that must be evaluated at points where the numerator and denominator are zero. 
The limiting forms must be solved for and the programming must take care to avoid
division by zero and use the limits instead.

The equations were coded at NASA Ames Research Center back in the 1960s and much
of the original history of the authors of the code has been lost. I rescued the 
original code and have updated it to modern Fortran in hopes that students and 
designers will not be daunted by the F and G functions in Rogers' report and 
use this for some interesting studies. There are many interesting variations 
associated with relocating the region of maximum thickness.

The report by Puckett and Stewart also includes solutions for the lift and
pitching moment of these wings. The lift equations have been added to rogers.f90
and the moment expressions may be added later.

The routines sample1.f90 and rogers.f90 have been checked and will compile
properly with either of the free Fortran compilers, gfortran or g95. To test
the program sample1, use the command
   gfortran sample1.f90 -o sample1.exe
The module rogers.f90 will be included automatically.

