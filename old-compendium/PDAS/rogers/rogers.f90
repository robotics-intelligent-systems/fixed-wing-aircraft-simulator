!+
MODULE Rogers
! ------------------------------------------------------------------------------
! PURPOSE-Compute the F-function and G-function from p.3 of reference 1.
!  Compute the factor in the drag coefficient equation that lets you compute 
!  drag coefficient. Compute lift-curve slope of arrow wing.
!
!
! AUTHOR - Ralph L. Carmichael, Public Domain Aeronautical Software

! REFERENCES -
!   1. Rogers, Arthur W. "Wave Drag of Arrow Wings with Modified
!      Double Wedge Section. Hughes Technical Note No. 568, 1 July 1958.
!   2. Puckett,A.E. and Stewart,H.J.: Aerodynamic Performance of Delta
!      Wings at Supersonic Speeds. Journal of the Aeronautical Sciences,
!      vol.14, no. 10, October 1947, pp.567-578.
!   3. Puckett,Allen E.: Supersonic Wave Drag of Thin Airfoils. Journal
!      of the Aeronautical Sciences, vol.13, no.9, September 1946,
!      pp.475-484.

! NOTE - To get drag coefficient, multiply return from DragCoeff by the
!   term 2*(tau**2)/(beta*PI)


! REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
!  3Feb83  0.5   RLC   Original coding
! 15Jun87  0.9   RLC   IMPLICIT NONE
!  2Oct97  1.0   RLC   Fortran90 module
! 26Dec97  1.1   RLC   Single and Double Precision versions
! 27Mar02  1.2   RLC   Added lift coefficient calculation
! 18Dec08  1.3   RLC   Made functions PURE

IMPLICIT NONE
  CHARACTER(LEN=*),PARAMETER,PUBLIC:: ROGERS_VERSION=" 1.3 (18Dec08)"
  INTEGER,PARAMETER,PRIVATE:: SP=KIND(1.0), DP=KIND(1.0D0)
  REAL(DP),PRIVATE,PARAMETER:: ZERO=0.0, ONE=1.0, TWO=2.0, FOUR=4.0, EIGHT=8.0
  REAL(DP),PRIVATE,PARAMETER:: PI=3.141592653589793238462643D0
  REAL(DP),PARAMETER:: HALFPI=PI/TWO
  REAL(DP),PRIVATE,PARAMETER:: EPS=0.00001

  PRIVATE:: ELLC2
  INTERFACE F
    MODULE PROCEDURE FSingle,FDouble
  END INTERFACE

  INTERFACE G
    MODULE PROCEDURE GSingle,GDouble
  END INTERFACE

  INTERFACE RogersCoeff
    MODULE PROCEDURE RogersCoeffSingle,RogersCoeffDouble
  END INTERFACE

  INTERFACE DragCoeff
    MODULE PROCEDURE DragCoeffSingle,DragCoeffDouble
  END INTERFACE

  INTERFACE LiftCoeff
    MODULE PROCEDURE LiftCoeffSingle,LiftCoeffDouble
  END INTERFACE


! NOTE - A user can simply code
!           x=F(m,s)
!        in the source code. If x,m, and s are all single precision,
!        then FSingle will be used. If x,m, and s are all double
!        precision, then FDouble will be used. 

CONTAINS

!+
PURE FUNCTION ELLC2(x) RESULT(e)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the complete elliptic integral of the second kind
! REF: Abramowitz and Stegun, Eq XXX
  REAL(DP),INTENT(IN):: x
  
  REAL(DP):: e
  REAL(DP),PARAMETER:: A1=0.44325141463D0, A2=0.06260601220D0
  REAL(DP),PARAMETER:: A3=0.04757383546D0, A4=0.01736506451D0
  REAL(DP),PARAMETER:: B1=0.24998368310D0, B2=0.09200180037D0
  REAL(DP),PARAMETER:: B3=0.04069697526D0, B4=0.00526449639D0
  REAL(DP),PARAMETER:: ZERO=1.0_DP, ONE=1.0_DP, TEENY=1E-25
  REAL(DP),PARAMETER,DIMENSION(5):: A = (/ &
    1.0D0, 0.44325141463D0, 0.06260601220D0, 0.04757383546D0, 0.01736506451D0 /)
  REAL(DP),PARAMETER,DIMENSION(5):: B = (/ &
    0.0D0, 0.24998368310D0, 0.09200180037D0, 0.04069697526D0, 0.00526449639D0 /)

  REAL(DP):: m1
!-------------------------------------------------------------------------------
  m1=ONE-x 
  IF (m1 < TEENY) THEN
    e=ONE
  ELSE
    e=ONE+m1*(A1+m1*(A2+m1*(A3+m1*A4))) 
    e=e  +m1*(B1+m1*(B2+m1*(B3+m1*B4)))*LOG(ONE/m1)
  END IF
  RETURN
END Function ELLC2   ! ---------------------------------------------------------

!+
PURE FUNCTION EvaluatePolynomial(x,a) RESULT(f)
! ------------------------------------------------------------------------------
! PURPOSE -Evaluate a polynomial with coefficients a(1),a(2),... at x
! NOTES - f=a(1)+a(2)*x+a(3)*x**2+....+a(n)*x**(n-1)
!   Straightforward implementation of Horner's rule
  REAL(DP),INTENT(IN):: x   ! evaluation point
  REAL(DP),INTENT(IN),DIMENSION(:):: a   ! coefficients
  REAL(DP):: f   ! value of polynomial at x

  INTEGER:: k,n
!-------------------------------------------------------------------------------
  n=SIZE(a)   ! order of polynomial is n-1, not n
  IF (n <= 0) THEN
    f=0.0
    RETURN
  END IF

  f=a(n)
  DO k=n-1,1,-1
    f=a(k)+x*f
  END DO

  RETURN
END Function EvaluatePolynomial   ! --------------------------------------------


!+
PURE FUNCTION FDouble(m,s) RESULT(t)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the F-function from the reference publication
  REAL(DP),INTENT(IN):: m   !
  REAL(DP),INTENT(IN):: s   !
  REAL(DP):: t

  REAL(DP):: ms,term1,term2
!-------------------------------------------------------------------------------
  ms=m*s     
  IF (ms <= ONE  .OR.  s >= ONE) THEN
    t=ZERO
  ELSE       
    term1=LOG(ms)/SQRT(ms*ms-ONE)
    term2=LOG((ms*m-ONE+SQRT((m*m-ONE)*(ms*ms-ONE)))/(m*(ONE-s)))
    t=((ONE-s)/(ONE+s))*(term1+term2/SQRT(m*m-ONE))
  END IF

  RETURN
END Function FDouble   !--------------------------------------------------------

!+
PURE FUNCTION FSingle(m,s) RESULT(t)
! ------------------------------------------------------------------------------
  REAL(SP),INTENT(IN):: m   !
  REAL(SP),INTENT(IN):: s   !
  REAL(SP):: t
  REAL(DP):: mm,ss,tt
!-------------------------------------------------------------------------------
  mm=m
  ss=s
  tt=FDouble(mm,ss)
  t=tt
  RETURN
END Function FSingle   !--------------------------------------------------------

!+
PURE FUNCTION GDouble(m,s) RESULT(t)
! ------------------------------------------------------------------------------
! PURPOSE - Compute the G-function from the reference publication
  REAL(DP),INTENT(IN):: m   !
  REAL(DP),INTENT(IN):: s   !

  REAL(DP):: t

  REAL(DP):: ms, term1,term2
  REAL(DP):: root1, root2
!-------------------------------------------------------------------------------
  IF (s >= ONE-EPS) THEN
    t=0.0
    RETURN
  END IF

  ms=m*s
  IF (ABS(m-ONE) < EPS) THEN
    term1=s+(HALFPI+ASIN(S))/SQRT(1-s*s)
    term2=ZERO
  ELSE IF (m < ONE) THEN
    term1=s*ACOS(m)/SQRT(ONE-m*m)
    term2=(HALFPI+ASIN(ms))/SQRT(ONE-ms*ms) 
  ELSE 
    root1=SQRT(m*m-ONE)
    term1=(LOG(m)+s*LOG(m+root1))/root1
    IF (ABS(ms-ONE) < EPS) THEN
      term2=TWO/(m-ONE+root1)
    ELSE IF (ms > ONE) THEN
      root2=SQRT(ms*ms-ONE)
      term2=LOG(ONE + TWO*root2/(m*(ONE-s)+root1-root2) )/root2
    ELSE 
      root2=SQRT(ONE-ms*ms)
      term2=TWO*ATAN(root2/(m-ms+root1))/root2
    END IF
  END IF
  t=(term1+term2)*(ONE-s)/(ONE+s)
  RETURN
END Function GDouble   ! -------------------------------------------------------

!+
PURE FUNCTION GSingle(m,s) RESULT(t)
! ------------------------------------------------------------------------------
  REAL(SP),INTENT(IN):: m   !
  REAL(SP),INTENT(IN):: s   !
  REAL(SP):: t
  REAL(DP):: mm,ss,tt
!-------------------------------------------------------------------------------
  mm=m
  ss=s
  tt=GDouble(mm,ss)
  t=tt
  RETURN
END Function GSingle   !--------------------------------------------------------


!+
PURE FUNCTION RogersCoeffDouble(n,a,b,d) RESULT(cd)
! ------------------------------------------------------------------------------
  REAL(DP),INTENT(IN):: n           ! tan l.e. sweep / beta
  REAL(DP),INTENT(IN):: a,b,d       ! non-dimensional geometrical parameters
  REAL(DP):: cd                     ! drag coeff factor

  REAL(DP):: r
  REAL(DP):: cd1a,cd1b,cd1c,cd1d
  REAL(DP):: cd3a,cd3b,cd3c,cd3d
!-------------------------------------------------------------------------------
  r=ONE-b
  cd1a=G(n,r)/(b*b)
  cd1b=r*F(n,r)/(b*b)
  cd1c=a*(F(n,a)-F(r*n,a/r))/(b*(a-d))
  cd1d=d*(F(n,d)-F(r*n,d/r))/(b*(a-d))

  cd3a=(G(n,d)-G(n,a))/(b*(a-d))
  cd3b=r*(G(r*n,d/r)-G(r*n,a/r))/(b*(a-d))
  cd3c=a*G(a*n,d/a)/((a-d)*(a-d))
  cd3d=d*F(a*n,d/a)/((a-d)*(a-d))

  cd=cd1a-cd1b-cd1c+cd1d -cd3a+cd3b+cd3c-cd3d
  RETURN
END Function RogersCoeffDouble   ! ---------------------------------------------


!+
PURE FUNCTION RogersCoeffSingle(n,a,b,d) RESULT(cd)
! ------------------------------------------------------------------------------
  REAL(SP),INTENT(IN):: n           ! tan l.e. sweep / beta
  REAL(SP),INTENT(IN):: a,b,d       ! non-dimensional geometrical parameters
  REAL(SP):: cd                     ! drag coeff factor

  REAL(DP):: nn,aa,bb,dd,cdd
!-------------------------------------------------------------------------------
  nn=n
  aa=a
  bb=b
  dd=d
  cdd=RogersCoeffDouble(nn,aa,bb,dd)
  cd=cdd
  
  RETURN
END Function RogersCoeffSingle   ! ---------------------------------------------

!+
PURE FUNCTION DragCoeffDouble(mach, tc, tanLE,tanTE, f1,f2) RESULT(cd)
! ------------------------------------------------------------------------------
! PURPOSE - compute the drag coefficient of a wing
  REAL(DP),INTENT(IN):: mach        ! Mach number
  REAL(DP),INTENT(IN):: tc          ! thickness/chord
  REAL(DP),INTENT(IN):: tanLE       ! tangent of  leading edge sweep angle
  REAL(DP),INTENT(IN):: tanTE       ! tangent of trailing edge sweep angle
  REAL(DP),INTENT(IN):: f1,f2       ! fractions of root chord for slope changes
  REAL(DP):: cd                     ! drag coefficient

  REAL(DP):: a,b,d
  REAL(DP):: beta
  REAL(DP):: n
  REAL(DP):: tau
!-------------------------------------------------------------------------------
  beta=SQRT(ABS(mach*mach-ONE))
  n=tanLE/beta
  d=tanTE/tanLE
  b=f1*(ONE-d)
  a=ONE-f2*(ONE-d)
  tau=tc*(ONE-d)
  cd=tau*tau*RogersCoeffDouble(n,a,b,d)/(beta*HALFPI)
  RETURN
END Function DragCoeffDouble   ! -----------------------------------------------


!+
PURE FUNCTION DragCoeffSingle(mach, tc, tanLE,tanTE, f1,f2) RESULT(cd)
! ------------------------------------------------------------------------------
! PURPOSE - compute the drag coefficient of a wing
  REAL(SP),INTENT(IN):: mach        ! Mach number
  REAL(SP),INTENT(IN):: tc          ! thickness/chord
  REAL(SP),INTENT(IN):: tanLE       ! tangent of  leading edge sweep angle
  REAL(SP),INTENT(IN):: tanTE       ! tangent of trailing edge sweep angle
  REAL(SP),INTENT(IN):: f1,f2       ! fractions of root chord for slope changes
  REAL(SP):: cd                     ! drag coefficient

  REAL(DP):: machd,tcd,tanLEd,tanTEd,f1d,f2d,cdd
!-------------------------------------------------------------------------------
  machd=mach                     ! make double precision copies of everything 
  tcd=tc
  tanLEd=tanLE
  tanTEd=tanTE
  f1d=f1
  f2d=f2
  cdd=DragCoeffDouble(machd,tcd,tanLEd,tanTEd,f1d,f2d)
  cd=cdd
  RETURN
END Function DragCoeffSingle   ! -----------------------------------------------

!+
PURE FUNCTION LiftCoeffDouble(mach,tanLE,tanTe) RESULT(cla)
! ------------------------------------------------------------------------------
! PURPOSE - Compute dCL/dalpha for a pointed tip wing (supersonic)
  REAL(DP),INTENT(IN):: mach        ! Mach number
  REAL(DP),INTENT(IN):: tanLE       ! tangent of  leading edge sweep angle
  REAL(DP),INTENT(IN):: tanTE       ! tangent of trailing edge sweep angle
  REAL(DP):: cla   ! lift-curve slope
  
  REAL(DP):: beta
  REAL(DP):: bracket
  REAL(DP):: m,m2
  REAL(DP):: xi,xi2
!-------------------------------------------------------------------------------
  beta=SQRT(mach*mach-ONE)
  xi=tanTE/tanLE
  xi2=xi*xi
  m=beta/tanLE
  m2=m*m
  
  IF (m > ONE) THEN
    bracket=ACOS(-xi/m)/SQRT(m2-xi2) + xi*ACOS(ONE/m)/SQRT(m2-ONE)
    cla=(EIGHT/PI)*m*bracket/(beta*(ONE+xi))
  ELSE
    bracket=xi/(xi+ONE) + (ONE-xi)*ACOS(-xi)/SQRT((ONE-xi2)**3)
    cla=FOUR*m*bracket/(beta*ELLC2(SQRT(ONE-m2)))
  END IF

  RETURN
END Function LiftCoeffDouble   ! -----------------------------------------------

!+
PURE FUNCTION LiftCoeffSingle(mach,tanLE,tanTe) RESULT(cla)
! ------------------------------------------------------------------------------
! PURPOSE - Compute dCL/dalpha for a pointed tip wing (supersonic)
  REAL(SP),INTENT(IN):: mach        ! Mach number
  REAL(SP),INTENT(IN):: tanLE       ! tangent of  leading edge sweep angle
  REAL(SP),INTENT(IN):: tanTE       ! tangent of trailing edge sweep angle
  REAL(SP):: cla   ! lift-curve slope
  
  REAL(DP):: machd,tanled,tanted,clad
!-------------------------------------------------------------------------------
  machd=mach   ! single to double
  tanled=tanle
  tanted=tante  
  clad=LiftCoeffDouble(machd,tanled,tanted)
  cla=clad   ! converts double to single
  RETURN
END Function LiftCoeffSingle   ! -----------------------------------------------


END Module Rogers   ! ==========================================================

