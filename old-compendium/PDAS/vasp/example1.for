!!! Modifications by RLC December 2005


      INCLUDE 'vaspproc.for'
      PROGRAM Example1
      DIMENSION F(3,3),NF(2),G(3,3),NG(2),H(2,3),NH(2),B(3,3)
      DIMENSION NB(2),C(3,3),NC(2),X0(3),NX0(2),XT(3),NXT(2),V1(3)
      DIMENSION NV1(2),V2(3),NV2(2),A1(3,3),NA1(2),U(3),NU(2),YT(3)
      DIMENSION NYT(2),W(18)
      DOUBLE PRECISION F,G,H,B,C,X0,U,XT,YT,TT,DELTAT,TFINAL,V1,V2,A1,W
      COMMON/MAX/MAXRC
      INTEGER:: nlp,lin
      CHARACTER(LEN=80):: title
      COMMON/LINES/NLP,LIN,TITLE

      OPEN(UNIT=5,FILE='example1.inp',STATUS='OLD',ACTION='READ')
      OPEN(UNIT=6,FILE='example1.out',STATUS='REPLACE',ACTION='WRITE')
      MAXRC=9
      CALL RDTITL()
      READ(5,100) TT,DELTAT,TFINAL
  100 FORMAT (8F10.2)
      CALL READ(5,F,NF,G,NG,H,NH,U,NU,X0,NX0)
  101 FORMAT(/,49X, 'TIME RESPONSE',/                                    &
     & '  TIME',23X,'STATE',38X, 'OUTPUT',/                              &
     & '   TT',9X,'XT(1)',9X,'XT(2)',9X,'XT(3)',14X,'YT(1)',9X,'YT(2)')
  102 FORMAT(/F7.2,3X,3E15.7,5X,3E15.7)
      CALL LNCNT(4)
      WRITE(6,101)

   10 CALL EAT(F,NF,TT,B,NB,C,NC,W,18)
      CALL MULT(B,NB,X0,NX0,V1,NV1)
      CALL MULT(C,NC,G,NG,A1,NA1)
      CALL MULT(A1,NA1,U,NU,V2,NV2)
      CALL ADD(V1,NV1,V2,NV2,XT,NXT)
      CALL MULT(H,NH,XT,NXT,YT,NYT)
      CALL LNCNT (2)
      WRITE(6,102) TT,(XT(I),I=1,3),(YT(I),I=1,2)
      TT=TT+DELTAT
      IF(TT.GT.TFINAL)  STOP
      GO TO 10
      END Program Example1

