      INCLUDE 'vaspproc.for'
      PROGRAM Example2
      DIMENSION F(5,5),NF(2),G(5,2),NG(2),J(2,1),NJ(2),R(1,1),NR(2)
      DIMENSION K(2,5),NK(2),H(7,5),NH(2),X(5),NX(2),T(4),NT(2)
      DIMENSION DUMMY(100)
      DOUBLE PRECISION F,G,J,R,K,H,X,T,DUMMY
      INTEGER:: nepr
      CHARACTER(LEN=16):: FMT1,FMT2
      COMMON/FORM/NEPR,FMT1,FMT2

      OPEN(UNIT=5,FILE='example2.inp',STATUS='OLD',ACTION='READ')
      OPEN(UNIT=6,FILE='example2.out',STATUS='REPLACE',ACTION='WRITE')
      CALL RDTITL
      CALL READ(5,F,NF,G,NG,J,NJ,R,NR,K,NK)
      CALL READ(3,H,NH,X,NX,T,NT)
      CALL TRNSI(F,NF,G,NG,J,NJ,R,NR,K,NK,H,NH,X,NX,T,DUMMY,100)
      STOP
      END Program Example2
