

VASP- VARIABLE DIMENSION AUTOMATIC SYNTHESIS PROGRAM           /vasp/readme.txt

The files for this program are in the directory vasp 
  readme.txt      this file of general description
  vaspproc.for    the source code for the VASP library in modern Fortran
  arc10616.txt    the original program description from COSMIC
  orig1.src       the original copy of the library source code (from COSMIC)
  orig2.src       the original copy of the examples source code (from COSMIC)

Sample programs that use the VASP library are:
  example1.for    an example program that uses VASP
  example1.inp    the input data for example1
  example1.out    the output from example1
  example2.for    an example program that uses VASP
  example2.inp    the input data for example2
  example2.out    the output from example2
  example3.for    an example program that uses VASP
  example3.inp    the input data for example3
  example3.out    the output from example3

The reference documents for this program may be accessed
from the ORACLS web page https://www.pdas.com/oraclsrefs.html. 

Vasp is not a program, but a collection of subroutines that may be 
called by a driver program.
Seven test programs are shown to illustrate the procedures for using VASP.
At the time of release of Version 16, the examples 4,5,6 are not 
working correctly. Example4 is especially dangerous as it goes into
an infinite write loop and can fill your entire hard disk.

Hint: the problem is with subroutine PSEUP and probably has to
do with equivalencing variables of different types and sizes.
NOTE - Dec 2010 - still a problem. 
I am not sure I will be able to solve this one. In seven years, no
one has called me to complain, so I may not ...
Oct 2020 - still no action. I don't think anyone is interested...


