!+
      PROGRAM VortexLatticeAnalysis
! ---------------------------------------------------------------------------
!     PURPOSE - Use the data on binary file fort25 (written by GEOM)
!        to perform the analysis and print the shape of the camber
!        surface for minimum induced drag.
!
!     AUTHORS - John E. Lamar, NASA Langley Research Center
!               Jeanne M. Peters, NASA Langley Research Center
!               Ralph L. Carmichael, Public Domain Aeronautical Software
!        Dr. R. K. NANGIA  - Sorting out some ARRAY inconsistencies 2002
!
!     REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
!  June76   *    JEL   Publication of NASA TN D-8090
!    ?         JEL&JMP Release of COSMIC program L-15160
! 10Jun96  0.1   RLC   Original recompilation in PC environment
! 25May96  0.2   RLC   REAL*8, output file named vlmd.out
! 29Dec96  0.3   RLC   Changed 3 occurences of AMIN1 to MIN
! 30Dec99  1.0   RLC   Reordered commons: 8-bytes first, 4-bytes last
! 2002           RKN   Some Array Inconsistencies sorted !
!     NOTES-
!
      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!     C O N S T A N T S                                                *
!***********************************************************************
      CHARACTER GREETING*60
      PARAMETER (GREETING='Vortex Lattice Minimum Drag')
!***********************************************************************
!     V A R I A B L E S                                                *
!***********************************************************************
      INTEGER errCode
      REAL*8 MACH

      COMMON /ALL/ BOT,BETA,PTEST,QTEST,TBLSCW(50),Q(400),PN(400),      &
     & PV(400),S(400),PSI(400),PHI(50),ZH(50),NSSW,M

      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,   &
     & RTCDHT(2),CONFIG,PLAN,MACH,SSWWA(50),XCFW,XCFT,YREG(1,2),        &
     & NSSWSV(2),MSV(2),KBOT,IPLAN

      COMMON /TOTHRE/ CIR(400)
      COMMON /CCRRDD/ CHORD(50),XTE(50),TSPAN,TSPANA,KBIT
!-----------------------------------------------------------------------
      WRITE(*,*) GREETING

      OPEN(UNIT=6, FILE='vlmd.out', STATUS='REPLACE', IOSTAT=errCode)
      IF (errCode .NE. 0) STOP 'Unable to open output file'


!!      subroutine finit
!**********************************************************************
!     Initialize file storage for WINGAL
!        6    - output file
!        10    - scratch binary
!        20    - scratch binary
!        90    - scratch formatted
!        25    - binary file containing data created by geomtry
!**********************************************************************
      open (unit=10, status='scratch', form='unformatted')
      open (unit=20, status='scratch', form='unformatted')
      open (unit=90, status='scratch', form='formatted')
      open (unit=25,status='old',form='unformatted',access='sequential',&
     &   file='fort25')

      WRITE(*,*) 'Computing...'

!        VORTEX LATTICE AERODYNAMIC COMPUTATION

      REWIND 25
      READ(25,END=10000) BOT,M,BETA,PTEST,QTEST,TBLSCW,Q,PN,PV,S,PSI,   &
     & PHI,ZH,NSSW,TWIST,CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,RTCDHT,   &
     & CONFIG,NSSWSV,MSV,KBOT,PLAN,IPLAN,MACH,SSWWA,CHORD,XTE,KBIT,     &
     & TSPAN,TSPANA,XCFW,XCFT ,IFLAG,YREG(1,1),YREG(1,2)

      write(*,*) ' iflag = ',iflag
      if (iflag.eq.1) write(*,*) ' call circul1'
      if (iflag.eq.1) call circul1
      if (iflag.eq.2) write(*,*) ' call circul2'
      if (iflag.eq.2) call circul2
      if (iflag.eq.3) write(*,*) ' call circul3'
      if (iflag.eq.3) call circul3
      write(*,*) ' call zocdetm'
      call zocdetm

      WRITE(*,*) 'Vortex Lattice Analysis has terminated successfully'
      STOP
!
10000 WRITE(*,*) 'end-of-file on 25'
      STOP
      END Program VortexLatticeAnalysis

!+
      SUBROUTINE FTLUP (X,Y,M,N,VARI,VARD)
! ---------------------------------------------------------------------------
!     PURPOSE - Interpolation
!     ***DOCUMENT DATE 09-12-69    SUBROUTINE REVISED 07-07-69 *********
!        MODIFICATION OF LIBRARY INTERPOLATION SUBROUTINE  FTLUP

      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!     A R G U M E N T S                                                *
!***********************************************************************
      REAL*8 X
      REAL*8 Y
      INTEGER M
      INTEGER N
      REAL*8 VARI(*)
      REAL*8 VARD(*)
!***********************************************************************
!     L O C A L   C O N S T A N T S                                    *
!***********************************************************************
      REAL*8 ONE
      PARAMETER(ONE=1.0D0)

!***********************************************************************
!     L O C A L   A R R A Y S                                          *
!***********************************************************************
      DIMENSION V(3), YY(2)
      DIMENSION II(43)

!-----------------------------------------------------------------------

!      INITIALIZE ALL INTERVAL POINTERS TO -1.0   FOR MONOTONICITY CHECK
      DATA (II(J),J=1,43)/43*-1/
      MA=IABS(M)

!            ASSIGN INTERVAL POINTER FOR GIVEN VARI TABLE
!      THE SAME POINTER WILL BE USED ON A GIVEN VARI TABLE EVERY TIME
      LI=MOD(LOCF(VARI(1)),43)+1
      I=II(LI)
      IF (I.GE.0) GO TO 6
      IF (N.LT.2) GO TO 6
!
!     MONOTONICITY CHECK
      IF (VARI(2)-VARI(1)) 2,2,4
!     ERROR IN MONOTONICITY
    1 K=LOCF(VARI(1))
      PRINT 17, J,K,(VARI(J),J=1,N),(VARD(J),J=1,N)
      STOP
!     MONOTONIC DECREASING
    2 DO 3 J=2,N
      IF (VARI(J)-VARI(J-1)) 3,1,1
    3 END DO
      GO TO 6
!     MONOTONIC INCREASING
    4 DO 5 J=2,N
      IF (VARI(J)-VARI(J-1)) 1,1,5
    5 END DO
!
!     INTERPOLATION
    6 IF (I.LE.0) I=1
      IF (I.GE.N) I=N-1
      IF (N.LE.1) GO TO 7
      IF (MA.NE.0) GO TO 8
!     ZERO ORDER
    7 Y=VARD(1)
      GO TO 16
!     LOCATE I INTERVAL (X(I).LE.X.LT.X(I+1))
    8 IF ((VARI(I)-X)*(VARI(I+1)-X)) 11,11,9
!     IN GIVES DIRECTION FOR SEARCH OF INTERVALS
                                                          ! little fix b
    9 IN=INT(SIGN(ONE, (VARI(I+1)-VARI(I))*(X-VARI(I))))
!     IF X OUTSIDE ENDPOINTS, EXTRAPOLATE FROM END INTERVAL
   10 IF ((I+IN).LE.0) GO TO 11
      IF ((I+IN).GE.N) GO TO 11
      I=I+IN
      IF ((VARI(I)-X)*(VARI(I+1)-X)) 11,11,10
   11 IF (MA.EQ.2) GO TO 12
!
!     FIRST ORDER
      Y=(VARD(I)*(VARI(I+1)-X)-VARD(I+1)*(VARI(I)-X))/(VARI(I+1)-VARI(I)&
     &)
      GO TO 16
!
!     SECOND ORDER
   12 IF (N.EQ.2) GO TO 1
      IF (I.EQ.(N-1)) GO TO 14
      IF (I.EQ.1) GO TO 13
!     PICK THIRD POINT
      SK=VARI(I+1)-VARI(I)
      IF ((SK*(X-VARI(I-1))).LT.(SK*(VARI(I+2)-X))) GO TO 14
   13 L=I
      GO TO 15
   14 L=I-1
   15 V(1)=VARI(L)-X
      V(2)=VARI(L+1)-X
      V(3)=VARI(L+2)-X
      YY(1)=(VARD(L)*V(2)-VARD(L+1)*V(1))/(VARI(L+1)-VARI(L))
      YY(2)=(VARD(L+1)*V(3)-VARD(L+2)*V(2))/(VARI(L+2)-VARI(L+1))
      Y=(YY(1)*V(3)-YY(2)*V(1))/(VARI(L+2)-VARI(L))
   16 II(LI)=I
      RETURN
!
!
   17 FORMAT ('1',' TABLE BELOW OUT OF ORDER FOR FTLUP  AT POSITION  ', &
     &I5,/' X TABLE IS STORED IN LOCATION ',I6//(8G15.8))
      END Subroutine FTLUP

      function locf(var)
      REAL*8 var(*)
      locf=1
      return
      END Function locf

!+
      SUBROUTINE SIMEQ (A,N,B,M,DTERM,IPIVOT,NMAX,ISCALE)
!   --------------------------------------------------------------------
!     PURPOSE - SOLUTION OF SIMULTANEOUS LINEAR EQUATIONS
!     *** DOCUMENT DATE 08-01-68   SUBROUTINE REVISED 08-01-68 *********

      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!     A R G U M E N T S                                                *
!***********************************************************************
      INTEGER N,M,NMAX
      REAL*8 A(NMAX,N)
      REAL*8 B(NMAX,M)
      REAL*8 DTERM
      INTEGER IPIVOT(N)
      INTEGER ISCALE
!***********************************************************************
!     L O C A L   V A R I A B L E S                                    *
!***********************************************************************
      EQUIVALENCE (IROW,JROW), (ICOLUM,JCOLUM), (AMAX,T,SWAP)
      real*8 r1, r2, determ, pivoti
!-----------------------------------------------------------------------

!     INITIALIZATION

    1 ISCALE=0
      R1=10.0d0**100
      R2=1.0d0/R1
      DETERM=1.0d0
      DO 2 J=1,N
    2 IPIVOT(J)=0
      DO 38 I=1,N
!
!     SEARCH FOR PIVOT ELEMENT
!
      AMAX=0.0
      DO 7 J=1,N
      IF (IPIVOT(J)-1) 3,7,3
    3 DO 6 K=1,N
      IF (IPIVOT(K)-1) 4,6,39
    4 IF (ABS(AMAX)-ABS(A(J,K))) 5,6,6
    5 IROW=J
      ICOLUM=K
      AMAX=A(J,K)
    6 END DO
    7 END DO
      IF (AMAX) 9,8,9
    8 DETERM=0.0d0
      ISCALE=0
      GO TO 39
    9 IPIVOT(ICOLUM)=IPIVOT(ICOLUM)+1
!
!     INTERCHANGE ROWS TO PUT PIVOT ELEMENT ON DIAGONAL
!
      IF (IROW-ICOLUM) 10,14,10
   10 DETERM=-DETERM
      DO 11 L=1,N
      SWAP=A(IROW,L)
      A(IROW,L)=A(ICOLUM,L)
   11 A(ICOLUM,L)=SWAP
      IF (M) 14,14,12
   12 DO 13 L=1,M
      SWAP=B(IROW,L)
      B(IROW,L)=B(ICOLUM,L)
   13 B(ICOLUM,L)=SWAP
   14 PIVOT=A(ICOLUM,ICOLUM)
      IF (PIVOT) 15,8,15
!
!     SCALE THE DETERMINANT
!
   15 PIVOTI=dble(PIVOT)
      IF (DABS(DETERM)-R1) 18,16,16
   16 DETERM=DETERM/R1
      ISCALE=ISCALE+1
      IF (DABS(DETERM)-R1) 21,17,17
   17 DETERM=DETERM/R1
      ISCALE=ISCALE+1
      GO TO 21
   18 IF (DABS(DETERM)-R2) 19,19,21
   19 DETERM=DETERM*R1
      ISCALE=ISCALE-1
      IF (DABS(DETERM)-R2) 20,20,21
   20 DETERM=DETERM*R1
      ISCALE=ISCALE-1
   21 IF (DABS(PIVOTI)-R1) 24,22,22
   22 PIVOTI=PIVOTI/R1
      ISCALE=ISCALE+1
      IF (DABS(PIVOTI)-R1) 27,23,23
   23 PIVOTI=PIVOTI/R1
      ISCALE=ISCALE+1
      GO TO 27
   24 IF (DABS(PIVOTI)-R2) 25,25,27
   25 PIVOTI=PIVOTI*R1
      ISCALE=ISCALE-1
      IF (DABS(PIVOTI)-R2) 26,26,27
   26 PIVOTI=PIVOTI*R1
      ISCALE=ISCALE-1
   27 DETERM=DETERM*PIVOTI
!
!     DIVIDE PIVOT ROW BY PIVOT ELEMENT
!
      DO 29 L=1,N
      IF (IPIVOT(L)-1) 28,29,39
   28 A(ICOLUM,L)=A(ICOLUM,L)/PIVOT
   29 END DO
      IF (M) 32,32,30
   30 DO 31 L=1,M
   31 B(ICOLUM,L)=B(ICOLUM,L)/PIVOT
!
!     REDUCE NON-PIVOT ROWS
!
   32 DO 38 L1=1,N
      IF (L1-ICOLUM) 33,38,33
   33 T=A(L1,ICOLUM)
      DO 35 L=1,N
      IF (IPIVOT(L)-1) 34,35,39
   34 A(L1,L)=A(L1,L)-A(ICOLUM,L)*T
   35 END DO
      IF (M) 38,38,36
   36 DO 37 L=1,M
   37 B(L1,L)=B(L1,L)-B(ICOLUM,L)*T
   38 CONTINUE
   39 dterm=sngl(determ)
      END Subroutine Simeq

!+
      SUBROUTINE DRAGSUB (R,A,Y,Z,S,IS,JS,WNK)
!   --------------------------------------------------------------------
!     PURPOSE -
!
      IMPLICIT REAL*8(A-H,O-Z)
!
!***********************************************************************
!     A R G U M E N T S                                                *
!***********************************************************************
      REAL*8 R
      REAL*8 A
      REAL*8 Y
      REAL*8 Z
      REAL*8 S
      REAL*8 IS,JS
      REAL*8 WNK
!-----------------------------------------------------------------------
      ZP=Z+S*SIN(A)
      YP=Y+S*COS(A)
      ZM=Z-S*SIN(A)
      YM=Y-S*COS(A)
      RL=SQRT(ZP**2+YP**2)
      RR=SQRT(ZM**2+YM**2)
      ZPOYP=ZP/YP
      ZMOYM=ZM/YM
      PHILTLJ=ATAN(ZPOYP)
      PHIRTLJ=ATAN(ZMOYM)
      PLMPI=PHILTLJ-R
      PRMPI=PHIRTLJ-R
      COSPLI=COS(PLMPI)
      COSPRI=COS(PRMPI)
      WNK=IS*COSPLI/RL-JS*COSPRI/RR
      RETURN
      END Subroutine DragSub

!+
      SUBROUTINE circul1
! ---------------------------------------------------------------------------
!     PURPOSE -
!
      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!     L O C A L   V A R I A B L E S                                    *
!***********************************************************************
      REAL*8 ISIGN,JSIGN
      real*8 mach
!***********************************************************************
!     L O C A L   A R R A Y S                                          *
!***********************************************************************
      DIMENSION A0(2), B0(2), A1(2), B1(2), C1(2), D1(2),               &
     & ISUM(2), ISUMP(2), ISUMP2(2),                                    &
     & PPP(100), WN(2), YY(2), ZZH(50), ZHH(100), YB(50),               &
     & Y(100), PPHI(50), XTT(50), XTA(100), CHD(100), A(8,8), CDRAG(8), &
     & IPIVOT(8), GAM(100,6), NMA(2), YQ(100), YQQ(50), YC(100), YA(100)
!***********************************************************************
!     C O M M O N   B L O C K   D E F I N I T I O N S                  *
!***********************************************************************
      COMMON /ALL/ BOT,BETA,PTEST,QTEST,TBLSCW(50),Q(400),PN(400),      &
     & PV(400),S(400),PSI(400),PHI(50),ZH(50),NSSW,M

      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,   &
     & RTCDHT(2),CONFIG,PLAN,MACH,SSWWA(50),XCFW,XCFT,YREG(1,2),        &
     & NSSWSV(2),MSV(2),KBOT,IPLAN

!      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,
!     1 RTCDHT(2),CONFIG,NSSWSV(2),MSV(2),KBOT,PLAN,IPLAN,MACH,
!     2 SSWWA(50),XCFW,XCFT,YREG(1,2)

      COMMON /TOTHRE/ CIR(400)
      COMMON /CCRRDD/ CHORD(50),XTE(50),TSPAN,TSPANA,KBIT
!-----------------------------------------------------------------------
      TOLC=(BOT*15.E-05)**2
!
!
      NMA(1)=0
      nma(2)=0
      PI=4.*ATAN(1.)
      RAD=180./PI
      BOTL=ABS(TSPAN)
      BOL=ABS(TSPANA)
      BOTT=BOTL
      IF(BOL.GT.BOTL) BOTT=BOL
      SNN=BOTT/(2.*NSSWSV(KBOT))
      DELTYB=2.*SNN
      NMA(KBOT)=BOTL/DELTYB
      NMA(KBIT)=BOL/DELTYB
      NMAX=NMA(1)+NMA(2)
      LM=1
      IF (IPLAN.EQ.2) LM=6
      IL=LM+1
      JM=LM+2
      IF (LM.EQ.1) JM=IL
      IM=LM+2
      DO 1 I=1,IM
      CDRAG(I)=0.
      DO 1 J=1,IM
    1 A(I,J)=0.
      DO 2 I=1,NMAX
      DO 2 J=1,LM
      GAM(I,J)=0.
    2 CONTINUE
      CDRAG(IL)=CLDES
      CDRAG(IM)=0.0
!
      SCWMIN=20.
      DO 3 I=1,NSSW
    3 SCWMIN=MIN(SCWMIN,TBLSCW(I))
      NSCWMIN=SCWMIN
      II=1
      DO 15 I=1,IPLAN
      BOTT=BOTL
      IF (I.EQ.KBIT) BOTT=BOL
      IB=NSSWSV(I)
      IC=MSV(1)+(I-1)*MSV(2)
      ID=IC+1
      IZ=NSSWSV(1)+(I-1)*NSSWSV(2)
      D=XCFW
      IF (I.EQ.2) D=XCFT
      AI=NSCWMIN*D+0.75
      IMAX=INT(AI)
      IF (D.EQ.1.) GO TO 4
      B0(I)=-1./(NSCWMIN*(1.-D))
      A0(I)=IMAX-B0(I)*(NSCWMIN+0.75)*(NSCWMIN-IMAX)
      GO TO 5
    4 B0(I)=0.
      A0(I)=IMAX
    5 ISUM(I)=0
      isump(i)=0
      isump2(i)=0
      IF (IMAX.EQ.0) GO TO 7
      DO 6 IN=1,IMAX
    6 ISUM(I)=ISUM(I)+IN
    7 IMM=IMAX+1
      IF (IMM.GT.NSCWMIN) GO TO 9
      DO 8 IN=IMM,NSCWMIN
      ISUMP(I)=ISUMP(I)+IN
    8 ISUMP2(I)=ISUMP2(I)+IN**2
    9 IAMM=NMA(I)
      IUZ=NSSWSV(I)
      YCAT=YREG(1,I)
      DO 11 J=1,IUZ
      JJ=J+(I-1)*NSSWSV(1)
      ZZH(J)=ZH(JJ)
      PPHI(J)=PHI(JJ)
      XTT(J)=XTE(JJ)
      CIR(J)=CHORD(JJ)
      YQQ(J)=Q(II)
      YA(JJ)=YQQ(J)
      II=II+TBLSCW(JJ)
      IE=IB-J+1
      ITL=TBLSCW(IZ)
      ID=ID-ITL
      IA=ID+ITL
      IF (IA.GT.IC) YCAT=YCAT-S(ID)
      IF (IA.GT.IC) GO TO 10
      YCAT=YCAT-S(ID)-S(IA)
   10 IZ=IZ-1
      YB(IE)=YCAT
   11 END DO
      DO 12 J=1,IUZ
      JJ=J+(I-1)*NSSWSV(1)
      YC(JJ)=YB(J)
   12 END DO
      YOB=-NMA(I)*2.*SNN-SNN+YREG(1,I)
      DO 14 K=1,IAMM
      KK=K+(I-1)*NMA(1)
      YOB=YOB+DELTYB
      Y(KK)=YOB
      CALL FTLUP (YOB,YQ(KK),+1,IUZ,YB,YQQ)
      CALL FTLUP (YOB,XTA(KK),+1,IUZ,YB,XTT)
      CALL FTLUP (YOB,CHD(KK),+1,IUZ,YB,CIR)
      CALL FTLUP (YOB,PPP(KK),+1,IUZ,YB,PPHI)
      CALL FTLUP (YOB,ZHH(KK),+1,IUZ,YB,ZZH)
      B1(I)=-CHD(KK)/NSCWMIN
      A1(I)=((XTA(KK)+CHD(KK))-0.75*B1(I))*A0(I)
      C1(I)=B0(I)*(XTA(KK)+2.*CHD(KK)-1.5*B1(I))
      D1(I)=B1(I)*B0(I)
!
!     THE FACTOR  8  IS USED INSTEAD OF THE FACTOR  4  TO TAKE INTO
!     ACCOUNT BOTH SIDES OF THE WING
!
      RB=A0(I)+B0(I)*ISUMP(I)
      CNNSTA=8.*SNN*COS(ATAN(PPP(KK)))/SREF
      RL=CNNSTA*RB
      RM=CNNSTA/CREF*(A1(I)+B1(I)*ISUM(I)+C1(I)*ISUMP(I)+D1(I)*ISUMP2(I)&
     &)
      YBT=-YQ(KK)/BOTT
      SYT=SQRT(1.-YBT**2)
      DO 13 JZ=1,3
      IF (IPLAN.EQ.1.AND.JZ.GT.1) GO TO 13
      JR=JZ+(I-1)*3
      SRU=SYT*YBT**(2*(JZ-1))
      GAM(KK,JR)=RB*SRU
      A(JR,IL)=A(JR,IL)+RL*SRU
      A(JR,IM)=A(JR,IM)+RM*SRU
   13 END DO
   14 END DO
   15 END DO
!
!
      DO 16 K=1,LM
      A(IL,K)=A(K,IL)
      A(IM,K)=A(K,IM)
   16 END DO
!
!
!     THE -A- MATRIX STANDS FOR THE DRAG MATRIX -CDV-
!
!
      DO 21 I=1,NMAX
      RPHI=ATAN(PPP(I))
      DO 20 J=1,NMAX
      SPHI=ATAN(PPP(J))
      YY(1)=YQ(I)-YQ(J)
      YY(2)=YQ(I)+YQ(J)
      ZZ=ZHH(I)-ZHH(J)
      DO 18 K=1,2
      ISIGN=1.
      jsign=1.
      IF (K.EQ.2) GO TO 17
      IF (YY(1).LT.TOLC) JSIGN=-1.
      IF (YY(1).LT.(-TOLC)) ISIGN=-1.
   17 YYY=YY(K)
      CALL DRAGSUB (RPHI,SPHI,YYY,ZZ,SNN,ISIGN,JSIGN,WN(K))
      SPHI=-SPHI
   18 END DO
      DO 19 KP=1,LM
      DO 19 KG=1,LM
      A(KP,KG)=A(KP,KG)+GAM(I,KP)*GAM(J,KG)*SNN*(WN(1)-WN(2))/(PI*SREF)
   19 CONTINUE
   20 END DO
   21 END DO
!
!
      REWIND 10
      WRITE(10) ((A(I,J),I=1,JM),J=1,JM)
      END FILE 10
      REWIND 20
      DO 23 I=1,LM
      DO 22 J=1,LM
      XTA(J)=A(I,J)+A(J,I)
   22 END DO
      WRITE(20) (XTA(IK),IK=1,LM)
   23 END DO
      END FILE 20
      REWIND 20
      DO 24 I=1,LM
      READ(20,END=24   ) (A(I,J),J=1,LM)
   24 END DO
      CALL SIMEQ (A,JM,CDRAG,1,DETERM,IPIVOT,8,ISCALE)
      REWIND 10
      READ(10,END=10000) ((A(I,J),I=1,JM),J=1,JM)
10000 CD=0.
      DO 25 I=1,LM
      DO 25 J=1,LM
   25 CD=CD+CDRAG(I)*A(I,J)*CDRAG(J)*2.
      JK=0
      DO 28 I=1,IPLAN
      BOTT=BOTL
      IF (I.EQ.KBIT) BOTT=BOL
      KA=1+(I-1)*NSSWSV(1)
      KB=NSSWSV(1)+(I-1)*NSSWSV(2)
      D=XCFW
      IF (I.EQ.2) D=XCFT
      DO 27 J=KA,KB
      YBT=-YA(J)/BOTT
      SYT=SQRT(1.-YBT**2)
      RJ=0.
      DO 26 JZ=1,3
      JR=JZ+(I-1)*3
      IF (IPLAN.EQ.1.AND.JZ.GT.1) GO TO 26
      SRU=SYT*YBT**(2*(JZ-1))
      RJ=RJ+CDRAG(JR)*SRU
   26 END DO
      NSCW=TBLSCW(J)
      AI=NSCW*D+0.75
      IMAX=INT(AI)
      DO 27 K=1,NSCW
      JK=JK+1
      E=1.
      IF (K.GT.IMAX) E=(1.-(K-.75)/NSCW)/(1.-D)
      CIR(JK)=E*RJ
   27 CONTINUE
   28 END DO
      WRITE(6,36) CLDES
      NR=0
      DO 29 NV=1,NSSW
      NSCW=TBLSCW(NV)
      NP=NR+1
      NR=NR+NSCW
      PHIPR=ATAN(PHI(NV))*RAD
      IF (NV.EQ.(NSSWSV(1)+1)) WRITE(6,37)
      DO 29 I=NP,NR
      PNPR=PN(I)*BETA
      PVPR=PV(I)*BETA
      PSIPR=ATAN(BETA*TAN(PSI(I)))*RAD
      WRITE(6,38) PNPR,PVPR,Q(I),ZH(NV),S(I),PSIPR,PHIPR,CIR(I)
   29 CONTINUE
      WRITE(6,34)
      WRITE(6,35) CREF,CAVE,STRUE,SREF,BOT,AR,ARTRUE,MACH
      CLTOT=0.
      cmtot=0.
      DO 31 I=1,NSSW
      IF (I.EQ.1) WRITE(6,41)
      IF (I.EQ.(NSSWSV(1)+1)) WRITE(6,42)
      SPANLD=0.
      DO 30 IJ=1,NSCWMIN
      IK=(I-1)*NSCWMIN+IJ
      SPANLD=SPANLD+2.*CIR(IK)
      CLTOT=CLTOT+8.*S(IK)*CIR(IK)/SREF*COS(ATAN(PHI(I)))
      CMTOT=CMTOT+8.*S(IK)*CIR(IK)*PN(IK)*BETA*COS(ATAN(PHI(I)))/(SREF*C&
     &REF)
   30 END DO
      WRITE(6,44) Q(IK),SPANLD
      IF (I.EQ.NSSWSV(1)) CL1=CLTOT
      IF (I.EQ.NSSWSV(1)) CM1=CMTOT
      IF (I.EQ.NSSWSV(1)) WRITE(6,43) CL1,CM1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) CL2=CLTOT-CL1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) CM2=CMTOT-CM1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) WRITE(6,43) CL2,CM2
   31 END DO
!
      WRITE(6,39) CLDES,CLTOT,CMTOT,CD
!
   32 CONTINUE
   33 CONTINUE
   34 FORMAT (////' REF.CHORD',2X,'C AVG.   TRUE AREA ',2X,             &
     & 'REF. AREA',4X,'B/2',4X,'REF. AR',2X,'TRUE AR',3X,'MACH'/)
   35 FORMAT (2F9.5,2F12.4,4F9.5)

   36 FORMAT ('1',///2X,'X',11X,'X',11X,'Y',11X,'Z',12X,'S',5X,         &
     &  'C/4 SWEEP',4X,'DIHEDRAL',3X,'GAMMA/U AT'/                      &
     &  2X,'C/4',9X,'3C/4',42X,'ANGLE',7X,'ANGLE',4X,'CLDES=',F7.4/)
   37 FORMAT (/15X,'SECOND PLANFORM HORSESHOE VORTEX DESCRIPTIONS'/)
   38 FORMAT (17X,8F12.5)
   39 FORMAT (/////15X,'CL DESIGN =',F10.6,5X,'CL COMPUTED=',F10.6/     &
     & 15X, 'CM COMPUTED=',F10.6,5X,'CD V=',F10.6)
   40 FORMAT (/////15X,'CL DES=',F10.6,5X,'CL COMPUTED=',F10.6/         &
     & 5X,'NO PITCHING MOMENT CONSTRAINT',5X,'CD V=',F10.6)
   41 FORMAT (////10X,                                                  &
     & 'F I R S T    P L A N F O R M    S P A N    L O A D I N G'//     &
     & 30X,'Y',11X,'CL*C')
   42 FORMAT (////10X,                                                  &
     & 'S E C O N D    P L A N F O R M    S P A N    L O A D I N G'//   &
     & 30X,'Y',11X,'CL*C')
   43 FORMAT (//20X,'CL DEVELOPED ON THIS PLANFORM=',F10.6/             &
     &          20X,'CM DEVELOPED ON THIS PLANFORM=',F10.6)
   44 FORMAT (25X,F10.5,3X,F10.5)

      END Subroutine Circul1


!+
      SUBROUTINE circul2
! ---------------------------------------------------------------------------
!     PURPOSE -
!
      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!     L O C A L   V A R I A B L E S                                    *
!***********************************************************************
      REAL*8 ISIGN,JSIGN
      real*8 mach
!***********************************************************************
!     L O C A L   A R R A Y S                                          *
!***********************************************************************
      DIMENSION A0(2), B0(2), A1(2), B1(2), C1(2), D1(2),               &
     & ISUM(2), ISUMP(2), ISUMP2(2),                                    &
     & PPP(100), WN(2), YY(2), ZZH(50), ZHH(100), YB(50),               &
     & Y(100), PPHI(50), XTT(50), XTA(102), CHD(100), A(102,102),       &
     & CDRAG(102), IPIVOT(102), NMA(2), YQ(100), YQQ(50), YC(100)

!***********************************************************************
!     C O M M O N   B L O C K   D E F I N I T I O N S                  *
!***********************************************************************
      COMMON /ALL/ BOT,BETA,PTEST,QTEST,TBLSCW(50),Q(400),PN(400),      &
     & PV(400),S(400),PSI(400),PHI(50),ZH(50),NSSW,M

      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,   &
     & RTCDHT(2),CONFIG,PLAN,MACH,SSWWA(50),XCFW,XCFT,YREG(1,2),        &
     & NSSWSV(2),MSV(2),KBOT,IPLAN

!      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,
!     1 RTCDHT(2),CONFIG,NSSWSV(2),MSV(2),KBOT,PLAN,IPLAN,MACH,
!     2 SSWWA(50),XCFW,XCFT,YREG(1,2)

      COMMON /TOTHRE/ CIR(400)
      COMMON /CCRRDD/ CHORD(50),XTE(50),TSPAN,TSPANA,KBIT
!-----------------------------------------------------------------------
!
!
      TOLC=(BOT*15.E-05)**2
!
!
      NMA(1)=0
      nma(2)=0
      PI=4.*ATAN(1.)
      RAD=180./PI
      BOTL=ABS(TSPAN)
      BOL=ABS(TSPANA)
      BOTT=BOTL
      IF(BOL.GT.BOTL) BOTT=BOL
      SNN=BOTT/100.
      DELTYB=2.*SNN
      NMA(KBOT)=BOTL/DELTYB
      NMA(KBIT)=BOL/DELTYB
      NMAX=NMA(1)+NMA(2)
      LM=NMAX
      IL=LM+1
      JM=LM+2
      IF (LM.EQ.NMA(1)) JM=IL
      IM=LM+2
      DO 1 I=1,IM
      CDRAG(I)=0.
      DO 1 J=1,IM
    1 A(I,J)=0.
      CDRAG(IL)=CLDES
      CDRAG(IM)=0.0
!
      SCWMIN=20.
      DO 2 I=1,NSSW
    2 SCWMIN=MIN(SCWMIN,TBLSCW(I))
      NSCWMIN=SCWMIN
      II=1
      DO 13 I=1,IPLAN
      IB=NSSWSV(I)
      IC=MSV(1)+(I-1)*MSV(2)
      ID=IC+1
      IZ=NSSWSV(1)+(I-1)*NSSWSV(2)
      D=XCFW
      IF (I.EQ.2) D=XCFT
      AI=NSCWMIN*D+0.75
      IMAX=INT(AI)
      IF (D.EQ.1.) GO TO 3
      B0(I)=-1./(NSCWMIN*(1.-D))
      A0(I)=IMAX-B0(I)*(NSCWMIN+0.75)*(NSCWMIN-IMAX)
      GO TO 4
    3 B0(I)=0.
      A0(I)=IMAX
    4 ISUM(I)=0
      isump(i)=0
      isump2(i)=0
      IF (IMAX.EQ.0) GO TO 6
      DO 5 IN=1,IMAX
    5 ISUM(I)=ISUM(I)+IN
    6 IMM=IMAX+1
      IF (IMM.GT.NSCWMIN) GO TO 8
      DO 7 IN=IMM,NSCWMIN
      ISUMP(I)=ISUMP(I)+IN
    7 ISUMP2(I)=ISUMP2(I)+IN**2
    8 IAMM=NMA(I)
      IUZ=NSSWSV(I)
      YCAT=YREG(1,I)
      DO 10 J=1,IUZ
      JJ=J+(I-1)*NSSWSV(1)
      ZZH(J)=ZH(JJ)
      PPHI(J)=PHI(JJ)
      XTT(J)=XTE(JJ)
      CIR(J)=CHORD(JJ)
      YQQ(J)=Q(II)
      II=II+TBLSCW(JJ)
      IE=IB-J+1
      ITL=TBLSCW(IZ)
      ID=ID-ITL
      IA=ID+ITL
      IF (IA.GT.IC) YCAT=YCAT-S(ID)
      IF (IA.GT.IC) GO TO 9
      YCAT=YCAT-S(ID)-S(IA)
    9 IZ=IZ-1
      YB(IE)=YCAT
   10 END DO
      DO 11 J=1,IUZ
      JJ=J+(I-1)*NSSWSV(1)
      YC(JJ)=YB(J)
   11 END DO
      YOB=-NMA(I)*2.*SNN-SNN+YREG(1,I)
      DO 12 K=1,IAMM
      KK=K+(I-1)*NMA(1)
      YOB=YOB+DELTYB
      Y(KK)=YOB
      CALL FTLUP (YOB,YQ(KK),+1,IUZ,YB,YQQ)
      CALL FTLUP (YOB,XTA(KK),+1,IUZ,YB,XTT)
      CALL FTLUP (YOB,CHD(KK),+1,IUZ,YB,CIR)
      CALL FTLUP (YOB,PPP(KK),+1,IUZ,YB,PPHI)
      CALL FTLUP (YOB,ZHH(KK),+1,IUZ,YB,ZZH)
      B1(I)=-CHD(KK)/NSCWMIN
      A1(I)=((XTA(KK)+CHD(KK))-0.75*B1(I))*A0(I)
      C1(I)=B0(I)*(XTA(KK)+2.*CHD(KK)-1.5*B1(I))
      D1(I)=B1(I)*B0(I)
!
!     THE FACTOR  8  IS USED INSTEAD OF THE FACTOR  4  TO TAKE INTO
!     ACCOUNT BOTH SIDES OF THE WING
!
      CNNSTA=8.*SNN*COS(ATAN(PPP(KK)))/SREF
      A(KK,IL)=CNNSTA*(A0(I)+B0(I)*ISUMP(I))
      A(KK,IM)=CNNSTA/CREF*(A1(I)+B1(I)*ISUM(I)+C1(I)*ISUMP(I)+D1(I)*ISU&
     &MP2(I))
   12 END DO
   13 END DO
!
!
      DO 14 K=1,LM
      A(IL,K)=A(K,IL)
      A(IM,K)=A(K,IM)
   14 END DO
!
!
!     THE -A- MATRIX STANDS FOR THE DRAG MATRIX -CDV-
!
!
      DO 17 I=1,LM
      RPHI=ATAN(PPP(I))
      CSR=A(I,IL)*SREF/(8.*SNN*COS(RPHI))
      DO 17 J=1,LM
      SPHI=ATAN(PPP(J))
      CSS=A(J,IL)*SREF/(8.*SNN*COS(SPHI))
      YY(1)=YQ(I)-YQ(J)
      YY(2)=YQ(I)+YQ(J)
      ZZ=ZHH(I)-ZHH(J)
      DO 16 K=1,2
      ISIGN=1.
      jsign=1.
      IF (K.EQ.2) GO TO 15
      IF (YY(1).LT.TOLC) JSIGN=-1.
      IF (YY(1).LT.(-TOLC)) ISIGN=-1.
   15 YYY=YY(K)
      CALL DRAGSUB (RPHI,SPHI,YYY,ZZ,SNN,ISIGN,JSIGN,WN(K))
      SPHI=-SPHI
   16 END DO
      A(I,J)=SNN*CSR*CSS*(WN(1)-WN(2))/(PI*SREF)
   17 CONTINUE
!
!
      REWIND 10
      WRITE(10) ((A(I,J),I=1,JM),J=1,JM)
      END FILE 10
      REWIND 20
      DO 19 I=1,LM
      DO 18 J=1,LM
      XTA(J)=A(I,J)+A(J,I)
   18 END DO
      WRITE(20) (XTA(IK),IK=1,LM)
   19 END DO
      END FILE 20
      REWIND 20
      DO 20 I=1,LM
      READ(20,END=20   ) (A(I,J),J=1,LM)
   20 END DO
      CALL SIMEQ (A,JM,CDRAG,1,DETERM,IPIVOT,102,ISCALE)
      WRITE(6,45)
      WRITE(6,47)
      REWIND 10
      READ(10,END=10000) ((A(I,J),I=1,JM),J=1,JM)
10000 CD=0.
      DO 21 I=1,LM
      DO 21 J=1,LM
   21 CD=CD+CDRAG(I)*A(I,J)*CDRAG(J)*2.
      DO 23 I=1,LM
      CRPHI=COS(ATAN(PPP(I)))
      WNII=0.
      DO 22 J=1,LM
      SPHI=ATAN(PPP(J))
      CSS=A(J,IL)*SREF/(8.*SNN*COS(SPHI))
      WNII=WNII+CDRAG(J)*A(I,J)*PI*SREF/(SNN*CSS*CRPHI)
   22 END DO
      WRITE(6,46) Y(I),CDRAG(I),WNII
      IF (I.EQ.NMA(1).AND.IPLAN.EQ.2) WRITE(6,48)
   23 END DO
      DO 26 I=1,IPLAN
      IUZ=NMA(I)
      DO 24 J=1,IUZ
      JJ=J+(I-1)*NMA(1)
      ZZH(J)=Y(JJ)
      XTT(J)=CDRAG(JJ)
   24 END DO
      IUU=NSSWSV(I)
      DO 25 J=1,IUU
      JJ=J+(I-1)*NSSWSV(1)
      CALL FTLUP (YC(JJ),PPP(JJ),+1,IUZ,ZZH,XTT)
   25 END DO
   26 END DO
      JK=0
      DO 28 I=1,IPLAN
      KA=1+(I-1)*NSSWSV(1)
      KB=NSSWSV(1)+(I-1)*NSSWSV(2)
      D=XCFW
      IF (I.EQ.2) D=XCFT
      DO 27 J=KA,KB
      NSCW=TBLSCW(J)
      AI=NSCW*D+0.75
      IMAX=INT(AI)
      DO 27 K=1,NSCW
      JK=JK+1
      E=1.
      IF (K.GT.IMAX) E=(1.-(K-.75)/NSCW)/(1.-D)
      CIR(JK)=PPP(J)*E
   27 CONTINUE
   28 END DO
      WRITE(6,36) CLDES
      NR=0
      DO 29 NV=1,NSSW
      NSCW=TBLSCW(NV)
      NP=NR+1
      NR=NR+NSCW
      PHIPR=ATAN(PHI(NV))*RAD
      IF (NV.EQ.(NSSWSV(1)+1)) WRITE(6,37)
      DO 29 I=NP,NR
      PNPR=PN(I)*BETA
      PVPR=PV(I)*BETA
      PSIPR=ATAN(BETA*TAN(PSI(I)))*RAD
      WRITE(6,38) PNPR,PVPR,Q(I),ZH(NV),S(I),PSIPR,PHIPR,CIR(I)
   29 CONTINUE
      WRITE(6,34)
      WRITE(6,35) CREF,CAVE,STRUE,SREF,BOT,AR,ARTRUE,MACH
      CLTOT=0.
      cmtot=0.
      DO 31 I=1,NSSW
      IF (I.EQ.1) WRITE(6,41)
      IF (I.EQ.(NSSWSV(1)+1)) WRITE(6,42)
      SPANLD=0.
      DO 30 IJ=1,NSCWMIN
      IK=(I-1)*NSCWMIN+IJ
      SPANLD=SPANLD+2.*CIR(IK)*COS(ATAN(PHI(I)))
      CLTOT=CLTOT+8.*S(IK)*CIR(IK)/SREF*COS(ATAN(PHI(I)))
      CMTOT=CMTOT+                                                      &
     &   8.*S(IK)*CIR(IK)*PN(IK)*BETA*COS(ATAN(PHI(I)))/(SREF*CREF)
   30 END DO
      WRITE(6,44) Q(IK),SPANLD
      IF (I.EQ.NSSWSV(1)) CL1=CLTOT
      IF (I.EQ.NSSWSV(1)) CM1=CMTOT
      IF (I.EQ.NSSWSV(1)) WRITE(6,43) CL1,CM1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) CL2=CLTOT-CL1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) CM2=CMTOT-CM1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) WRITE(6,43) CL2,CM2
   31 END DO
!
      WRITE(6,39) CLDES,CLTOT,CMTOT,CD
!
   32 CONTINUE
   33 CONTINUE

   34 FORMAT (////' REF.CHORD',2X,'C AVG.   TRUE AREA ',2X,             &
     & 'REF. AREA',4X,'B/2',4X,'REF. AR',2X,'TRUE AR',3X,'MACH'/)
   35 FORMAT (2F9.5,2F12.4,4F9.5)

   36 FORMAT ('1',///'    X',9X,'X',9X,'Y',9X,'Z',9X,'S',5X,'C/4 SWEEP',&
     & 4X,'DIHEDRAL',3X,'GAMMA/U AT'/                                   &
     &   2X,'C/4',7X,'3C/4',36X,'ANGLE',7X,'ANGLE',4X,'CLDES=',F7.4/)
   37 FORMAT (/15X,'SECOND PLANFORM HORSESHOE VORTEX DESCRIPTIONS'/)
   38 FORMAT (1X,8F10.5)
   39 FORMAT (/////15X,'CL DESIGN =',F10.6,5X,'CL COMPUTED=',F10.6/     &
     & 15X, 'CM COMPUTED=',F10.6,5X,'CD V=',F10.6)
   40 FORMAT (/////15X,'CL DES=',F10.6,5X,'CL COMPUTED=',F10.6/         &
     & 15X,'NO PITCHING MOMENT CONSTRAINT',5X,'CD V=',F10.6)
!41    FORMAT (////40X,56HF I R S T    P L A N F O R M    S P A N    L O
!     1A D I N G//60X,1HY,11X,4HCL*C)
!42    FORMAT (////40X,58HS E C O N D    P L A N F O R M    S P A N    L
!     1O A D I N G//60X,1HY,11X,4HCL*C)
!43    FORMAT (//50X,30HCL DEVELOPED ON THIS PLANFORM=,F10.6/
!     1          50X,30HCM DEVELOPED ON THIS PLANFORM=,F10.6)
!44    FORMAT (55X,F10.5,3X,F10.5)
   41 FORMAT (////10X,                                                  &
     & 'F I R S T    P L A N F O R M    S P A N    L O A D I N G'//     &
     & 30X,'Y',11X,'CL*C')
   42 FORMAT (////10X,                                                  &
     & 'S E C O N D    P L A N F O R M    S P A N    L O A D I N G'//   &
     & 30X,'Y',11X,'CL*C')
   43 FORMAT (//20X,'CL DEVELOPED ON THIS PLANFORM=',F10.6/             &
     &          20X,'CM DEVELOPED ON THIS PLANFORM=',F10.6)
   44 FORMAT (25X,F10.5,3X,F10.5)

   45 FORMAT(////                                                       &
     & '  S P A N W I S E    S C A L E    F A C T O R S   A N D'/       &
     & '  ( N O R M A L    W A S H ) /'                                 &
     & '( U  *  C O S I N E ( D I H E D R A L ) )'//                    &
     & 3X,'DISTANCE ALONG PLANFORM',5X,'FACTORS',5X,'WN/(U*COS(PHI))')
   46 FORMAT (9X,F10.5,10X,F10.5,3X,F10.5)
   47 FORMAT (10X,'FIRST PLANFORM')
   48 FORMAT (10X,'SECOND PLANFORM')

      END Subroutine Circul2

!+
      SUBROUTINE CIRCUL3
! ---------------------------------------------------------------------------
!     PURPOSE -
!
      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!     L O C A L   V A R I A B L E S                                    *
!***********************************************************************
      REAL*8 ISIGN,JSIGN
      real*8 mach

!***********************************************************************
!     L O C A L   A R R A Y S                                          *
!***********************************************************************
      DIMENSION A0(2), B0(2), A1(2), B1(2), C1(2), D1(2),               &
     & ISUM(2), ISUMP(2), ISUMP2(2), PPP(100), WN(2), YY(2),            &
     & ZZH(50), ZHH(100), YB(50), Y(100), PPHI(50), XTT(50), XTA(102),  &
     & CHD(100), A(102,102), CDRAG(102), NMA(2),                        &
     & YQ(100), YQQ(50), YC(100), V(102,102)

!***********************************************************************
!     C O M M O N   B L O C K   D E F I N I T I O N S                  *
!***********************************************************************
      COMMON /ALL/ BOT,BETA,PTEST,QTEST,TBLSCW(50),Q(400),PN(400),      &
     & PV(400),S(400),PSI(400),PHI(50),ZH(50),NSSW,M

      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,   &
     & RTCDHT(2),CONFIG,PLAN,MACH,SSWWA(50),XCFW,XCFT,YREG(1,2),        &
     & NSSWSV(2),MSV(2),KBOT,IPLAN

!      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,
!     1 RTCDHT(2),CONFIG,NSSWSV(2),MSV(2),KBOT,PLAN,IPLAN,MACH,
!     2 SSWWA(50),XCFW,XCFT,YREG(1,2)

      COMMON /TOTHRE/ CIR(400)
      COMMON /CCRRDD/ CHORD(50),XTE(50),TSPAN,TSPANA,KBIT
!-----------------------------------------------------------------------
!
      TOLC=(BOT*15.E-05)**2
!
!
      NMA(1)=0
      nma(2)=0
      PI=4.*ATAN(1.)
      RAD=180./PI
      BOTL=ABS(TSPAN)
      BOL=ABS(TSPANA)
      BOTT=BOTL
      IF(BOL.GT.BOTL) BOTT=BOL
      SNN=BOTT/100.
      DELTYB=2.*SNN
      NMA(KBOT)=BOTL/DELTYB
      NMA(KBIT)=BOL/DELTYB
      NMAX=NMA(1)+NMA(2)
      LM=NMAX
      IL=LM+1
      JM=LM+2
      IF (LM.EQ.NMA(1)) JM=IL
      IM=LM+2
      DO 1 I=1,IM
      CDRAG(I)=0.
      DO 1 J=1,IM
    1 A(I,J)=0.
      CDRAG(IL)=CLDES
      CDRAG(IM)=0.0
!
      SCWMIN=20.
      DO 2 I=1,NSSW
    2 SCWMIN=MIN(SCWMIN,TBLSCW(I))
      NSCWMIN=SCWMIN
      II=1
      DO 13 I=1,IPLAN
      IB=NSSWSV(I)
      IC=MSV(1)+(I-1)*MSV(2)
      ID=IC+1
      IZ=NSSWSV(1)+(I-1)*NSSWSV(2)
      D=XCFW
      IF (I.EQ.2) D=XCFT
      AI=NSCWMIN*D+0.75
      IMAX=INT(AI)
      IF (D.EQ.1.) GO TO 3
      B0(I)=-1./(NSCWMIN*(1.-D))
      A0(I)=IMAX-B0(I)*(NSCWMIN+0.75)*(NSCWMIN-IMAX)
      GO TO 4
    3 B0(I)=0.
      A0(I)=IMAX
    4 ISUM(I)=0
      isump(i)=0
      isump2(i)=0
      IF (IMAX.EQ.0) GO TO 6
      DO 5 IN=1,IMAX
    5 ISUM(I)=ISUM(I)+IN
    6 IMM=IMAX+1
      IF (IMM.GT.NSCWMIN) GO TO 8
      DO 7 IN=IMM,NSCWMIN
      ISUMP(I)=ISUMP(I)+IN
    7 ISUMP2(I)=ISUMP2(I)+IN**2
    8 IAMM=NMA(I)
      IUZ=NSSWSV(I)
      YCAT=YREG(1,I)
      DO 10 J=1,IUZ
      JJ=J+(I-1)*NSSWSV(1)
      ZZH(J)=ZH(JJ)
      PPHI(J)=PHI(JJ)
      XTT(J)=XTE(JJ)
      CIR(J)=CHORD(JJ)
      YQQ(J)=Q(II)
      II=II+TBLSCW(JJ)
      IE=IB-J+1
      ITL=TBLSCW(IZ)
      ID=ID-ITL
      IA=ID+ITL
      IF (IA.GT.IC) YCAT=YCAT-S(ID)
      IF (IA.GT.IC) GO TO 9
      YCAT=YCAT-S(ID)-S(IA)
    9 IZ=IZ-1
      YB(IE)=YCAT
   10 END DO
      DO 11 J=1,IUZ
      JJ=J+(I-1)*NSSWSV(1)
      YC(JJ)=YB(J)
   11 END DO
      YOB=-NMA(I)*2.*SNN-SNN+YREG(1,I)
      DO 12 K=1,IAMM
      KK=K+(I-1)*NMA(1)
      YOB=YOB+DELTYB
      Y(KK)=YOB
      CALL FTLUP (YOB,YQ(KK),+1,IUZ,YB,YQQ)
      CALL FTLUP (YOB,XTA(KK),+1,IUZ,YB,XTT)
      CALL FTLUP (YOB,CHD(KK),+1,IUZ,YB,CIR)
      CALL FTLUP (YOB,PPP(KK),+1,IUZ,YB,PPHI)
      CALL FTLUP (YOB,ZHH(KK),+1,IUZ,YB,ZZH)
      B1(I)=-CHD(KK)/NSCWMIN
      A1(I)=((XTA(KK)+CHD(KK))-0.75*B1(I))*A0(I)
      C1(I)=B0(I)*(XTA(KK)+2.*CHD(KK)-1.5*B1(I))
      D1(I)=B1(I)*B0(I)
!
!     THE FACTOR  8  IS USED INSTEAD OF THE FACTOR  4  TO TAKE INTO
!     ACCOUNT BOTH SIDES OF THE WING
!
      CNNSTA=8.*SNN*COS(ATAN(PPP(KK)))/SREF
      A(KK,IL)=CNNSTA*(A0(I)+B0(I)*ISUMP(I))
      A(KK,IM)=CNNSTA/CREF*(A1(I)+B1(I)*ISUM(I)+C1(I)*ISUMP(I)+D1(I)*ISU&
     &MP2(I))
   12 END DO
   13 END DO
!
!
      DO 14 K=1,LM
      A(IL,K)=A(K,IL)
      A(IM,K)=A(K,IM)
   14 END DO
!
!
!     THE -A- MATRIX STANDS FOR THE DRAG MATRIX -CDV-
!
!
      DO 17 I=1,LM
      RPHI=ATAN(PPP(I))
      CSR=A(I,IL)*SREF/(8.*SNN*COS(RPHI))
      DO 17 J=1,LM
      SPHI=ATAN(PPP(J))
      CSS=A(J,IL)*SREF/(8.*SNN*COS(SPHI))
      YY(1)=YQ(I)-YQ(J)
      YY(2)=YQ(I)+YQ(J)
      ZZ=ZHH(I)-ZHH(J)
      DO 16 K=1,2
      ISIGN=1.
      jsign=1.
      IF (K.EQ.2) GO TO 15
      IF (YY(1).LT.TOLC) JSIGN=-1.
      IF (YY(1).LT.(-TOLC)) ISIGN=-1.
   15 YYY=YY(K)
      CALL DRAGSUB (RPHI,SPHI,YYY,ZZ,SNN,ISIGN,JSIGN,WN(K))
      SPHI=-SPHI
   16 END DO
      A(I,J)=SNN*CSR*CSS*(WN(1)-WN(2))/(PI*SREF)
   17 CONTINUE
!
!
      REWIND 10
      WRITE(10) ((A(I,J),I=1,JM),J=1,JM)
      END FILE 10
      REWIND 20
      DO 19 I=1,LM
      DO 18 J=1,LM
      XTA(J)=A(I,J)+A(J,I)
   18 END DO
      WRITE(20) (XTA(IK),IK=1,LM)
   19 END DO
      END FILE 20
      REWIND 20
      DO 20 I=1,LM
      READ(20,END=20   ) (A(I,J),J=1,LM)
   20 END DO
      CALL GIASOS (3,102,102,JM,JM,A,1,CDRAG,15,XTA,V,IRANK,AP,IERR)
      WRITE(6,34) IRANK,IERR
      WRITE(6,46)
      WRITE(6,48)
      REWIND 10
      READ(10,END=10000) ((A(I,J),I=1,JM),J=1,JM)
10000 CD=0.
      DO 21 I=1,LM
      DO 21 J=1,LM
   21 CD=CD+CDRAG(I)*A(I,J)*CDRAG(J)*2.
      DO 23 I=1,LM
      CRPHI=COS(ATAN(PPP(I)))
      WNII=0.
      DO 22 J=1,LM
      SPHI=ATAN(PPP(J))
      CSS=A(J,IL)*SREF/(8.*SNN*COS(SPHI))
      WNII=WNII+CDRAG(J)*A(I,J)*PI*SREF/(SNN*CSS*CRPHI)
   22 END DO
      WRITE(6,47) Y(I),CDRAG(I),WNII
      IF (I.EQ.NMA(1).AND.IPLAN.EQ.2) WRITE(6,49)
   23 END DO
      DO 26 I=1,IPLAN
      IUZ=NMA(I)
      DO 24 J=1,IUZ
      JJ=J+(I-1)*NMA(1)
      ZZH(J)=Y(JJ)
      XTT(J)=CDRAG(JJ)
   24 END DO
      IUU=NSSWSV(I)
      DO 25 J=1,IUU
      JJ=J+(I-1)*NSSWSV(1)
      CALL FTLUP (YC(JJ),PPP(JJ),+1,IUZ,ZZH,XTT)
   25 END DO
   26 END DO
      JK=0
      DO 28 I=1,IPLAN
      KA=1+(I-1)*NSSWSV(1)
      KB=NSSWSV(1)+(I-1)*NSSWSV(2)
      D=XCFW
      IF (I.EQ.2) D=XCFT
      DO 27 J=KA,KB
      NSCW=TBLSCW(J)
      AI=NSCW*D+0.75
      IMAX=INT(AI)
      DO 27 K=1,NSCW
      JK=JK+1
      E=1.
      IF (K.GT.IMAX) E=(1.-(K-.75)/NSCW)/(1.-D)
      CIR(JK)=PPP(J)*E
   27 CONTINUE
   28 END DO
      WRITE(6,37) CLDES
      NR=0
      DO 29 NV=1,NSSW
      NSCW=TBLSCW(NV)
      NP=NR+1
      NR=NR+NSCW
      PHIPR=ATAN(PHI(NV))*RAD
      IF (NV.EQ.(NSSWSV(1)+1)) WRITE(6,38)
      DO 29 I=NP,NR
      PNPR=PN(I)*BETA
      PVPR=PV(I)*BETA
      PSIPR=ATAN(BETA*TAN(PSI(I)))*RAD
      WRITE(6,39) PNPR,PVPR,Q(I),ZH(NV),S(I),PSIPR,PHIPR,CIR(I)
   29 CONTINUE
      WRITE(6,35)
      WRITE(6,36) CREF,CAVE,STRUE,SREF,BOT,AR,ARTRUE,MACH
      CLTOT=0.
      cmtot=0.
      DO 31 I=1,NSSW
      IF (I.EQ.1) WRITE(6,42)
      IF (I.EQ.(NSSWSV(1)+1)) WRITE(6,43)
      SPANLD=0.
      DO 30 IJ=1,NSCWMIN
      IK=(I-1)*NSCWMIN+IJ
      SPANLD=SPANLD+2.*CIR(IK)
      CLTOT=CLTOT+8.*S(IK)*CIR(IK)/SREF*COS(ATAN(PHI(I)))
      CMTOT=CMTOT+8.*S(IK)*CIR(IK)*PN(IK)*BETA*COS(ATAN(PHI(I)))/(SREF*C&
     &REF)
   30 END DO
      WRITE(6,45) Q(IK),SPANLD
      IF (I.EQ.NSSWSV(1)) CL1=CLTOT
      IF (I.EQ.NSSWSV(1)) CM1=CMTOT
      IF (I.EQ.NSSWSV(1)) WRITE(6,44) CL1,CM1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) CL2=CLTOT-CL1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) CM2=CMTOT-CM1
      IF (I.EQ.NSSW.AND.IPLAN.EQ.2) WRITE(6,44) CL2,CM2
   31 END DO
!
      WRITE(6,40) CLDES,CLTOT,CMTOT,CD
!
   32 CONTINUE
   33 CONTINUE

   34 FORMAT (10X,I10,10X,I10)
   35 FORMAT (////' REF.CHORD',2X,'C AVG.   TRUE AREA ',2X,             &
     & 'REF. AREA',4X,'B/2',4X,'REF. AR',2X,'TRUE AR',3X,'MACH'/)
   36 FORMAT (2F9.5,2F12.4,4F9.5)

   37 FORMAT ('1',///2X,'X',11X,'X',11X,'Y',11X,'Z',12X,'S',5X,         &
     &   'C/4 SWEEP',4X,'DIHEDRAL',3X,'GAMMA/U AT'/                     &
     & 2X,'C/4',9X,'3C/4',42X,'ANGLE',7X,'ANGLE',4X,'CLDES=',F7.4/)
   38 FORMAT (/15X,'SECOND PLANFORM HORSESHOE VORTEX DESCRIPTIONS'/)
   39 FORMAT (17X,8F12.5)
   40 FORMAT (/////15X,'CL DESIGN =',F10.6,5X,'CL COMPUTED=',F10.6/     &
     & 15X, 'CM COMPUTED=',F10.6,5X,'CD V=',F10.6)
   41 FORMAT (/////15X,'CL DES=',F10.6,5X,'CL COMPUTED=',F10.6/         &
     & 15X,'NO PITCHING MOMENT CONSTRAINT',5X,'CD V=',F10.6)
!42    FORMAT (////40X,56HF I R S T    P L A N F O R M    S P A N    L O
!     1A D I N G//60X,1HY,11X,4HCL*C)
!43    FORMAT (////40X,58HS E C O N D    P L A N F O R M    S P A N    L
!     1O A D I N G//60X,1HY,11X,4HCL*C)
!44    FORMAT (//50X,30HCL DEVELOPED ON THIS PLANFORM=,F10.6/
!     1          50X,30HCM DEVELOPED ON THIS PLANFORM=,F10.6)
!45    FORMAT (55X,F10.5,3X,F10.5)
   42 FORMAT (////10X,                                                  &
     & 'F I R S T    P L A N F O R M    S P A N    L O A D I N G'//     &
     & 30X,'Y',11X,'CL*C')
   43 FORMAT (////10X,                                                  &
     & 'S E C O N D    P L A N F O R M    S P A N    L O A D I N G'//   &
     & 30X,'Y',11X,'CL*C')
   44 FORMAT (//20X,'CL DEVELOPED ON THIS PLANFORM=',F10.6/             &
     &          20X,'CM DEVELOPED ON THIS PLANFORM=',F10.6)
   45 FORMAT (25X,F10.5,3X,F10.5)
!46    FORMAT(/////2X, 127HS P A N W I S E    S C A L E    F A C T O R S
!     1   A N D    ( N O R M A L    W A S H ) /( U  *  C O S I N E ( D I
!     2H E D R A L ) )//30X,23HDISTANCE ALONG PLANFORM,5X,7HFACTORS,5X,1
!     3HWN/(U*COS(PHI)))
!47    FORMAT (36X,F10.5,10X,F10.5,3X,F10.5)
   46 FORMAT(////                                                       &
     & '  S P A N W I S E    S C A L E    F A C T O R S   A N D'/       &
     & '  ( N O R M A L    W A S H ) /'                                 &
     & '( U  *  C O S I N E ( D I H E D R A L ) )'//                    &
     & 3X,'DISTANCE ALONG PLANFORM',5X,'FACTORS',5X,'WN/(U*COS(PHI))')
   47 FORMAT (9X,F10.5,10X,F10.5,3X,F10.5)

   48 FORMAT (10X,'FIRST PLANFORM')
   49 FORMAT (10X,'SECOND PLANFORM')


      END Subroutine Circul3


      subroutine ZOCDETM
      IMPLICIT REAL*8(A-H,O-Z)

                                      ! RKN see array defns in SPLINE
      dimension a(400),d1(1),d2(1),KAB(1)

      DIMENSION YY(2), FV(2), FW(2), DZDX(400), XXCC(20), WOU(20)
      DIMENSION X3C4(22), ALOC(22,1), T(41)
      DIMENSION SS(41,1), SS1(41,1), SS2(41,1)
      DIMENSION S2(22,1), S3(22,1), DELY(22,1), H(22), PSUM(41,1)

      COMMON /ALL/ BOT,BETA,PTEST,QTEST,TBLSCW(50),Q(400),PN(400),      &
     &             PV(400),S(400),PSI(400),PHI(50),ZH(50),NSSW,M
      COMMON /TOTHRE/ CIR(400)
      COMMON /CCRRDD/ CHORD(50),XTE(50),TSPAN,TSPANA,KBIT
      COMMON /INSUB23/ APSI,APHI,XX,YYY,ZZ,SNN,TOLC
!
!
!     PART 3 - COMPUTE Z/C VERSUS X/C
!
!
!     THE TOLERANCE SET AT THIS POINT IN THE PROGRAM MAY NEED TO BE
!     CHANGED FOR COMPUTERS OTHER THAN THE CDC 6000 SERIES
!
!
      RAD = 180./(4. * ATAN(1.))
      WRITE(6,12)
      TOLC=(BOT*15.E-05)**2
      IZZ=1
      NNV=TBLSCW(IZZ)
      DO 3 NV=1,M
      DZDX(NV)=0.
      IZ=1
      NNN=TBLSCW(IZ)
      DO 2 NN=1,M
      APHI=ATAN(PHI(IZ))
      APSI=PSI(NN)
      XX=PV(NV)-PN(NN)
      YY(1)=Q(NV)-Q(NN)
      YY(2)=Q(NV)+Q(NN)
      ZZ=ZH(IZZ)-ZH(IZ)
      SNN=S(NN)
      DO 1 I=1,2
      YYY=YY(I)
      CALL INFSUB (BOT,FV(I),FW(I))
      APHI=-APHI
      APSI=-APSI
    1 END DO
      FVN=FW(1)+FW(2)-(FV(1)+FV(2))*PHI(IZZ)
      DZDX(NV)=DZDX(NV)+FVN*CIR(NN)/12.5663704
      IF (NN.LT.NNN.OR.NN.EQ.M) GO TO 2
      IZ=IZ+1
      NNN=NNN+TBLSCW(IZ)
    2 END DO
      IF (NV.LT.NNV.OR.NV.EQ.M) GO TO 3
      IZZ=IZZ+1
      NNV=NNV+TBLSCW(IZZ)
    3 END DO
!
!
!       INTEGRATE DZ/DX TO OBTAIN Z/C VERSUS X/C AT THE VARIOUS Y LOCATI
!
!
      LA=1
      LB=0
      DO 9 I=1,NSSW
      CPHI = COS(ATAN(PHI(I)))
      IN=TBLSCW(I)
      IF (I.EQ.1) GO TO 4
      LA=LA+TBLSCW(I-1)
    4 LB=LB+TBLSCW(I)
      DO 5 J=LA,LB
      N=J-LA+1
      WOU(N)=-DZDX(J)
      XXCC(N)=(N-0.25)/IN
      K=IN+1+LA-J
      X3C4(K)=PV(J)*BETA
    5 ALOC(K,1)=-DZDX(J)
      Y=Q(LA)/BOT
      WRITE(6,10) Q(LA),Y,CHORD(I)
      WRITE(6,13)
      WRITE(6,17) (WOU(IJ),IJ=1,IN)
      WRITE(90,1090) (-WOU(IJ),IJ=1,IN)
 1090 FORMAT(8F10.5)
      WRITE(6,14)
      WRITE(6,17) (XXCC(IJ),IJ=1,IN)
      K1=IN+2
      K2=IN+1
      ALOC(1,1)=ALOC(2,1)
      ALOC(K1,1)=ALOC(K2,1)
      X3C4(1)=XTE(I)
      X3C4(K1)=XTE(I)+CHORD(I)
              
      D1(1)=0.      ! RKN as array(1)
      d2(1)=0.                 ! RKN
      KAB(1)=1                 ! RKN
      DO 6 L=1,41
    6 T(L)=XTE(I)+CHORD(I)*(L-1)*.025
      T(41)=XTE(I)+CHORD(I)   ! make it exact   RLC

      IW=0
!      CALL SPLINE (22,1,41,K1,1,41,X3C4,ALOC,T,A,SS,SS1,SS2,S2,S3,DELY,
!     &    H,IW,D1,D2,1,PSUM)
      CALL SPLINE (22,1,41,K1,1,41,X3C4,ALOC,T,A,SS,SS1,SS2,S2,S3,DELY, &
     &    H,IW,D1,D2,KAB(1),PSUM)      ! RKN
                             
      DO 7 L=1,40
      K=42-L
      J=41-L
    7 PSUM(K,1)=PSUM(J,1)
      PSUM(1,1)=0.
!        print*, psum(41,1),chord(i)  ! RKN
!        pause ! RKN
      ALPIN=ATAN(PSUM(41,1)/CHORD(I))* RAD
      WRITE(*,'('' ALPIN, CHORD AoA(X-Z PLANE)='',F9.4,'' DEG.'')')ALPIN
      WRITE(6,19)ALPIN
      WRITE(6,15)
      WRITE(6,16)
      DO 8 L=1,41
      K=42-L
      XOC=1.+(XTE(I)-T(K))/CHORD(I)
      ZOC=PSUM(K,1)/CHORD(I)
      X=XOC*CHORD(I)
      ZOCPR=ZOC * CPHI
      DLTZPR=PSUM(K,1)*CPHI
      WRITE(6,11)XOC,ZOC,ZOCPR,X,PSUM(K,1),DLTZPR
    8 END DO
      WRITE(6,18)
    9 END DO
   10 FORMAT (5X,'Y=',F10.4,11X,'Y/B/2=',F10.4,11X,'CHORD=',F10.4//)
   11 FORMAT(6F13.4)
   12 FORMAT ('1',25X,'LOCAL ELEVATION DATA'///)
   13 FORMAT (11X,'SLOPES,DZ/DX,AT SLOPE POINTS,FROM FRONT TO REAR'/)
   14 FORMAT (12X,'CORRESPONDING X/C LOCATIONS FROM FRONT TO REAR'/)
   15 FORMAT (////28X,'LOCAL ELEVATION'//)
   16 FORMAT(10X,'X/C',10X,'Z/C',1X,'(Z/C)COS(DIH)',6X,'DELTA X',6X,    &
     & 'DELTA Z',1X,'(DLT Z)COS(DIH)'/)
   17 FORMAT (1X,10F8.4)
   19 FORMAT(///8X,'ALPIN CHORD ANGLE OF ATTACK IN X-Z PLANE =',        &
     & F9.4,'  DEGREES')
   18 FORMAT ('1')

      END Subroutine Zocdetm

      SUBROUTINE INFSUB (BOT,FVI,FWI)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /INSUB23/ PSII,APHII,XXX,YYY,ZZZ,SNN,TOLRNC
      FC=COS(PSII)
      FS=SIN(PSII)
      FT=FS/FC
      FPC=COS(APHII)
      FPS=SIN(APHII)
      FPT=FPS/FPC
      F1=XXX+SNN*FT*FPC
      F2=YYY+SNN*FPC
      F3=ZZZ+SNN*FPS
      F4=XXX-SNN*FT*FPC
      F5=YYY-SNN*FPC
      F6=ZZZ-SNN*FPS
      FFA=(XXX**2+(YYY*FPS)**2+FPC**2*((YYY*FT)**2+(ZZZ/FC)**2-2.*XXX*YY&
     &Y*FT)-2.*ZZZ*FPC*(YYY*FPS+XXX*FT*FPS))
      FFB=(F1*F1+F2*F2+F3*F3)**.5
      FFC=(F4*F4+F5*F5+F6*F6)**.5
      FFD=F5*F5+F6*F6
      FFE=F2*F2+F3*F3
      FFF=(F1*FPC*FT+F2*FPC+F3*FPS)/FFB-(F4*FPC*FT+F5*FPC+F6*FPS)/FFC
!
!
!     THE TOLERANCE SET AT THIS POINT IN THE PROGRAM MAY NEED TO BE
!     CHANGED FOR COMPUTERS OTHER THAN THE CDC 6000 SERIES
!
!
      IF (ABS(FFA).LT.(BOT*15.E-5)**2) GO TO 1
      FVONE=(XXX*FPS-ZZZ*FT*FPC)*FFF/FFA
      FWONE=(YYY*FT-XXX)*FFF/FFA*FPC
      GO TO 2
    1 FVONE=0.
      fwone=0.
!
    2 IF (ABS(FFD).LT.TOLRNC) GO TO 3
      FVTWO=F6*(1.-F4/FFC)/FFD
      FWTWO=-F5*(1.-F4/FFC)/FFD
      GO TO 4
    3 FVTWO=0.
      fwtwo=0.
!
    4 IF (ABS(FFE).LT.TOLRNC) GO TO 5
      FVTHRE=-F3*(1.-F1/FFB)/FFE
      FWTHRE=F2*(1.-F1/FFB)/FFE
      GO TO 6
    5 FVTHRE=0.
      fwthre=0.
!
    6 FVI=FVONE+FVTWO+FVTHRE
      FWI=FWONE+FWTWO+FWTHRE
      RETURN
      END Subroutine Infsub

      SUBROUTINE SPLINE (MNPTS,MNCVS,MMAX,N,NCVS,M,X,Y,T,PROXIN,SS,SS1, &
     &          SS2,S2,S3,DELY,H,IW,D1,D2,KAB,PSUM)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION TH(50), DELH(50,1), CT(50), TH2(50), DELSQH(50)
      DIMENSION ST2(50,1)
      DIMENSION PSUM(MMAX,MNCVS)
      DIMENSION X(MNPTS), Y(MNPTS,MNCVS), T(MMAX), DELY(MNPTS,MNCVS)
      DIMENSION S2(MNPTS,MNCVS), S3(MNPTS,MNCVS)
      DIMENSION SS1(MMAX,MNCVS),SS(MMAX,MNCVS)
      DIMENSION H(MNPTS), SS2(MMAX,MNCVS), PROXIN(MNCVS)
      DIMENSION DELSQY(50), H2(50), C(50), D(50)
      DIMENSION D1(NCVS), D2(NCVS), KAB(NCVS)
      IF (IW) 9,1,9
    1 N1=N-1
      IW=2
      DO 8 K=1,NCVS
        DO 2 I=1,N1
          H(I)=X(I+1)-X(I)
          II=I+1
          DELY(I,K)=(Y(II,K)-Y(I,K))/H(I)
    2   C(I)=H(I)
        DO 3 I=2,N1
          H2(I)=(H(I-1)+H(I))*2.
          DELSQY(I)=(DELY(I,K)-DELY(I-1,K))*6.
    3   CONTINUE
        IF (KAB(K).EQ.0) GO TO 4
        H2(1)=2.*H(1)
        H2(N)=2.*H(N1)
        DELSQY(1)=6.*(DELY(1,K)-D1(K))
        DELSQY(N)=(D2(K)-DELY(N1,K))*6.
        GO TO 5
    4   H2(1)=1.0
        H2(N)=1.0
        C(1)=0.0
        H(N1)=0.0
        DELSQY(1)=0.0
        DELSQY(N)=0.0
    5   CALL TRIMAT (H,H2,C,DELSQY,D,N)
        DO 6 I=1,N
    6     S2(I,K)=D(I)
        H(N1)=C(N1)
        DO 7 I=1,N1
          II=I+1
    7   S3(I,K)=(S2(II,K)-S2(I,K))/H(I)
    8 END DO
    9 CONTINUE
      J=0
   10 J=J+1
      I=1
      IF (T(J)-X(1)) 14,17,11
   11 IF (T(J)-X(N)) 13,15,14
   12 IF (T(J)-X(I)) 16,17,13
   13 I=I+1
      GO TO 12
   14 CONTINUE
      WRITE(6,25) J
      WRITE(6,26) (X(I),I=1,N)
      WRITE(6,26) (Y(I,1),I=1,N)
      WRITE(6,26) (t(i),i=1,m)
      GO TO 19
   15 I=N
   16 CONTINUE
      IW=-I
      I=I-1
   17 DO 18 K=1,NCVS
      HT1=T(J)-X(I)
      II=I+1
      HT2=T(J)-X(II)
      PROD=HT1*HT2
      SS2(J,K)=S2(I,K)+HT1*S3(I,K)
      DELSQS=(S2(I,K)+S2(II,K)+SS2(J,K))/6.
      SS(J,K)=Y(I,K)+HT1*DELY(I,K)+PROD*DELSQS
      SS1(J,K)=DELY(I,K)+(HT1+HT2)*DELSQS+PROD*S3(I,K)/6.0
   18 END DO
   19 CONTINUE
      IF (J.LT.M) GO TO 10
      M1=M-1
      DO 24 K=1,NCVS
      DO 20 I=1,M1
      TH(I)=T(I+1)-T(I)
      II=I+1
      DELH(I,K)=(SS(II,K)-SS(I,K))/TH(I)
      CT(I)=TH(I)
   20 END DO
      DO 21 I=2,M1
      TH2(I)=(TH(I-1)+TH(I))*2.
      DELSQH(I)=(DELH(I,K)-DELH(I-1,K))*6.
   21 END DO
      TH2(1)=1.
      th2(m)=1.
      CT(1)=0
      TH(M1)=0
      DELSQH(1)=0.
      delsqh(m)=0.
      CALL TRIMAT (TH,TH2,CT,DELSQH,D,M)
      DO 22 I=1,M
      ST2(I,K)=D(I)
   22 END DO
      TH(M1)=CT(M1)
      PROXIN(K)=0.0
      DO 23 I=1,M1
      II=I+1
      PROXIN(K)=PROXIN(K)+.5*TH(I)*(SS(I,K)+SS(II,K))-TH(I)**3*(ST2(I,K)&
     &+ST2(II,K))/24.
      PSUM(I,K)=PROXIN(K)
   23 END DO
   24 END DO
      RETURN
!
   25 FORMAT (I4,'TH ARGUMENT OUT OF RANGE')
   26 FORMAT (10F8.3)
      END Subroutine Spline

      SUBROUTINE TRIMAT (A,B,C,D,T,N)
      IMPLICIT REAL*8(A-H,O-Z)
!      DIMENSION A(1), B(1), C(1), D(1), T(1), W(50), SV(50), G(50)
                                                            ! RKN obviou
      DIMENSION A(*),B(*),C(*),D(*),T(*),W(50),SV(50),G(50)
!
!     THIS ROUTINE SOLVES THE TRIDIAGONAL (EXCEPT TWO ELEMENTS)   MATRIX
!
      W(1)=B(1)
      SV(1)=C(1)/B(1)
      G(1)=D(1)/W(1)
      NM1=N-1
      DO 2 K=2,N
      KM1=K-1
      W(K)=B(K)-A(KM1)*SV(KM1)
      IF (K.EQ.N) GO TO 1
      SV(K)=C(K)/W(K)
    1 G(K)=(D(K)-A(KM1)*G(KM1))/W(K)
    2 END DO
      T(N)=G(N)
      DO 3 K=1,NM1
      KK=N-K
      T(KK)=G(KK)-SV(KK)*T(KK+1)
    3 END DO
      RETURN
      END Subroutine Trimat

      subroutine DUMMY
      IMPLICIT REAL*8(A-H,O-Z)
      URDUMB=0.
      STOP
      END Subroutine Dummy



      SUBROUTINE GIASOS(IOP,MD,ND,M,N,A,NOS,B,IAC,Q,V,IRANK,APLUS,IERR)
!     F5.2
!***********************************************************************
!
! PURPOSE     TO COMPUTE THE SINGULAR VALUE DECOMPOSITION OF A REAL M X
!             N MATRIX A BY PERFORMING THE A=UQV (T) FACTORIZATION,
!             WITH OPTIONS FOR THE RANK,THE SINGULAR VALUES, AN
!             ORTHOGONAL BASIS FOR THE HOMOGENOUS SOLUTION , AND THE
!             PSEUDO INVERSE OF A AND A LEAST SQUARES SOLUTION FOR THE
!             MATRIX PROBLEM AX=B.
!
! USE
!
!     CALL GIASOS(IOP,MD,ND,M,N,A,NOS,B,IAC,Q,V,IRANK,APLUS,IERR)
!
!       IOP   OPTION CODE
!
!         IOP=1  RANK WILL BE RETURNED TO THE CALLING PROGRAM IN
!                IRANK. THE ORDERED SINGULAR VALUES WILL BE RETURNED IN
!
!         IOP=2  IN ADDITION TO THE OPTIONS IN IOP=1 AN ORTHOGONAL
!                BASIS FOR THE HOMOGENOUS SOLUTION WILL BE RETURNED IN
!                THE LAST N-IRANK COLUMNS OF THE V MATRIX.  THE U
!                TRANSFORMATION MATRIX WILL BE RETURNED IN MATRIX A.
!
!         IOP=3  SAME AS IOP=2.  IN ADDITION THE LEAST SQUARES SOLUTIONS
!                WILL BE RETURNED IN MATRIX B.
!
!         IOP=4  SAME AS IOP=2.  IN ADDITION THE PSEUDO INVERSE WILL BE
!                RETURNED IN APLUS.
!
!         IOP=5  SAME AS IOP=4.  IN ADDITION THE LEAST SQUARES SOLUTIONS
!                WILL BE RETURNED IN MATRIX B.
!
!       MD    INPUT INTEGER SPECIFING THE MAXIMUM ROW DIMENSION FOR A.
!
!       ND    INPUT INTEGER SPECIFING THE MAXIMUM ROW DIMENSION FOR V
!
!       M     INPUT INTEGER SPECIFING THE NUMBER OF ROWS IN A.
!
!       N     INPUT INTEGER SPECIFING THE NUMBER OF COLUMNS IN A.
!
!       A     AN INPUT/OUTPUT TWO-DIMENSIONAL REAL ARRAY WITH ROW DIMEN-
!             SION MD AND COLUMN DIMENSION AT LEAST N.  ON INPUT, A
!             CONTAINS THE INPUT MATRIX A WHICH IS DESTROYED.  ON OUTPUT
!             A CONTAINS THE ISOMETRIC MATRIX U EXCEPT WHEN IOP=1.
!
!       NOS   NUMBER OF RIGHT HAND SIDES TO BE SOLVED.
!
!       B     AN INPUT/OUTPUT TWO-DIMENSIONAL ARRAY(MD X NOS) USED FOR
!             IOP=3 OR IOP=5.  ON INPUT,B CONTAINS THE RIGHT  HAND SIDES
!             FOR THE SYSTEM OF EQUATIONS TO BE SOLVED.  ON OUTPUT, B
!             CONTAINS THE LEAST SQUARES SOLUTIONS FOR THE EQUATIONS.
!             B NEED NOT BE DIMENSIONED FOR OTHER OPTIONS.
!
!       IAC   AN INPUT INTEGER SPECIFING THE NUMBER OF DECIMAL DIGITS OF
!             ACCURACY  IN THE ELEMENTS OF THE INPUT A MATRIX.  THIS
!             VALUE IS USED TO DETERMINE THE TEST FOR ZERO SINGULAR
!             VALUES, THUS DETERMINING RANK.
!
!             IF IAC.GT.13  THE ZERO TEST WILL BE COMPUTED USING THE
!                           E-NORM OF A MULTIPLIED BY 2**(-48) .
!
!             IF IAC.LT.13  THE ZERO TEST WILL BE COMPUTED USING THE
!                           E-NORM OF A MULTIPLIED BY 10**(-IAC2) ,
!                           WHERE IAC2 = MAX0(IAC,6)
!
!       Q     A ONE DIMENSIONAL ARRAY OF SIZE N WHICH WILL CONTAIN THE
!             ORDERED SINGULAR VALUES.
!
!       V     AN OUTPUT TWO DIMENSIONAL ARRAY (ND X N) WHICH CONTAINS
!             ORTHOGONAL V MATRIX EXCEPT WHEN     IOP=1.  THE V MATRIX
!             UPON RETURN FROM THE SUBROUTINE WILL CONTAIN AN ORTHOGONAL
!             BASIS FOR THE HOMOGENOUS SOLUTIONS IN THE LAST N-IRANK
!             COLUMNS FOR ALL OPTIONS EXCEPT 1 .
!
!       IRANK RANK OF THE MATRIX A (OUTPUT)
!
!       APLUS AN OUTPUT TWO DIMENSIONAL ARRAY (ND X M) WHICH CONTAINS
!             THE PSEUDO INVERSE OF MATRIX A. IF IOP DOES NOT EQUAL
!             4 OR 5 THIS ARRAY NEED NOT BE DIMENSIONED BUT A DUMMY
!             PARAMETER MUST APPEAR IN THE CALLING SEQUENCE.
!
!       IERR  ERROR INDICATOR
!
!             K=0    IMPLIES NORMAL RETURN
!
!             K.GT.0 IMPLIES KTH SINGULAR VALUE NOT FOUND AFTER 30 ITER.
!             K=-1   IMPLIES THAT USING THE GIVEN IAC(ACCURACY REQUIRE-
!                    MENT), THIS MATRIX IS CLOSE TO A MATRIX WHICH IS OF
!                    LOWER RANK THAN IRANK AND IF THE ACCURACY IS
!                    REDUCED THE RANK OF THE MATRIX MAY ALSO BE REDUCED.
!
!  LATEST REVISION OCTOBER 1, 1980
!  COMPUTER SCIENCES CORP., HAMPTON, VA.
!
      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!
      LOGICAL WITHU,WITHV
      DIMENSION A(MD,N),        V(ND,N),Q(N),E(256)
      DIMENSION B(MD,NOS),APLUS(ND,M)
!
      TOL=1.0E-30
      SIZE=0.0
      NP1=N+1
!
!     COMPUTE THE E-NORM OF MATRIX A AS ZERO TEST FOR SINGULAR VALUES
!
      SUM=0.0
      DO 500 I=1,M
      DO 500 J=1,N
  500 SUM = SUM + A(I,J)**2
      ZTEST = SQRT(SUM)
      IF (IAC.GT.13)  GO TO 505
      IF (IAC.LT.6) IAC = 6
      ZTEST = ZTEST*10.**(-IAC)
      GO TO 510
  505 ZTEST = ZTEST * 2.0**(-48)
!
      if (ztest.lt.1.2e-38) stop ' out of bounds in giasos '
  510 IF (IOP.NE.1 ) GO TO 515
      WITHU=.FALSE.
      WITHV=.FALSE.
      GO TO 520
  515 WITHU=.TRUE.
      WITHV=.TRUE.
  520 CONTINUE
      G = 0.0
      X = 0.0
      DO 30 I = 1,N
!
!     HOUSEHOLDER REDUCTION TO BIDIAGONAL FORM.
!
      E(I) = G
      S = 0.0
      L = I+1
!
!     ANNIHILATE THE I-TH COLUMN BELOW DIAGONAL.
!
      DO 3 J = I,M
    3 S = S + A(J,I)**2
      G = 0.0
      IF(S .LT. TOL)    GO TO 10
      G = SQRT(S)
      F = A(I,I)
      IF(F .GE. 0.0)   G = -G
      H = F*G -S
      A(I,I) = F-G
      IF(I .EQ. N)   GO TO 10
        DO 9 J = L,N
        S = 0.0
        DO 7 K = I,M
    7   S = S +A(K,I)*A(K,J)
        F = S/H
        DO 8 K = I,M
    8   A(K,J) =A(K,J) + F*A(K,I)
    9   CONTINUE
   10 Q(I) = G
      IF(I .EQ. N)   GO TO 20
!
!     ANNIHILATE THE I-TH ROW TO RIGHT OF SUPER-DIAG.
!
      S = 0.0
      DO 11 J = L,N
   11 S = S + A(I,J)**2
      G = 0.0
      IF (S .LT. TOL)    GO TO 20
        G = SQRT(S)
        F = A(I,I+1)
        IF(F .GE. 0.0)   G = -G
        H = F*G -S
        A(I,I+1) = F - G
        DO 15 J = L,N
   15   E(J) = A(I,J)/H
        DO 19 J = L,M
        S = 0.0
        DO 16 K = L,N
   16   S = S + A(J,K) * A(I,K)
        DO 17 K = L,N
   17   A(J,K) = A(J,K) + S*E(K)
   19   CONTINUE
   20 Y = ABS(Q(I)) + ABS(E(I))
      IF(Y .GT. SIZE)    SIZE = Y
   30 END DO
      IF(.NOT. WITHV)   GO TO 41
!
!     ACCUMULATION OF RIGHT TRANSFORMATIONS.
!
      DO 40 II = 1,N
      I = NP1 - II
      IF(I .EQ. N)   GO TO 39
      IF(G .EQ. 0.0)   GO TO 37
      H = A(I,I+1)*G
      DO 32 J = L,N
   32 V(J,I) = A(I,J)/H
      DO 36 J = L,N
      S = 0.0
      DO 33 K = L,N
   33 S = S + A(I,K)*V(K,J)
      DO 34 K = L,N
   34 V(K,J) = V(K,J) + S*V(K,I)
   36 END DO
   37 DO 38 J = L,N
      V(I,J) = 0.0
   38 V(J,I) = 0.0
   39 V(I,I) = 1.0
      G = E(I)
   40 L = I
   41 CONTINUE
      IF(.NOT. WITHU)   GO TO 53
!
!     ACCUMULATION OF LEFT TRANSFORMATIONS.
!
      DO 52 II = 1,N
      I = NP1 -II
      L = I + 1
      G = Q(I)
      IF(I .EQ. N)   GO TO 43
      DO 42 J = L,N
   42 A(I,J) = 0.0
   43 CONTINUE
      IF(G .EQ. 0.0)   GO TO 49
      IF(I .EQ. N)   GO TO 47
        H = A(I,I)*G
        DO 46 J = L,N
        S = 0.0
        DO 44 K = L,M
   44   S = S + A(K,I)*A(K,J)
        F = S/H
        DO 45 K = I,M
   45   A(K,J) = A(K,J) +  F*A(K,I)
   46   CONTINUE
   47 DO 48 J = I,M
   48 A(J,I) = A(J,I)/G
      GO TO 51
   49 DO 50 J = I,M
   50 A(J,I) = 0.0
   51 A(I,I) = A(I,I) + 1.0
   52 END DO
   53 CONTINUE
!
!     DIAGONALIZATION OF BIDIAGONAL FORM.
!
      DO 100 KK=1,N
        K=NP1-KK
        ITCNT=0
        KP1=K+1
!
!      TEST F SPLITTING.
!
   59   CONTINUE
        DO 60 LL=1,K
          L=KP1-LL
          IF((SIZE+ABS(E(L))).EQ.SIZE)    GO TO 64
          LM1=L-1
          IF((SIZE+ABS(Q(LM1))).EQ.SIZE)    GO TO 61
   60   CONTINUE
!
!      CANCELLATION OF E(L) IF L .GT. 1.
!
   61   C=0.0
        S=1.0
        L1=L-1
        DO 63 I=L,K
          F=S*E(I)
          E(I)=C*E(I)
          IF((SIZE+ABS(F)).EQ.SIZE)   GO TO 64
          G=Q(I)
          Q(I)=SQRT(F*F+G*G)
          H=Q(I)
          IF(H.NE.0.0)GO TO 611
          C=0.0
          S=1.0
          GO TO 612
  611     C=G/H
          S=-F/H
  612     IF(.NOT.WITHU)GO TO 63
            DO 62 J=1,M
              Y=A(J,L1)
              Z=A(J,I)
              A(J,L1)=Y*C+Z*S
              A(J,I)= -Y*S+Z*C
   62       CONTINUE
!
   63   CONTINUE
!
!      TEST F CONVERGENCE.
!
   64 Z=Q(K)
      IF(L.EQ.K)   GO TO 75
      IF(ITCNT .LE. 30)   GO TO 65
      IERR = KK
      RETURN
   65 ITCNT = ITCNT + 1
!
!       SHIFT FROM LOWER 2X2.
!
        X=Q(L)
        Y=Q(K-1)
      G=E(K-1)
      H=E(K)
      F=((Y-Z)*(Y+Z)+(G-H)*(G+H))/(2.0*H*Y)
      G=SQRT(F*F+1.0)
      IF(F.LT.0.0)  G=-G
      F = ((X-Z)*(X+Z)+H*(Y/(F+G)-H))/X
!
!
!     NEXT QR TRANSFORMATION.
!
      C=1.0
      S=1.0
      LP1=L+1
      DO 73 I=LP1,K
        G=E(I)
        Y=Q(I)
        H=S*G
        G=C*G
        Z=SQRT(F*F+H*H)
        E(I-1)=Z
        IF(Z.NE.0.0)GO TO 66
        C=0.0
        S=1.0
        GO TO 67
   66   C=F/Z
        S=H/Z
   67   F=X*C+G*S
        G=-X*S+G*C
        H=Y*S
        Y=Y*C
        IF(.NOT.WITHV)   GO TO 70
          DO 68 J=1,N
            X=V(J,I-1)
            Z=V(J,I)
            V(J,I-1)=X*C+Z*S
            V(J,I)=-X*S+Z*C
   68     CONTINUE
!
   70   Z=SQRT(F*F+H*H)
        Q(I-1)=Z
        IF(Z.NE.0.0)GO TO 71
        C=0.0
        S=1.0
        GO TO 711
   71   C=F/Z
        S=H/Z
  711   F=C*G+S*Y
        X=-S*G+C*Y
        IF(.NOT.WITHU)   GO TO 73
          DO 72 J=1,M
            Y=A(J,I-1)
            Z=A(J,I)
            A(J,I-1)=Y*C+Z*S
            A(J,I)=-Y*S+Z*C
   72     CONTINUE
!
!
   73   E(L) = 0.0
        E(K)=F
        Q(K)=X
        GO TO 59
!
!       CONVERGENCE.
!
   75   CONTINUE
        IF(Z.GE.0.0)   GO TO 100
          Q(K)=-Z
          IF(.NOT.WITHV)   GO TO 100
          DO 76 J=1,N
   76     V(J,K)=-V(J,K)
  100   CONTINUE
!
      IERR = 0
      DO 280  II=2,N
      I= II-1
      K=I
      P=Q(I)
!
      DO 250  J=II,N
      IF (Q(J).LE.P) GO TO 250
      K=J
      P=Q(J)
  250 END DO
!
      IF (K.EQ.I) GO TO 280
      Q(K) = Q(I)
      Q(I) = P
!
      IF(IOP.EQ.1) GO TO 280
!
      DO 260  J=1,N
      P= V(J,I)
      V(J,I)= V(J,K)
      V(J,K)= P
  260 END DO
!
      DO 270  J=1,M
      P = A(J,I)
      A(J,I)= A(J,K)
      A(J,K)= P
  270 END DO
!
  280 END DO
!
      J=N
  290 IF (Q(J).GT.ZTEST.OR.J.EQ.0) GO TO 300
      Q(J)=0.0
      J=J-1
      GO TO 290
  300 IRANK =J
      IF (IRANK .EQ. 0) RETURN
      TEMP = ZTEST/Q(J)
      IF (TEMP.GT..0625)    IERR=-1
!
      IF (IOP.LT. 3)  RETURN
      IF(IOP.GT.3) GO TO 170
      DO 160  L=1,NOS
      DO 130  J=1,IRANK
      SUM=0.0
      DO 120  I=1,M
  120 SUM =SUM + A(I,J)*B(I,L)
  130 E(J)= SUM/Q(J)
!
      DO 150  K=1,N
      SUM=0.0
      DO 140  I=1,IRANK
  140 SUM =SUM  + V(K,I)*E(I)
  150 B(K,L)=SUM
  160 END DO
      RETURN
  170 DO 200  J=1,M
      DO 190  I=1,N
      SUM=0.0
      DO 180  K=1,IRANK
  180 SUM =SUM + V(I,K)*A(J,K)/Q(K)
  190 APLUS(I,J)= SUM
  200 END DO
!
      IF( IOP .EQ.4) RETURN
      DO 230  K=1,NOS
      DO 220  I=1,N
      SUM=0.0
      DO 210  J=1,M
  210 SUM=SUM+ APLUS(I,J)*B(J,K)
  220 E(I)=SUM
      DO 225  I=1,N
  225 B(I,K)=E(I)
  230 END DO
      RETURN
      END Subroutine Giasos

