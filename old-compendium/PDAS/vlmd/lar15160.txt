LAR-15160
VLMD - VORTEX-LATTICE CODE FOR DETERMINATION OF MEAN CAMBER SURFACE
FOR TRIMMED NONCOPLANER PLANFORMS WITH MINIMUM VORTEX DRAG
 
This program represents a subsonic aerodynamic method for determining
the mean camber surface of trimmed noncoplaner planforms with minimum
vortex drag.  With this program, multiple surfaces can be designed
together to yield a trimmed configuration with minimum induced drag at
some specified lift coefficient.  The method uses a vortex-lattice and
overcomes previous difficulties with chord loading specification.
A Trefftz plane analysis is used to determine the optimum span loading
for minimum drag.  The program then solves for the mean camber surface
of the wing associated with this loading. Pitching-moment or
root-bending-moment constraints can be employed at the design lift
coefficient.  Sensitivity studies of vortex-lattice arrangements have
been made with this program and comparisons with other theories show
generally good agreement. The program is very versatile and has been
applied to isolated wings, wing-canard configurations, a tandem wing,
and a wing-winglet configuration. 

The design problem solved with this code is essentially an optimization one.
A subsonic vortex-lattice is used to determine the span load
distribution(s) on bent lifting line(s) in the Trefftz plane.
A Lagrange multiplier technique determines the required loading which is
used to calculate the mean camber slopes, which are then integrated to
yield the local elevation surface.  The problem of determining the
necessary circulation matrix is simplified by having the chordwise shape
of the bound circulation remain unchanged across each span, though the
chordwise shape may vary from one planform to another.  The circulation
matrix is obtained by calculating the spanwise scaling of the chordwise
shapes.  A chordwise summation of the lift and pitching-moment is utilized
in the Trefftz plane solution on the assumption that the trailing wake
does not roll up and that the general configuration has specifiable chord
loading shapes.

VLMD is written in FORTRAN for IBM PC series and compatible computers
running MS-DOS.  This program requires 360K of RAM for execution.
The Ryan McFarland FORTRAN compiler and PLINK86 are required to recompile
the source code; however, a sample executable is provided on the diskette.
The standard distribution medium for VLMD is a 5.25 inch 360K MS-DOS format
diskette.  VLMD was originally developed for use on CDC 6000 series
computers in 1976.  It was originally ported to the IBM PC in 1986, and,
after minor modifications, the IBM PC port was released in 1993.

COSMIC, and the COSMIC logo are registered trademarks of the
National Aeronautics and Space Administration. All other brands and 
product names are the trademarks of their respective holders.

LANGUAGE: FORTRAN 77

MACHINE REQUIREMENTS: IBM PC Series

PROGRAM SIZE: Approximately 2863 source statements

DISTRIBUTION MEDIA: 5.25 Inch IBM PC DOS Format Diskette

PROGRAM NUMBER -LAR-15160

DOMESTIC - DOCUMENTATION PRICE $21.00 - PROGRAM PRICE  $150.00

INTERNATIONAL - DOCUMENTATION PRICE $42.00 - PROGRAM PRICE  $300.00

