!+
      PROGRAM VortexLatticeGeometry
!   --------------------------------------------------------------------
!     PURPOSE -
!
!     AUTHORS - John E. Lamar, NASA Langley Research Center
!               Jeanne M. Peters, NASA Langley Research Center
!               Ralph L. Carmichael, Public Domain Aeronautical Software
!
!     REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
!  June76   *    JEL   Publication of NASA TN D-8090
!              JEL&JMP Release of COSMIC program L-15160
! 23Mar96  0.1   RLC   Minor mods to adapt to PC environment
! 25May96  0.2   RLC   MAKING IMPLICIT REAL*8
! 30Dec99  1.0   RLC   Reordered commons: 8-bytes first, 4-bytes last
!
!     NOTES-
!
      IMPLICIT REAL*8(A-H,O-Z)
!***********************************************************************
!     C O N S T A N T S                                                *
!***********************************************************************
      CHARACTER GREETING*60, AUTHOR*41, AUTHOR2*41, VERSION*30,         &
     &          FAREWELL*60
      PARAMETER (GREETING='geom - front end to VLMD               ')
      PARAMETER (AUTHOR='J.Peters, J.Lamar,NASA; R.Carmichael,PDAS')
      PARAMETER (AUTHOR2='FREE-FORMAT input by Dr R K NANGIA, 2002')
      PARAMETER (VERSION='1.0 (30Dec99)' )
      PARAMETER (FAREWELL=                                              &
     & ' File GEOM.OUT added to the directory. READY for NEXT STAGE')
!***********************************************************************
!     V A R I A B L E S                                                *
!***********************************************************************
      CHARACTER fileName*80
!***********************************************************************
!     A R R A Y S                                                      *
!***********************************************************************
      DIMENSION XREF(25), YREF(25), SAR(25), A(25), RSAR(25), X(25)
      DIMENSION Y(25), BOTSV(2), SA(2), VBORD(51), SPY(50,2), KFX(2)
      DIMENSION IYL(50,2), IYT(50,2)

!***********************************************************************
!     C O M M O N   B L O C K   D E F I N I T I O N S                  *
!***********************************************************************

      COMMON /ALL/ BOT,BETA,PTEST,QTEST,TBLSCW(50),Q(400),PN(400),      &
     & PV(400),S(400),PSI(400),PHI(50),ZH(50),NSSW,M

      COMMON /MAINONE/ TOTAL,AAN(2),XS(2),YS(2),XREG(25,2),YREG(25,2),  &
     & AREG(25,2),DIH(25,2),XX(25,2),YY(25,2),AS(25,2),TTWD(25,2),      &
     & AN(2),ZZ(25,2),IFLAG,ICODEOF,KFCTS(2),MCD(25,2),MMCD(25,2)

      COMMON /ONETHRE/ TWIST(2),CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,   &
     & RTCDHT(2),CONFIG,PLAN,MACH,SSWWA(50),NSSWSV(2),MSV(2),KBOT,IPLAN

      COMMON /CCRRDD/ CHORD(50),XTE(50),TSPAN,TSPANA,KBIT

      REAL*8 MACH
      CHARACTER PRTCON*10

!-----------------------------------------------------------------------
      write(*,'('' ======================================='')')
      WRITE(*,*) GREETING
      WRITE(*,*) AUTHOR
      WRITE(*,*) AUTHOR2
      WRITE(*,*) 'Version '//VERSION

 9998 write(*,'('' ======================================='')')
      write(*,*)
      WRITE(*,'(A)')                                                    &
     & ' Please ENTER name of the INPUT file: c*.inp etc...'
      READ(*,'(A)') fileName   ! RKN comment out to try same input etc.                                 
      IF (fileName .EQ. ' ') STOP       ! RKN

      OPEN (UNIT=5, FILE=fileName, STATUS='OLD', IOSTAT=k)
      IF (k .NE. 0) THEN
        WRITE(*,*) 'Unable to open this file for input. Try again'
        GO TO 9998
      END IF

      OPEN (UNIT=6, FILE='geom.out', STATUS='REPLACE', IOSTAT=k)
      IF (k .NE. 0) STOP 'Unable to open output file'

      open (unit=50, status='scratch', form='formatted',                &
     &   access='sequential')
      open (unit=25, form='unformatted',access='sequential',            &
     &   file='fort25')

      write(6,*) ' geomtry'
!
      REWIND 50
!
!       PART ONE - GEOMETRY COMPUTATION
!
!                  SECTION ONE - INPUT OF REFERENCE WING POSITION
!
!
      ICODEOF=0
      total=0.
      ptest=0.
      qtest=0.
      twist(1)=0.
      twist(2)=0.
      IF (TOTAL.EQ.0.) RTCDHT(1)=0.0
      if (total.eq.0.) rtcdht(2)=0.0
      YTOL=1.E-10
      AZY=1.E+13
      PIT=1.5707963
      RAD=57.29578
      IF (TOTAL.GT.0.) GO TO 7
!
!
!     SET PLAN EQUAL TO 1. FOR A WING ALONE COMPUTAION - EVEN FOR A
!     VARIABLE SWEEP WING
!     SET PLAN EQUAL TO 2. FOR A WING - TAIL COMBINATION
!
!     SET TOTAL EQUAL TO THE NUMBER OF SETS
!       OF GROUP TWO DATA PROVIDED
!
!      READ(5,98,END=93) PLAN,TOTAL,CREF,SREF,XLOCTN   !RKN remove data
      READ(5,*,END=93) PLAN,TOTAL,CREF,SREF,XLOCTN
!      write(*,98) PLAN,TOTAL,CREF,SREF,XLOCTN
      write(*,'(5f10.4,'' plan total cref sref xloctn'')')              &
     &            PLAN,TOTAL,CREF,SREF,XLOCTN
    1 IPLAN=PLAN
!
!
!     SET AAN(IT) EQUAL TO THE MAXIMUM NUMBER OF CURVES REQUIRED TO
!     DEFINE THE PLANFORM PERIMETER OF THE (IT) PLANFORM.
!
!     SET RTCDHT(IT) EQUAL TO THE ROOT CHORD HEIGHT OF THE LIFTING
!     SURFACE (IT),WHOSE PERIMETER POINTS ARE BEING READ IN, WITH
!     RESPECT TO THE WING ROOT CHORD HEIGHT
!
      WRITE(6,96)
!      WRITE(*,96) !RKN
      DO 6 IT=1,IPLAN
!      READ(5,98,END=10001) AAN(IT),XS(IT),YS(IT),RTCDHT(IT) !RKN remove
      READ(5,*,END=10001) AAN(IT),XS(IT),YS(IT),RTCDHT(IT)
      write(*,'(I4,4F10.4,''  IT AAN XS YS RTCDHT'')')                  &
     &                      IT,AAN(IT),XS(IT),YS(IT),RTCDHT(IT)
10001 N=AAN(IT)
      N1=N+1
      MAK=0
      IF (IPLAN.EQ.1) PRTCON=' '
      IF (IPLAN.EQ.2.AND.IT.EQ.1) PRTCON='    FIRST '
      IF (IPLAN.EQ.2.AND.IT.EQ.2) PRTCON='   SECOND '
      WRITE(6,97) PRTCON,N,RTCDHT(IT),XS(IT),YS(IT)
      WRITE(6,109)
!       WRITE(*,109) RKN
      DO 5 I=1,N1
!      READ(5,98,END=10002) XREG(I,IT),YREG(I,IT),DIH(I,IT),AMCD ! RKN r
      READ(5,*,END=10002) XREG(I,IT),YREG(I,IT),DIH(I,IT),AMCD
      write(*,'(2i4,4f10.4,''  I IT XREG YREG DIH AMCD'')')             &
     &                       I,IT,XREG(I,IT),YREG(I,IT),DIH(I,IT),AMCD
10002 XREG(I,IT)=XREG(I,IT)-XLOCTN
      MCD(I,IT)=AMCD
      IF (I.EQ.1) CYCLE   !!! GO TO 5
      IF (MAK.NE.0.OR.MCD(I-1,IT).NE.2) GO TO 2
      MAK=I-1
    2 IF (ABS(YREG(I-1,IT)-YREG(I,IT)).LT.YTOL) GO TO 3
      AREG(I-1,IT)=(XREG(I-1,IT)-XREG(I,IT))/(YREG(I-1,IT)-YREG(I,IT))
      ASWP=ATAN(AREG(I-1,IT))*RAD
      GO TO 4
    3 YREG(I,IT)=YREG(I-1,IT)
      AREG(I-1,IT)=AZY
      ASWP=90.
    4 J=I-1
!
!     WRITE PLANFORM PERIMETER POINTS AND ANGLES
!
      WRITE(6,106) J,XREG(J,IT),YREG(J,IT),ASWP,DIH(J,IT),MCD(J,IT)
      DIH(J,IT)=TAN(DIH(J,IT)/RAD)
    5 END DO
      KFCTS(IT)=MAK
      WRITE(6,106) N1,XREG(N1,IT),YREG(N1,IT)
    6 END DO

!
!                        PART 1 - SECTION 2
!         READ GROUP 2 DATA AND COMPUTE DESIRED WING POSITION
!
!
!     SET SA(1),SA(2) EQUAL TO THE SWEEP ANGLE,IN DEGREES, FOR THE FIRST
!     CURVE(S) THAT CAN CHANGE SWEEP FOR EACH PLANFORM

! 7     READ(5,105,END=93) CONFIG,SCW,VIC,MACH,CLDES,SA(1),SA(2) ! RKN
! 105  FORMAT(BZ,5F5.1,2F10.4)
    7 READ(5,*,END=93) CONFIG,SCW,VIC,MACH,CLDES,SA(1),SA(2)
      write(*,'(4f5.1,3f10.4,'' CONFIG SCW VIC MACH CLDES SA(1),SA(2)'' &
     &)')CONFIG,SCW,VIC,MACH,CLDES,SA(1),SA(2)

      WRITE(6,99) CONFIG
    8 IF (PTEST.NE.0..AND.QTEST.NE.0.) GO TO 95
      IF (SCW.EQ.0.) GO TO 10
      DO 9 I=1,50
    9 TBLSCW(I)=SCW
      GO TO 11
! all of this code between here and statement number 11 is
! undocumented. The user guide says clearly that SCW must not
! be zero. The only way to get to 10 is if SCW==0
! 10    READ(5,98,END=10004) STA   ! RKN no format
   10 READ(5,*,END=10004) STA
      write(*,'(f10.4,''  STA'')') STA
10004 NSTA=STA
!      READ(5,98,END=11   ) (TBLSCW(I),TBLSCW(I+1),TBLSCW(I+2),TBLSCW(I+
!     13),TBLSCW(I+4),TBLSCW(I+5),TBLSCW(I+6),TBLSCW(I+7),I=1,NSTA,8)

      READ(5,*,END=11) (TBLSCW(I),TBLSCW(I+1),TBLSCW(I+2),TBLSCW(I+3),  &
     &    TBLSCW(I+4),TBLSCW(I+5),TBLSCW(I+6),TBLSCW(I+7),I=1,NSTA,8)
      write(5,98)(TBLSCW(I),TBLSCW(I+1),TBLSCW(I+2),TBLSCW(I+3),        &
     &    TBLSCW(I+4),TBLSCW(I+5),TBLSCW(I+6),TBLSCW(I+7),I=1,NSTA,8)

   11 DO 37 IT=1,IPLAN
      N=AAN(IT)
      N1=N+1
      DO 12 I=1,N
      XREF(I)=XREG(I,IT)
      YREF(I)=YREG(I,IT)
      A(I)=AREG(I,IT)
      RSAR(I)=ATAN(A(I))
      IF (A(I).EQ.AZY) RSAR(I)=PIT
   12 END DO
      XREF(N1)=XREG(N1,IT)
      YREF(N1)=YREG(N1,IT)
      IF (KFCTS(IT).GT.0) GO TO 13
      K=1
      SA(IT)=RSAR(1)*RAD
      GO TO 14
   13 K=KFCTS(IT)
   14 WRITE(6,102) K,SA(IT),IT
      SB=SA(IT)/RAD
      IF (ABS(SB-RSAR(K)).GT.(.1/RAD)) GO TO 17
!     REFERENCE PLANFORM COORDINATES ARE STORED UNCHANGED FOR WINGS
!            WITHOUT CHANGE IN SWEEP
      DO 16 I=1,N
      X(I)=XREF(I)
      Y(I)=YREF(I)
      IF (RSAR(I).EQ.PIT) GO TO 15
      A(I)=TAN(RSAR(I))
      GO TO 16
   15 A(I)=AZY
   16 SAR(I)=RSAR(I)
      X(N1)=XREF(N1)
      Y(N1)=YREF(N1)
      GO TO 35
!
!     CHANGES IN WING SWEEP ARE MADE HERE
!
   17 IF (MCD(K,IT).NE.2) GO TO 94
      KA=K-1
      DO 18 I=1,KA
      X(I)=XREF(I)
      Y(I)=YREF(I)
   18 SAR(I)=RSAR(I)
!     DETERMINE LEADING EDGE INTERSECTION BETWEEN FIXED AND VARIABLE
!            SWEEP WING SECTIONS
      SAR(K)=SB
      A(K)=TAN(SB)
      SAI=SB-RSAR(K)
      X(K+1)=XS(IT)+                                                    &
     &  (XREF(K+1)-XS(IT))*COS(SAI)+(YREF(K+1)-YS(IT))*SIN(SAI)
      Y(K+1)=YS(IT)+                                                    &
     &  (YREF(K+1)-YS(IT))*COS(SAI)-(XREF(K+1)-XS(IT))*SIN(SAI)
      IF (ABS(SB-SAR(K-1)).LT.(.1/RAD)) GO TO 19
      Y(K)=X(K+1)-X(K-1)-A(K)*Y(K+1)+A(K-1)*Y(K-1)
      Y(K)=Y(K)/(A(K-1)-A(K))
      X(K)=A(K)*X(K-1)-A(K-1)*X(K+1)+A(K-1)*A(K)*(Y(K+1)-Y(K-1))
      X(K)=X(K)/(A(K)-A(K-1))
      GO TO 20
!     ELIMINATE EXTRANEOUS BREAKPOINTS
   19 X(K)=XREF(K-1)
      Y(K)=YREF(K-1)
      SAR(K)=SAR(K-1)
   20 K=K+1
!     SWEEP THE BREAKPOINTS ON THE VARIABLE SWEEP PANEL
!        (IT ALSO KEEPS SWEEP ANGLES IN FIRST OR FOURTH QUADRANTS)
   21 K=K+1
      SAR(K-1)=SAI+RSAR(K-1)
   22 IF (SAR(K-1).LE.PIT) GO TO 23
      SAR(K-1)=SAR(K-1)-3.1415927
      GO TO 22
   23 IF (SAR(K-1).GE.(-PIT)) GO TO 24
      SAR(K-1)=SAR(K-1)+3.1415927
      GO TO 23
   24 IF ((SAR(K-1)).LT..0) GO TO 25
      IF (SAR(K-1)-PIT) 28,26,26
   25 IF (SAR(K-1)+PIT) 27,27,28
   26 A(K-1)=AZY
      GO TO 29
   27 A(K-1)=-AZY
      GO TO 29
   28 A(K-1)=TAN(SAR(K-1))
   29 KK=MCD(K,IT)
      GO TO (31,30), KK
                                           ! added by RLC  23Mar96
      STOP 'FATAL ERROR - kk not 1 or 2'
!!!      CALL GOTOER
   30 Y(K)=YS(IT)+(YREF(K)-YS(IT))*COS(SAI)-(XREF(K)-XS(IT))*SIN(SAI)
      X(K)=XS(IT)+(XREF(K)-XS(IT))*COS(SAI)+(YREF(K)-YS(IT))*SIN(SAI)
      GO TO 21
!     DETERMINE THE TRAILING EDGE INTERSECTION
!        BETWEEN FIXED AND VARIABLE SWEEP WING SECTIONS
   31 IF (ABS(RSAR(K)-SAR(K-1)).LT.(.1/RAD)) GO TO 32
      Y(K)=XREF(K+1)-X(K-1)-A(K)*YREF(K+1)+A(K-1)*Y(K-1)
      Y(K)=Y(K)/(A(K-1)-A(K))
      X(K)=A(K)*X(K-1)-A(K-1)*XREF(K+1)+A(K-1)*A(K)*(YREF(K+1)-Y(K-1))
      X(K)=X(K)/(A(K)-A(K-1))
      GO TO 33
   32 X(K)=XREF(K+1)
      Y(K)=YREF(K+1)
   33 K=K+1
!     STORE REFERENCE PLANFORM COORDINATES ON INBOARD FIXED TRAILING
!     EDGE
      DO 34 I=K,N1
      X(I)=XREF(I)
      Y(I)=YREF(I)
   34 SAR(I-1)=RSAR(I-1)
   35 DO 36 I=1,N
      XX(I,IT)=X(I)
      YY(I,IT)=Y(I)
      MMCD(I,IT)=MCD(I,IT)
      TTWD(I,IT)=DIH(I,IT)
   36 AS(I,IT)=A(I)
      XX(N1,IT)=X(N1)
      YY(N1,IT)=Y(N1)
      AN(IT)=AAN(IT)
   37 END DO
!
!       LINE UP BREAKPOINTS AMONG PLANFORMS
!
      BOTSV(1)=0.
      botsv(2)=0.
      WRITE(6,108)
      DO 49 IT=1,IPLAN
      NIT=AN(IT)+1
      DO 43 ITT=1,IPLAN
      IF (ITT.EQ.IT) GO TO 431   ! was 43
      NITT=AN(ITT)+1
      DO 42 I=1,NITT
      JPSV=0
      DO 38 JP=1,NIT
      IF (YY(JP,IT).EQ.YY(I,ITT)) GO TO 421   ! was 42
   38 END DO
      DO 39 JP=1,NIT
      IF (YY(JP,IT).LT.YY(I,ITT)) GO TO 40
   39 END DO
      GO TO 421  ! was 42
   40 IF(JP.EQ.1) GO TO 421  ! was 42
      JPSV=JP
      IND=NIT-(JPSV-1)
      DO 41 JP=1,IND
      K2=NIT-JP+2
      K1=NIT-JP+1
      XX(K2,IT)=XX(K1,IT)
      YY(K2,IT)=YY(K1,IT)
      MMCD(K2,IT)=MMCD(K1,IT)
      AS(K2,IT)=AS(K1,IT)
      TTWD(K2,IT)=TTWD(K1,IT)
   41 END DO
      YY(JPSV,IT)=YY(I,ITT)
      AS(JPSV,IT)=AS(JPSV-1,IT)
      TTWD(JPSV,IT)=TTWD(JPSV-1,IT)
      XX(JPSV,IT)=                                                      &
     &  (YY(JPSV,IT)-YY(JPSV-1,IT))*AS(JPSV-1,IT)+XX(JPSV-1,IT)
      MMCD(JPSV,IT)=MMCD(JPSV-1,IT)
      AN(IT)=AN(IT)+1.
      NIT=NIT+1
  421 CONTINUE
   42 END DO
  431 CONTINUE
   43 END DO
!
!     SEQUENCE WING COORDINATES FROM TIP TO ROOT
!
      N1=AN(IT)+1.
      DO 44 I=1,N1
   44 Q(I)=YY(I,IT)
      DO 48 J=1,N1
      HIGH=1.
      DO 45 I=1,N1
      IF ((Q(I)-HIGH).GE.0.) CYCLE  !! GO TO 45
      HIGH=Q(I)
      IH=I
   45 END DO
      IF (J.NE.1) GO TO 46
      BOTSV(IT)=HIGH
      KFX(IT)=IH
   46 Q(IH)=1.
      SPY(J,IT)=HIGH
      IF (IH.GT.KFX(IT)) GO TO 47
      IYL(J,IT)=1
      IYT(J,IT)=0
      GO TO 481
   47 IYL(J,IT)=0
      IYT(J,IT)=1
  481 CONTINUE
   48 END DO
   49 END DO
!
!     SELECT MAXIMUM B/2 AS THE WING SEMISPAN.   IF BOTH FIRST AND
!     SECOND PLANFORMS HAVE SAME SEMISPAN THEN THE SECOND PLANFORM IS
!     TAKEN TO BE THE WING.
!
      KBOT=1
      IF (BOTSV(1).GE.BOTSV(2)) KBOT=2
      BOT=BOTSV(KBOT)
!
!     COMPUTE NOMINAL HORSESHOE VORTEX WIDTH ALONG WING SURFACE
!
      TSPAN=0
      ISAVE=KFX(KBOT)-1
      I=KFX(KBOT)-2
   50 IF (I.EQ.0) GO TO 51
      IF (TTWD(I,KBOT).EQ.TTWD(ISAVE,KBOT)) GO TO 52
   51 CTWD=COS(ATAN(TTWD(ISAVE,KBOT)))
      TLGTH=(YY(ISAVE+1,KBOT)-YY(I+1,KBOT))/CTWD
      TSPAN=TSPAN+TLGTH
      IF (I.EQ.0) GO TO 53
      ISAVE=I
   52 I=I-1
      GO TO 50
   53 VI=TSPAN/VIC
      VSTOL=VI/2
!
      TSPANA=0.
      KBIT=2
      IF (IPLAN.EQ.1) GO TO 57
      IF (KBOT.EQ.2) KBIT=1
      ISAVEA=KFX(KBIT)-1
      IA=KFX(KBIT)-2
   54 IF (IA.EQ.0) GO TO 55
      IF (TTWD(IA,KBIT).EQ.TTWD(ISAVEA,KBIT)) GO TO 56
   55 CTWDA=COS(ATAN(TTWD(ISAVEA,KBIT)))
      TLGTHA=(YY(ISAVEA+1,KBIT)-YY(IA+1,KBIT))/CTWDA
      TSPANA=TSPANA+TLGTHA
      IF (IA.EQ.0) GO TO 57
      ISAVEA=IA
   56 IA=IA-1
      GO TO 54
   57 CONTINUE
!     ELIMINATE PLANFORM BREAKPOINTS WHICH ARE WITHIN (B/2)/2000 UNITS
!     LATERALLY
!
      DO 59 IT=1,IPLAN
      N=AN(IT)
      N1=N+1
      DO 59 J=1,N
      AA=ABS(SPY(J,IT)-SPY(J+1,IT))
      IF (AA.EQ.0..OR.AA.GT.ABS(TSPAN/2000.)) GO TO 59
      IF (AA.GT.YTOL) WRITE(6,111) SPY(J+1,IT),SPY(J,IT)
      DO 58 I=1,N1
      IF (YY(I,IT).NE.SPY(J+1,IT)) CYCLE  !! GO TO 58
      YY(I,IT)=SPY(J,IT)
   58 END DO
      SPY(J+1,IT)=SPY(J,IT)
   59 CONTINUE
!
!     COMPUTE Z COORDINATES
!
      DO 63 IT=1,IPLAN
      JM=AN(IT)+1.
      n1=jm
      DO 60 JZ=1,N1
   60 ZZ(JZ,IT)=RTCDHT(IT)
      JZ=1
   61 JZ=JZ+1
      IF (JZ.GT.KFX(IT)) GO TO 62
      ZZ(JZ,IT)=ZZ(JZ-1,IT)+(YY(JZ,IT)-YY(JZ-1,IT))*TTWD(JZ-1,IT)
      GO TO 61
   62 JM=JM-1
      IF (JM.EQ.KFX(IT)) GO TO 631
      ZZ(JM,IT)=ZZ(JM+1,IT)+(YY(JM,IT)-YY(JM+1,IT))*TTWD(JM,IT)
      GO TO 62
  631 CONTINUE
   63 END DO
!
!     WRITE PLANFORM PERIMETER POINTS ACTUALLY USED IN THE COMPUTATIONS
!
      WRITE(6,100)
      DO 65 IT=1,IPLAN
      N=AN(IT)
      N1=N+1
      IF (IT.EQ.2) WRITE(6,110)
      DO 64 KK=1,N
      TOUT=ATAN(TTWD(KK,IT))*RAD
      AOUT=ATAN(AS(KK,IT))*RAD
      IF (AS(KK,IT).EQ.AZY) AOUT=90.
      WRITE(6,101) KK,XX(KK,IT),YY(KK,IT),ZZ(KK,IT),                    &
     & AOUT,TOUT,MMCD(KK,IT)
   64 END DO
      WRITE(6,101) N1,XX(N1,IT),YY(N1,IT),ZZ(N1,IT)
   65 END DO
!
!     PART ONE - SECTION THREE - LAY OUT YAWED HORSESHOE VORTICES
!
      STRUE=0.
      NSSWSV(1)=0
      nsswsv(2)=0
      msv(1)=0
      msv(2)=0
      DO 74 IT=1,IPLAN
      N1=AN(IT)+1.
      I=0
      J=1
      YIN=BOTSV(IT)
      ILE=KFX(IT)
      ite=ile
!     DETERMINE SPANWISE BORDERS OF HORSESHOE VORTICES
   66 IXL=0
      ixt=0
      I=I+1
      CPHI=COS(ATAN(TTWD(ILE,IT)))
      IF (YIN.GE.(SPY(J,IT)+VSTOL*CPHI)) GO TO 67
!     BORDER IS WITHIN VORTEX SPACING TOLERANCE (VSTOL) OF BREAKPOINT
!     THEREFORE USE THE NEXT BREAKPOINT INBOARD FOR THE BORDER
      VBORD(I)=YIN
      GO TO 70
!     USE NOMINAL VORTEX SPACING TO DETERMINE THE BORDER
   67 VBORD(I)=SPY(J,IT)
!     COMPUTE SUBSCRIPTS ILE AND ITE TO INDICATE WHICH
!     BREAKPOINTS ARE ADJACENT AND WHETHER THEY ARE ON THE WING LEADING
!       EDGE OR THE TRAILING EDGE
   68 IF (J.GE.N1) GO TO 69
      IF (SPY(J,IT).NE.SPY(J+1,IT)) GO TO 69
      IXL=IXL+IYL(J,IT)
      IXT=IXT+IYT(J,IT)
      J=J+1
      GO TO 68
   69 YIN=SPY(J,IT)
      IXL=IXL+IYL(J,IT)
      IXT=IXT+IYT(J,IT)
      J=J+1
   70 IPHI=ILE-IXL
      IF (J.GE.N1) IPHI=1
      YIN=YIN-VI*COS(ATAN(TTWD(IPHI,IT)))
      IF (I.NE.1) GO TO 72
   71 ILE=ILE-IXL
      ITE=ITE+IXT
      GO TO 66
!     COMPUTE COORDINATES FOR CHORDWISE ROW OF HORSESHOE VORTICES
   72 YQ=(VBORD(I-1)+VBORD(I))/2.
      HW=(VBORD(I)-VBORD(I-1))/2.
      IM1=I-1+NSSWSV(1)
      ZH(IM1)=ZZ(ILE,IT)+(YQ-YY(ILE,IT))*TTWD(ILE,IT)
      PHI(IM1)=TTWD(ILE,IT)
      SSWWA(IM1)=AS(ILE,IT)
      XLE=XX(ILE,IT)+AS(ILE,IT)*(YQ-YY(ILE,IT))
      XET=XX(ITE,IT)+AS(ITE,IT)*(YQ-YY(ITE,IT))
      XLOCAL=(XLE-XET)/TBLSCW(IM1)
!
!     COMPUTE WING AREA PROJECTED TO THE X - Y PLANE
!
      STRUE=STRUE+XLOCAL*TBLSCW(IM1)*(HW*2.)*2.
!
      NSCW=TBLSCW(IM1)
      DO 73 JCW=1,NSCW
      AJCW=JCW-1
      XLEL=XLE-AJCW*XLOCAL
      NTS=JCW+MSV(1)+MSV(2)
      PN(NTS)=XLEL-.25*XLOCAL
      PV(NTS)=XLEL-.75*XLOCAL
      PSI(NTS)=((XLE-PN(NTS))*AS(ITE,IT)+                               &
     &  (PN(NTS)-XET)*AS(ILE,IT))/(XLE-XET)*CPHI
      S(NTS)=HW/CPHI
      Q(NTS)=YQ
   73 END DO
      MSV(IT)=MSV(IT)+NSCW
!
!     TEST TO DETERMINE WHEN WING ROOT IS REACHED
      IF(VBORD(I).LT.YREG(1,IT)) GO TO 71
!
      NSSWSV(IT)=I-1
   74 END DO
      M=MSV(1)+MSV(2)
!
!     COMPUTE ASPECT RAT10  AND  AVERAGE CHORD
!
      BOT=-BOT
      AR=4.*BOT*BOT/SREF
      ARTRUE=4.*BOT*BOT/STRUE
      CAVE=STRUE/(2.*BOT)
      BETA=(1.-MACH*MACH)**.5
      WRITE(6,114) M
      WRITE(6,115) (IT,MSV(IT),NSSWSV(IT),IT=1,IPLAN)
      IF (SCW.NE.0.) WRITE(6,112) SCW
      IF (SCW.EQ.0.) WRITE(6,113) (TBLSCW(I),I=1,NSTA)

!               PLOT PLANFORM CONFIGURATIONS ON THE
!               LINE PRINTER
!
                                            ! name changed RLC 23Mar96
      CALL PlotPlanform(iplan,xx,yy,an,0,0)
!
!
!     APPLY PRANDTL-GLAUERT CORRECTION
!
      DO 75 NV=1,M
      PSI(NV)=ATAN(PSI(NV)/BETA)
      PN(NV)=PN(NV)/BETA
   75 PV(NV)=PV(NV)/BETA
      NSSW=NSSWSV(1)+NSSWSV(2)
      JN=0
      DO 77 JSSW=1,NSSW
      CHORD(JSSW)=0.
      NSCW=TBLSCW(JSSW)
      DO 76 JSCW=1,NSCW
      JN=JN+1
      CHORD(JSSW)=CHORD(JSSW)-2.*(PV(JN)-PN(JN))*BETA
   76 END DO
   77 XTE(JSSW)=(PV(JN)+(PV(JN)-PN(JN))/2.)*BETA
      PHISUM=0.
      DO 78 IKY=1,NSSW
      PHISUM=PHISUM+PHI(IKY)
   78 END DO
      IFLAG=1
!     write(6,*) ' iplan is ',iplan
!     write(6,*) ' phisum is ',phisum
      IF (IPLAN.EQ.1.AND.PHISUM.NE.0.) IFLAG=2
      IF (IPLAN.EQ.2.AND.PHISUM.NE.0.) GO TO 79
      GO TO 83
   79 DO 81 IP=1,IPLAN
      IA=1+(IP-1)*NSSWSV(1)
      IB=NSSWSV(1)+(IP-1)*NSSWSV(2)
      IC=1-(IP-2)*NSSWSV(1)
      ID=NSSWSV(1)-(IP-2)*NSSWSV(2)
      DO 80 IU=IA,IB
      DO 80 IZ=IC,ID
!     write(6,1000) iu,iz,zh(iu),zh(iz)
!1000  format(2i5,2e13.6)
      IF (ZH(IU).EQ.ZH(IZ)) GO TO 82
   80 CONTINUE
   81 END DO
      IFLAG=2
      GO TO 83
   82 IFLAG=3
   83 CONTINUE
!     write(6,*) ' 83 '
!     write(6,*) ' iflag is ',iflag
!      READ(5,122,END=10005) XCFW,XCFT
      READ(5,*,END=10005) XCFW,XCFT
      write(*,'(2F10.4,''  XCFW XCFT'')') XCFW,XCFT
10005 IF (M.GT.400) GO TO 86
      NSW=NSSWSV(1)+NSSWSV(2)
      IF (NSW.GT.50) GO TO 85
      ITSV=0
      DO 84 IT=1,IPLAN
      IF (AN(IT).LE.25.) CYCLE  !! GO TO 84
      WRITE(6,118) IT,AN(IT)
      ITSV=1
   84 END DO
      IF (ITSV.GT.0) GO TO 91
      GO TO 87
   85 WRITE(6,117) NSW
      GO TO 91
   86 WRITE(6,116) M
      GO TO 91
   87 REWIND 25
      WRITE(25) BOT,M,BETA,PTEST,QTEST,TBLSCW,Q,PN,PV,S,PSI,PHI,ZH,      &
     & NSSW,TWIST,CREF,SREF,CAVE,CLDES,STRUE,AR,ARTRUE,RTCDHT,CONFIG,    &
     & NSSWSV,MSV,KBOT,PLAN,IPLAN,MACH,SSWWA,CHORD,XTE,KBIT,TSPAN,       &
     & TSPANA,XCFW,XCFT ,IFLAG,YREG(1,1),YREG(1,2)
      END FILE 25
      GO TO (88,89,90), IFLAG
      STOP 'Fatal Error - IFLAG not 1,2 or 3' ! added by RLC  23Mar96
!!!      CALL GOTOER
   88 WRITE(6,119)
      WRITE(50,123)
      GO TO 92
   89 WRITE(6,120)
      WRITE(50,124)
      GO TO 92
   90 WRITE(6,121)
      WRITE(50,125)
      GO TO 92
   91 TOTAL=TOTAL-1.
      WRITE(50,126)
   92 CONTINUE
      END FILE 50
      WRITE(*, '(A)' ) FAREWELL
      STOP '======= Program GEOM has finished successfully ========='
   93 ICODEOF=1
      WRITE(6,103) CONFIG
      GO TO 91
   94 ICODEOF=2
      WRITE(6,104) K,IT
      GO TO 91
   95 ICODEOF=3
      WRITE(6,107) PTEST,QTEST
      GO TO 91
!
!
   96 FORMAT (' GEOMETRY DATA')
   97 FORMAT (///1X,A10,' REFERENCE PLANFORM HAS ',I3,' CURVES'//       &
     & ' ROOT CHORD HEIGHT =',F12.5/                                    &
     & ' VARIABLE SWEEP PIVOT POSITION'/                                &
     & '   X(S) =',F12.5,'   Y(S) =',F12.5//                            &
     & '    BREAK POINTS FOR THE REFERENCE PLANFORM '/)
   98 FORMAT (BZ,8F10.4)
   99 FORMAT ('1'//'      CONFIGURATION NO.',F8.0/)
  100 FORMAT ('  POINT',7X,'X',11X,'Y',11X,'Z',5X,'SWEEP',7X,'DIHEDRAL',&
     & 4X,'MOVE'/44X,'ANGLE',8X,'ANGLE',6X,'CODE'/)
  101 FORMAT (5X,I5,3F12.5,2F14.5,I6)
  102 FORMAT (/5X,'CURVE',I3,' IS SWEPT',F12.5,                         &
     & ' DEGREES ON PLANFORM', I3)
  103 FORMAT ('1'///5X,                                                 &
     & 'END OF FILE ENCOUNTERED AFTER CONFIGURATION',F7.0)
  104 FORMAT ('1'///5X,                                                 &
     & 'THE FIRST VARIABLE SWEEP CURVE SPECIFIED (K =',I3,              &
     & ' ) DOES NOT HAVE AN M CODE OF 2 FOR PLANFORM',I4)
  105 FORMAT(BZ,5F5.1,2F10.4)
  106 FORMAT (5X,I5,2F12.5,2F16.5,4X,I4)
  107 FORMAT ('1'///1X,'ERROR - PROGRAM CANNOT PROCESS PTEST =',F5.1,   &
     & ' AND QTEST =',F5.1)
  108 FORMAT (//5X,'BREAK POINTS FOR THIS CONFIGURATION'//)
  109 FORMAT (5X,'POINT',6X,'X',11X,'Y',11X,'SWEEP',10X,'DIHEDRAL',7X,  &
     & 'MOVE'/15X,'REF',9X,'REF',10X,'ANGLE',11X,'ANGLE',9X,'CODE'/)
  110 FORMAT (/'  SECOND PLANFORM BREAK POINTS'/)
  111 FORMAT (////5X,'THE BREAKPOINT LOCATED SPANWISE AT',F11.5,3X,     &
     & 'HAS BEEN ADJUSTED TO',F9.5////)
  112 FORMAT (/5X,F5.0,' HORSESHOE VORTICES IN EACH CHORDWISE ROW')
  113 FORMAT (/5X,'TABLE OF HORSESHOE VORTICES IN EACH CHORDWISE ROW'/  &
     & 7X,'(FROM TIP TO ROOT BEGINNING WITH FIRST PLANFORM)'//          &
     &  10F5.0/10F5.0)
  114 FORMAT (///5X,I5,                                                 &
     & ' HORSESHOE VORTICES USED ON THE LEFT HALF OF THE CONFIGURATION' &
     & //'  PLANFORM       TOTAL        SPANWISE'/)
  115 FORMAT (I8,I13,I14)
  116 FORMAT ('1'//10X,I6,' HORSESHOE VORTICES LAID OUT.'/              &
     & '    THIS IS MORE THAN THE 400 MAXIMUM.'//                       &
     & '    THIS CONFIGURATION IS ABORTED.')
  117 FORMAT ('1'/5X,I6,' ROWS OF HORSESHOE VORTICES LAID OUT.'/        &
     & '    THIS IS MORE THAN THE 50 MAXIMUM.'//                        &
     & '    THIS CONFIGURATION IS ABORTED.')
  118 FORMAT ('1'//5X,'PLANFORM',I6,' HAS',I6,' BREAKPOINTS.'/          &
     & '      THE MAXIMUM DIMENSIONED IS 25.'//                         &
     & '      THIS CONFIGURATION IS ABORTED.')
  119 FORMAT (///20X,'MINIMUM FIELD LENGTH = 51000')
  120 FORMAT (///20X,'MINIMUM FIELD LENGTH = 63000')
  121 FORMAT (///20X,'MINIMUM FIELD LENGTH = 112000')
  122 FORMAT (BZ,6F10.4)
  123 FORMAT ('*COMPILE VLMCDRAGS,VLMCCIR1,VLMCZOC')
  124 FORMAT ('*COMPILE VLMCDRAGS,VLMCCIR2,VLMCZOC')
  125 FORMAT ('*COMPILE VLMCDRAGS,VLMCCIR3,VLMCZOC')
  126 FORMAT ('*COMPILE VLMCDUMMY')

      END Program VortexLatticeGeometry

!+
      SUBROUTINE PlotPlanform(iplan,xx,yy,an,icon,itheta)
!   --------------------------------------------------------------------
!     PURPOSE - this routine prepares a plot diagram of the input
!        planform configuration(s) for the line printer
!
!     AUTHOR - Robert Gray,  Computer Sciences Corp  1980
!
!     NOTES-
!
      IMPLICIT NONE
!***********************************************************************
!     A R G U M E N T S                                                *
!***********************************************************************
      INTEGER iplan   ! number of planforms to be plotted
      REAL*8 xx(25,2) ! x coordinate array
      REAL*8 yy(25,2) ! y coordinate array
      REAL*8 an(2)    ! number of points in planform less one
      INTEGER icon    ! icon --0 requests contour drawings.
      INTEGER itheta  ! angle of rotation desired in degrees,
!                       0 for no rotation

!***********************************************************************
!     L O C A L   C O N S T A N T S                                    *
!***********************************************************************
      character*1 iplus
      character*1 iblank
      character*1 nchar(2)
      PARAMETER (IBLANK=' ', IPLUS='+')
!!!      data iblank /' '/
!!!      data iplus /'+'/

!***********************************************************************
!     L O C A L   V A R I A B L E S                                    *
!***********************************************************************
      character*1 icc
      REAL*8 a,b,c,d
      REAL*8 cans,sans
      INTEGER i,j,k,m,n
      INTEGER icnter
      INTEGER icount
      INTEGER ict
      INTEGER ileft
      INTEGER indx
      INTEGER iptint
      INTEGER iright
      INTEGER iscan
      INTEGER itmp
      INTEGER ix
      INTEGER maxxx,minxx,maxyy,minyy
      INTEGER maxnxx,minnxx
      REAL*8 rptint
      REAL*8 rscale
      REAL*8 rxx,ryy
      REAL*8 rymax,rymin
      REAL*8 scan
      REAL*8 theta
      REAL*8 xdim,ydim
      REAL*8 xscale,yscale
      REAL*8 xmin,xmax, ymin,ymax
      REAL*8 xup,yup

!***********************************************************************
!     L O C A L   A R R A Y S                                          *
!***********************************************************************
      character*1 narray (100)
      REAL*8 xxx(25,2), yyy(25,2)
      INTEGER nxx(26,2), nyy(26,2)
      INTEGER ixpts(50)

      data nchar/'@','#'/
      data narray /100*' '/
      data ymin /99999.0/, ymax /-99999.0/
      data xmin /99999.0/, xmax /-99999.0/

!-----------------------------------------------------------------------
!        rotate planform(s)

      theta = itheta * (3.14159/180)
      cans = cos (theta)
      sans = sin (theta)
      do 5 i = 1, iplan
        m = an(i) + 1
        do 5 n = 1,m
          xxx(n,i) = xx(n,i) * cans - yy(n,i) * sans
          yyy(n,i) = xx(n,i) * sans + yy(n,i) * cans
    5 continue

!             rescale all x and y coordinates
!             y is 6 per inch on the line printer, max 7 inches
!             x is 10 per inch, max 10 inches

!             find minimum and maximum coordinates
      do 10 i = 1,iplan
        m = an(i) + 1
        do 20 n = 1,m
          if (yyy(n,i) .lt. ymin) ymin = yyy(n,i)
          if (yyy(n,i) .gt. ymax) ymax = yyy(n,i)
          if (xxx(n,i) .lt. xmin) xmin = xxx(n,i)
          if (xxx(n,i) .gt. xmax) xmax = xxx(n,i)
   20   continue
   10 continue
!               values used as subscripts should be greater
!               than or equal to 1

      if (ymin .gt. 0.0) yup= 0.0 - ymin
      if (ymin .le. 0.0) yup = abs(ymin)
        do 70 i = 1,iplan
         m = an(i) + 1
         do 80 n = 1,m
         yyy(n,i) = yyy(n,i) + yup + 1.0
   80    continue
   70 continue
      ymax = ymax + yup + 1
      ymin = ymin + yup + 1.0


      if (xmin .gt. 0.0) xup = 0.0 - xmin
      if (xmin .le. 0.0) xup = abs(xmin)
        do 85 i = 1,iplan
          m = an(i) + 1
          do 90 n = 1,m
          xxx(n,i) = xxx(n,i) + xup + 1.0
   90   continue
   85  continue
      xmax = xmax + xup + 1.0
      xmin = xmin + xup + 1.0

!              find y scaling factor--maximum 42 lines
      ydim = ymax
      yscale = 42.0/ydim
!              find x scaling factor--10 characters per inch
!              and in proportion to y inches
      xscale = yscale * 1.666
      xdim = xmax * xscale
      if (xdim .le. 100.49) go to 30
!              scaled x dimension is greater than 100, rescale

      rscale = 100.0/xdim
      yscale = yscale * rscale
      xscale = xscale * rscale

!              compute all rescaled x and y coordinates,
!              rounding each
   30 do 40 i = 1,iplan
        m = an(i) + 1
        do 50 n = 1,m
          rxx = xxx(n,i) * xscale * 10.0 + 5.0
          nxx(n,i) = rxx/10.0
          ryy = yyy(n,i) * yscale * 10.0 + 5.0
          nyy(n,i) = ryy/10.0
   50   continue
   40 continue

!              find integer maxima and minima

      ymax  = ymax *  yscale * 10.0 + 5.0
      maxyy = ymax/10.0
      ymin  = ymin * yscale * 10.0 + 5.0
      minyy = ymin/10
      xmax = xmax * xscale * 10.0 + 5.0
      maxxx = xmax/10.0
      xmin = xmin * xscale * 10.0 + 5.0
      minxx = xmin/10.0

!              center the data

      icnter = ((100 - (maxxx - minxx)) /2) - minxx
        do 65 i = 1,iplan
          m = an(i) + 1
          do 65 n = 1,m
            nxx(n,i) = nxx(n,i) + icnter
   65   continue


!            write heading
      write(6,802) (i,nchar(i),i=1,iplan)
  802 format(1h1,51x,34hAPPROXIMATE PLANFORM CONFIGURATION,/            &
     &(4x,9hPLANFORM ,i2,6h  IS  ,a1))

!              draw the input planform configurations

!             for each scan line

      do 500 iscan = minyy,maxyy
!             carriage control for the first planform on a line
!             is blank, afterwards it is a plus
      icc = iblank

!            for each planform
!            find points on this scan line

      do 400 i = 1,iplan
        m = an(i) + 1
!             find breakpoints on this line
        do 110 n = 1,m
          if (nyy(n,i) .eq. iscan) narray (nxx(n,i)) = nchar(i)
  110   continue

!             insure a closed planform
      nxx(m+1,i) = nxx(1,i)
      nyy(m+1,i) = nyy(1,i)

!            offset scan line to insure vertices are not intersected

      scan = iscan + 0.1

!            find points at which scan line intersects line
!            segments

      icount = 0
!            for each line segment...
      do 300 n = 1,m
!            if this segment is horizontal, it must be
!            filled separately.

        if (iscan .ne. nyy(n,i)) go to 120
        if (nyy(n+1,i) .ne. nyy(n,i)) go to 120
          ileft = min0(nxx(n,i),nxx(n+1,i))
          iright = max0(nxx(n,i),nxx(n+1,i))
          do 130 j = ileft,iright
            narray(j) = nchar(i)
  130     continue
          icount = icount + 1
          maxnxx = max0(nxx(n,i),nxx(n+1,i))
          minnxx = min0(nxx(n,i),nxx(n+1,i))
          ixpts(icount) = minnxx
          icount = icount + 1
          ixpts(icount) = maxnxx

  120     rymax = amax0(nyy(n,i),nyy((n+1),i))
          rymin = amin0(nyy(n,i),nyy((n+1),i))
          if (scan .lt. rymin .or. scan .gt. rymax ) go to 300

!              the scan line will intersect
!              this segment

          a = scan - nyy((n+1),i)
          b = nxx((n+1),i) - nxx(n,i)
          c = nyy((n+1),i) - nyy(n,i)
!              if segment is horizontal,
!              ignore it
          if (nyy((n+1),i) .eq. nyy(n,i)) go to 300
          d =a * b/c

!              the point of intersection is
!              (iptint,scan)

          rptint = (d + nxx(n + 1,i)) * 10.0 + 5.0
          iptint = rptint/10.0
          maxnxx = max0(nxx(n,i),nxx((n+1),i))
          minnxx = min0(nxx(n,i),nxx((n+1),i))

!              if intersection is to the left or
!              right of endpoints, ignore it

          if (iptint .gt. maxnxx .or. iptint .lt. minnxx) go to 300

!              store and count iptint

          icount = icount + 1
          ixpts(icount) = iptint
  300   continue

!              if only one intersection
!              found, go to next planform or scanline

        if (icount .le. 1) go to 400

!              sort the intersections. they
!              will be filled left to right.

        ict = icount - 1
        do 310 k = 1,ict
          indx = ict - k + 1
          do 310 j = 1,indx
            if (ixpts(j) .lt. ixpts(j+1)) go to 310
            itmp = ixpts(j)
            ixpts(j) = ixpts(j+1)
            ixpts(j+1) = itmp
  310   continue
!        contour drawing requested, place
!            intersection points in narray

        if (icon .ne. 0) go to 320
        do 315 n = 1,icount
          narray(ixpts(n)) = nchar(i)
  315   continue
        go to 340

!        contrast drawing requested, fill in
!            between intersection points

  320   do 330 n= 1,icount,2
          ileft = ixpts(n)
          iright = ixpts (n+1)
          do 330 ix = ileft,iright
            narray(ix) = nchar(i)
  330   continue

!            write the line
  340   write(6,804) icc,narray
  804 format (a1,18x,100a1)
!            prepare to overwrite the next planform
        icc = iplus
!            clear narray for next planform
        do 350 n = 1,100
          narray(n) = iblank
  350   continue

  400 continue

  500 continue

      return
      END Subroutine PlotPlanform
