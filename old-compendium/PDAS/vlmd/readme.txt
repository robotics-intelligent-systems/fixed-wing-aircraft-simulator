

VORTEX LATTICE MINIMUM DRAG (VLMD)                        /vlmd/readme.txt

The files for this program are in directory vlmd 
  readme.txt      this file of general information
  read.me         extra input information supplied by Raj Nangia
  input.txt       instructions for use of VLMD
  geom.for        the source code for geometry 
  analysis.for    the source code for analysis            
 
  lar15160.txt    the original COSMIC description of the program

Sample cases for this program are:
  c1.inp         input data for test case 1
  c1.out         combined output from geom and analysis for this test case.
  c2.inp         input data for test case 2
  c2.out         combined output from geom and analysis for this test case.
  c3.inp         input data for test case 3
  c3.out         combined output from geom and analysis for this test case.
  c4.inp         input data for test case 4
  c4.out         combined output from geom and analysis for this test case.

The reference documents for this program may be accessed
from the web page https://www.pdas.com/vlmdrefs.html. 

To compile this program for your computer, use the commands
   gfortran  geom.f90     -o geom.exe
   gfortran  analysis.for -o analysis.exe
Linux and Macintosh users may prefer to omit the .exe on the file name.


The geom program asks for the name of the input file. This must be a file 
written to conform to the instructions in the NASA TN. Geom produces a file 
called geom.out and a specially formatted file called FORT25. This file is 
not designed to be read or printed; it is the input to Analysis. When 
Analysis is launched, it asks no questions, but takes the file called FORT25 
and completes the remainder of the calculations. This produces a file called
analysis.out that is designed to be printed. 


                      DESCRIPTION (FROM NASA)

A new subsonic method has been developed by which the mean camber surface
can be determined for trimmed noncoplanar planforms with minimum vortex
drag. The method uses a vortex lattice and overcomes previous difficulties
with chord loading specification. This method uses a Trefftz plane analysis
to determine the optimum span loading for minimum drag, then solves for the
mean camber surface of the wing, which will provide the required loading.
Pitching-moment or root-bending-moment constraints can be employed as well
at the design lift coefficient.
