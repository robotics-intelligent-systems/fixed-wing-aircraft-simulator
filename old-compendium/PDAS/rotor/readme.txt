

AEROELASTIC ANALYSIS FOR ROTORCRAFT                           /rotor/readme.txt
   IN FLIGHT OR IN A WIND TUNNEL

The files for this program are in the directory rotor 
  readme.txt      this file of general description
  arc11150.txt    the original program description from COSMIC
  dynamics.src    the original source code for program DYNAMICS
  rotor2.src      the original source code for program ROTOR2
  support.src     the original source code for program SUPPORT
  tilting.src     the original source code for program TILTING
  dynamics.for    modern source code for program DYNAMICS
  rotor2.for      modern source code for program ROTOR2
  support.for     modern source code for program SUPPORT
  tilting.for     modern source code for program TILTING


This program is a "work in progress" and is not ready for general release.
I have included it so those who have a special interest may see
the original code plus my modifications to date.
 
