

FAST MARS RELAY COMMUNICATION LINK                          /relay/readme.txt

The files for this program are in the directory relay 
  readme.txt      this file of general description
  original.src    the original copy of the source code (from COSMIC)
  lar10658.txt    the original program description from COSMIC
  relay.f90       the source code converted to modern Fortran
  case1.inp       the input for a test case. Unfortunately, no output known.


This program is a "work in progress" and is not ready for general release.
I have included it so those who have a special interest may see
the original code plus my modifications to date.
