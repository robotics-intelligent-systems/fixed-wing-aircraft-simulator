

W12SC3- SUPERSONIC WING DESIGN AND ANALYSIS                /w12sc3/readme.txt

The files for this program are in the directory w12sc3 
  readme.txt      this file of general description
  input.txt       guide to preparing input file
  w12sc3.f90      the complete source code
  files.cmn       description of common block /FILES/
  head.cmn        description of common block /HEAD/
  option.cmn      description of common block /OPTION/
  shell.cmn       description of common block /SHELL/
  lar13239.txt    the original program description from COSMIC
  original.src    the original copy of the source code (from COSMIC)
  
Sample cases for this program are:
  case1.inp       input data
  case1.out       output data for case1.inp


To compile this program for your computer, use the command
   gfortran  w12sc3.f90 -o w12sc3.exe
Linux and Macintosh users may prefer to omit the .exe on the file name.
 
This program first asks for the name of the input file. This must
be a file written to conform to input.txt.  After calculating 
the solution, the program produces a file called w12sc3.out that contains
a wealth of information concerning the flow problem to be solved.

There still may be problems with this code. I have only been
checking it for a short time, but I thought it would be OK to go
ahead and release it.
