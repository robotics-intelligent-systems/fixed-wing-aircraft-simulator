VIEWER - DRAW THREE DIMENSIONAL SURFACES

This program must be recompiled each time you want to view
a new function. I have included a sample with a number of
functions. The way you do this is to have exactly one call
un-commented. If you have more than one, you will get the two
pictures superimposed. 
