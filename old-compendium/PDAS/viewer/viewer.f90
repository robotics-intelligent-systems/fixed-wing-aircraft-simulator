                          !   viewer.f90


!+
MODULE TestFunctions               
! ------------------------------------------------------------------------------
! PURPOSE - Functions used by Akima, Renka, etc. to test
!   multi-dimensional interpolation algorithms.

! AUTHOR - Ralph L. Carmichael, Public Domain Aeronautical Software

! REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
! 17Jul97  0.5   RLC   Original coding
! 21Jul97  0.51  RLC   Finished coding with function F1
! 27Dec99  0.6   RLC   Added Poles,Maxwell, CollapsingString
 
IMPLICIT NONE

  PUBLIC:: CollapsingString
  PUBLIC:: F1,F2,F3,F4,F5,F6
  PUBLIC:: Maxwell
  PUBLIC:: Poles

CONTAINS

!+
FUNCTION Poles(x,y) RESULT(f)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: f
  COMPLEX,PARAMETER:: ONE = CMPLX(1.0, 0.0)
  COMPLEX:: z
  INTRINSIC:: ABS, CMPLX, EPSILON, MIN
!-------------------------------------------------------------------------------
  z=CMPLX(x,y)

  IF (ABS(z) < 10.0*EPSILON(x)) THEN
    f=20.0
  ELSE IF (ABS(z-1.0) < 10.0*EPSILON(x) ) THEN
    f=20.0
  ELSE
    f=ABS(1.0/z + 1.0/((z-1.0)**2) )
    f=MIN(f, 20.0)
  END IF

  RETURN
END Function Poles   ! ---------------------------------------------------------

!+
FUNCTION Maxwell(x,y) RESULT(f)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: f
  INTRINSIC:: EXP
!-------------------------------------------------------------------------------
  f=EXP(-(1.0/x + 0.5/y))*(x*y)**(-1.5)
  RETURN
END Function Maxwell   ! -------------------------------------------------------

!+
FUNCTION CollapsingString(x,y) RESULT(f)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: f
!-------------------------------------------------------------------------------
  f=0.5*(1.0/(1.0+8.0*(x-y)**2) + 1.0/(1.0+8.0*(x+y)**2) )
  RETURN
END Function CollapsingString   ! ----------------------------------------------

!+
FUNCTION F1(x,y) RESULT(z)
! ------------------------------------------------------------------------------
! PURPOSE - Function given by Akima , p.361
  REAL,INTENT(IN):: x,y
  REAL:: z
  REAL:: t1,t2,t3,t4
  INTRINSIC:: EXP
!-------------------------------------------------------------------------------
  t1=0.75*EXP(-((9.0*x-2.0)**2 + (9.0*y-2.0)**2)/4.0)
  t2=0.75*EXP(-((9.0*x+1.0)**2)/49.0 - (9.0*y+1.0)/10.0)
  t3=0.5*EXP(-((9.0*x-7.0)**2 + (9.0*y-3.0)**2)/4.0)
  t4=-0.20*EXP(-(9.0*x-4.0)**2 -(9.0*y-7.0)**2)
  z=t1+t2+t3+t4
  RETURN
END Function F1   ! ------------------------------------------------------------

!+
FUNCTION F2(x,y) RESULT(z)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: z
  INTRINSIC:: TANH
!-------------------------------------------------------------------------------
  z=(TANH(9.0*y-9.0*x) + 1.0)/9.0
  RETURN
END Function F2   ! ------------------------------------------------------------

!+
FUNCTION F3(x,y) RESULT(z)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: z
  INTRINSIC:: COS
!-------------------------------------------------------------------------------
  z=(1.25+COS(5.4*y))/(6.0*(1.0+(3.0*x-1.0)**2))
  RETURN
END Function F3   ! ------------------------------------------------------------

!+
FUNCTION F4(x,y) RESULT(z)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: z
  INTRINSIC:: EXP
!-------------------------------------------------------------------------------
  z=EXP(-81.0*((x-0.5)**2 + (y-0.5)**2)/16.0)/3.0
  RETURN
END Function F4   ! ------------------------------------------------------------

!+
FUNCTION F5(x,y) RESULT(z)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: z
  INTRINSIC:: EXP
!-------------------------------------------------------------------------------
  z=EXP(-81.0*((x-0.5)**2 + (y-0.5)**2)/4.0)/3.0
  RETURN
END Function F5   ! ------------------------------------------------------------

!+
FUNCTION F6(x,y) RESULT(z)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: x,y
  REAL:: z
  INTRINSIC:: SQRT
!-------------------------------------------------------------------------------
  z=SQRT(64.0-81.0*((x-0.5)**2 + (y-0.5)**2))/9.0 - 0.5
  RETURN
END Function F6   ! ------------------------------------------------------------


END Module TestFunctions   ! ===================================================

!+
MODULE ViewerProcedures
! ------------------------------------------------------------------------------
! PURPOSE - Collect the procedures used in Viewer

  INTEGER,PUBLIC:: DBG=3
  REAL,PARAMETER,PRIVATE:: PI = 3.141592653589793238462643383279502884197
  REAL,PRIVATE:: DEG2RAD = PI/180
  PUBLIC:: FillArray
  PUBLIC:: Plot3D
  PRIVATE:: MakeMatrix
  PRIVATE:: RotateAndWrite

CONTAINS

!+
SUBROUTINE FillArray(startValue,endValue, array, spacingCode)
! ------------------------------------------------------------------------------
! PURPOSE - fill an array from start to end. The intermediate points are
!    computed according to various spacing rules.

  REAL,INTENT(IN):: startValue,endValue
  REAL,INTENT(OUT),DIMENSION(:):: array
  INTEGER,INTENT(IN),OPTIONAL:: spacingCode
                              ! =2 full cosine
                              ! =3 half cosine
                              ! =4 half sine
                              ! anything else (or nothing)= uniform spacing
  INTEGER:: k,n
  REAL,PARAMETER:: HALFPI=0.5*PI
  REAL,ALLOCATABLE,DIMENSION(:):: temp
  INTRINSIC:: COS,SIN
!-------------------------------------------------------------------------------
  n=SIZE(array)
  IF (n <= 0) RETURN

  array(n)=endValue
  array(1)=startValue
  IF (n <= 2) RETURN

  ALLOCATE(temp(n-2))
  temp=(/ (REAL(k),k=1,n-2) /) / REAL(n-1)

  IF (Present(spacingCode)) THEN
    SELECT CASE(spacingCode)
      CASE (2)
        temp=0.5*(1.0-COS(PI*temp))       ! full cosine, dense near both ends
      CASE (3)
        temp=1.0-COS(HALFPI*temp)             ! half cosine, dense near start
      CASE (4)
        temp=SIN(HALFPI*temp)                     ! half sine, dense near end
    END SELECT
  END IF

  array(2:n-1)=startValue + (endValue-startValue)*temp

  DEALLOCATE(temp)

  RETURN
END Subroutine FillArray   ! ---------------------------------------------------

!+
SUBROUTINE Plot3D(efu, x,y, alpha,beta,gamma, f)
! ------------------------------------------------------------------------------
  INTEGER,INTENT(IN):: efu
  REAL,INTENT(IN),DIMENSION(:):: x,y
  REAL,INTENT(IN):: alpha,beta,gamma

  INTERFACE
    FUNCTION f(a,b) RESULT(c)
      REAL,INTENT(IN):: a,b
      REAL:: c
    END Function f
  END INTERFACE

  INTEGER,PARAMETER:: NPLOT=101
  REAL,DIMENSION(NPLOT):: xplot,yplot
  REAL,DIMENSION(3,3):: t
  REAL,ALLOCATABLE,DIMENSION(:,:):: za,zb
  REAL:: zmax,zmin,zmean
  INTEGER:: i,j
  INTEGER:: nx,ny
  INTRINSIC:: MAX,MIN, MAXVAL,MINVAL
!-------------------------------------------------------------------------------
  nx=SIZE(x)
  ny=SIZE(y)

  CALL MakeMatrix(alpha,beta,gamma, t)

  WRITE(DBG,*) "X"
  WRITE(DBG,*) x
  WRITE(DBG,*) "Y"
  WRITE(DBG,*) y
  WRITE(DBG,*) "T"
  WRITE(DBG,*) t

!  WRITE(efu,*) "LINEWIDTH 0.25"   ! sharp and thin


  ALLOCATE(za(nx,NPLOT), zb(NPLOT,ny) )
  CALL FillArray(x(1),x(nx),xplot)
  CALL FillArray(y(1),y(ny),yplot)
  DO i=1,nx
    DO j=1,NPLOT
      za(i,j)=f(x(i),yplot(j))
    END DO
  END DO

  DO i=1,NPLOT
    DO j=1,ny
      zb(i,j)=f(xplot(i),y(j))
    END DO
  END DO

  zmax=MAX(MAXVAL(za), MAXVAL(zb))
  zmin=MIN(MINVAL(za), MINVAL(zb))
  zmean=0.5*(zmax+zmin)
  WRITE(DBG,*) "MIN,MAX,MEAN:", zmin,zmax,zmean
  za(:,:)=(za(:,:)-zmean)/(zmax-zmin)
  zb(:,:)=(zb(:,:)-zmean)/(zmax-zmin)

  CALL FillArray(y(1),y(ny),yplot)
  DO i=1,nx
    xplot(1:NPLOT)=x(i)
    CALL RotateAndWrite(efu, xplot,yplot,za(i,:), t)
  END DO

  CALL FillArray(x(1),x(nx),xplot)
  DO j=1,ny
    yplot(1:NPLOT)=y(j)
    CALL RotateAndWrite(efu, xplot,yplot,zb(:,j), t)
  END DO

  DEALLOCATE(za,zb)
  RETURN
END Subroutine Plot3D   ! ------------------------------------------------------

!+
SUBROUTINE MakeMatrix(alpha,beta,gamma, t)
! ------------------------------------------------------------------------------
  REAL,INTENT(IN):: alpha,beta,gamma   ! degrees
  REAL,INTENT(OUT),DIMENSION(:,:):: t

!  REAL,PARAMETER:: PI=3.14159265

  REAL:: a,b,c
  REAL:: sina,sinb,sinc, cosa,cosb,cosc  
  INTRINSIC:: COS,SIN
!-------------------------------------------------------------------------------
  a=alpha*DEG2RAD
  b=beta*DEG2RAD
  c=gamma*DEG2RAD

  sina = SIN(a)
  sinb = SIN(b)
  sinc = SIN(c)
  cosa = COS(a)
  cosb = COS(b)
  cosc = COS(c)
  write(dbg,*) "SIN/COS in MakeMatrix"
  write(dbg,*) alpha,beta,gamma
  write(dbg,*) sina,sinb,sinc, cosa,cosb,cosc
  t(1,1) = cosc*cosb
  t(1,2) = cosc*sinb*sina-sinc*cosa
  t(1,3) = cosc*sinb*cosa+sinc*sina
  t(2,1) = sinc*cosb
  t(2,2) = sinc*sinb*sina+cosc*cosa
  t(2,3) = sinc*sinb*cosa-cosc*sina
  t(3,1) = -sinb
  t(3,2) = cosb*sina
  t(3,3) = cosb*cosa
  RETURN
END Subroutine MakeMatrix   ! --------------------------------------------------

!+
SUBROUTINE RotateAndWrite(efu, x,y,z, t)
! ------------------------------------------------------------------------------
  INTEGER,INTENT(IN):: efu
  REAL,INTENT(IN),DIMENSION(:):: x,y,z
  REAL,INTENT(IN),DIMENSION(:,:):: t

  REAL,DIMENSION(3,SIZE(x)):: xyzOrig, xyzTran
  INTEGER:: j
  INTRINSIC:: MATMUL
!-------------------------------------------------------------------------------
  xyzOrig(1,:)=x
  xyzOrig(2,:)=y
  xyzOrig(3,:)=z
  xyzTran=MATMUL(t,xyzOrig)

  WRITE(DBG,'(6F12.4)' ) (xyzOrig(:,j), xyzTran(:,j), j=1,SIZE(x))

  WRITE(efu,'(2F15.6)' ) (xyzTran(2,j),xyzTran(3,j),j=1,SIZE(x))
  WRITE(efu,*) " "  ! start a new line
  RETURN
END Subroutine RotateAndWrite   ! ----------------------------------------------

END Module ViewerProcedures   ! ================================================


!+
MODULE PostScriptProcedures
! ------------------------------------------------------------------------------
! PURPOSE -

IMPLICIT NONE

!-------------------------------------------------------------------------------
CONTAINS



!+
SUBROUTINE BigWindow(vminx,vmaxx,vminy,vmaxy,                     &  
    xmin,xmax, ymin,ymax, xmin2,xmax2,ymin2,ymax2)
! ------------------------------------------------------------------------------
! PURPOSE - Define a window that contains the data in the rectangle
!    [xmin,xmax]x[ymin,ymax] and has the same aspect ratio as the
!    viewport [vminx,vmaxx]x[vminy,vmaxy]
IMPLICIT NONE

! the four quantities in each line below are left,right,bottom,top
  REAL,INTENT(IN)::  vminx,vmaxx, vminy,vmaxy  ! viewport
  REAL,INTENT(IN)::  xmin,xmax, ymin,ymax      ! data window
  REAL,INTENT(OUT)::  xmin2,xmax2,ymin2,ymax2   ! resulting window

  REAL:: dx              ! window size in x-direction if y is dominant
  REAL:: dy              ! window size in y-direction if x is dominant
!-------------------------------------------------------------------------------
  IF ((xmax-xmin)*(vmaxy-vminy) .GT. (ymax-ymin)*(vmaxx-vminx)) THEN
    xmin2=xmin
    xmax2=xmax
    dy=(xmax-xmin)*(vmaxy-vminy)/(vmaxx-vminx)
    ymin2=0.5*(ymax+ymin-dy)
    ymax2=0.5*(ymax+ymin+dy)
  ELSE
    dx=(ymax-ymin)*(vmaxx-vminx)/(vmaxy-vminy)
    xmin2=0.5*(xmax+xmin-dx)
    xmax2=0.5*(xmax+xmin+dx)
    ymin2=ymin
    ymax2=ymax
  END IF

  RETURN
END Subroutine BigWindow   ! ---------------------------------------------------

!+
SUBROUTINE ScanGnu(efu, xmin,xmax, ymin,ymax)
! ------------------------------------------------------------------------------
! PURPOSE - 
IMPLICIT NONE
  INTEGER,INTENT(IN):: efu
  REAL,INTENT(OUT):: xmin,xmax, ymin,ymax

  INTEGER:: code
  REAL:: x,y

  CHARACTER(LEN=80):: dummy
!-------------------------------------------------------------------------------
  xmin=1E37
  xmax=-xmin
  ymin=xmin
  ymax=xmax

  REWIND(efu)
  DO
    READ(efu,'(A)', IOSTAT=code) dummy
    IF (code .LT. 0) EXIT
    IF (dummy(1:1) .EQ. '#') CYCLE
    IF (dummy .EQ. ' ') CYCLE

    READ(dummy,*,IOSTAT=code) x,y
    IF (code .NE. 0) CYCLE

    xmax=MAX(xmax,x)
    xmin=MIN(xmin,x)
    ymax=MAX(ymax,y)
    ymin=MIN(ymin,y)
  END DO

  RETURN
END Subroutine ScanGnu   ! -----------------------------------------------------

!+
SUBROUTINE CreateFig(gnu,fig)
! ------------------------------------------------------------------------------
! PURPOSE -
IMPLICIT NONE
  INTEGER,INTENT(IN):: gnu,fig

  CHARACTER(LEN=80):: dummy
  CHARACTER(LEN=80),DIMENSION(1000):: data
  INTEGER:: i,k ,code
!-------------------------------------------------------------------------------

  Write(fig,*) 'LINEWIDTH 0.5'

  k=0
  DO
    Read(gnu,'(A)',IOSTAT=code) dummy
    IF (code .LT. 0) EXIT
    IF (dummy(1:1) .EQ. '#') CYCLE
    IF (dummy .EQ. ' ') THEN
      IF (k .GT. 1) THEN
        Write(fig, '(A,I5)')  'DATA ', k
        WRITE(fig,'(A)') (Trim(data(i)),i=1,k)
      END IF
      k=0
    ELSE
      k=k+1
      data(k) = dummy
    END IF
  END DO

  Write(fig,'(A)') 'ENDFRAME ----------------------------------------------'

  RETURN
END Subroutine CreateFig   ! ---------------------------------------------------

!+
SUBROUTINE CreatePs(gnu,ps)
! ------------------------------------------------------------------------------
! PURPOSE -
IMPLICIT NONE
  INTEGER,INTENT(IN):: gnu,ps

  INTEGER:: efu
  INTEGER,PARAMETER:: FIG=7

  REAL:: xmin,xmax, ymin,ymax
!      REAL:: xmin2,xmax2, ymin2,ymax2
  REAL:: xlow,xhigh,ylow,yhigh
  REAL,PARAMETER:: VMINX=0.01, VMAXX=0.99, VMINY=0.03, VMAXY=0.73


!  REAL MARGINPS,SCALEPS, MARGINPCL,SCALEPCL
  REAL,PARAMETER:: MARGINPS=36.0, SCALEPS=720.0
!  PARAMETER (MARGINPCL=0.0, SCALEPCL=10160.0)

  CHARACTER(LEN=132):: buffer
  INTEGER:: code
  INTEGER:: i,n
  REAL:: x,y, sx,sy
  REAL:: sclx,scly
  INTEGER:: ix,iy
!-------------------------------------------------------------------------------
  REWIND(UNIT=gnu)
  CALL ScanGnu(gnu, xmin,xmax, ymin,ymax)
  REWIND(UNIT=gnu)
  CALL BigWindow(vminx,vmaxx,vminy,vmaxy,                           &  
     &    xmin,xmax, ymin,ymax, xlow,xhigh, ylow,yhigh)
  sclx=(vmaxx - vminx) / (xhigh - xlow)
  scly=(vmaxy - vminy) / (yhigh - ylow)

  OPEN(UNIT=FIG,FILE='viewer.fig',STATUS='REPLACE',ACTION='WRITE')
  CALL CreateFig(gnu,FIG)
  CLOSE(UNIT=GNU)
  CLOSE(UNIT=FIG)
  OPEN(UNIT=FIG,FILE='viewer.fig',STATUS='OLD',ACTION='READ')

  WRITE(PS,*) '%!PS-Adobe'
  WRITE(PS,*) '612 0 translate 90 rotate'
  WRITE(PS,*) '0.5 setlinewidth'

  DO
    Read(fig, '(A)', IOSTAT=code) buffer
    IF (code .LT. 0) EXIT

    IF (buffer(1:4) .EQ. 'DATA') THEN
      READ(buffer(6:10), '(I5)' ) n
      Read(fig,*) x, y
      sx=VMINX + (x-xlow)*sclx
      sy=VMINY + (y-ylow)*scly
      sx=MARGINPS +SCALEPS*sx
      sy=MARGINPS +SCALEPS*sy
      WRITE(Ps,'(2F9.1,A)' )  sx, sy, ' moveto'
      DO i=2,n
        Read(fig,*) x, y
        sx=VMINX + (x-xlow)*sclx
        sy=VMINY + (y-ylow)*scly
        sx=MARGINPS +SCALEPS*sx
        sy=MARGINPS +SCALEPS*sy
        Write(Ps, '(2F9.1,A)' ) sx, sy,  ' lineto'
      END DO
    ELSE IF (buffer(1:8) .EQ. 'ENDFRAME') THEN
      Write(Ps,*) 'stroke showpage'
    END IF
  END DO
  CLOSE(UNIT=gnu)
  Close(UNIT=PS)

  RETURN
END Subroutine CreatePs   ! ----------------------------------------------------

END Module PostScriptProcedures   ! ============================================

!+
PROGRAM Viewer
! ------------------------------------------------------------------------------
! PURPOSE - Rotate and draw 3D surfaces

! AUTHORS - R.Bruce Canwright,Jr. and Paul Swigert, Lewis Research Center
!  Ralph L. Carmichael,  Public Domain Aeronautical Software
!
! REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
! June68   1.0 RBC&PS  Initial publication of TM X-1598
!   ?          RBC&PS  Release to COSMIC as program LEW-10482
! 29Aug97  2.0   RLC   Made modern Fortran 90 version
! 27Dec99  2.5   RLC   Adapted to use gnuplot for plotting
! 12Feb09  2.6   RLC   Created module ViewerProcedures

! REFERENCE - PLOT3D - A Package of Fortran Subprograms to Draw
!   Thre-Dimensional Surfaces
!   by R.Bruce Canwright,Jr. and Paul Swigert, Lewis Research Center
!   NASA TM X-1598, June 1968.
!   COSMIC Program LEW-10482

USE TestFunctions
USE ViewerProcedures
USE PostScriptProcedures
IMPLICIT NONE

  CHARACTER(LEN=*),PARAMETER:: GREETING = "viewer - Make 3D plots"
  CHARACTER(LEN=*),PARAMETER:: FAREWELL = &
    "files viewer.gnu and viewer.dbg added to your directory"

  INTEGER,PARAMETER:: GNU=1, PS=2
  INTEGER:: choice =1

  INTEGER:: errCode
  REAL,ALLOCATABLE,DIMENSION(:):: x,y
!-------------------------------------------------------------------------------
  WRITE(*,*) GREETING

  OPEN(UNIT=GNU, FILE='viewer.gnu', IOSTAT=errCode,   &
    STATUS='REPLACE', ACTION='WRITE', POSITION='REWIND')
  IF (errCode /= 0) THEN
    WRITE(*,*) "Unable to open viewer.gnu"
    STOP
  END IF

  OPEN(UNIT=DBG, FILE='viewer.dbg', IOSTAT=errCode,   &
    STATUS='REPLACE', ACTION='WRITE', POSITION='REWIND')
  IF (errCode == 0) THEN
    WRITE(DBG,*) "Created by Viewer"
  ELSE
    WRITE(*,*) "Unable to open viewer.dbg"
    STOP
  END IF


  choice=4

  SELECT CASE(choice)
    CASE(1)
      ALLOCATE(x(7),y(31))
      CALL FillArray(0.0,1.0, x)
      CALL FillArray(0.0,1.0, y)
      CALL Plot3D(GNU, x,y, 20.0,20.0,45.0, F2)
      DEALLOCATE(x,y)
    CASE(2)
      ALLOCATE(x(7),y(31))
      CALL FillArray(0.0,1.0, x)
      CALL FillArray(0.0,1.0, y)
      CALL Plot3D(GNU, x,y, 20.0,60.0,45.0, F2)
      DEALLOCATE(x,y)
    CASE(3)
      ALLOCATE(x(7),y(31))
      CALL FillArray(-3.0,3.0, x)
      CALL FillArray(-3.0,3.0, y)
      CALL Plot3D(GNU, x,y, 20.0,20.0,45.0, Poles)
      DEALLOCATE(x,y)
    CASE(4)
      ALLOCATE(x(7),y(31))
      CALL FillArray(-3.0,3.0, x)
      CALL FillArray(-3.0,3.0, y)
      CALL Plot3D(GNU, x,y, 20.0,60.0,45.0, Poles)
      DEALLOCATE(x,y)
    CASE(5)
      ALLOCATE(x(5),y(11))
      CALL FillArray(-5.0,5.0, x)
      CALL FillArray(0.0, 4.0, y)
      CALL Plot3D(GNU, x,y, 0.0,35.0,45.0, CollapsingString)
      DEALLOCATE(x,y)
    CASE(6)
      ALLOCATE(x(7),y(31))
      CALL FillArray(0.15, 3.0, x)
      CALL FillArray(0.15, 3.0, y)
      CALL Plot3D(GNU, x,y, 0.0,35.0,45.0, Maxwell)
      DEALLOCATE(x,y)
  END SELECT
  

      CLOSE(UNIT=GNU) 

! call PostScript writer here...
      OPEN(UNIT=GNU, FILE='viewer.gnu',STATUS='OLD',ACTION='READ')
      OPEN(UNIT=PS, FILE='viewer.ps',STATUS='REPLACE',ACTION='WRITE')
      WRITE(*,*) 'Scene computed. Generating PostScript file.'
      CALL CreatePs(GNU,PS)

  WRITE(*,*) FAREWELL
  WRITE(*,*) "Normal termination of Viewer"
  STOP



END Program Viewer   ! ======================================================
