
  
VIEWER - DRAW THREE DIMENSIONAL SURFACES                   /viewer/readme.txt

The files for this program are in the directory viewer 
  readme.txt      this file
  viewer.txt      instructions for use of viewer
  viewer.f90      the complete source code
  lew10482.txt    the original program description from COSMIC
  original.src    the original source code (from COSMIC)


The reference documents for this program may be accessed
from the web page https://www.pdas.com/viewerrefs.html. 

To compile this program for your computer, use the command
   gfortran  viewer.f90 -o viewer.exe
Linux and Macintosh users may prefer to omit the .exe on the file name.


DESCRIPTION

Viewer is a programs to draw three-dimensional surfaces of the form 
z = f(x,y). The function f and the boundary values for x and y are 
the inputs. The surface thus defined may be drawn after 
arbitrary rotations.

However, it is designed to draw only functions in rectangular coordinates 
expressed explicitly in the above form. It cannot, for example, draw a 
sphere. Output is by off-line incremental plotter or online microfilm 
recorder. This package, unlike other packages, will plot any function of 
the form z = f(x,y) and portrays continuous and bounded functions of two 
independent variables. With curve fitting; however, it can draw experimental
data and pictures which cannot be expressed in the above form.

The method used is division into a uniform rectangular grid of the given x 
and y ranges. The values of the supplied function at the grid points (x, y) 
are calculated and stored; this defines the surface. The surface is portrayed
by connecting successive (y,z) points with straight-line segments for each 
x value on the grid and, in turn, connecting successive (x,z) points for 
each fixed y value on the grid. These lines are then projected by parallel 
projection onto the fixed yz-plane for plotting.

The program produces a file called viewer.gnu that may be used with gnuplot. 
For example

   gnuplot>plot 'viewer.gnu' with lines
