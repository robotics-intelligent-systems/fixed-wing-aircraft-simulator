

VARIABLE METRIC ALGORITHM FOR CONSTRAINED OPTIMIZATION      /vmaco/readme.txt

The files for this program are in the directory vmaco 
  readme.txt      this file of general description
  original.src    the original copy of the source code (from COSMIC)
  vmaco.f90       the source code for the Vmaco module
  test1.f90       the source code for the first sample case
  test1.out       the output produced by running test1
  test2.f90       the source code for the second sample case
  test2.out       the output produced by running test2
  msc21275.txt    the original program description from COSMIC

The reference documents for this program may be accessed
from the web page https://www.pdas.com/vmacorefs.html. 

Vmaco is not a program, but a module of subroutines that may be called 
by a driver program to search for the minimum. 
Two test programs are shown to illustrate the procedures for using Vmaco. 
