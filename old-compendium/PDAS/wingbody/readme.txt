

NASA-AMES WINGBODY PANEL CODE                               /wingbody/readme.txt

The files for this program are in the directory wingbody 
  readme.txt      this file
  wingbody.for    the complete source code
  wingbody.txt    instructions for preparing input & interpreting output
  namelist.txt    general comments about namelist.

Sample cases for this program are:
  wdcase1.inp     input data for test case similar to case1 for WaveDrag
  wdcase1.out     output from WingBody for this test case.
  tnd7505.inp     input data for wing-body configuration of NASA TN D-7505
  tnd7505.out     output from WingBody for this test case.
  tnd4211.inp     input data for wing-body configuration of NASA TN D-4211
  tnd4211.out     output from WingBody for this test case.
  caseslnx.zip    above cases with Unix end of line
                  (original references TN D-4211 and TN D-7505 are in 2wgs/ )


The reference documents for this program may be accessed
from the web page https://www.pdas.com/wingbodyrefs.html. 

To compile this program for your computer, use the command
   gfortran  wingbody.f90 -o wingbody.exe
Linux and Macintosh users may prefer to omit the .exe on the file name.


This program asks for the name of the input file. This must be a file written 
to conform to the instructions in wingbody.doc. The program produces a file 
called wingbody.out that may be printed or scrolled on your screen. There is 
no graphical output.



                      DESCRIPTION

Panel codes are the success stories of the early days of computational 
aerodynamics. Panel codes generate the solution to problems in aerodynamics
by superposition of elementary solutions.

This code is popularly known as the NASA AMES WINGBODY program. It is the 
outgrowth of a series of contracts with the Boeing Company for the develop-
ment of a method of computing optimum camber and twist distributions on 
wing-body combinations at supersonic speeds. The contract work was done from 
1966 to 1969.  The principal investigator was Frank Woodward.

After the completion of the contract work, the program underwent additional 
development at Ames and was widely distributed to airplane manufacturers 
and research laboratories. The original code for the IBM 7090 was updated 
when later machines, such as the 360 and the CDC 7600, appeared.  In 1975 
and later, the powerful surface panel codes such as VSAERO and PANAIR became
available and have today more or less replaced the mean surface codes, such 
as WingBody, among those who have access to large computers.

These surface panel codes themselves have been replaced by computational 
fluid dynamics codes solving Euler or Navier-Stokes equations. However 
there is still a tradeoff of time spent setting up the input for a 
computational technique versus the accuracy of the method.

I have brought this program back to life for this collection as it has
a certain balance of speed, ease of use and range of applicability
that can make it an appropriate method of choice for airplane conceptual 
design.
