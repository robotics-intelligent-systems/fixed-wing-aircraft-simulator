

DESIGN OF TWO-DIMENSIONAL SUPERSONIC TURBINE                  /rblade/readme.txt
   ROTOR BLADES WITH BOUNDARY-LAYER CORRECTION


The files for this program are in the directory rblade 
  lew11744.txt    the original program description from COSMIC
  readme.txt      this file of general description
  original.src    the original copy of the source code (from COSMIC)


This program is a "work in progress" and is not ready for general release.
I have included it so those who have a special interest may see
the original code plus my modifications to date.
 
