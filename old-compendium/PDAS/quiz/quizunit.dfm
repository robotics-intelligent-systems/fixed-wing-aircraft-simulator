object Form1: TForm1
  Left = 194
  Top = 107
  Width = 544
  Height = 375
  Caption = 'The Aeronautical Quizzer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object memQuestion: TMemo
    Left = 16
    Top = 16
    Width = 369
    Height = 153
    TabStop = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object memAnswer: TMemo
    Left = 16
    Top = 176
    Width = 369
    Height = 153
    TabStop = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object radLevel: TRadioGroup
    Left = 400
    Top = 176
    Width = 113
    Height = 89
    Caption = 'level'
    ItemIndex = 0
    Items.Strings = (
      'beginner'
      'intermediate'
      'expert')
    TabOrder = 4
    OnClick = radLevelClick
  end
  object bntQuit: TButton
    Left = 440
    Top = 48
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Quit'
    TabOrder = 6
    OnClick = bntQuitClick
  end
  object radQuiz: TRadioGroup
    Left = 400
    Top = 80
    Width = 113
    Height = 89
    Caption = 'Quiz'
    ItemIndex = 0
    Items.Strings = (
      'Alphabet'
      'Morse'
      'Dimensionless'
      'Airport Codes')
    TabOrder = 3
    OnClick = radQuizClick
  end
  object btnBegin: TButton
    Left = 440
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Begin'
    Default = True
    TabOrder = 0
    TabStop = False
    OnClick = btnBeginClick
  end
  object radOrder: TRadioGroup
    Left = 400
    Top = 272
    Width = 113
    Height = 65
    Caption = 'Order'
    ItemIndex = 0
    Items.Strings = (
      'forward'
      'reverse')
    TabOrder = 5
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Top = 8
  end
end
