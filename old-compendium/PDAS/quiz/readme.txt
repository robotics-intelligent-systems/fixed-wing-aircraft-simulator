

QUIZ - Learn the Phonetic Alphabet and other drills         /quiz/readme.txt

The files for this program are in the directory quiz 
  readme.txt      this file - general description
  quizzer.dpr     The Delphi Project File
  quizzer.exe     The executable program for Microsoft Windows
  quizUnit.dfm    The form definition
  quizUnit.pas    The source code associated with quizUnit.dfm

This is a simple program that will drill you so that you will learn
the phonetic alphabet used by air traffic control. Actually, it is
used in any situation where the connection may be noisy and total
accuracy is needed.

A variant on the program drills on the Morse Code for radio-telegraph
usage. Another variant uses a set of non-dimensional numbers used in
fluid mechanics and another tests you on three character airport codes.

Of course, the main idea here is to show you how to set up such a
program so you can create the tables for the subject you need to learn.
Some other suggestions are
  1. State capitals
  2. Capitals of various countries
  3. Foreign language vocabulary drill
  4. Important supreme court cases
  5. Nicknames of sporting teams
  6. Names of airplanes, e.g. P-51 == Mustang

All you need to do is to build two lists - the "A-words" and the "B-words".
The program puts up a random A-word and you are supposed to guess the
B-word. You are your own judge. There is no entry of answers or anything
like that.

I may include some more quizzes next year. Send me your contributions
and I will credit them if added. Since there are only four quiz subjects 
included with this version, I used a radio box to make the selection. 
We may need an alternate selection method if we have lots of quizzes.
The other thing you may wish to change is the timing. It should be obvious
from the source code. You will need a Delphi compiler if you want to make
changes, but you don't need the expensive version. The so-called Standard
Version is what I used to make this program. 

This program was written as a new project and is not based on a program
from a national laboratory. The whole program, source code and all is
declared to be open source - public domain. If you send me a contribution, 
I assume you are declaring it to be public domain.

This project was obviously meant for you to modify and experiment. For many
of the programs (such as Panair or Datcom), I do NOT recommend making
modifications unless you know for sure what you are doing. But this one just
invites experimentation. Try various timing intervals, fonts, font-sizes.
I made the overall window small because I was thinking of a laptop screen.
For classroom use, go for big window, big fonts. You might add a button that
says "Don't ask me that one again", and the coding, of course. The
dimensionless numbers quiz would be better with equations, but how
should we present them? Pictures? The possibilities go on and on...

There is a "right way" to pronounce the words in the alphabet,chosen so that 
non-english speakers would have a good chance of being understood. 
Try to say them as:

AL'-FAH   BRAH'-VOH   CHAR'-LEE or SHAR'-LEE   DELL'-TAH    ECK'-OH
FOKS'-TROT   GOLF   HOH-TEL'   IN'-DEE-AH   JEW'-LEE-ETT   KEY'-LOH
LEE'-MAH   MIKE   NO-VEM'-BER   OS'-CAH   PAH-PAH'   KEH-BECK'   ROW'-ME-OH
SEE-AIR'-RAH   TAN'-GO   YOU'-NEE-FORM or OO'-NEE-FORM   VIK'-TAH
WISS'-KEY   ECKS'-RAY   YAN'-KEY   ZOO'-LOO

Americans may find LIMA and PAPA a bit different from the usual.

P.S. Don't call this alphabet the International Phonetic Alphabet. That is
something quite different - it is what language scholars use when making a
written record of rare languages or regional dialects.
