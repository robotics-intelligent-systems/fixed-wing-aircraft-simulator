unit QuizUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    memQuestion: TMemo;
    memAnswer: TMemo;
    radLevel: TRadioGroup;
    bntQuit: TButton;
    Timer1: TTimer;
    radQuiz: TRadioGroup;
    btnBegin: TButton;
    radOrder: TRadioGroup;
    procedure bntQuitClick(Sender: TObject);
    procedure btnBeginClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure radQuizClick(Sender: TObject);
    procedure radLevelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

TYPE
   
  ProblemSet = CLASS        // not used. Maybe later
    n : CARDINAL;
    fontSize : INTEGER;
    timerAdvance : ARRAY[0..2] OF INTEGER;
    aStrings : ARRAY OF STRING;
    bStrings : ARRAY OF STRING;
  END;

  CONST
    aWordsAlphabet : ARRAY[0..25] OF STRING = ( 'A', 'B', 'C', 'D',
      'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
      'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    bWordsAlphabet : ARRAY[0..25] OF STRING = ( 'alpha',
     'bravo',  'charlie', 'delta',    'echo',    'foxtrot',
     'golf',   'hotel',   'india',    'juliett', 'kilo',
     'lima',   'mike',    'november', 'oscar',   'papa',
     'quebec', 'romeo',   'sierra',   'tango',   'uniform',
     'victor', 'whiskey', 'xray',     'yankee',  'zulu');

    bWordsMorse : ARRAY[0..25] OF STRING = ( '.-',
     '-...',  '-.-.', '-..',  '.',    '..-.',
     '--.',   '....', '..',   '.---', '-.-',
     '.-..',  '--',   '-.',   '---',  '.--.',
     '--.-',  '.-.',  '...',  '-',    '..-',
     '...-',  '.--',  '-..-', '-.--', '--..');


    NDIM = 15;
    aWordsDimensionless : ARRAY[0..NDIM-1] OF STRING = (
      'ratio of buoyant force to viscous force',
      'ratio of velocity to maximum velocity in expansion',
      'ratio of kinetic energy to thermal energy in compressible flow',
      'ratio of mean free path to characteristic length',
      'ratio of velocity to speed of sound',
      'ratio of inertia force to viscous force',
      '(head loss/velocity head) times diameter/length in pipe flow',
      'ratio of shear stress to dynamic pressure',
      'ratio of time constant of system to period of pulsation in pulsating flow',
      'ratio of pressure force to viscous force in laminar flow',
      'ratio of momentum diffusivity to thermal diffusivity in heat transfer',
      'ratio of gravity to thermal diffusivity if free convection',
      'ratio of heat transferred to fluid to heat transported by fluid (forced convection)',
      'ratio of viscous force to gravity force in slow flow',
      'ratio of total heat transfer to conductive heat transfer in convection'
    );
    bWordsDimensionless : ARRAY[0..NDIM-1] OF STRING = (
      'Archimedes Number',
      'Crocco Number (or Laval Number)',
      'Dulong Number (or Eckert Number)',
      'Knudsen Number (1/Knudsen = Smoluckowski Number)',
      'Mach Number (= Sqrt(Cauchy Number))',
      'Reynolds Number',
      'Darcy Number',
      'Fanning Number',
      'Hodgson Number',
      'Poiseuille Number',
      'Prandtl Number',
      'Rayleigh Number',
      'Stanton Number',
      'Stokes Number',
      'Nusselt Number'
    );

    NPORTS = 40;
    aWordsAirportCodes : ARRAY[0..NPORTS-1] OF STRING = (
      'LAX', 'SJC', 'ORD', 'JFK', 'IAD',
      'IAH', 'ATL', 'MSP', 'MRY', 'RDU',
      'SAN', 'SCL', 'FRA', 'LHR', 'LGW',
      'SDY', 'NAY', 'NRT', 'ORY', 'OTP',
      'DFW', 'SEA', 'PHL', 'RNO', 'BOS',
      'SIN', 'YYS', 'YQX', 'YVR', 'ZRH',
      'MIA', 'CDG', 'DTW', 'SYD', 'AMS',
      'PIK', 'JNB', 'STL', 'MUC', 'LOS'
    );
    bWordsAirportCodes : ARRAY[0..NPORTS-1] OF STRING = (
      'Los Angeles',
      'San Jose, California',
      'Chicago (O''Hare)',
      'New York (Kennedy)',
      'Washington (Dulles)',

      'Houston (Intercontinental)',
      'Atlanta',
      'Minneapolis / St. Paul',
      'Monterey, California',
      'Raleigh / Durham, N.C.',

      'San Diego',
      'Santiago, Chile',
      'Frankfurt, Germany',
      'London (Heathrow)',
      'London (Gatwick)',

      'Sidney, Montana, USA',
      'Beijing, China',
      'Tokyo, Japan (Narita)',
      'Paris, France (Orly)',
      'Bucharest, Romania (Otopeni)',

      'Dallas / Fort Worth',
      'Seattle / Tacoma',
      'Philadelphia',
      'Reno',
      'Boston',

      'Singapore',
      'Toronto',
      'Gander, Newfoundland',
      'Vancouver',
      'Zurich',

      'Miami',
      'Paris, de Gaulle',
      'Detroit',
      'Sydney, Australia',
      'Amsterdam',

      'Glasgow, Prestwick',
      'Johannesburg',
      'St. Louis, Lambert',
      'Munich',
      'Lagos, Nigeria'

    );
var
  Form1: TForm1;
  kRandom : CARDINAL;
  showAnswer : BOOLEAN;
  nWords : CARDINAL;
  aWords : ARRAY[0..100] OF STRING;
  bWords : ARRAY[0..100] OF STRING;

implementation

{$R *.DFM}

PROCEDURE LoadWords(const k : INTEGER);
// Load the arrays aWords and bWords from the appropriate
//   constant arrays, depending on the quiz
// k is the ItemIndex of radQuiz
  VAR j : INTEGER;
BEGIN
 case k of
    0: BEGIN     { alphabet }
         nWords:=26;
         Form1.memQuestion.font.Size:=48;
         Form1.memAnswer.font.Size:=48;
         FOR j:=0 TO 25 DO aWords[j]:=aWordsAlphabet[j];
         FOR j:=0 TO 25 DO bWords[j]:=bWordsAlphabet[j];
       END;
    1: BEGIN     { Morse code }
         nWords:=26;
         Form1.memQuestion.font.Size:=48;
         Form1.memAnswer.font.Size:=48;
         FOR j:=0 TO 25 DO aWords[j]:=aWordsAlphabet[j];
         FOR j:=0 TO 25 DO bWords[j]:=bWordsMorse[j];
       END;
    2: BEGIN     { dimensionless numbers }
         nWords:=NDIM;
         Form1.memQuestion.font.Size:=16;
         Form1.memAnswer.font.Size:=16;
         FOR j:=0 TO NDIM-1 DO aWords[j]:=aWordsDimensionless[j];
         FOR j:=0 TO NDIM-1 DO bWords[j]:=bWordsDimensionless[j];
       END;
    3: BEGIN     { airport codes }
         nWords:=NPORTS;
         Form1.memQuestion.font.Size:=24;
         Form1.memAnswer.font.Size:=24;
         FOR j:=0 TO NPORTS-1 DO aWords[j]:=aWordsAirportCodes[j];
         FOR j:=0 TO NPORTS-1 DO bWords[j]:=bWordsAirportCodes[j];
       END;
  END;
END;   // --- End of Procedure LoadWords

PROCEDURE SetTimerInterval(const k,j : INTEGER);

// k is the itemIndex of radQuiz
// j is the itemIndex of redLevel
BEGIN


  CASE k OF
    0 : CASE j OF
          0: Form1.timer1.Interval:=2000;
          1: Form1.timer1.Interval:=1000;
          2: Form1.timer1.Interval:=500;
         END;
    1 : CASE j OF
          0: Form1.timer1.Interval:=2000;
          1: Form1.timer1.Interval:=1000;
          2: Form1.timer1.Interval:=500;
        END;
    2 : CASE j OF
          0: Form1.timer1.Interval:=10000;
          1: Form1.timer1.Interval:=5000;
          2: Form1.timer1.Interval:=1000;
        END;
    3 : CASE j OF
          0: Form1.timer1.Interval:=5000;
          1: Form1.timer1.Interval:=2000;
          2: Form1.timer1.Interval:=1000;
        END;
  END;

END;

procedure TForm1.btnBeginClick(Sender: TObject);
begin
  Randomize;
  showAnswer:=TRUE;
  timer1.Enabled:=TRUE;
  btnBegin.Hide;
  LoadWords(Form1.radQuiz.itemIndex);
  SetTimerInterval(Form1.radQuiz.ItemIndex,Form1.radLevel.ItemIndex);
  kRandom:=Random(nWords); { returns [0,nWords-1] }
end;


procedure TForm1.bntQuitClick(Sender: TObject);
begin
  Application.Terminate
end;


procedure TForm1.Timer1Timer(Sender: TObject);
  VAR oldK:CARDINAL;
begin
  showAnswer:=NOT showAnswer;

  IF showAnswer THEN
    BEGIN
      IF Form1.radOrder.ItemIndex=0 THEN
        memAnswer.Lines.Add(bWords[kRandom])
      ELSE
        memAnswer.Lines.Add(aWords[kRandom]);
      oldK:=kRandom;
      REPEAT
        kRandom:=Random(NWORDS)
      UNTIL kRandom<>oldK;
    END
  ELSE
    BEGIN
      memQuestion.Lines.Clear;
      memAnswer.Lines.Clear;
      IF Form1.radOrder.ItemIndex = 0 THEN
        memQuestion.Lines.Add(aWords[kRandom])
      ELSE
        memQuestion.Lines.Add(bWords[kRandom])
    END
end;

procedure TForm1.radQuizClick(Sender: TObject);
begin
  LoadWords(Form1.radQuiz.itemIndex);
  SetTimerInterval(Form1.radQuiz.ItemIndex,Form1.radLevel.ItemIndex);
  showAnswer:=TRUE;
end;

procedure TForm1.radLevelClick(Sender: TObject);
BEGIN
  SetTimerInterval(Form1.radQuiz.ItemIndex,Form1.radLevel.ItemIndex);
  showAnswer:=TRUE;
END;

end.
