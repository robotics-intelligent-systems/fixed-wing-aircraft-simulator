

TPS MULTIDIMENSIONAL HEAT CONDUCTION PROGRAM                    /tps/readme.txt


The files for this program are in the directory tps 
  readme.txt      this file of general description
  original.src    the original copy of the source code (from COSMIC)
  msc18616.txt    the program description from NASA COSMIC
  case1.inp       input data for a sample case

There is no program description nor input data description
for this program. A cursory look indicates that there will
a lot of work required to get this program running with
modern Fortran. The authors (NASA Marshall or Rockwell) are
T.M.Danza, L.W.Fesler, and R.D.Morgan.



