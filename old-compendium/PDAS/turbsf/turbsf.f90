!+
FUNCTION TurbulentSkinFriction(reynoldsNumber, machNumber, temperature) &
                               RESULT(cf) 
! ---------------------------------------------------------------------------
! PURPOSE - Compute the value of total flat-plate all-turbulent skin
!    friction using the method of Sommer and Short (NACA TM xxxx).
!    Although this semi-empirical method is somewhat more complicated
!    than other formulas that are given in textbooks of aerodynamics,
!    it does exhibit good correlation in the upper hypersonic regime
!    where temperature is a significant parameter.
!    At lower Mach Numbers, the result is quite insensitive to the
!    value of temperature.
  IMPLICIT NONE
  REAL,INTENT(IN):: reynoldsNumber   ! based on length of plate
  REAL,INTENT(IN),OPTIONAL:: machNumber
  REAL,INTENT(IN),OPTIONAL:: temperature          ! Kelvin
  
  REAL(KIND=4):: cf
  REAL(KIND=8),PARAMETER:: SUTH = 110.4
  REAL(KIND=8):: reynolds,mach,tempK,xx,cfi,z,denom,rstar
!----------------------------------------------------------------------------
  reynolds=reynoldsNumber   ! makes it KIND=8
  IF (PRESENT(machNumber)) THEN
    mach=machNumber     ! makes it KIND=8
  ELSE
    mach=0.0
  END IF
  IF (PRESENT(temperature)) THEN
    tempK=temperature   ! makes it KIND=8
  ELSE
    tempK=300.0   ! stratosphere
  END IF
  xx=LOG10(reynolds)-1.5
  cfi=0.088/(xx*xx)    ! Sievells and Payne
  z=1.0+0.115*mach*mach
  denom=(tempK+SUTH)/(z*tempK+SUTH)
  rstar=reynolds/(denom*(z**2.5))
  xx=xx/(LOG10(rstar)-1.5)
  cf=xx*xx*(cfi/z)
  RETURN
END Function TurbulentSkinFriction ! ========================================
