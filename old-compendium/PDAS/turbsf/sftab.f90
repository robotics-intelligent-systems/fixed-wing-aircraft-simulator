!+
PROGRAM SkinFrictionTables
! ---------------------------------------------------------------------------
! PURPOSE - Print a table of values of turbulent skin-friction coefficient to
!  confirm proper coding of function TurbulentSkinFriction
! AUTHOR - Ralph L. Carmichael, Public Domain Aeronautical Software

  IMPLICIT NONE

  REAL,PARAMETER,DIMENSION(13):: RNTAB = (/ 1E5,2E5,5E5, 1E6,2E6,5E6, &
                                            1E7,2E7,5E7, 1E8,2E8,5E8, 1E9 /) 
  REAL,PARAMETER,DIMENSION(6):: MACHTAB = (/ 0,1,2,3,5,10 /)                                           
  
  INTEGER:: i,j
  REAL,DIMENSION(6):: cf
!----------------------------------------------------------------------------
  WRITE(*,*) 'AVERAGE TURBULENT SKIN FRICTION COEFF. FOR VARIOUS VALUES OF'
  WRITE(*,*) 'MACH NUMBER AND REYNOLDS NUMBER. TEMPERATURE = 250K FOR ALL.'
  WRITE(*,'(8X,6F8.1)' ) MACHTAB
  DO j=1,SIZE(RNTAB)  ! all using default of T=250
    DO i=1,SIZE(MACHTAB)
      cf(i)=TurbulentSkinFriction(RNTAB(j), MACHTAB(i))   ! two arguments
    END DO
    WRITE(*, '(ES8.1, 6F8.5)' ) RNTAB(j), cf
  END DO
  WRITE(*,*)

  WRITE(*,*) 'AVERAGE TURBULENT SKIN FRICTION COEFF. FOR VARIOUS VALUES OF'
  WRITE(*,*) 'TEMPERATURE AND REYNOLDS NUMBER. MACH=1.0 FOR ALL.'
  WRITE(*,*) "             50     100     150     200     250     300"
  DO j=1,SIZE(RNTAB)
    DO i=1,SIZE(cf)
      cf(i)=TurbulentSkinFriction(RNTAB(j), 1.0, REAL(50*i))   ! three args.
    END DO
    WRITE(*, '(ES8.1, 6F8.5)' ) RNTAB(j), cf
  END DO
  WRITE(*,*)

  WRITE(*,*) 'AVERAGE TURBULENT SKIN FRICTION COEFF VS. REYNOLDS NUMBER'
  WRITE(*,*) 'USING DEFAULT VALUES OF MACH=0 AND T=250K'
  DO j=1,SIZE(RNTAB)  ! default values of Mach=0 and T=250
    cf(1)=TurbulentSkinFriction(RNTAB(j))   ! only one argument
    WRITE(*, '(ES8.1, 6F8.5)' ) RNTAB(j), cf(1)
  END DO

  WRITE(*,*)
  WRITE(*,*) "Normal stop in SkinFrictionTables"
  STOP

CONTAINS

!+
FUNCTION TurbulentSkinFriction(reynoldsNumber, machNumber, temperature) &
                               RESULT(cf) 

! ---------------------------------------------------------------------------
! PURPOSE - Compute the value of total flat-plate all-turbulent skin
!    friction using the method of Sommer and Short (NACA TM 3391).
!    Although this semi-empirical method is somewhat more complicated
!    than other formulas that are given in textbooks of aerodynamics,
!    it does exhibit good correlation in the upper hypersonic regime
!    where temperature is a significant parameter.
!    At lower Mach Numbers, the result is quite insensitive to the
!    value of temperature.
! AUTHOR - Ralph L. Carmichael, Public Domain Aeronautical Software

  IMPLICIT NONE
  REAL,INTENT(IN):: reynoldsNumber   ! based on length of plate
  REAL,INTENT(IN),OPTIONAL:: machNumber
  REAL,INTENT(IN),OPTIONAL:: temperature          ! kelvins
  
  REAL(KIND=4):: cf
  REAL(KIND=8),PARAMETER:: SUTH = 110.4   ! kelvins
  REAL(KIND=8):: reynolds,mach,tempK,xx,cfi,z,denom,rstar
!----------------------------------------------------------------------------
  reynolds=reynoldsNumber   ! makes it KIND=8
  IF (PRESENT(machNumber)) THEN
    mach=machNumber     ! makes it KIND=8
  ELSE
    mach=0.0
  END IF
  IF (PRESENT(temperature)) THEN
    tempK=temperature   ! makes it KIND=8
  ELSE
    tempK=250.0   ! stratosphere
  END IF
  xx=LOG10(reynolds)-1.5
  cfi=0.088/(xx*xx)    ! Sievells and Payne
  z=1.0+0.115*mach*mach
  denom=(tempK+SUTH)/(z*tempK+SUTH)
  rstar=reynolds/(denom*(z**2.5))
  xx=xx/(LOG10(rstar)-1.5)
  cf=xx*xx*(cfi/z)                                   ! converts to KIND of cf
  RETURN
END Function TurbulentSkinFriction ! ----------------------------------------

END PROGRAM SkinFrictionTables   ! ==========================================
