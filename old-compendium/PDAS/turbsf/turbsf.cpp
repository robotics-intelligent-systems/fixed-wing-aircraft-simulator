#include <math.h>

float TurbulentSkinFriction(const float reynoldsNumber,
			    const float machNumber,
			    const float temperature)   // deg Kelvin

{
  const double SUTH = 110.4;

  double reynolds=double(reynoldsNumber);
  double mach=double(machNumber);
  double temp=double(temperature);
  double xx=log10(reynolds)-1.5;
  double cfi=0.088/(xx*xx);    // Sievells and Payne
  double z=1.0+0.115*mach*mach;
  double denom=(temp+SUTH)/(z*temp+SUTH);
  double rstar=reynolds/(denom*pow(z,2.5));
  xx=xx/(log10(rstar)-1.5);
  return float((cfi/z)*xx*xx);
}   // -------------------------------- End of function TurbulentSkinFriction
