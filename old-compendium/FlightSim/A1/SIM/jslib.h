#ifndef jsLib_H
#define jsLib_H

extern unsigned int  jsLib_AnalogueData[32];
extern unsigned char jsLib_DigitalDataA;
extern unsigned char jsLib_DigitalDataB;
extern unsigned char jsLib_DigitalDataC;
extern unsigned char jsLib_DigitalDataD;

extern unsigned char jsLib_DigitalOutputA;
extern unsigned char jsLib_DigitalOutputB;

extern float         jsLib_Temperature;

void jsLib_UpdateIO(unsigned char DigitalDataA, unsigned char DigitalDataB);

void jsLib_Wait();

void jsLib_Shutdown();

void BEGIN_jsLib();

#endif
