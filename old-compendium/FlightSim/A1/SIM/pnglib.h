/*
  pnglib.h
*/

#ifndef PNGLIB_H
#define PNGLIB_H

int PngLib_SavePngFile(char [], int x, int y, int xs, int ys);

void BEGIN_PngLib(void);

#endif
